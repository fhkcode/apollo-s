/****************************************************************
 * The contents of this file are Kidde proprietary and confidential.
 * *************************************************************/
// PEAK_BUTTON.C
// Debounces peak button and controls peak button flag
#define	PEAK_BUTTON_C


#include	"common.h"
#include	"peak_button.h"
#include	"sound.h"
#include	"serial.h"
#include    "esclight.h"
#include    "diaghist.h"
#include	"button.h"
#include    "cocompute.h"


#ifdef	_CONFIGURE_PEAK_BUTTON

#define PEAK_BTN_CHECK_INTERVAL_MS  100     // Button Sample rate
#define PEAK_BTN_TIMEOUT            100      // 10 seconds in 100 ms units, 
#define PEAK_HOLD_INTERVAL_MS       2000    // 2 seconds
#define PEAK_CHIRP_INTERVAL			40    // 4 seconds

#define PEAK_BTN_NL_RATE            300     // ms Nightlight Intensity change rate
#define PEAK_BTN_NL_OFF_RATE        1000    // ms Nightlight Off Time


static unsigned char Peak_BTN_State = 0;
UCHAR Peak_BTN_Cnt;
UCHAR Peak_BTN_Chirp_Timeout = 0;

//******************************************************************************
unsigned int Peak_BTN_Task(void)
{

    switch (Peak_BTN_State)
    {
        case	PEAK_BTN_STATE_IDLE:

			FLAG_PEAK_BUTTON_NOT_IDLE = 0 ;

			if(!FLAG_PTT_ACTIVE)
			{
				if( (FLAG_CALIBRATION_COMPLETE == 1)
						&& (FLAG_CALIBRATION_UNTESTED == 0)
						&& ( FLAG_VOICE_SEND == 0 ) )
				{
					#ifdef _CONFIGURE_VOICE
						// Enable Peak Button Input
						TRISBbits.TRISB6 = 1;
					#endif

					if(PORT_PEAK_BTN == 1)
					{
						FLAG_PEAK_BUTTON_NOT_IDLE = 1 ;	// Leaving Idle State
						Peak_BTN_State = PEAK_BTN_STATE_DEBOUNCE ;
					}

					FLAG_PEAK_BUTTON_EDGE_DETECT = 0 ;

				}
			}

			break ;

        case	PEAK_BTN_STATE_DEBOUNCE:

			// See if button is still depressed
			if (PORT_PEAK_BTN == 1)
			{
					// Marketing wanted to disable this button chirp
					// Audible Button Detected Feedback Beep
					// SND_Set_Chirp_Cnt(1) ;
					// Reset sound task so that chirp happens right away.
					// MAIN_Reset_Task(TASKID_SOUND_TASK) ;

					// Button is debounced.
					Peak_BTN_State = PEAK_BTN_STATE_FUNCTION ;
					Peak_BTN_Cnt = PEAK_BTN_TIMEOUT;

//					Payload[5] = 0x06;

					#ifdef _CONFIGURE_DIGITAL_DISPLAY
						MAIN_Reset_Task(TASKID_DISP_UPDATE) ;
					#endif

					#ifdef _CONFIGURE_DIAG_HIST
						Diag_Hist_Que_Push(HIST_PEAK_BTN_EVENT);
					#endif

										
			}
			else
			{
					// Button is up, go back and wait for another button press.
					Peak_BTN_State = PEAK_BTN_STATE_IDLE ;
					FLAG_PEAK_BUTTON_EDGE_DETECT = 0 ;
			}
			
			break ;

        case	PEAK_BTN_STATE_FUNCTION:
			if((PORT_PEAK_BTN == 1) && (PORT_TEST_BTN == 1))
			{
				Payload.button_press = 0x07;	//clear peak co ppm.
				Peak_BTN_Chirp_Timeout = PEAK_CHIRP_INTERVAL;
				Peak_BTN_State = PEAK_BTN_STATE_WAIT_RELEASE;
			}
			else
			{
				// This is the Display Peak CO state
				if (PORT_PEAK_BTN == 1)
				{						
				   #ifdef	_CONFIGURE_ESCAPE_LIGHT
					#ifdef _CONFIGURE_ESC_LIGHT_TEST
					// Test only (With AC Off only test the Escape Light)
					if(FLAG_AC_DETECT == 0)
					{
						FLAG_ESC_LIGHT_ON = 1;
						return Make_Return_Value(PEAK_BTN_CHECK_INTERVAL_MS);
					}
					#endif
				   #endif

					if((Peak_BTN_Cnt <= 90) && (Peak_BTN_Cnt > 50))
					{
						if(PORT_TEST_BTN != 1)
						{
							Payload.button_press = 0x06;	// User presses & holds "Peak Level" button  
												//for 2 seconds to show peak co ppm.
                            Payload.peak_co_ppm_msb = (wPeakPPM >> 8) & 0xFF;
                            Payload.peak_co_ppm_lsb = wPeakPPM  & 0xFF;
                            
                            Flags_PEAK_LCD_ACTIVE = 1;
						}
					}
					// Display Peak for a Maximum of 5 Seconds
					// If > 5 seconds, enter Night Light Intensity state
					if(--Peak_BTN_Cnt <= 50)
					{
					   #ifdef   _CONFIGURE_ESCAPE_LIGHT

						Peak_BTN_State = PEAK_BTN_STATE_NIGHTLIGHT ;
						FLAG_ESC_LIGHT_ON = 0;

						// Proceed to Nightlight State
						Esc_Next_NL_Intensity(INIT_NL_STATE);

					   #else

						// Button Timeout, return to Idle
//						Peak_BTN_State = PEAK_BTN_STATE_IDLE ;
//						FLAG_PEAK_BUTTON_EDGE_DETECT = 0 ;
						
					   #endif
						
						Peak_BTN_Chirp_Timeout = PEAK_CHIRP_INTERVAL;
						Peak_BTN_State = PEAK_BTN_STATE_WAIT_RELEASE ;
//						return Make_Return_Value(PEAK_BTN_CHECK_INTERVAL_MS);
					}

					// Else, Remain in this state until Button RELEASED or Timeout

				}
				else
				{
				   #ifdef	_CONFIGURE_ESCAPE_LIGHT
						#ifdef _CONFIGURE_ESC_LIGHT_TEST
							// Test only
							FLAG_ESC_LIGHT_ON = 0;
						#endif
				   #endif
				   if(Peak_BTN_Cnt > 90)
				   {
					   	//User presses and releases ��Peak Level�� button 
						// within 2 seconds to show temp/humidity scrolling.
						Payload.button_press = 0x05;	
						// Button is released, go back and wait for another button press.
						Peak_BTN_State = PEAK_BTN_STATE_IDLE ;
						FLAG_PEAK_BUTTON_EDGE_DETECT = 0 ;

						return Make_Return_Value(PEAK_HOLD_INTERVAL_MS);					   
				   }
				   else
				   {
					   Peak_BTN_State = PEAK_BTN_STATE_IDLE ;  //peak button press-hold release
				   }

				}

			}
			break ;

        case    PEAK_BTN_STATE_NIGHTLIGHT:
			{

			   #ifdef _CONFIGURE_ESCAPE_LIGHT
				// This is the Display Peak CO state
				if (PORT_PEAK_BTN == 1)
				{
					FLAG_NIGHT_LIGHT_ON = 1;

					// Each pass we cycle thru NL intensities
					//  until button is released (to select it)
					Esc_Next_NL_Intensity(NEXT_NL_STATE);
					MAIN_Reset_Task(TASKID_DISP_UPDATE) ;

					if(FLAG_NIGHT_LIGHT_DISABLED )
						return Make_Return_Value(PEAK_BTN_NL_OFF_RATE);
					else
						return Make_Return_Value(PEAK_BTN_NL_RATE);
				}
				else
				{
					#ifdef _CONFIGURE_ESC_LIGHT_TEST
						// Test only
						FLAG_ESC_LIGHT_ON = 0;
					#endif

					// Use Current Intensity and turn off Light
					FLAG_NIGHT_LIGHT_ON = 0;

					// Button is released, go back and wait for another button press.
					Peak_BTN_State = PEAK_BTN_STATE_IDLE ;
					FLAG_PEAK_BUTTON_EDGE_DETECT = 0 ;
				}
			   #endif

			}
			break;
			
	case PEAK_BTN_STATE_WAIT_RELEASE:
		if (PORT_PEAK_BTN == 1)
		{
			if(--Peak_BTN_Chirp_Timeout == 0)
			{
				Peak_BTN_Chirp_Timeout = PEAK_CHIRP_INTERVAL;
				SND_Do_Chirp();
			}
		}
		else
		{
			Peak_BTN_State = PEAK_BTN_STATE_IDLE ;
			FLAG_PEAK_BUTTON_EDGE_DETECT = 0 ;			
		}
		break;

    }

    return Make_Return_Value(PEAK_BTN_CHECK_INTERVAL_MS) ;

}

//*************************************************************************
unsigned char Peak_BTN_Get_State(void)
{
    return  Peak_BTN_State ;
}




//*************************************************************************
// else no peak Button Configured
//*************************************************************************

#else

unsigned int Peak_BTN_Task(void)
{
	return Make_Return_Value(32000) ;

} 

void Peak_Button_Init(void) 
{
	
}

unsigned char Peak_BTN_Get_State(void) 
{
	return(PEAK_BTN_STATE_IDLE)	;
}
#endif

