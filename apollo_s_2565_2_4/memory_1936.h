/* 
 * File:   memory_1936.h
 * Author: Bill.Chandler
 *
 * Created on Nov 12, 2013
 */

#ifndef _MEMORY_1936_H
#define	_MEMORY_1936_H

#ifdef	__cplusplus
extern "C" {
#endif

 
    // Protoypes
    void Memory_Read_Block(UCHAR num_bytes, UCHAR source, UCHAR *dest);
    UCHAR Memory_Write_Block(UCHAR num_bytes, UCHAR *source, UCHAR dest);
    UCHAR Memory_Byte_Write(UCHAR byte, UCHAR dest);
    UCHAR Memory_Byte_Write_Verify(UCHAR byte, UCHAR dest);
    UCHAR Memory_Byte_Read(UCHAR addr);

#ifdef	__cplusplus
}
#endif

#endif	/* _MEMORY_1936_H */

