/******************************************************************
 * The contents of this file are Kidde proprietary and confidential.
 * ****************************************************************/
// REDLED.C
// Controls the Red LED blinking
#define _REDLED_C


#include    "common.h"
#include    "redled.h"

#include    "sound.h"
#include    "systemdata.h"
#include    "fault.h"



#define     RLED_TASK_TIME_100MS	100		// 100 ms
#define     RLED_TASK_TIME_500MS	500		// 500 ms
#define     RLED_TASK_TIME_1_SEC	1000		// 1 sec
#define     RLED_TASK_TIME_2_SEC	2000		// 2 sec
#define     RLED_TASK_TIME_30_SEC	30000		// 30 sec
#define     RLED_TASK_TIME_60_SEC	60000		// 60 sec


#define	STATE_RLED_INIT	   		0
#define	STATE_RLED_NORM			1
#define	STATE_RLED_FAULT		2

#define STATE_RLED_BLINK_IDLE           0
#define	STATE_RLED_BLINK_ON      	1
#define	STATE_RLED_BLINK_OFF   		2


static UCHAR RLED_State = STATE_RLED_INIT;
static UCHAR RLED_Blink_Count = 0 ;


// Global / Static Initialization (not needed for PIC)
/*
void RLED_Init(void)
{
	RLED_State = STATE_RLED_INIT ;
        RLED_Blink_State = STATE_RLED_BLINK_OFF;
	RLED_Blink_Count = 0;
	
}
*/

//****************************************************************
unsigned int RLED_Manager(void) 
{
	
    switch (RLED_State)
    {
        case	STATE_RLED_INIT:
                RLED_State = STATE_RLED_NORM;
        break;

        case	STATE_RLED_NORM:

          if ( FLAG_CO_ALARM_ACTIVE == 1)
          {
                /* While in Alarm Mode, Allow Sound Module to
                    control Red LED and wait in this state for now */
          }
          else
          {

                if(FLAG_RLED_ON)
                {
                    // Configure Red LED for write, ON, and enabled
                    RED_LED_ON

                }
                else if(FLAG_TAMPER_ACTIVE)
                {
                    RED_LED_ON
                }
                else if(FLAG_FAULT_FATAL)
                {
                    RLED_State = STATE_RLED_FAULT;
                }
                else if(FLAG_RLED_BLINK)
                {

                }
                else
                {
                    RED_LED_OFF
                }
          }

        break;

        case	STATE_RLED_FAULT:

          if (FLAG_CO_ALARM_ACTIVE)
          {
                // Fault recovery - Exit Fault Mode
                RLED_State = STATE_RLED_NORM;
                RLED_Blink_Count = 0;
          }
          else
          {
            if(RLED_Blink_Count == 0)
            {
                if(!FLAG_FAULT_FATAL)
                {
                    // Fault recovery - Exit Fault Mode
                    RLED_State = STATE_RLED_NORM;
                    RLED_Blink_Count = 0;
                }
                else
                {
                    // Allow Error Code Blinking under Calibration Faults
                    if(FLAG_CAL_ERROR == 1)
                    {
                        RLED_Blink_Count = FatalFaultCode;

                        // Test only
                        /*
                        SER_Send_Int('E', FatalFaultCode, 16) ;
                        SER_Send_Prompt() ;
                        */

                        return	Make_Return_Value(RLED_TASK_TIME_30_SEC) ;
                    }
                    else
                    {
                        return	Make_Return_Value(RLED_TASK_TIME_2_SEC) ;
                    }
                }
            }
            else
            {
                // Blink out the Code every 30 seconds
                RLED_Blink_Count--;
                FLAG_RLED_BLINK = 1;

                return	Make_Return_Value(RLED_TASK_TIME_500MS) ;

            }
          }
          break;
    }
    // Red LED is active mode only task
    return	Make_Return_Value(RLED_TASK_TIME_100MS) ;
}

//****************************************************************
// In order for the Red LED to begin blinking a new pattern immediately, this
// function should be called after changing the blink pattern.
void RLED_Reset_Task(void) 
{
    MAIN_Reset_Task(TASKID_RLED_MANAGER) ;
    RLED_State = STATE_RLED_INIT;

}


//****************************************************************
// Red LED ON/OFF routines for external use in Sound Module
//  and local use
void RLED_On (void)
{
    RED_LED_ON
}

void RLED_Off (void)
{
    RED_LED_OFF
}


//****************************************************************
// Red LED Blink control
UINT RLED_Blink(void)
{
    if(FLAG_RLED_BLINK)
    {
        FLAG_RLED_BLINK = 0;
        RED_LED_ON;
        delay_ms(25);
        RED_LED_OFF;
    }
    else
    {
        // No Action
    }
    return Make_Return_Value(100) ;
}
