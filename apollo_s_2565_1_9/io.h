/* 
 * File:   io.h
 * Author: bill.chandler
 *
 * Created on Nov 13, 2013, 6:40 AM
 */

#ifndef IO_H
#define	IO_H

#ifdef	__cplusplus
extern "C" {
#endif

        // Define IO bit assignments
        // Port A
#define PORT_BAT_VOLT       PORTAbits.RA0
#define PORT_CO_VOLT        PORTAbits.RA1
//#define PORT_SEG_A          PORTAbits.RA2
#define PORT_LCD_PWR_CTRL   PORTAbits.RA2
#define PORT_AC_DETECT      PORTAbits.RA3
#define PORT_SEG_E          PORTAbits.RA4
#define PORT_CO_TEST        PORTAbits.RA5
#define PORT_BAT_TEST       PORTAbits.RA6
#define PORT_SEG_C          PORTAbits.RA7

        // Port B
#define PORT_LGT_SENSOR     PORTBbits.RB1
#define PORT_GREEN_LED      PORTBbits.RB1
#define PORT_DIGSEL2        PORTBbits.RB2
#define PORT_TEST_BTN       PORTBbits.RB3
#define PORT_DIGSEL0        PORTBbits.RB4
#define PORT_DIGSEL1        PORTBbits.RB5
#define PORT_PEAK_BTN       PORTBbits.RB6
#define PORT_HORN_DISABLE   PORTBbits.RB7

        // Port C
#define PORT_T1OSC_O        PORTCbits.RC0
#define PORT_T1OSC_I        PORTCbits.RC1
#define PORT_SEG_D          PORTCbits.RC2
#define PORT_SEG_F          PORTCbits.RC3
#define PORT_SEG_B          PORTCbits.RC4
#define PORT_SEG_G          PORTCbits.RC5
#define PORT_TAMPER_SW      PORTCbits.RC6
#define PORT_ESC_BATTERY    PORTCbits.RC6
#define PORT_TX             PORTCbits.RC6
#define PORT_RX             PORTCbits.RC7

    
// Define Debug Pin based upon Model
#ifndef _CONFIGURE_VOICE
    #ifndef _CONFIGURE_ESCAPE_LIGHT
        // Use Voice or Escape Output pin
        #define PORT_DEBUG_TP       PORTBbits.RB0
    #else
        // Use TX output pin (Don't connect Serial Port)
        //#define PORT_DEBUG_TP       PORTCbits.RC6
    #endif

#else
        // Use TX output pin (Don't connect Serial Port)
        #define PORT_DEBUG_TP       PORTCbits.RC6
#endif


#ifdef	_CONFIGURE_VOICE
    // These bits yet to be defined for voice option
    #define PORT_VOICE_CLK      PORTBbits.RB6
    #define PORT_VOICE_DATA     PORTBbits.RB0
#endif

#ifdef	_CONFIGURE_ESCAPE_LIGHT
    // These bits yet to be defined for escape light
    #define PORT_ESCAPE_LIGHT      PORTBbits.RB0
    #define ESCAPE_LIGHT_OUTPUT    TRISBbits.TRISB0 = 0;
    #define ESCAPE_LIGHT_INPUT     TRISBbits.TRISB0 = 1;

    // When the Nightlight is to be kept off (Set RB0 High)
    // When Nightlight is enabled PWM will drive RB0
    #define NIGHTLIGHT_OFF         PORTBbits.RB0 = 1;



    #define ESCAPE_BATTERY_ON       PORTCbits.RC6 = 1;
    #define ESCAPE_BATTERY_OFF      PORTCbits.RC6 = 0;
    #define ESCAPE_BATTERY_OUTPUT   TRISCbits.TRISC6 = 0;
    #define ESCAPE_BATTERY_INPUT    TRISCbits.TRISC6 = 1;

#endif


    // Light Sensor LED
#define LIGHT_LED_ON            PORT_GREEN_LED = 1;
#define LIGHT_LED_OFF           PORT_GREEN_LED = 0;


// Make Green Led Pin analog input
#define LIGHT_LED_INPUT        TRISBbits.TRISB1 = 1;
#define LIGHT_LED_ANALOG_IN    ANSELBbits.ANSB1 = 1;
// Make Green Led Pin output
#define LIGHT_LED_OUTPUT        TRISBbits.TRISB1 = 0;
#define LIGHT_LED_DIGITAL_OUT   ANSELBbits.ANSB1 = 0;




// Define some I/O Actions

#define GREEN_LED_ON        PORT_GREEN_LED = 1;
#define GREEN_LED_OFF       PORT_GREEN_LED = 0;

#define CO_TEST_ON          PORT_CO_TEST = 1;
#define CO_TEST_OFF         PORT_CO_TEST = 0;

#define HORN_ON             PORT_HORN_DISABLE = 0;
#define HORN_OFF            PORT_HORN_DISABLE = 1;

#define BAT_TEST_ON         PORT_BAT_TEST = 1;
#define BAT_TEST_OFF        PORT_BAT_TEST = 0;

#ifdef	__cplusplus
}
#endif

#endif	/* IO_H */

