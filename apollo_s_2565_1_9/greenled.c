/****************************************************************
 * The contents of this file are Kidde proprietary  and confidential.
 * *************************************************************/
// greenled.c
#define	_GREENLED_C

#include    "common.h"
#include    "greenled.h"

#include    "systemdata.h"
#include    "cocalibrate.h"
#include    "a2d.h"
#include    "light.h"


#define	STATE_GLED_INIT	   		0
#define	STATE_GLED_CAL_BLINKING 	1
#define	STATE_GLED_NORM_BLINKING	2

#define	STATE_GLED_BLINK_IDLE  		0
#define	STATE_GLED_BLINK_ON      	1
#define	STATE_GLED_BLINK_OFF   		2


// Define Blink rates
#define GLED_BLINK_100_MS     100       // 100 ms
#define GLED_BLINK_200_MS     200       // 0 Cal
#define GLED_BLINK_300_MS     300       // 300 ms
#define GLED_BLINK_500_MS     500       // 500 ms
#define GLED_BLINK_1_SEC      1000      // Functional CO Mode
#define GLED_BLINK_2_SEC      2000      // Low Battery Hush
#define GLED_BLINK_10_SEC     10000     // CO Peak Memory
#define GLED_BLINK_60_SEC     60000     // Low Power Standby Mode

// Set Green LED Blink Time in ms
#define GRN_BLINK_TIME         25

// A/D Reference for PIC = VDD
// FVR to calcultae VDD = 1024 mv
#define FVR_MVOLTS			1024
// FVR_MVOLTS * 1024
#define FVR_FACTOR                      1048576


//Local Prototypes

// Local Variables
static UCHAR GLED_State = STATE_GLED_INIT;


UINT GLED_Manager(void)
{
    switch (GLED_State)
    {
        case STATE_GLED_INIT:
        {
            // Check Cal Status State
            GLED_State = STATE_GLED_CAL_BLINKING ;
        }
        break;

        case STATE_GLED_CAL_BLINKING:
            if (CAL_STATE_CALIBRATED == CAL_Get_State() )
            {
                // Already Calibrated, Proceed to Normal Operation
                GLED_State = STATE_GLED_NORM_BLINKING ;
            }
            else if ( (CAL_STATE_WAITING == CAL_Get_State() ) || (CAL_STATE_WAIT_DELAY == CAL_Get_State() )  )
            {
                GREEN_LED_OFF
//                SER_Send_String("Wait");

            }
            // We're in a Calibration State, Determine action
            else if ( CAL_STATE_VERIFY == CAL_Get_State() )
            {
                // If in 400 PPM verification, Green LED is solid ON
                GREEN_LED_ON
            }
            else if ( CAL_STATE_ZERO == CAL_Get_State())
            {
                // Zero cal blinking
                FLAG_GLED_BLINK = 1;
                return  Make_Return_Value(GLED_BLINK_100_MS) ;
            }
            else if (CAL_STATE_150 == CAL_Get_State())
            {
                // 150 Cal Blinking
                FLAG_GLED_BLINK = 1;
                return  Make_Return_Value(GLED_BLINK_300_MS) ;
            }
        break;

        case STATE_GLED_NORM_BLINKING:

            // Default to normal Blink Time

            // Functional CO Test is currently disabled in this Model
            #ifdef _CONFIGURE_CO_FUNC_TEST
                if(FLAG_FUNC_CO_TEST)
                {
                    // Blink every second
                    FLAG_GLED_BLINK = 1;
                    return Make_Return_Value(GLED_BLINK_1_SEC) ;

                }
            #endif

            // Disable  Peak CO blinks, as LED display will
            //  indicate this feature
            /*
            if (!FLAG_PTT_ACTIVE && (FLAG_CO_PEAK_MEMORY)  )
            {
                    // Blink every 10 seconds
                    FLAG_GLED_BLINK = 1;
                    return	Make_Return_Value(GLED_BLINK_10_SEC) ;
            }
            */

            // Disable 2 Sec Hush Blinking for Digital Model since the 
            // LED digital Display indicates this with the OFF message
            /*
            else if (FLAG_LB_HUSH_ACTIVE) 
            {

                    // Blink every 2 seconds
                    FLAG_GLED_BLINK = 1;
                    return	Make_Return_Value(GLED_BLINK_2_SEC) ;

            }
            */
            if( (FLAG_POWERUP_DELAY_ACTIVE == 0)
                  && (FLAG_AC_DETECT==0 ) )
            {
                if(FLAG_MODE_ACTIVE == 1)
                {
                    if((FLAG_PTT_ACTIVE) && (FLAG_CO_ALARM_CONDITION))
                    {
                        GREEN_LED_ON
                        return Make_Return_Value(GLED_BLINK_1_SEC) ;
                    }
                    else
                    {
                        // Battery Powered Active Blink Rate
                        FLAG_GLED_BLINK = 1;
                    }
                    return	Make_Return_Value(GLED_BLINK_60_SEC) ;
                }
                else
                {
                    if(!FLAG_LIGHT_MEAS_ACTIVE)
                    {

                        // Battery Powered Standby Blink Command
                        FLAG_GLED_BLINK = 1;
                        GLED_Blink();

                        return Make_Return_Value(GLED_BLINK_60_SEC) ;
                    }
                    else
                    {
                        return Make_Return_Value(GLED_BLINK_1_SEC) ;

                    }

                }
            }
            else
            {
                if((FLAG_PTT_ACTIVE) && (FLAG_CO_ALARM_CONDITION))
                {
                    GREEN_LED_ON
                }
                // Else No Green LED activity
            }
        break;

    }
    return  Make_Return_Value(GLED_BLINK_1_SEC) ;
}



//****************************************************************
// Green LED ON/OFF routines for external/local use
void GLED_On(void)
{
    GREEN_LED_ON
}

void GLED_Off(void)
{
    GREEN_LED_OFF
}


//****************************************************************
// Green LED Blink control
UINT GLED_Blink(void)
{

    if((FLAG_GLED_BLINK == 1) && (!FLAG_LIGHT_MEAS_ACTIVE))
    {
        // Delay Blink if Voice Transmission is Active
        if (!FLAG_VOICE_SEND)
        {
            //Main_TP_On();

            FLAG_GLED_BLINK = 0;
            GREEN_LED_ON;
            delay_ms(GRN_BLINK_TIME);
            GREEN_LED_OFF;

            //Main_TP_Off();
        }
    }
    else
    {
        // No Action
    }
    return Make_Return_Value(GLED_BLINK_100_MS) ;

}

