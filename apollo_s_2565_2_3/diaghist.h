/****************************************************************
 * The contents of this file are Kidde proprietary and confidential.
 * *************************************************************/
// DIAGHIST.h
// Routines for the logging of events.

#ifndef _DIAGHIST_H
#define _DIAGHIST_H

#ifdef  _CONFIGURE_DIAG_HIST



    /* 48 Events per Queue
    *  2 bytes per event */
    #define MAX_EVENTS      48

    struct event_info
    {
        UCHAR	eventcode ;         // Event Code in MS nibble
        UCHAR	eventday ;
    } ;

    /*
       Define Diag History data structure:
       1. Must be 256 bytes (for backward compatibility)
       2. Pad as needed to keep 16 bit word alignment for UINT values
       3. Major and Minor queues should be 96 bytes (48 events each)
    */
    struct  HistData {

        //***** 0x00 - 0x0F locations *****
        // Primary Copy of Data Structure

        //*****	0x10 - 0x1F locations *****
        // Unused


        //*****	0x20 - 0x2F locations *****
        UCHAR   PTT_Count;        		// 20
        UCHAR   Peak_Button_Count;		// 21
        UCHAR   Low_Battery_Count;		// 22
        UINT    Battery_Day_1;    		// 23, 24
        UINT    Battery_Year_1;			// 25, 26
        UINT    Battery_Now;			// 27, 28
        UCHAR   Spare_26;			// 29
        UCHAR   Spare_27;			// 2A
        UINT	CO_Last_Alarm;  		// 2B, 2C
        UCHAR	Spare_2D;			// 2D
        UCHAR   REV_MAJOR;			// 2E
        UCHAR   REV_MINOR;			// 2F

        //***** 0x30 - 0x3F locations *****
        UCHAR	CO_Alarm_Count;			// 30
        UCHAR	CO_Fault_Count;			// 31
        UCHAR	CO_Short_Count;                 // 32
        UCHAR	Spare_33;       		// 33

        UINT	Peak_CO_Value;			// 34, 35
        UINT	Peak_CO_Day;			// 36, 37

        UCHAR	PWR_Reset_Count;		// 38
        UCHAR	AC_PWR_Count;			// 39
        UCHAR	Spare_3A;			// 3A

        UCHAR 	Spare_3B;       		// 3B
        UCHAR 	EE_Err_Cnt;			// 3C
        UCHAR   QPTR_Cksum;			// 3D
        UCHAR   Major_Queue_Pointer;		// 3E
        UCHAR   Minor_Queue_Pointer;		// 3F

        //	0x40 - 0xFF locations
        //      These Event Queues must be initialized (filled with 0xFF)
        struct event_info Major_Event_Queue[MAX_EVENTS] ;	// 40 - 9F major queue data
        struct event_info Minor_Event_Queue[MAX_EVENTS] ;	// A0 - FF minor queue data

    };

    //Prototypes
    void Diag_Hist_Que_Push(UCHAR event_code) ;
    void Diag_Hist_Test_Peak(UINT ppm) ;
    void Diag_Hist_Bat_Record(UCHAR battime, UINT batvolt);

    void Diag_Hist_EE_Error(void);

    //Definitions for Battery Voltage Recording
    #define BAT_DAY_ONE     1
    #define BAT_YEAR_ONE    2
    #define BAT_CURRENT     3

    //
    // History Events (Note: Bit 7 is reset to indicate a Minor Event)
    #define HIST_MINOR_0_EVENT		0x00	// Unused
    #define HIST_MINOR_1_EVENT		0x01	// Unused
    #define HIST_MINOR_2_EVENT		0x02	// Unused
    #define HIST_MINOR_3_EVENT		0x03	// Unused
    #define HIST_LOW_BATT_MODE		0x04	// Low Battery Mode Entered
    #define HIST_ABN_LOW_BATT_EVENT     0x05	// Abnormally Low Battery
    #define HIST_MINOR_6_EVENT		0x06	// Unused
    #define HIST_MINOR_7_EVENT		0x07	// Unused
    #define HIST_MINOR_8_EVENT		0x08	// Unused
    #define HIST_TAMPER_EVENT		0x09	// Unused
    #define HIST_AC_POWER_OFF		0x0A	// AC Power Off detected
    #define HIST_AC_POWER_ON		0x0B	// AC Power On detected
    #define HIST_POWER_RESET		0x0C	// Power On Reset Event
    #define HIST_PTT_EVENT          0x0D	// Push to Test Pressed
    #define HIST_PEAK_BTN_EVENT		0x0E	// Peak Button Pressed
    #define HIST_MINOR_F_EVENT		0x0F	// Unused

    // History Events (Note: Bit 7 is set to indicate a Major Event)
    #define HIST_MAJOR_0_EVENT		0x80	// Unused
    #define HIST_MAJOR_1_EVENT		0x81	// Unused
    #define HIST_SENS_HEALTH_FAULT	0x82	// 'E02' Sensor Health
    #define HIST_SENS_SHORT_FAULT	0x83	// 'E03' Sensor Short
    #define HIST_PTT_FAULT		0x84	// Push to Test Fault
    #define HIST_MAJOR_5_EVENT		0x85	// Unused
    #define HIST_SENS_CALIB_FAULT	0x86	// Calibration Fault 'E06'
    #define HIST_SENS_GAS_FAULT		0x87	// Unused
    #define HIST_MEMORY_FAULT		0x88	// Eeprom Memory Fault 'E08'
    #define HIST_LIFE_EXP		0x89	// Life Expiration Fault 'E09'
    #define HIST_PHOTOCHAMBER_FAULT	0x8A	// Unused
    #define HIST_SMK_ALARM_HUSH_EVENT	0x8B	// Unused
    #define HIST_SMK_ALARM_ON_EVENT	0x8C	// Unused
    #define HIST_GAS_ALARM_EVENT	0x8D	// Unused
    #define HIST_ALARM_OFF_EVENT	0x8E	// CO Alarm OFF Event Detected
    #define HIST_ALARM_ON_EVENT		0x8F	// CO Alarm ON  Event Detected

    // Offset into EEProm
    #define DIAG_HIST_START         0x20

    #define DIAG_MAJOR_QUE_START    0x40
    #define DIAG_MAJOR_QUE_END      0x9F
    #define DIAG_MINOR_QUE_START    0xA0
    #define DIAG_MINOR_QUE_END      0xFF

    // Event Queue Pointers
    #define MAJOR_QPTR_INIT         DIAG_MAJOR_QUE_START
    #define MINOR_QPTR_INIT         DIAG_MINOR_QUE_START
    #define QPTR_CKSUM_INIT         (MAJOR_QPTR_INIT + MINOR_QPTR_INIT)

    #define DIAG_CNT_EE_ERR       0x3C

    #define DIAG_QUE_CKSUM        0x3D
    #define DIAG_MAJOR_QUE_PTR    0x3E
    #define DIAG_MINOR_QUE_PTR    0x3F


    // Define Event Counter offsets
    #define DIAG_CNT_PTT          0x20
    #define DIAG_CNT_PEAK_BTN     0x21
    #define DIAG_CNT_LOW_BATT     0x22
    #define DIAG_CNT_ALARM_ON     0x30
    #define DIAG_CNT_SENS_HEALTH  0x31
    #define DIAG_CNT_SENS_SHORT   0x32
    #define DIAG_CNT_POWER_RESET  0x38
    #define DIAG_CNT_AC_POWER     0x39

    // Define other History offsets
    #define DIAG_BATTERY_DAY_1    0x23
    #define DIAG_BATTERY_YEAR_1   0x25
    #define DIAG_BATTERY_CURRENT  0x27

    #define DIAG_CO_LAST_ALARM    0x2B

    #define EE_UNUSED             0x33
    #define DIAG_PEAK_CO_VALUE    0x34
    #define DIAG_PEAK_CO_DAY      0x36


#endif
#endif


	
