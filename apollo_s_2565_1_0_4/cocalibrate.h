/****************************************************************
 * The contents of this file are Kidde proprietary and confidential.
 * *************************************************************/
// CALIBRATE.H

#ifndef _CALIBRATE_H
#define _CALIBRATE_H

    // Prototypes
    void CAL_Init(void) ;
    UINT CAL_Manager(void) ;
    UCHAR CAL_Get_State(void) ;

    //
    // Cal States
    //
    #define	CAL_STATE_ZERO		0
    #define	CAL_STATE_150		1
    #define	CAL_STATE_VERIFY	2
    #define	CAL_STATE_WAITING	3
    #define	CAL_STATE_CALIBRATED	4
    #define	CAL_STATE_WAIT_DELAY	5

    //
    //  Definition Statements
    //
    #define STATUS_PARTIAL_BITMAP	0
    #define STATUS_COMPLETE_BITMAP	1
    #define STATUS_UNTESTED_BITMAP	2

    //
    // Cal Status definitions
    //
    #define STATUS_CAL_ZERO             0
    #define STATUS_CAL_150              1
    #define STATUS_CAL_COMPLETE         3
    #define STATUS_CAL_FINAL_TEST       7


    //
    //  The programmer is dependent on the follow value.
    //
    #define CALIBRATION_TABLE_ADDRESS		0x0800 - 32

    #define CALIBRATION_DEFAULT_TEMPERATURE	256	// do not change
    #define CALIBRATION_DEFAULT_HEALTH		0x0465	// do not change

    //
    //  Values for manual calibration.
    //
    #define CALIBRATION_OFFSET			0
    #define CALIBRATION_SLOPE			256
    #define CALIBRATION_TEMPERATURE		256
    #define CALIBRATION_HEALTH			CALIBRATION_DEFAULT_HEALTH

    #define CALIBRATION_PPM_LEVEL		150


    #define CALIBRATION_STATUS_INDEX		MEMORY_STATUS_ADDRESS - MEMORY_STATUS_ADDRESS
    #define CALIBRATION_OFFSET_INDEX		MEMORY_OFFSET_ADDRESS - MEMORY_STATUS_ADDRESS
    #define CALIBRATION_HEALTH_INDEX		MEMORY_HEALTH_ADDRESS - MEMORY_STATUS_ADDRESS
    #define CALIBRATION_SLOPE_INDEX		MEMORY_SLOPE_ADDRESS  - MEMORY_STATUS_ADDRESS
    #define CALIBRATION_NUMBER_OF_ENTRIES	CALIBRATION_SLOPE_INDEX + 2


    // With Gain set to 3 (800K)
    // With 1 mv offset voltage
    //#define CALIBRATION_OFFSET_MAXIMUM	70
    //#define CALIBRATION_OFFSET_MINIMUM	10

#ifdef _CONFIGURE_NO_REGULATOR
    //TODO: These are TBD limits (Bench Testing avg = ~150 - 170)
    //          0 PPM Cal
    #define CALIBRATION_OFFSET_MAXIMUM		260
    #define CALIBRATION_OFFSET_MINIMUM		60

    //		150 PPM Cal
    // Consider that 0 PPM Offset is subtracted from Limits 
    #define CALIBRATION_SLOPE_MAXIMUM		650
    #define CALIBRATION_SLOPE_MINIMUM		200
#else
    //TODO: These are TBD limits (Bench Testing avg = ~60)
    //          0 PPM Cal
    #define CALIBRATION_OFFSET_MAXIMUM		100
    #define CALIBRATION_OFFSET_MINIMUM		20

    //		150 PPM Cal
    #define CALIBRATION_SLOPE_MAXIMUM		200
    #define CALIBRATION_SLOPE_MINIMUM		50
#endif


#endif
		
		
