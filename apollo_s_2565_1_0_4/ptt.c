#include	"common.h"
#include	"ptt.h"

#include	"systemdata.h"
#include	"sound.h"
#include	"fault.h"
#include	"comeasure.h"
#include	"life.h"
#include        "memory_1936.h"
#include        "diaghist.h"
#include        "greenled.h"

// State constants
#define		PTT_STATE_IDLE				0
#define		PTT_STATE_WAIT_CO_ALARM			1
#define		PTT_STATE_WAIT_CO_CLEAR			2

#define		PTT_DEFAULT_INT_MS			500	// 1/2 second
#define		PTT_CO_ALARM_THRESHOLD			70	// PPM
#define		PTT_WAIT_CO_ALARM_TIME_MS		8000	// 8 seconds


#ifdef _CONFIGURE_NO_REGULATOR
#define		VMEAS_AD_TEST_THRESHOLD			500	// mVolts
#else
#define		VMEAS_AD_TEST_THRESHOLD			100	// AD Counts
#endif

static UCHAR PTT_State = 0 ;
static UCHAR PTT_Count ;


//******************************************************************************
unsigned int PTT_Task(void)
{
    switch (PTT_State)
    {
        case	PTT_STATE_IDLE:
            // Wait here until the PTT Active flag gets set, probably in the
            // button module. Wait for a Button Voice to Send before
            // Starting PTT
            if ((FLAG_PTT_ACTIVE) && (!FLAG_VOICE_SEND))
            {

                PTT_Count = 20; 	// Set Alarm Timeout
                
                 #ifdef _CONFIGURE_LITHIUM
                    // Reset De-Pass Timer every Test Sequence
                    Depass_Timer = DEPASSIVATION_INTERVAL;
                #endif

                // Start the PTT sequence.
                #ifdef _CONFIGURE_DIAG_HIST
                    Diag_Hist_Que_Push(HIST_PTT_EVENT);
                #endif

                // Next state.
                PTT_State = PTT_STATE_WAIT_CO_ALARM ;

                /* MAIN_Reset_Task(TASKID_DISP_UPDATE) ; */

                // Reset the CO State Machine, and send charge pulses to
                //  test CO Sensor output
                MEAS_Reset() ;
                MEAS_CO_Charge(wVout_Baseline + VMEAS_AD_TEST_THRESHOLD) ;


            }
            break ;


        case	PTT_STATE_WAIT_CO_ALARM:

            //SER_Send_String("Ptt");

            if (wPPM > PTT_CO_ALARM_THRESHOLD)
            {

                // CO detected, Set Alarm Condition
                FLAG_CO_ALARM_CONDITION = 1 ;

                // Allow Green LED to Come On immediately
                MAIN_Reset_Task(TASKID_GLED_MANAGER) ;

                // Proceed to Clear CO Alarm state
                PTT_State = PTT_STATE_WAIT_CO_CLEAR ;

                return	Make_Return_Value(PTT_WAIT_CO_ALARM_TIME_MS) ;
            }
            else
            {

                if(--PTT_Count == 0)
                {
                    //  Unit has not gone into alarm. That's a PTT fault.
                    FLT_Fault_Manager(FAULT_PTT) ;
                    FLAG_PTT_ACTIVE = 0 ;
                    PTT_State = PTT_STATE_IDLE ;
                }
                else
                {
                    // Hold Test charge level during sampling
                    MEAS_CO_Charge(wVout_Baseline + VMEAS_AD_TEST_THRESHOLD) ;
                }
            }
            break ;

        case	PTT_STATE_WAIT_CO_CLEAR:

           FLAG_CO_ALARM_CONDITION = 0 ;

            // When sounder state is zero the sounder has stopped
            if (!SND_Get_State())
            {
                FLAG_PTT_RESET = 1 ;

                // Writing to the WDT register without a password should cause
                // the controller to reset.
                SOFT_RESET 
            }
            break ;

    }

    return  Make_Return_Value(PTT_DEFAULT_INT_MS) ;
}

