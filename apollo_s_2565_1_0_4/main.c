/****************************************************************
 * The contents of this file are Kidde proprietary  and confidential.
 * *************************************************************/
//MAIN.C
#define _MAIN_C

#include    "common.h"
#include    "defmsgs.h"

#include    "memory_1936.h"
#include    "diaghist.h"

#include    "a2d.h"
#include    "alarmprio.h"
#include    "battery.h"
#include    "button.h"
#include    "coalarm.h"
#include    "cocalibrate.h"
#include    "cocompute.h"
#include    "comeasure.h"
#include    "command_proc.h"
#include    "fault.h"
#include    "life.h"
#include    "ptt.h"
#include    "sound.h"
#include    "systemdata.h"
#include    "tamper.h"
#include    "greenled.h"
#include    "peak_button.h"
#include    "led_display.h"
#include    "esclight.h"
#include    "voice.h"
#include    "light.h"


// Local Prototypes
void Initialize_PowerUp(void) ;
void Main_Check_Revision(void);
void Main_Init_Powerup(void);
void Main_Init_Power_Dly_Timer(void);
void Main_Initialize(void);
void Initialize_Engine(void) ;
//void Clear_Linear_Ram(void);
//void Clear_Common_Ram(void);

void Main_Wait_T1Osc_Stable(void);
void Main_Reset_Sleep_Timer(void);

void Main_Set_Low_Power(void) ;
void Main_Set_Active(void) ;
void Main_Check_Low_Power_Logic(void) ;
void Wifi_Module_enable(void);
void Payload_Output(void);
UINT DebugTask(void) ;



#ifdef _CONFIGURE_USEC_TIMERS
// Sets the integer that is passed in to the current value of the timer.
void Main_Get_1_Microsecond_Timer(UCHAR *timer_var);
void Main_Get_16_Microsecond_Timer(UCHAR *timer_var);

void Timer_Test_Usecs(void);

#endif


// Defines

#define	MAX_NUM_TASKS		18
#define	NUM_MODES			2

//#define	NUM_TASKS_ACTIVE	17
#define	NUM_TASKS_ACTIVE	16
//#define	NUM_TASKS_LOWPOWER	7
#define	NUM_TASKS_LOWPOWER	6

#define	MODE_ACTIVE			0
#define	MODE_LOW_POWER		1
#define	MS_PER_TIC_ACTIVE	10
#define	MS_PER_TIC_LOWPOWER	1000

#define	MAIN_TIC_INTERVAL_10MS		10	// milliseconds
#define	TIMER1_INIT_VALUE_10MS		(65536 - 328)

#define	MAIN_TIC_INTERVAL_100MS		100	// milliseconds
#define	TIMER1_INIT_VALUE_100MS		(65536 - 3277)

#define	MAIN_TIC_INTERVAL_1000MS	1000	// milliseconds
#define	TIMER1_INIT_VALUE_1000MS	(65536 - 32768)

#define MAIN_PWR_DELAY_TIME			500   // 5.0 seconds

//*********************************  WDT  **************************************
#define	MAIN_WDT_STARTUP_TIMEOUT    0b00011000		//4 Sec
#define	MAIN_WDT_NORMAL_TIMEOUT     0b00010110		//2 Sec


//****************************  Clock Stuff  ***********************************
// Task Time Tic Intervals in ms
#define	MAIN_TIC_INTERVAL_ACTIVE	10
#define	MAIN_TIC_INTERVAL_LOWPOWER	1000


#define	TIMER_1SEC_ACTIVE_COUNT 	(1000/MAIN_TIC_INTERVAL_ACTIVE)	// in Active Tics
#define	TIMER_ONE_MINUTE_COUNT  	60				// in 1000ms Tics
#define TIMER_ONE_HOUR_COUNT		60              // in Minutes
#define TIMER_5_MINUTES				5

//***********************************************************************************
/* Notes: Data Memory allocation and Initialization
 * For initialized variables - initialized by code at declaration
 * (i.e =  static UCHAR b1SecCount = TIMER_1SEC_ACTIVE_COUNT ;)
 * These are located by compiler in .data psects
 *
 * For unitialized variables, compiler startup code will clear to 0.
 * These are located by compiles in .bss psects
 *
 * For variables that are not to be cleared or initialized by startup code
 * (i.e CO accumulators or peak PPM memory)
 * use the 'persistent' qualifier at declaration and init as needed in the code
 * These are located by compiles in .nv psects
 *
 * Local auto and parameter variables are located by compiler in .cstack
 * psects. These psects contain the compiled stack.
*/

// Local Static variables
static UINT TicTimer = 0 ;
static UINT CurrentMsPerTic = 0;
static UCHAR CurrentMode = 0 ;
static UCHAR b1SecCount = TIMER_1SEC_ACTIVE_COUNT ;
static UINT  PowerupAwakeTimer = 0 ;
//UCHAR Payload[PAYLOAD_LENGTH] = {0};
struct UartPayload Payload;

// Global Variables (extern in header file)

UCHAR bOneMinuteTic = 0;
UCHAR OneMinuteCount = TIMER_ONE_MINUTE_COUNT;
UCHAR b1SecTimer = 0;
UCHAR b1HourCount = TIMER_ONE_HOUR_COUNT;

#ifdef _CONFIGURE_ESC_LIGHT_CYCLING

UINT ESC_Light_Timer = 1;   // Start cycling in One Minute

// OFF/ON cycle times (minutes)
#define ESC_LIGHT_OFF   180
#define ESC_LIGHT_ON    30

#endif



UCHAR volatile PCON_Save = 0;
UCHAR volatile STATUS_Save = 0;
//////////////////////////////////////////////////////////////////
// To add another task:
//	1. Declare a TaskInfo variable and initialize.
//	2. Add a pointer to the TaskInfo structure to TaskTable
//	3. Add a task ID in main.h.  Make sure that the ID corresponds to the 
//		position of the TaskInfo pointer in TaskTable
//	4. Include the header file of the task module above.  Task prototype is
//		UINT Task(void)
//  5. Add the task ID to the desired mode in Main_Mode and increment the
//		NUM_TASKS_xxx for the mode.

/////////////////////////////////////////////////////////////////
struct TaskInfo
{
  volatile UINT TaskTime ;
  volatile UINT TaskInterval;
  UINT (*funcptr)(void) ;
} ;

struct TaskInfo SoundTaskInfo ;
struct TaskInfo ButtonTaskInfo ;
struct TaskInfo PTT_TaskInfo ;
struct TaskInfo BatteryTestInfo ;
struct TaskInfo DebugTaskInfo ;
struct TaskInfo AlarmPrioInfo ;
struct TaskInfo COAlarmInfo ;
struct TaskInfo CalManagerInfo ;
struct TaskInfo COMeasureInfo ;
struct TaskInfo TroubleManagerInfo ;
struct TaskInfo GrnLedInfo ;
struct TaskInfo GrnLedBlinkInfo ;
//struct TaskInfo DispUpdateInfo ;
struct TaskInfo PeakButtonInfo ;
struct TaskInfo EscLightInfo ;
struct TaskInfo LightInfo;
struct TaskInfo VoiceInfo ;



//  The number of tasks in each mode.
const unsigned int TaskNumbers[NUM_MODES] = {NUM_TASKS_ACTIVE, NUM_TASKS_LOWPOWER} ;

struct TaskInfo  * const(TaskTable)[] = {
					&SoundTaskInfo,
					&ButtonTaskInfo, 
					&PTT_TaskInfo, 
					&BatteryTestInfo,
					&DebugTaskInfo, 
					&AlarmPrioInfo, 
					&COAlarmInfo,
					&CalManagerInfo, 
					&COMeasureInfo, 
					&TroubleManagerInfo,
					&GrnLedInfo,
					&GrnLedBlinkInfo,
//			        &DispUpdateInfo,
					&PeakButtonInfo,
					&EscLightInfo,
					&LightInfo,
					&VoiceInfo 
				};
										
// Test Only
/*
const UCHAR Main_Mode[NUM_MODES][MAX_NUM_TASKS] = {
                                {	TASKID_DEBUG_TASK,
                                        TASKID_DEBUG_TASK,
                                        TASKID_DEBUG_TASK,
                                        TASKID_DEBUG_TASK,
                                        TASKID_DEBUG_TASK,
                                        TASKID_DEBUG_TASK,
                                        TASKID_DEBUG_TASK,
                                        TASKID_DEBUG_TASK,
                                        TASKID_DEBUG_TASK,
                                        TASKID_DEBUG_TASK,
                                        TASKID_DEBUG_TASK,
                                        TASKID_DEBUG_TASK,
                                        TASKID_DEBUG_TASK,
                                        TASKID_DEBUG_TASK,
                                        TASKID_DEBUG_TASK,
                                        TASKID_DEBUG_TASK,
                                        TASKID_DEBUG_TASK
                                },

                                {
                                        TASKID_DEBUG_TASK,
                                        TASKID_DEBUG_TASK,
                                        TASKID_DEBUG_TASK,
                                        TASKID_DEBUG_TASK,
                                        TASKID_DEBUG_TASK,
                                        TASKID_DEBUG_TASK,
                                        TASKID_DEBUG_TASK
                                } } ;
*/


// Define the tasks that are in each mode.
const UCHAR Main_Mode[NUM_MODES][MAX_NUM_TASKS] = {
                                {
                                    TASKID_SOUND_TASK,
                                    TASKID_BUTTON_TASK,
                                    TASKID_PTT_TASK,
                                    TASKID_BATTERY_TEST,
                                    TASKID_ALARM_PRIORITY,
                                    TASKID_DEBUG_TASK,
                                    TASKID_COALARM,
                                    TASKID_CALIBRATE,
                                    TASKID_COMEASURE,
                                    TASKID_TROUBLE_MANAGER,
//                                    TASKID_DISP_UPDATE, 
                                    TASKID_GLED_MANAGER,
                                    TASKID_GLED_BLINK_TASK,
                                    TASKID_PEAK_BUTTON_TASK,
                                    TASKID_ESCAPE_LIGHT,
                                    TASKID_LIGHT_TASK,
                                    TASKID_VOICE_TASK
                                },

                                {
                                    TASKID_COMEASURE,
                                    TASKID_COALARM,
                                    TASKID_BATTERY_TEST,
                                    TASKID_TROUBLE_MANAGER,
//                                    TASKID_DISP_UPDATE,
                                    TASKID_LIGHT_TASK,
                                    TASKID_GLED_MANAGER

                               /*
                                 TASKID_DEBUG_TASK,
                                 TASKID_DEBUG_TASK,
                                 TASKID_DEBUG_TASK,
                                 TASKID_DEBUG_TASK,
                                 TASKID_DEBUG_TASK,
                                 TASKID_DEBUG_TASK,
                                 TASKID_DEBUG_TASK
                               */
                                } } ;



//*************************************************************************
int main( void ) 
{
    unsigned int i = 0 ;
    unsigned int interval ;

    // Test only
    PCON_Save = PCON;
    STATUS_Save = STATUS;

    // Init non-reset data if Power Reset
    Main_Init_Powerup();

    // Init the WatchDog Timer
    #ifdef  _CONFIGURE_WATCHDOG_TIMER_OFF
        WDTCONbits.SWDTEN = 0;
    #else
        WDTCON = MAIN_WDT_STARTUP_TIMEOUT;
        WDTCONbits.SWDTEN = 1;
        CLRWDT();
    #endif


    // Init Data Memory
    // Since not using absolute address locations can't clear all Data
    // since it would always clear any non-reset data
    // Data cleared or Initialized in Startup Code by C Code declarations
    //    Clear_Linear_Ram();
    //    Clear_Common_Ram();

    FLAG_RESET = 1;

    // Initialize Data Structure from non-Volatile Flash
//    SYS_Init();

    // Initialize I/O
    Main_Initialize();

    // Test Only
//    Main_TP_Off();

	//delay_ms(500); // delay 500 ms to wait the power stable 
	
	SYS_Init();	// move the EE data loading after power stabilization.
	
    BORCONbits.SBOREN = 1;      // Enable Software Brownout
    
    // Power Reset?
    if(PCONbits.nPOR == 0)
    {
        PCONbits.nPOR = 1;      // Reset Power On Reset Flag
        #ifdef  _CONFIGURE_DIAG_HIST
            // Record Diagnostic History Power Reset
            Diag_Hist_Que_Push(HIST_POWER_RESET);
        #endif
    }

    SER_Init() ;    // Test for Serial Port Initialization
    
    #ifdef _CONFIGURE_TAMPER_SWITCH
        Tamper_Init();  // Init Tamper must be called after SER_Init()
    #endif
    
    Life_Init() ;   // Verify no Life Expiration

    /*
    // *********** Set a Trap *************
    do{
        //ANSELB = 0b00000000;
        //TRISB  = 0b00001000;
        //PORTB  = 0b10000000;

        Main_TP_On();
        Main_TP_Off();

    }while(1);
    */

    #ifdef  _CONFIGURE_DEBUG_PIN_ENABLED
    //    Main_TP_On();
    #endif

   #ifdef _CONFIGURE_ACDC
    //Test and Update Power Status every second
    A2D_Test_AC() ;
    #endif
    FLAG_LCD_POWER_DELAY = 1;
   
    while(1)    //	Main Task Process/Sleep Loop
    {
        // *** TEST POINT ***
        #ifdef	_CONFIGURE_DEBUG_PIN_ENABLED
            //Main_TP_On();
        #endif

        if ((TicTimer - TaskTable[ Main_Mode[CurrentMode][i] ]->TaskTime) >=
            (TaskTable[ Main_Mode[CurrentMode][i] ]->TaskInterval))
        {
                
            // Test Only
            /*
            if(i == TASKID_COALARM)
            {
                SER_Send_Int('k', (TaskTable[Main_Mode[CurrentMode][TASKID_COALARM]]->TaskTime ), 10) ;
                SER_Send_Prompt() ;
            }
            */

            /*
            if(i == TASKID_GLED_MANAGER)
            {
                SER_Send_Int('t', TicTimer, 10) ;

                SER_Send_Int('k', (TaskTable[Main_Mode[CurrentMode][i]]->TaskTime ), 10) ;
                SER_Send_Int('i', (TaskTable[Main_Mode[CurrentMode][i]]->TaskInterval ), 10) ;
                SER_Send_Prompt() ;
            }
            */

		// task timer has expired
			#ifdef	_CONFIGURE_DEBUG_PIN_ENABLED
				//Main_TP_On();
			#endif

			interval = TaskTable[ Main_Mode[CurrentMode][i] ]->funcptr() ;

            #ifdef	_CONFIGURE_DEBUG_PIN_ENABLED
                //Main_TP_Off();
            #endif

            // Always Set Returned interval and update Task Time
			TaskTable[Main_Mode[CurrentMode][i]]->TaskInterval = interval ;
			TaskTable[Main_Mode[CurrentMode][i]]->TaskTime = TicTimer ;

            /*  Comment out the old method of Task interval/time update
            if (interval != TaskTable[Main_Mode[CurrentMode][i]]->TaskInterval)
            {
                        // different interval received
                        TaskTable[Main_Mode[CurrentMode][i]]->TaskInterval = interval ;
                        TaskTable[Main_Mode[CurrentMode][i]]->TaskTime = TicTimer ;
            }
            else
            {
                        //no change in interval
                        TaskTable[Main_Mode[CurrentMode][i]]->TaskTime += interval ;
            }
            */

            // Test Only
            /*
            if(i == TASKID_COALARM)
            {
                SER_Send_Int('K', (TaskTable[Main_Mode[CurrentMode][TASKID_COALARM]]->TaskTime ), 10) ;
                SER_Send_Prompt() ;
            }
            */

            // Test Only
            /*
            if(i == TASKID_GLED_MANAGER)
            {
                SER_Send_Int('T', TicTimer, 10) ;

                SER_Send_Int('K', (TaskTable[Main_Mode[CurrentMode][i]]->TaskTime ), 10) ;
                SER_Send_Int('I', (TaskTable[Main_Mode[CurrentMode][i]]->TaskInterval ), 10) ;
                SER_Send_Prompt() ;
            }
            */        
        }

        i++ ;

        if (i > TaskNumbers[CurrentMode] - 1)
        {
            // *** TEST POINT ***
            #ifdef	_CONFIGURE_DEBUG_PIN_ENABLED
				//Main_TP_Off();
            #endif

                // Reset Task pointer and Bump TIC Time
            i = 0 ;
            TicTimer++ ;

            // ****************************************************************
            // Put anything here that has to happen at the end of all the tasks
            FLAG_RESET = 0; // Clear Reset Flag after 1st task Initialization

			// BC - Good place to update any Global system timers
			// i.e - b1SecTimer, bOneMinuteTimer... etc

			if(FLAG_MODE_ACTIVE)
			{
				// Check the Serial Port
				SER_Process_Rx();

				#ifdef _CONFIGURE_LITHIUM
					if(FLAG_DEPASSIVATION_ACTIVE == 1)
					{
						LFE_Depassivation();
					}
				#endif

				if(--b1SecCount == 0)   // based on 10 ms Tics
				{
					// Test Only
					//Main_TP_On();

					b1SecTimer ++;
					b1SecCount = TIMER_1SEC_ACTIVE_COUNT;

					#ifdef	_CONFIGURE_ACTIVE_TEST
						if (FLAG_SERIAL_PORT_ACTIVE)
						{
							// *** Test Only (Output Active Flags ***
							SER_Send_Int(' ', *( (UINT*)&Flags_Active1), 16) ;
							SER_Send_Char(',');
							SER_Send_Int(' ', *( (UINT*)&Flags_Active2), 16) ;
							SER_Send_Prompt() ;
							SER_Send_Int(' ', *( (UINT*)&Flags_Active3), 16) ;
							SER_Send_Prompt() ;
						}
					#endif

					#ifdef _CONFIGURE_ACDC
						// Test and Update Power Status every second
						A2D_Test_AC() ;
					#endif

					#ifdef _CONFIGURE_TAMPER_SWITCH
						Tamper_Process();
					#endif

					// Test Only
					//Main_TP_Off();
						
					Payload_Output();
				}
			}
			else
			{
				// Test Only
				//Main_TP_On();
				//Main_TP_Off();

				// Test Only
				//Main_TP_On();

				// We're in Low Power Mode
				// based on 1 Second Tics
				b1SecTimer ++;

				#ifdef _CONFIGURE_ACDC
					// Test and Update Power Status every second
					A2D_Test_AC() ;
				#endif

				#ifdef _CONFIGURE_TAMPER_SWITCH
					Tamper_Process();
				#endif

				// Here we can perform other Low Power Mode tasks
				// not included in the Task table (to save power)
				// i.e
				// Test Low power Mode, Tamper Swith
				// Test Low Power Mode, Battery Test

				// For ACDC non-Digital we may just put them in Standby Task table???

				// Test Only
				//Main_TP_Off();

			}

			if(OneMinuteCount == b1SecTimer)
			{
				// Validate Data Structure Checksum once a minute
				if(FALSE == SYS_Validate_Data_Structure())
				{
					SOFT_RESET
				}
//				if (FLAG_SERIAL_PORT_ACTIVE)
//				{
//					SER_Send_Prompt() ;
//					SER_Send_String("I");
//					SER_Send_Int('D', ( (SYS_RamData.Unit_ID_High << 8) + SYS_RamData.Unit_ID_Low  ), 16) ;
//					SER_Send_Prompt() ;
//				}

				#ifdef _CONFIGURE_ID_MANAGEMENT
				  if (FLAG_SERIAL_PORT_ACTIVE)
				  {
					Output_Unit_ID();
				  }
				#endif

				// TODO: Remove, Test Code only
				/*
				#ifdef _CONFIGURE_DIAG_HIST
					Diag_Hist_Bat_Record(BAT_CURRENT, BAT_Last_Volt);
					Diag_Hist_Bat_Record(BAT_DAY_ONE, 0x4ff);
					Diag_Hist_Bat_Record(BAT_YEAR_ONE, 0x355);
				#endif
				*/
				
				FLAG_LANGUAGE_SEL = 1; // 1 minutes pass, disable language selection.
				
				bOneMinuteTic ++;
				OneMinuteCount = b1SecTimer + TIMER_ONE_MINUTE_COUNT;

				#ifdef _CONFIGURE_ESC_LIGHT_CYCLING
					// Test only, Cycle the Escape Light
					if(--ESC_Light_Timer == 0)
					{
						if(FLAG_ESC_LIGHT_ON)
						{
							// Use unused Tamper Flag to switch
							//  keep in Active Mode
							FLAG_TAMPER_ACTIVE = 1;

							FLAG_ESC_LIGHT_ON = 0;
							ESC_Light_Timer = ESC_LIGHT_OFF;
						}
						else
						{
							// Use unused Tamper Flag to switch
							//  keep in Active Mode
							FLAG_TAMPER_ACTIVE = 1;

							FLAG_ESC_LIGHT_ON = 1;
							ESC_Light_Timer = ESC_LIGHT_ON;
						}
					}
				#endif

              
				if(bOneMinuteTic == 5)
				{
				  // 5 Minute Tasks
				}

				#ifdef _CONFIGURE_ACCEL_LIGHT_TEST
					// Record every minuite, therefore, 1 Day = 24 minutes
					#ifdef _CONFIGURE_LIGHT_SENSOR
						Light_Record();
					#endif
				#else

					if(--b1HourCount == 0)
					 {
						 // Hourly tasks
						 b1HourCount = TIMER_ONE_HOUR_COUNT;
						 #ifdef _CONFIGURE_LIGHT_SENSOR
							 Light_Record();
						 #endif
					 }

				#endif

				// Update and Test life counter every minute
				LFE_Check() ;

				#ifdef _CONFIGURE_DIGITAL_DISPLAY

					if(!FLAG_MODE_ACTIVE)
					{
						// At PPM Detect Threshold start enabling display when in Low Power Mode
						// At Peak Memory start enabling display when in Low Power Mode
						if( (wPPM > 30) || (FLAG_CO_PEAK_MEMORY == 1 ) )
						{
							// Once a minute in Low Power Mode (switch to Active Mode)
							// to enable display for few seconds (use FLAG_POWERUP_DELAY_ACTIVE for now)
							Main_Init_Power_Dly_Timer();

						}
					}
				#endif
				           // add during low battery close lcd
					if(!FLAG_MODE_ACTIVE)
					{
						// At PPM Detect Threshold start enabling display when in Low Power Mode
						// At Peak Memory start enabling display when in Low Power Mode
						if( (wPPM > 30) || (FLAG_CO_PEAK_MEMORY == 1 ) )
						{
							// Once a minute in Low Power Mode (switch to Active Mode)
							// to enable display for few seconds (use FLAG_POWERUP_DELAY_ACTIVE for now)
							Main_Init_Power_Dly_Timer();

						}
					}
			}

			// If a command has been received, send to command processor
			if (FLAG_SERIAL_PORT_ACTIVE && FLAG_RX_COMMAND_RCVD )
			//if (FLAG_SERIAL_PORT_ACTIVE ||  FLAG_RX_COMMAND_RCVD )
			{
				FLAG_RX_COMMAND_RCVD = 0 ;
				Command_Processor(receive_buffer) ;
			}

                
			// ***************************************************************************
			// ***************************************************************************
			// 			All tasks have finished.  Set wakeup time & then sleep
			// ***************************************************************************
			// ***************************************************************************
			#ifndef _CONFIGURE_NO_SLEEP
			{
				#ifdef	_CONFIGURE_SIMULATE_SLEEP
				{
					#ifdef	_CONFIGURE_DEBUG_PIN_ENABLED
						//Main_TP_Off();
					#endif

					if (FLAG_POWERUP_DELAY_ACTIVE)
					{
						PowerupAwakeTimer++ ;

						if ((PowerupAwakeTimer == 8) && (!FLAG_LOW_POWER_ACTIVE_CYCLE) )
						{
							// Turn off Power On Beep Here
							PORT_HORN_DISABLE = 1;
						}

					#ifdef	_CONFIGURE_VOICE
						if (PowerupAwakeTimer == 100)
						{
							// Only play Intro voice on powerup
							if (FLAG_INHIBIT_INTRO_VOICE == 0)
							{
								VCE_Play(VCE_PUSH_TEST_BUTTON) ;                                                    
							}
						}
					#endif

						if (PowerupAwakeTimer >= MAIN_PWR_DELAY_TIME)
						{
							FLAG_POWERUP_DELAY_ACTIVE = 0 ;

							if (!FLAG_INHIBIT_REV_MSG)
								Main_Check_Revision();

						}
					}

					if(!FLAG_MODE_ACTIVE)
					{
						// *************************************************************
						// *************** Simulated Low Power Mode ********************
						// *************************************************************
						// *** We're in Simulated Low Power Sleep Mode
						// Since the debugger can't debug through sleep, for testing
						// we try to simulate a sleep instruction to the analog die.

						#ifdef	_CONFIGURE_DEBUG_PIN_ENABLED
							//Main_TP_Toggle();
						#endif

						// Sleep Simulation Timer Code here
						do
						{
							CLRWDT();

						}while(PIR1bits.TMR1IF == 0);
							// *************************************************************
							// *************** END Simulated Low Power Mode ****************
					}
					else
					{
						// *************************************************************
						// **************** Simulated Active Mode **********************
						// *************************************************************
						// *** We're in Simulated Active Mode

						do
						{
							CLRWDT();

						}while(PIR1bits.TMR1IF == 0);

						// *************************************************************
						// **************** END Simulated Active Mode ******************
					}
				}
				#else	// End of <_CONFIGURE_SIMULATE_SLEEP>
				{
					//************************************************************************
					//************************************************************************
					//		This is Low Power Sleep Mode
					//************************************************************************
					//************************************************************************

					if (FLAG_POWERUP_DELAY_ACTIVE == 1)
					{

						#ifdef	_CONFIGURE_DEBUG_PIN_ENABLED
							Main_TP_On();
						#endif

						PowerupAwakeTimer++ ;
						if ((PowerupAwakeTimer == 2) && (!FLAG_LOW_POWER_ACTIVE_CYCLE) )
						{
							// Turn off Power On Beep Here
							PORT_HORN_DISABLE = 1;
						}

						#ifdef	_CONFIGURE_VOICE
							if (PowerupAwakeTimer == 10)
							{
								// Only play Intro voice on powerup
								if (FLAG_INHIBIT_INTRO_VOICE == 0)
								{
									VCE_Play(VCE_PUSH_TEST_BUTTON) ;
								}
							}

						#endif

						if (PowerupAwakeTimer > MAIN_PWR_DELAY_TIME)
						{
							FLAG_POWERUP_DELAY_ACTIVE = 0 ;
							FLAG_LOW_POWER_ACTIVE_CYCLE = 0;

							if (!FLAG_INHIBIT_REV_MSG)
							{
								Main_Check_Revision();
							}
						}

						// Use Active Mode ms Tic Timer during Power Up delay
						do
						{
							//WDT will expire if Timer 1 hangs (xtal short)
							//CLRWDT();

						}while(PIR1bits.TMR1IF == 0);

						#ifdef	_CONFIGURE_DEBUG_PIN_ENABLED
							Main_TP_Off();
						#endif
					}
					else
					{
						if(FLAG_MODE_ACTIVE)
						{
							// *************************************************************
							// ****************** Normal Active Mode ***********************
							// *************************************************************
							// *** We're in Normal Operation Active Mode

							#ifdef _CONFIGURE_DEBUG_TASK_TIMING
								#ifdef _CONFIGURE_DEBUG_PIN_ENABLED
									Main_TP_Off();      // TP Off during Sleep Time
								#endif
							#endif

							// Wait for Active Mode Tic to expire Here
							do
							{
								//WDT will expire if Timer 1 hangs (xtal short)

							}while(PIR1bits.TMR1IF == 0);
							// *************************************************************
							// **************** END Normal Active Mode *********************
						}
						else
						{
							// *************************************************************
							// ******************** Low Power Mode *************************
							// *************************************************************
							// *** We're in Normal Operation Low Power Sleep Mode

							#ifdef _CONFIGURE_DEBUG_TASK_TIMING
							 #ifdef _CONFIGURE_DEBUG_PIN_ENABLED
								Main_TP_Off();      // TP Off during Sleep Time
							 #endif
							#endif

							// Turn off Brown Out (saves current)
							BORCONbits.SBOREN = 0;
							// Insure Turned off FVR to save power
							FVRCONbits.FVREN = 0;

							// CLRWDT();       // WDT is cleared at Sleep automatically

							// -------------------------------------------------------------------------
							GOTOSLEEP
							// -------------------------------------------------------------------------

							// CLRWDT();        // WDT is cleared at Wake automatically

							 // Turn on Brown Out
							BORCONbits.SBOREN = 1;
							FVRCONbits.FVREN = 1;

							// Can Determine source of Wake here
							// 1. Sleep Timer
							// 2. WDT Timeout (nTO bit == 0)
							if(STATUSbits.nTO == 0)
							{
								HORN_ON                 // Turn on Horn
								while(1);               // Wait here for WDT reset
							}


							// 3. Button Press (RB3 pin and RB6)
							// Need to filter transients on button inputs (ESD)
							// As a short transient will wake and turn on the display
							// briefly.
							if(IOCBFbits.IOCBF3 == 1)
							{
							   delay_ms(60);
							   IOCBFbits.IOCBF3 = 0;
							   if(PORT_TEST_BTN == 1)
							   {
									FLAG_BUTTON_EDGE_DETECT = 1;
							   }
							   INTCONbits.IOCIF = 0;

							}
							if(IOCBFbits.IOCBF6 == 1)
							{
							   delay_ms(60);
							   IOCBFbits.IOCBF6 = 0;
							   if(PORT_PEAK_BTN == 1)
							   {
									FLAG_PEAK_BUTTON_EDGE_DETECT = 1;
							   }
							   INTCONbits.IOCIF = 0;

							}
							// *************************************************************
							// ******************** END Low Power Mode *********************
						}
					}
				}
				#endif	// end of <_CONFIGURE_SIMULATE_SLEEP>
			}
			#else	// (_CONFIGURE_NO_SLEEP - These are Configured Non-Sleep Modes)
			{
				// *************************************************************
				// ******************* NON-Sleep Modes *************************
				// *************************************************************

				if (FLAG_POWERUP_DELAY_ACTIVE)
				{
					PowerupAwakeTimer++ ;

					if ((PowerupAwakeTimer == 2) && (!FLAG_LOW_POWER_ACTIVE_CYCLE) )
					{
						// Turn off Power On Beep Here
						PORT_HORN_DISABLE = 1;
					}

					#ifdef	_CONFIGURE_VOICE
						if (PowerupAwakeTimer == 10)
						{
							// Only play Intro voice on powerup
							if (FLAG_INHIBIT_INTRO_VOICE == 0)
							{
								VCE_Play(VCE_PUSH_TEST_BUTTON) ;
							}
						}

					#endif

					if (PowerupAwakeTimer >= MAIN_PWR_DELAY_TIME)
					{
						FLAG_POWERUP_DELAY_ACTIVE = 0 ;
						PowerupAwakeTimer = 0 ;

						if (!FLAG_INHIBIT_REV_MSG)
							Main_Check_Revision();

					}
				}

				#ifdef	_CONFIGURE_DEBUG_PIN_ENABLED
					//Main_TP_Toggle();
				#endif

				if(!FLAG_MODE_ACTIVE)
				{
					// *************************************************************
					// **************** Non-Sleep Standby Mode *********************
					// *************************************************************
					// *** We're in Standby non-sleep

					// Wait for Low Power Tic to expire Here
					do
					{
						CLRWDT();

					}while(PIR1bits.TMR1IF == 0);


					// *************************************************************
					// ************** END Non-Sleep Standby Mode *******************
				}
				else
				{
					// *************************************************************
					// **************** Non-Sleep Active Mode *********************
					// *************************************************************
					// *** We're in Active non-sleep

					// Finish Active Mode TIC time here
					do
					{
						CLRWDT();

					}while(PIR1bits.TMR1IF == 0);


					// **************** END Non-Sleep Active Mode ******************
					// *************************************************************
				}
			}
			#endif	// <_CONFIGURE_NO_SLEEP>


			#ifdef _CONFIGURE_DEBUG_TASK_TIMING
			 #ifdef _CONFIGURE_DEBUG_PIN_ENABLED
				Main_TP_On();      // TP On during Task Time
			 #endif
			#endif


			// Here we have completed Tic times (either by wake from sleep or simulated sleep timeout
			// 1. Reset the Sleep Timer
			// 2. Check for Low Power Mode
			// 3. WDT control if needed

			// Test for need to Change to Active or Standby
			Main_Check_Low_Power_Logic() ;
			// Reset Sleep Timer for next TIC depending upon Mode
			Main_Reset_Sleep_Timer();


			#ifdef	_CONFIGURE_WATCHDOG_TIMER_OFF
				WDTCONbits.SWDTEN = 0;
			#else
				WDTCON = MAIN_WDT_NORMAL_TIMEOUT;
				WDTCONbits.SWDTEN = 1;
			#endif
		}
    }	//end of Engine Task Process/Sleep Loop
}	// end of Main


void Main_Init_Powerup(void)
{
     // Test for Soft Reset (reset instruction)
    if(PCONbits.nRI ==1)
    {
        // Power Reset, clear accumulators & non-reset flags
        COAlarm_Init();

        // Reset all the Non-reset Flags at Power Reset
        Flags_NR.ALL = 0;

        // Init Peak CO memory at power reset
        wPeakPPM = 0;

        #ifdef	_CONFIGURE_ESCAPE_LIGHT
            // Set Default Nightlight Pulse
            Esc_Init();
        #endif

        #ifdef _CONFIGURE_LIGHT_SENSOR
            // Initialize the light data Structure on Power Reset
            Light_Data_Init();
        #endif

    }
    else
    {
        FLAG_INHIBIT_INTRO_VOICE = 1 ;
    }
    
   // Init Power Reset bits
    PCONbits.nBOR = 1;
    PCONbits.nRMCLR = 1;
    PCONbits.STKOVF = 0;
    PCONbits.STKUNF = 0;

}

void Main_Init_Power_Dly_Timer(void)
{
    FLAG_POWERUP_DELAY_ACTIVE = 1 ;
    FLAG_LOW_POWER_ACTIVE_CYCLE = 1 ;	// Indicate that this is not Reset cycle
    PowerupAwakeTimer = 0 ;			// reset the Delay Timer too

    // Insure intro voice is not played
    FLAG_INHIBIT_INTRO_VOICE = 1;
}



//*************************************************************************
void MAIN_Reset_Task(UCHAR taskID)
{
	// To reset the task, set the time to the current time and
	// set the interval to 0.  This will cause the task to execute
	// on the next tic.
	TaskTable[taskID]->TaskTime = TicTimer ;
	TaskTable[taskID]->TaskInterval = 0 ;
}


//*************************************************************************
// Test Task Only
// 
unsigned int DebugTask(void)
{
	
	
	
	
    return	Make_Return_Value(60000) ;
}

//****************************************************************
// Output Revision if Configured
void Main_Check_Revision(void)
{
	// Only let this occur at Power On
	FLAG_INHIBIT_REV_MSG = 1;


	#ifdef _CONFIGURE_OUTPUT_REVISION
		// To track interim FW version changes during development
		// append letter of primary version number
	if (FLAG_SERIAL_PORT_ACTIVE)
	{

		//Test Only
	//        SER_Send_String("Sta-");
	//        SER_Send_Byte(STATUS_Save) ;
	//        SER_Send_Prompt() ;
	//        SER_Send_String("Rst-");
	//        SER_Send_Byte(PCON_Save) ;
	//        SER_Send_Prompt() ;

//		#ifdef	_CONFIGURE_2547
//			SER_Send_String("Model 2547 ") ;
//		#endif
//		#ifdef	_CONFIGURE_2548
//			SER_Send_String("Model 2548 ") ;
//		#endif
//		#ifdef	_CONFIGURE_2549
//			SER_Send_String("Model 2549 ") ;
//		#endif
//		#ifdef	_CONFIGURE_2551
//			SER_Send_String("Model 2551 ") ;
//		#endif
//		#ifdef	_CONFIGURE_2565
//			SER_Send_String("Model 2565 ") ;
//		#endif
////
//		SER_Send_Prompt() ;
//		SER_Send_String(REV_STRING) ;
//		SER_Send_Prompt() ;

//		#ifdef _CONFIGURE_ID_MANAGEMENT
//		    Output_Unit_ID();
//		#endif

	}
		
	#endif
	
}

#ifdef _CONFIGURE_ID_MANAGEMENT
    void Output_Unit_ID(void)
    {
        //SER_Send_Prompt() ;
//        SER_Send_String("I");
//        SER_Send_Int('D', ( (SYS_RamData.Unit_ID_High << 8) + SYS_RamData.Unit_ID_Low  ), 16) ;
//        SER_Send_Prompt() ;
    }
#endif



#define TIMER0_16_MHZ   0b10000001  // Timer0, clock=fosc/4 (16 Mhz), prescaler 001 = divide by 4
#define OSC_16_MHZ      0b01111010  // System Clock - 16 MHZ, Internal Osc (0x0111 1010)

#define TIMER0_INT_2MS  0b10000111  // Timer0, clock=fosc/4 (16 Mhz/4 = 4Mhz), prescaler 111 = divide by 256
#define TIMER0_1MS      (256-16)    // 16 clocks at 64usec = 1 ms
#define TIMER0_2MS      (256-32)    // 32 clocks at 64usec = 2 ms
//*************************************************************************
void Main_Initialize(void)
{

    // Init the Oscillator and Timer0 Prescaler
    // Timer 0 clock is 16MHz/4 = 4MHZ  with /4 prescaler = 1 MHz
    // (i.e a 1 usec clock.)
    //
    OPTION_REG = TIMER0_16_MHZ;
    OSCCON = OSC_16_MHZ;

    /*
    // Port A Initialization
     * Inputs: Bit 0-Battery Test Voltage(AN0)
     *         Bit 1-CO Sensor Voltage (AN1)
     *         Bit 3 - AC Detect (AN3)
     * Outputs:
     *         Bit 2, 4-7 (set to 0)
     *
    */

    ANSELA = 0b00001011;
    TRISA  = 0b00001011;    // Inputs: Bit 0,1,3 -Analog input
    PORTA  = 0b00000000;	
    PORT_LCD_PWR_CTRL = 1;

    /*
    // Port B Initialization
     * Inputs: Bit 3 - Test Button
     *         Bit 6 - Peak Button
     *         Bit 1 - Light Sensor (Green Led)
     *
     * Outputs:Bit 7 - Horn Disable (set to 1)
     *         Bit 6 - Voice Clock (when voice is active)
     *         Bit 0 - Voice Data  (when voice is active)
     *         Bit 0 - Escape Light when Escape Light active
     *         Bit 1 - Green Led
     *         Bit 2, 4,5 - Digit Selects
     *
     *
     */
    #ifdef _CONFIGURE_VOICE

        // Since sharing Peak Btn and Voice Clk, Init Peak Button/Voice pin as Output Low
        // and flip to Input when reading the peak pin (as long as voice is not active)
        // Bit 6 is initialized as Output/Low
        // Bit 0 is initialized as Output/Low
        ANSELB = 0b00000000;
        TRISB  = 0b00001000;
        PORTB  = 0b10000000;
    #else

        #ifdef _CONFIGURE_ESCAPE_LIGHT
            // Bit 6 is initialized as Input (Peak Button)
            // Bit 0 is initialized as Output/High (Nightlight Current Disabled)
            ANSELB = 0b00000000;
            TRISB  = 0b01001000;
            PORTB  = 0b10000001;


        #else
            // Bit 6 is initialized as Input (Peak Button)
            // Bit 0 is initialized as Output/Low
            ANSELB = 0b00000000;
            TRISB  = 0b01001000;
            PORTB  = 0b10000000;
        #endif

    #endif


    /*
    // Port C Initialization
     *
     * Special Function: Bit 6 - TX
     *                   Bit 7 - RX  (Init as input to look for serial port)
     *                   Bit 0 - T1OSC Out
     *                   Bit 1 - T1OSC In
     *
     * Inputs;
     *                   Bit 6 - Input (Escape Light Bat Off or Serial Tx)

     *
     * Outputs: Bit 2-5  (Set to 0)
    */
    #ifdef _CONFIGURE_ESCAPE_LIGHT
        TRISC  = 0b11000011;
        PORTC  = 0b01000000;
    #else
        TRISC  = 0b10000011;
        PORTC  = 0b00000000;
    #endif
                                

    // Configure Timer 1 for TIOSC.
    T1CON = 0b10001101;     // Timer 1 On, T1OSCEN enabled (XTAL on T1OSC pins)
    T1GCON = 0b00000000;    // Timer 1 Gating Off



#ifndef _CONFIGURE_SIMULATOR_DEBUG
    Main_Wait_T1Osc_Stable();
#endif


    FLAG_MODE_ACTIVE = 1;
    Main_Reset_Sleep_Timer();

#ifdef	_CONFIGURE_PPM_FILTER
    FLAG_FILTER_PPM_ENABLE = 1;
#endif

    CAL_Init() ;		// Read Cal Status &Init CO Cal Status Flags
    if (!FLAG_CALIBRATION_COMPLETE)
    {
        FLAG_INHIBIT_INTRO_VOICE = 1 ;
    }

    FLAG_POWERUP_DELAY_ACTIVE = 1 ;     // For Intro Voice Timing
	
    Initialize_Engine() ;

    // This forces Initialization into Active Mode.
    FLAG_MODE_ACTIVE = 0 ;
    Main_Set_Active() ;
    Main_Check_Low_Power_Logic() ;

#ifdef _CONFIGURE_DIGITAL_DISPLAY
    // Setup Timer 0 display refresh interrupts
    // Increase prescaler to 256 (4MHz/256= 15625 KHz)
    // 64 uSec clock set TMR) to (256-32) to Interrupt every ms.
    INTCONbits.GIE = 0;
    INTCONbits.TMR0IF = 0;
    INTCONbits.TMR0IE = 0;
    
    // Init Timer0
    TMR0 = TIMER0_2MS;
    OPTION_REG = TIMER0_INT_2MS;

    // Enable Ints
    INTCONbits.TMR0IE = 1;

    INTCONbits.GIE = 1;
#endif
    // Configure IOC for rising edge on RB3 to sense the test pin.
    // and RB6 for  the Peak Button
    IOCBP = 0b01001000;

    // Set the IOCIE bit in INTCON to enable the IOC function for test button.
    INTCONbits.IOCIE = 1;

	/*Load the battery scale, to calculate battery usage in battery.c*/
	BAT_Scale = Memory_Byte_Read(0x07);
	BAT_Scale = ( BAT_Scale << 8 ) | Memory_Byte_Read(0x06);
	

#ifdef _CONFIGURE_USEC_TIMERS
    // Test Code Only for 8-bit CCI interface timers
    // Create 2 timers  (1uSec and 16uSec Timers)
    // FOsc/4 = 4Mhz = 250 ns clock

    // Set Prescaler to 1:4, Post scaler 1:1, Timer On = 1usec
    T2CON = 0x05;
    PIE1bits.TMR2IE = 0;    // Not using Ints

    // Set Prescaler to 1:16, Post scaler 1:1, Timer On = 16usec
    T4CON = 0x0C;
    PIE3bits.TMR4IE = 0;    // Not using Ints

#endif


}


#ifdef _CONFIGURE_USEC_TIMERS
// Sets the integer that is passed in to the current value of the timer.
void Main_Get_1_Microsecond_Timer(UCHAR *timer_var)
{
    // For 8 bit timer we can probably can read while running
    // Turn timer off because integer read is not atomic.
    T2CONbits.TMR2ON = 0 ;

//    *timer_var = *((UINT*)&TMR2) ;
    *timer_var = ((UINT)TMR2) ;

    T2CONbits.TMR2ON = 1 ;

}

void Main_Get_16_Microsecond_Timer(UCHAR *timer_var)
{
    // For 8 bit timer we can probably can read while running
    // Turn timer off because integer read is not atomic.
    T4CONbits.TMR4ON = 0 ;

//    *timer_var = *((UINT*)&TMR4) ;
    *timer_var = ((UINT)TMR4) ;

    T4CONbits.TMR4ON = 1 ;

}

// Test Code only for Usec timers
void Timer_Test_Usecs(void)
{
    UCHAR start, current;

    // Test 16 user timer to time 1ms
    Main_Get_16_Microsecond_Timer(&start) ;
    Main_TP_On();
    do
    {
        Main_Get_16_Microsecond_Timer(&current) ;

    } while ( current - start <= 63) ;    // 63 = ~1008 microseconds
    Main_TP_Off();

    // Delay between Pulses
    delay_us(100);

    // Test 1 user timer to time 100uSecs
    Main_Get_1_Microsecond_Timer(&start) ;
    Main_TP_On();
    do
    {
        Main_Get_1_Microsecond_Timer(&current) ;

    } while ( current - start <= 100) ;    // microseconds
    Main_TP_Off();

}



#endif


/******************************************************************************
 * 
 * Clears all GPR user data memory. (Uses Linear Addressing)
 * 
*/
/*
#define LINEAR_START 		0x2000
#define LINEAR_END		0x21F0  	//(Banks 0-6, GPR, 496 bytes)

void Clear_Linear_Ram(void)
{
    UCHAR *dataptr;
    dataptr = (UCHAR *) LINEAR_START;

    for(UINT i = LINEAR_START; i != LINEAR_END; i++)
    {
         *dataptr = 0;
         dataptr++;
    }
}
*/


/******************************************************************************
 * 
;
;  Clears user data memory. Bank 0 0x070 - 0x077
; 
 * 
*/
/*
#define COMMON_START 	0x070
#define COMMON_END	0x07F

void Clear_Common_Ram(void)
{
   for(UCHAR* dataptr = (UCHAR *)COMMON_START; dataptr != (UCHAR*)COMMON_END; dataptr++)
   {
         *dataptr = 0;
   }
}
*/

/*********************************************************************
; This function waits until T1 Osc stabilizes.  Circuit tolerances can
; effect the startup time.  Long startups could cause the WDT to fire
; at startup causing a double beep.
;
*/
void Main_Wait_T1Osc_Stable(void)
{
   CLRWDT();
   HORN_ON                      // Horn On

   while(TMR1L & 0x04);         // Wait for bit 2 low
   CLRWDT();

   for (UCHAR loop_ctr = 20; loop_ctr != 0; loop_ctr--)
   {
      while(!(TMR1L & 0x04));   // Wait for bit 2 high

      // Clear Timer0 to time the Timer1 rollover (bit 2).
      TMR0 = 0;
      while(TMR1L & 0x04);      // Wait for bit 2 low

      // TMR0 contains the number of clock cycles in eight
      // TMR1 LSB.  If greater than 100 usec, continue

      //100 clocks @ 1 usec (2 MHz or fosc/4)/2, with fosc = 8 MHz
      //100 clocks @ 1 usec (4 MHz or fosc/4)/4, with fosc = 16 MHz
      if(TMR0 < 100)
      {
        CLRWDT();
        loop_ctr = 20;          // Remain in Loop with Horn ON
      }
   }

   HORN_OFF       // Horn Off

}

void Main_Reset_Sleep_Timer(void)
{
    /*
    Stop the timer and configure it to timeout in
    100/1000 ms.  Alarm will wake up from sleep when the timer
    interrupts, thus the enable bit and the peripheral int
    enable must both be set.
    */

    /*
     Timer must be stopped to ensure that the value is not
     corrupted during the write.
    */
    T1CONbits.TMR1ON = 0;   // Timer1 Off

    if(FLAG_MODE_ACTIVE)
    {
        // In active mode we do not Sleep (so we don't need Timer 1 Ints)
        PIE1bits.TMR1IE = 0;
        PIR1bits.TMR1IF = 0;
        INTCONbits.PEIE = 0;
        
#ifdef _CONFIGURE_DIGITAL_DISPLAY
        // Enable Display Refresh Ints
        INTCONbits.TMR0IE = 1;
        INTCONbits.GIE = 1;
#endif
        TMR1 = TIMER1_INIT_VALUE_10MS ;
    }
    else
    {
        // Sleep Timing set Timer 1 Ints to Wake from Sleep
        PIE1bits.TMR1IE = 1;
        PIR1bits.TMR1IF = 0;
        INTCONbits.PEIE = 1;
        
#ifdef _CONFIGURE_DIGITAL_DISPLAY
        // Disable Display Refresh Ints
        INTCONbits.TMR0IE = 0;
        // Global Ints must be disabled in sleep Mode
#endif
        INTCONbits.GIE = 0;

        TMR1 = TIMER1_INIT_VALUE_1000MS ;
    }

    T1CONbits.TMR1ON = 1;    // Timer1 On

}

//*****************************************************************************
unsigned int Make_Return_Value(unsigned int milliseconds)
{
    if(milliseconds > CurrentMsPerTic)
    {
	return (milliseconds/CurrentMsPerTic) ;
    }
    else
    {
        return (1);
    }
}

//*****************************************************************************
// This function calculates the number of tics in the current mode before the
// task is scheduled to execute.
UINT Main_Calc_Current_Tics(UCHAR Task_ID)
{
	UINT tics ;	
	//
	// Time in tics = Interval - (tics - Last time)
	// ==>Time in tics = Interval - tics + Last time
	//
	tics = TaskTable[Task_ID]->TaskInterval ;
	tics -= TicTimer ;
	tics += TaskTable[Task_ID]->TaskTime ;
	return tics ;	
}

// *****************************************************************************
//
// 
void Main_Recalc_Int_Active(UCHAR Task_ID)
{
	if (TaskTable[Task_ID]->TaskInterval != 0)
	{
		UINT tics = Main_Calc_Current_Tics(Task_ID) ;
		
		// Multiply the number of tics by 100 to get the number of tics
		// in active mode and update interval.
		TaskTable[Task_ID]->TaskInterval = tics * 100 ;
		
		// Change last execution time of task to current time.
		TaskTable[Task_ID]->TaskTime = TicTimer ;	
	}
}


// *****************************************************************************
//
// 
void Main_Recalc_Int_Standby(UCHAR Task_ID)
{
	// In certain cases a task may have reset another task.  If this happens,
	// the TaskInterval will be zero.  The task recalculations will break if
	// the TaskInterval is zero.
	if (TaskTable[Task_ID]->TaskInterval != 0)
	{
		UINT tics = Main_Calc_Current_Tics(Task_ID) ;
		
		// Divide the number of tics by 100 to get the number of tics
		// in standby mode and update interval.
		TaskTable[Task_ID]->TaskInterval = 
		 tics / (MAIN_TIC_INTERVAL_LOWPOWER/MAIN_TIC_INTERVAL_ACTIVE) ;
		
		// Add one to make up for truncation in division
		TaskTable[Task_ID]->TaskInterval++ ;
		
		// Change last execution time of task to current time.
		TaskTable[Task_ID]->TaskTime = TicTimer ;
	}	
}


// ****************************************************************
// Call this function to transition from low power to active mode.
//
void Main_Set_Active(void)
{
    // If we're already in active mode just return.
    if (FLAG_MODE_ACTIVE == 0)
    {
        //
        // This flag notifies all interested tasks that they are operating
        // in active mode.
        //
        FLAG_MODE_ACTIVE = 1 ;

        // In active mode we do not wake from Sleep
        PIE1bits.TMR1IE = 0;
        PIR1bits.TMR1IF = 0;
        INTCONbits.PEIE = 0;

        // Enable Display Refresh Ints
        INTCONbits.TMR0IE = 1;
        INTCONbits.GIE = 1;

        #ifdef _CONFIGURE_ESCAPE_LIGHT
            // Set Escape Light I/O for Light Off
            PORT_ESCAPE_LIGHT = 1;
            ESCAPE_LIGHT_OUTPUT

        #endif
        //Payload_Output();

        //
        // Set the mode.
        //
        CurrentMode = MODE_ACTIVE ;
        CurrentMsPerTic = MS_PER_TIC_ACTIVE ;

        MAIN_Reset_Task(TASKID_PTT_TASK) ;
        MAIN_Reset_Task(TASKID_SOUND_TASK) ;
//        MAIN_Reset_Task(TASKID_DISP_UPDATE) ;
        Main_Recalc_Int_Active(TASKID_COALARM) ;
        Main_Recalc_Int_Active(TASKID_TROUBLE_MANAGER) ;
        Main_Recalc_Int_Active(TASKID_COMEASURE) ;
        Main_Recalc_Int_Active(TASKID_GLED_MANAGER) ;
        Main_Recalc_Int_Active(TASKID_BATTERY_TEST) ;

//        Display_Set_Intensity(LED_DISP_BRIGHT) ;

    }
}

// ****************************************************************
// Call this function to transition from active to low power mode.
//
void Main_Set_Low_Power(void)
{
        // Test Only
        // return;

	// If we're already in low power mode just return.
	if (FLAG_MODE_ACTIVE == 1)
        {
            //
            // This flag notifies all interested tasks that they are not operating
            // in active mode.
            //
            FLAG_MODE_ACTIVE = 0 ;

            #ifdef _CONFIGURE_VOICE
                // Insure Peak Button is set to Input
                TRISBbits.TRISB6 = 1;
            #endif

            // Sleep Mode - Ints Off
            PIE1bits.TMR1IE = 1;
            PIR1bits.TMR1IF = 0;
            INTCONbits.PEIE = 1;

            // Disable Display Refresh Ints
            INTCONbits.TMR0IE = 0;
            INTCONbits.GIE = 0;
            
//	   PORT_LCD_PWR_CTRL = 0;
//	   FLAG_LCD_POWER_ON = 0;
			
            //
            // Set the mode.
            //
            CurrentMode = MODE_LOW_POWER ;
            CurrentMsPerTic = MAIN_TIC_INTERVAL_LOWPOWER ;

//            Display_Off();
			
	   PORT_LCD_PWR_CTRL = 0;
	   FLAG_LCD_POWER_ON = 0;

            #ifdef _CONFIGURE_ESCAPE_LIGHT
                // Set Escape Light I/O to High Impedance when in LP Mode
                ESCAPE_LIGHT_INPUT

            #endif

            //
            // Tasks need to be reset here so that they start fresh
            // with the new wakeup time.
            //
            Main_Recalc_Int_Standby(TASKID_COMEASURE) ;
                
            #ifdef _CONFIGURE_DIGITAL_DISPLAY
                MAIN_Reset_Task(TASKID_DISP_UPDATE) ;
            #endif

            Main_Recalc_Int_Standby(TASKID_BATTERY_TEST) ;
            Main_Recalc_Int_Standby(TASKID_COALARM) ;
            Main_Recalc_Int_Standby(TASKID_TROUBLE_MANAGER) ;
            Main_Recalc_Int_Standby(TASKID_GLED_MANAGER) ;
            Main_Recalc_Int_Standby(TASKID_LIGHT_TASK) ;

            // probably not required here, but left in since it won't hurt anything
            SND_Reset_Task() ;

        }
}

// ****************************************************************
// This function performs the logic to determine if the firmware needs
// to be in low power or active.
//
void Main_Check_Low_Power_Logic(void)
{
 #ifdef	_CONFIGURE_SERIAL_WAKESLEEP_DEBUG
	char char2send ;
 #endif
 
 	// Set Active Mode if any Active Flags are set
 	// Includes:
 	// FLAG_PTT_ACTIVE, FLAG_BUTTON_EDGE_DETECT, FLAG_POWERUP_DELAY_ACTIVE, 
 	// FLAG_VOICE_SEND, FLAG_CO_ALARM_CONDITION, FLAG_SERIAL_PORT_ACTIVE,
 	// FLAG_CALIBRATION_NONE, FLAG_CALIBRATION_PARTIAL, FLAG_CALIBRATION_UNTESTED,
 	// FLAG_SOUND_NOT_IDLE, FLAG_BUTTON_NOT_IDLE, FLAG_CHIRPS_ACTIVE
 	// FLAG_FAULT_FATAL


 	if (Flags_Active1.ALL != 0)
 	{
                //GLED_On();
 		Main_Set_Active() ;

 	}
 	else if (Flags_Active2.ALL != 0)
 	{
                //GLED_On();
  		Main_Set_Active() ;
  	}
 	else if (Flags_Active3.ALL != 0)
 	{
                //GLED_On();
 		Main_Set_Active() ;
  	}
	else
	{
		GLED_Off();
		Main_Set_Low_Power() ;
	}
	
}


//****************************************************************
void Initialize_Engine(void)
{
	UINT i ;
	
	// Since TaskTable is made of integer pointers, divide by two
	// to get actual number of structs to init.
	for (i=0 ; i<sizeof(TaskTable)/2; i++)
	{
		TaskTable[i]->TaskInterval = 0 ;
		TaskTable[i]->TaskTime = 0 ;
	}
	
	SoundTaskInfo.funcptr = SounderTask ;
	ButtonTaskInfo.funcptr = BTN_Task ;
    PTT_TaskInfo.funcptr = PTT_Task ;
	BatteryTestInfo.funcptr = BatteryTest_Task ;
	DebugTaskInfo.funcptr = DebugTask ;
	AlarmPrioInfo.funcptr = AlarmPriorityTask ;
	COAlarmInfo.funcptr = COAlarmTask ;
	CalManagerInfo.funcptr = CAL_Manager ;
	COMeasureInfo.funcptr = COMeasureTask ;
    TroubleManagerInfo.funcptr = SND_Trouble_Manager ;
	GrnLedInfo.funcptr = GLED_Manager ;
	GrnLedBlinkInfo.funcptr = GLED_Blink ;
//	DispUpdateInfo.funcptr = Display_Update_Task ;
    PeakButtonInfo.funcptr = Peak_BTN_Task ;
    EscLightInfo.funcptr = Esc_Light_Process;
    LightInfo.funcptr = Light_Manager;
    VoiceInfo.funcptr = VCE_Process;
        
}

//**********************************************************************************
void Main_Delay(UINT microseconds)
{
    for(unsigned int i = 0; i!= microseconds; i++)
    {
        
    }

}


// Test point Code only  (TP49 = Test Point)
void Main_TP_On(void)
{

  #ifdef    _CONFIGURE_DEBUG_PIN_ENABLED
   PORT_DEBUG_TP = 1;          // Used as Test Point Signal
  #endif
}

void Main_TP_Off(void)
{
  #ifdef    _CONFIGURE_DEBUG_PIN_ENABLED
    PORT_DEBUG_TP = 0;          // Used as Test Point Signal
  #endif
}

void Main_TP_Toggle(void)
{
  #ifdef    _CONFIGURE_DEBUG_PIN_ENABLED
    LATB ^= PORT_DEBUG_TP;    // Used as Test Point Signal
  #endif
}

void Wifi_Module_enable(void)
{
	if(FLAG_LCD_POWER_ON == 0)
	{
		PORT_LCD_PWR_CTRL = 1;
		FLAG_LCD_POWER_ON = 1;
		FLAG_SERIAL_PORT_ACTIVE = 1;
		 TXSTAbits.TXEN = 1;
		 TXSTAbits.BRGH = 1;
		return;	// wait 1 sec for esp32 power on.
	}
	
}

void Payload_Output(void)
{
	UCHAR index = 0;
	UCHAR * payload_ptr = (UCHAR*) &Payload;
	static UCHAR delay_close_lcd_count = 0;
		
	Payload.status = FLAG_CALIBRATION_COMPLETE|(FLAG_AC_DETECT << 1)|( FLAG_PTT_FAULT << 2)|
				(FLAG_LOW_BATTERY << 3) | ((FLAG_CO_SENSOR_SHORT|FLAG_SENSOR_OPEN) << 4)| ((FLAG_EOL_MODE|FLAG_EOL_FATAL)<< 5) |
				(FLAG_MEMORY_FAULT << 6) | (FLAG_CO_ALARM_CONDITION <<7);
	
//		Payload.status = FLAG_CALIBRATION_COMPLETE|(FLAG_AC_DETECT << 1)|( (FLAG_PTT_FAULT&FLAG_BUTTON_NOT_IDLE) << 2)|
//				((FLAG_LOW_BATTERY & (Payload.button_press == 0x03))<< 3) | ((FLAG_CO_SENSOR_SHORT&FLAG_BUTTON_NOT_IDLE) << 4)| ((FLAG_EOL_MODE &(Payload.button_press == 0x04) )<< 5) |
//				((FLAG_MEMORY_FAULT&FLAG_BUTTON_NOT_IDLE) << 6) | (FLAG_CO_ALARM_CONDITION <<7);
	
//	if(Payload.status & 0xFE)
//	{
//		if(FLAG_LCD_POWER_ON == 0)
//		{
//			PORT_LCD_PWR_CTRL = 1;
//			FLAG_LCD_POWER_ON = 1;
//			FLAG_SERIAL_PORT_ACTIVE = 1;
//			 TXSTAbits.TXEN = 1;
//			 TXSTAbits.BRGH = 1;
//			return;	// wait 1 sec for esp32 power on.
//		}
//	}
//	else
//	{
//		PORT_LCD_PWR_CTRL = 0;
//		FLAG_LCD_POWER_ON = 0;
//		FLAG_SERIAL_PORT_ACTIVE = 0;
//		 TXSTAbits.TXEN = 0;
//		TXSTAbits.BRGH = 0;
//	}
	if(FLAG_CALIBRATION_COMPLETE ==0) //||(FLAG_CALIBRATION_NONE) )
	{
		Wifi_Module_enable();
	}
	//if(Payload.status & 0x02)		//PTT turn on lcd
	//if(FLAG_CALIBRATION_COMPLETE)
	else
	{
		if((FLAG_AC_DETECT == 1))
		{
			Wifi_Module_enable();
		}
		else if(FLAG_LCD_POWER_DELAY == 1)
		{
			if(FLAG_LCD_POWER_ON == 0)
			{
				PORT_LCD_PWR_CTRL = 1;
				FLAG_LCD_POWER_ON = 1;
				FLAG_SERIAL_PORT_ACTIVE = 1;
				 TXSTAbits.TXEN = 1;
				 TXSTAbits.BRGH = 1;
				//return;	// wait 1 sec for esp32 power on.
			}
			else
			{
				static UCHAR delay_count = 0;
				delay_count ++;
				if((FLAG_EOL_MODE == 1)||(FLAG_EOL_FATAL == 1))
				{
					if(delay_count >10)
					{
						delay_count = 0;
						PORT_LCD_PWR_CTRL = 0;
						FLAG_LCD_POWER_ON = 0;
						FLAG_SERIAL_PORT_ACTIVE = 0;
						 TXSTAbits.TXEN = 0;
						TXSTAbits.BRGH = 0;
						FLAG_LCD_POWER_DELAY = 0;
					}
				}
				else
				{
					if(delay_count >5)
					{
						delay_count = 0;
						PORT_LCD_PWR_CTRL = 0;
						FLAG_LCD_POWER_ON = 0;
						FLAG_SERIAL_PORT_ACTIVE = 0;
						 TXSTAbits.TXEN = 0;
						TXSTAbits.BRGH = 0;
						FLAG_LCD_POWER_DELAY = 0;
					}
				}

			}
		}
		else if( FLAG_PTT_ACTIVE == 1 )
		{
			Wifi_Module_enable();
		}
		else if((FLAG_CO_ALARM_ACTIVE) && (FLAG_LOW_BATTERY == 0))
		{
			Wifi_Module_enable();
		}
		else if((FLAG_EOL_MODE == 1)||(FLAG_EOL_FATAL == 1))
		{
			if(FLAG_LCD_POWER_ON == 0)
			{
				if(Payload.button_press == 0x04)
				{
					PORT_LCD_PWR_CTRL = 1;
					FLAG_LCD_POWER_ON = 1;
					FLAG_SERIAL_PORT_ACTIVE = 1;
					 TXSTAbits.TXEN = 1;
					 TXSTAbits.BRGH = 1;
				}
			}
			else
			{
				static UCHAR eol_delay_count = 0;
				eol_delay_count ++;
				if(eol_delay_count >10)
				{
					PORT_LCD_PWR_CTRL = 0;
					FLAG_LCD_POWER_ON = 0;
					FLAG_SERIAL_PORT_ACTIVE = 0;
					 TXSTAbits.TXEN = 0;
					TXSTAbits.BRGH = 0;
					eol_delay_count = 0;
				}
			}
		}
		else if((FLAG_BUTTON_NOT_IDLE == 1) && (FLAG_MEMORY_FAULT ) )
		{
			if(FLAG_LCD_POWER_ON == 0)
			{
				PORT_LCD_PWR_CTRL = 1;
				FLAG_LCD_POWER_ON = 1;
				FLAG_SERIAL_PORT_ACTIVE = 1;
				 TXSTAbits.TXEN = 1;
				 TXSTAbits.BRGH = 1;
			}
			else
			{
				static UCHAR delay_count = 0;
				delay_count ++;
				if(delay_count >5)
				{
					delay_count = 0;
					PORT_LCD_PWR_CTRL = 0;
					FLAG_LCD_POWER_ON = 0;
					FLAG_SERIAL_PORT_ACTIVE = 0;
					 TXSTAbits.TXEN = 0;
					TXSTAbits.BRGH = 0;
					FLAG_LCD_POWER_DELAY = 0;
				}
			}
		}
//		else	if ((FLAG_CO_SENSOR_SHORT == 1) && (FLAG_CO_ALARM_ACTIVE == 0))
		else	if ( ((FLAG_CO_SENSOR_SHORT == 1) ||( FLAG_SENSOR_OPEN == 1))&& (FLAG_CO_ALARM_ACTIVE == 0))
		{
			static UCHAR short_delay_count = 0;
			if(FLAG_LCD_POWER_ON == 0)
			{
				if(FLAG_BUTTON_NOT_IDLE ==1)
				{
					PORT_LCD_PWR_CTRL = 1;
					FLAG_LCD_POWER_ON = 1;
					FLAG_SERIAL_PORT_ACTIVE = 1;
					 TXSTAbits.TXEN = 1;
					 TXSTAbits.BRGH = 1;
					 
					 short_delay_count = 5;
				}
			}
			else
			{
				short_delay_count --;
				if(short_delay_count <0) 
				{
					short_delay_count = 0;
				}
				if(short_delay_count == 0)
				{
					PORT_LCD_PWR_CTRL = 0;
					FLAG_LCD_POWER_ON = 0;
					FLAG_SERIAL_PORT_ACTIVE = 0;
					 TXSTAbits.TXEN = 0;
					TXSTAbits.BRGH = 0;
					
					if(FLAG_SENSOR_FAULT_RST == 1)
					{
						SOFT_RESET
					}
				}
			}
		}
		else if((FLAG_LOW_BATTERY == 1)
						&& (FLAG_PTT_ACTIVE == 0) 
						&& (FLAG_CO_ALARM_ACTIVE == 0)
						&& (FLAG_FAULT_FATAL == 0) 
						&& (FLAG_AC_DETECT == 0)
						&& (FLAG_CO_SENSOR_SHORT == 0))
		{
			if(FLAG_LCD_POWER_ON == 0)
			{
				if(Payload.button_press == 0x03)
				//if(FLAG_BUTTON_EDGE_DETECT == 1)
				{
					PORT_LCD_PWR_CTRL = 1;
					FLAG_LCD_POWER_ON = 1;
					FLAG_SERIAL_PORT_ACTIVE = 1;
					 TXSTAbits.TXEN = 1;
					 TXSTAbits.BRGH = 1;
					return;	// wait 1 sec for esp32 power on.
				}
			}
			else
			{
				static UCHAR delay_count = 0;
				delay_count ++;
				if(delay_count >5)
				{
					delay_count = 0;
					PORT_LCD_PWR_CTRL = 0;
					FLAG_LCD_POWER_ON = 0;
					FLAG_SERIAL_PORT_ACTIVE = 0;
					 TXSTAbits.TXEN = 0;
					TXSTAbits.BRGH = 0;
				}
			}
		}
		else if ((!FLAG_PTT_ACTIVE) && (Flags_PEAK_LCD_ACTIVE) )
		{
			if(FLAG_LCD_POWER_ON == 0)
			{
				PORT_LCD_PWR_CTRL = 1;
				FLAG_LCD_POWER_ON = 1;
				FLAG_SERIAL_PORT_ACTIVE = 1;
				 TXSTAbits.TXEN = 1;
				 TXSTAbits.BRGH = 1;
				//return;	// wait 1 sec for esp32 power on.
			}
			else
			{
				static UCHAR peak_delay_count = 0;
				peak_delay_count ++;
				if(peak_delay_count >25)
				{
					peak_delay_count = 0;
					PORT_LCD_PWR_CTRL = 0;
					FLAG_LCD_POWER_ON = 0;
					FLAG_SERIAL_PORT_ACTIVE = 0;
					 TXSTAbits.TXEN = 0;
					TXSTAbits.BRGH = 0;
					Flags_PEAK_LCD_ACTIVE = 0;
				}
			}
		}
	//	//else if((Payload.status & 0x04)&&(!FLAG_CO_ALARM_CONDITION))		//no low battery and alarm turn on lcd
	//	{
	//		if(FLAG_LCD_POWER_ON == 0)
	//		{
	//			PORT_LCD_PWR_CTRL = 1;
	//			FLAG_LCD_POWER_ON = 1;
	//			FLAG_SERIAL_PORT_ACTIVE = 1;
	//			 TXSTAbits.TXEN = 1;
	//			 TXSTAbits.BRGH = 1;
	//			return;	// wait 1 sec for esp32 power on.
	//		}
	//	}
	//	else if(Payload.status & 0x7c)	//button pressed 
	//	{
	//		if(FLAG_BUTTON_NOT_IDLE |(Payload.button_press == 0x03) | (Payload.button_press == 0x04) )
	//		{
	//			 if(FLAG_PTT_FAULT |FLAG_LOW_BATTERY  | FLAG_CO_SENSOR_SHORT | FLAG_EOL_MODE  |FLAG_MEMORY_FAULT )
	//			{
	//			   delay_close_lcd_count = 50;
	//			}
	//		}
	//	}
		else
		{	
	//		if((delay_close_lcd_count == 0 )&& (FLAG_LCD_POWER == 0))
	//		{
				PORT_LCD_PWR_CTRL = 0;
				FLAG_LCD_POWER_ON = 0;
				FLAG_SERIAL_PORT_ACTIVE = 0;
				 TXSTAbits.TXEN = 0;
				TXSTAbits.BRGH = 0;
	//		}
	//		else
	//		{
	//			delay_close_lcd_count --;
	//			if(delay_close_lcd_count  <=0 )
	//			{
	//				delay_close_lcd_count = 0;
	//			}
	//		}
		}
	}
	
	if (FLAG_LCD_POWER_ON) //only output when AC is on.
	{
		// Wait for TX Register ready for Char
		while (TXSTAbits.TRMT == 0);

		Payload.checksum = 0;    // clear checksum.

		do
		{
			// Send the next character
			TXREG = payload_ptr[index];
			Payload.checksum += payload_ptr[index];

			while (TXSTAbits.TRMT == 0); // First, make sure that the xmit buffer is empty.

			index ++;
		} while (index < sizeof(Payload)) ;    

		static UCHAR button_press_timeout= 0;


		button_press_timeout ++;
		//if(button_press_timeout >=5)	//2
		{

			//if(Payload.button_press == 0x05)	
			{
				if(button_press_timeout >=5)
				{
					button_press_timeout = 0;
					Payload.button_press = 0; //clear button press after each transmission.
				}
			}
//			else
//			{
//				button_press_timeout = 0;
//				Payload.button_press = 0; //clear button press after each transmission.
//			}
		}
	}
}


//*****************************************************************************
//	INTERRUPT VECTORS
//*****************************************************************************
// Interrupts are disabled in Low Power Mode, however, Timer Interrupt Flags
// and Interrupt on Change flags are still active to wake from Sleep.
// Those flags are also cleared here when switching to active mode to prevent
// repeated interrupt calls.


//Timer0 Timer Interrupts for Display Refresh
void interrupt main_interrupt_routine(void)
{
   
     static unsigned char T0Count = 0 ;

    #ifdef _CONFIGURE_DEBUG_INTS
      #ifdef    _CONFIGURE_DEBUG_PIN_ENABLED
        //Main_TP_On();
      #endif
    #endif

    // Add Int Flag checks to determine Interrupt Source
     
     // Timer 1 Ints are enabled during Low Power Sleep Mode
     // Timer 1 Flag is polled during active mode
     if(PIR1bits.TMR1IF == 1)
     {
//         SER_Send_String("Timer 1\r ");
        // Sleep Timer INTS only during Low Power Sleep Mode
        if(FLAG_MODE_ACTIVE == 0)
        {
        //  In Low Power Sleep Mode clear the Int Flag
            PIR1bits.TMR1IF = 0;
            
        }
        else
        {
            //  Don't want to clear flag in Active Mode
            //  (it will foul up active task polled timing)
            //  PIR1bits.TMR1IF = 0;

        }

     }

     // The Test Button will generate an Interrupt on Change in Active Mode
     // Clear the IOC flags
     if(INTCONbits.IOCIF == 1)
     {
         // The Test and Peak Buttons are on Bit 3 and 6 (clear the IOC flags)
         IOCBFbits.IOCBF3 = 0;
         IOCBFbits.IOCBF6 = 0;
         INTCONbits.IOCIF == 0;

         FLAG_BUTTON_EDGE_DETECT = 1;

     }

     // Display Refresh Timer INT?
     if(INTCONbits.TMR0IF == 1)
     {
#ifdef _CONFIGURE_DIGITAL_DISPLAY
         // In Active Mode Only the LED Display is Refreshed
         if(FLAG_MODE_ACTIVE == 1)
         {
            #ifdef _CONFIGURE_DEBUG_INTS
              #ifdef    _CONFIGURE_DEBUG_PIN_ENABLED
                //Main_TP_On();
              #endif
            #endif

             if(LED_DISP_OFF != Display_Get_Intensity_Int() )
             {
                T0Count++;
                if( LED_DISP_BRIGHT == Display_Get_Intensity_Int() )
                {
                    if(T0Count == 8)
                    {
                        Display_Refresh_Task() ;
                        T0Count = 0;
                    }
                }
                else
                {
                    Display_Refresh_Task() ;
                    T0Count = 0;
                }
             }

         }

        // Reset Timer0
        TMR0 = TIMER0_1MS;
#endif
        // Timer 0 Interrupt
        INTCONbits.TMR0IF = 0;  //Clear INT Flag

     }

    #ifdef _CONFIGURE_DEBUG_INTS
      #ifdef    _CONFIGURE_DEBUG_PIN_ENABLED
        //Main_TP_Off();
      #endif
    #endif

}