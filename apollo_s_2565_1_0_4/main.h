/****************************************************************
 * The contents of this file are Kidde proprietary and confidential.
 * *************************************************************/
// MAIN.H
#ifndef	_MAIN_H
#define	_MAIN_H

    #define	TASKID_SOUND_TASK			0
    #define	TASKID_BUTTON_TASK			1
    #define	TASKID_PTT_TASK				2
    #define	TASKID_BATTERY_TEST			3
    #define	TASKID_DEBUG_TASK			4
    #define	TASKID_ALARM_PRIORITY       5
    #define	TASKID_COALARM				6
    #define	TASKID_CALIBRATE			7
    #define	TASKID_COMEASURE			8
    #define	TASKID_TROUBLE_MANAGER      9
    #define	TASKID_GLED_MANAGER			10
    #define	TASKID_GLED_BLINK_TASK      11
//    #define	TASKID_DISP_UPDATE			12
    #define	TASKID_PEAK_BUTTON_TASK     12
    #define TASKID_ESCAPE_LIGHT         13
    #define TASKID_LIGHT_TASK           14
    #define TASKID_VOICE_TASK           15
//    #define	TASKID_PEAK_BUTTON_TASK     13
//    #define TASKID_ESCAPE_LIGHT         14
//    #define TASKID_LIGHT_TASK           15
//    #define TASKID_VOICE_TASK           16


    #define	NUM_RX_BUFFER_CHARS         7

    #define PAYLOAD_LENGTH              9

    extern 	char receive_buffer[NUM_RX_BUFFER_CHARS] ;
    extern 	UCHAR bOneMinuteTic ;
    extern 	UCHAR b1SecTimer ;
    
//    extern UCHAR Payload[PAYLOAD_LENGTH];
    extern struct UartPayload Payload;

    void	Main_TP_On(void);
    void	Main_TP_Off(void);
    void	Main_TP_Toggle(void);
    void	MAIN_Reset_Task(UCHAR taskID) ;
    void 	Main_Delay(UINT) ;
    UINT 	Make_Return_Value(UINT milliseconds) ;

    void Main_Init_Power_Dly_Timer(void);

  #ifdef _CONFIGURE_ID_MANAGEMENT
    void    Output_Unit_ID(void) ;
  #endif

    struct UartPayload
    {
       UCHAR co_ppm_msb;
       UCHAR co_ppm_lsb;
       UCHAR peak_co_ppm_msb;
       UCHAR peak_co_ppm_lsb;
       UCHAR status;
       UCHAR button_press;
       UCHAR battery_remainder;
       UCHAR calibration;
//       UCHAR debug;
       UCHAR checksum;
    };

#endif
