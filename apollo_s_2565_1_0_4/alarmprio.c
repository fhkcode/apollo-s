/****************************************************************
 * The contents of this file are Kidde proprietary and confidential.
 * *************************************************************/
// ALARMPRIO.C
// State machine for determining the priority of alarms.

#define	_ALARMPRIO_C


#include    "common.h"
#include    "alarmprio.h"
#include    "comeasure.h"
#include    "diaghist.h"
#include    "led_display.h"

#define ALARM_PRIORITY_INTERVAL_MS		100


//
// Alarm state constants.  Must adjust these if new jumps are
// inserted in ALM_Jump_Table below.
//
#define	ALM_STATE_ALARM_NONE			0
#define	ALM_STATE_MASTER_SMOKE			1
#define	ALM_ALARM_MASTER_CO			2
#define	ALM_ALARM_SLAVE_SMOKE			3
#define	ALM_ALARM_SLAVE_CO			4

#define ALM_6_SEC_RETURN_TIME    6000                    // 6 seconds
#define ALM_4_SEC_RETURN_TIME    4000                    // 6 seconds

#ifdef	_CONFIGURE_BAT_CONSERVE

	#define	TIMER_BAT_CONSERVE		4	// 4 minutes 
        // Prototype for Bat Conserve Routines
	void ALM_Init_Bat_Conserve_Timer(void) ;
	UINT ALM_Alarm_Bat_Conserve_Logic(void) ;
	
	static UCHAR ALM_Timer_Bat_Conserve ;
#endif

#ifdef	_CONFIGURE_INTERCONNECT	

	#define	TIMER_CO_SLAVE_ALARM_TIMEOUT	10	// = (10 secs) 
	#define	TIMER_SMK_SLAVE_ALARM_TIMEOUT	10	// = (10 secs)

	void ALM_Init_Slave_Timer(void) ;
		
	static UCHAR ALM_Start_Time ;
#endif

static UCHAR ALM_State = ALM_STATE_ALARM_NONE ;

// Global / Static Initialization (not needed for PIC)
/*
void Alarmprio_Init(void)
{
    ALM_State = ALM_STATE_ALARM_NONE ;
}
*/

/*****************************************************************************/
UINT AlarmPriorityTask(void) 
{
	switch (ALM_State)
	{
		case ALM_STATE_ALARM_NONE:
			
			// This is CO only project
                        /*
			if (FLAG_MASTER_SMOKE_ACTIVE) 
			{
				FLAG_SMK_INITIATING_ALARM = 1 ;
				FLAG_SMOKE_ALARM_ACTIVE = 1 ;
				FLAG_SMOKE_ALARM_MEMORY = 1 ;
				ALM_State = ALM_STATE_MASTER_SMOKE ;
			} 
			*/
			#ifdef	_CONFIGURE_INTERCONNECT			
				else if(FLAG_SLAVE_SMOKE_ACTIVE)
				{
					FLAG_SMOKE_ALARM_ACTIVE = 1 ;
					ALM_Init_Slave_Timer() ;
					ALM_State = ALM_ALARM_SLAVE_SMOKE ;
				}
				else if(FLAG_INTCON_CO_RECEIVED)
				{
					// A CO alarm was received via interconnect
					// Set alarm and go to slave state
					//
					FLAG_CO_ALARM_ACTIVE = 1 ;
					FLAG_SLAVE_CO_ACTIVE = 1 ;
					FLAG_BAT_CONSRV_MODE_ACTIVE = 0 ;
					ALM_Init_Slave_Timer() ;
					ALM_State = ALM_ALARM_SLAVE_CO ;
				}
			#endif
		
			if(FLAG_CO_ALARM_CONDITION)
			{
				#ifdef	_CONFIGURE_ESCAPE_LIGHT
					Display_Set_Intensity(LED_DISP_BRIGHT) ;
				#endif

				#ifdef _CONFIGURE_DIAG_HIST
						// Only Record if entering alarm
						if(!FLAG_CO_ALARM_ACTIVE && !FLAG_PTT_ACTIVE )
						{
							Diag_Hist_Que_Push(HIST_ALARM_ON_EVENT);
						}
				#endif

				//
				// Master CO alarm sounds the CO temporal pattern.
				//
				FLAG_CO_ALARM_ACTIVE = 1 ;
				#ifdef _CONFIGURE_VOICE
						FLAG_PLAY_VOICE = 1 ;
				#endif
				ALM_State = ALM_ALARM_MASTER_CO ;
			}
			else
			{
				break ;
			}
			#ifdef	_CONFIGURE_BAT_CONSERVE
                // If an Alarm Condition detected, start Bat Conserve Timer
                ALM_Init_Bat_Conserve_Timer() ;
			#endif
		break ;

                /*  This is a CO Alarm only
		case ALM_STATE_MASTER_SMOKE:
			//
			// If the master smoke flag has been cleared, exit Master Smoke mode.
			//
			if (!FLAG_MASTER_SMOKE_ACTIVE) 
			{
                            ALM_State = ALM_STATE_ALARM_NONE ;
                            FLAG_SMK_INITIATING_ALARM = 0 ;
                            FLAG_SMOKE_ALARM_ACTIVE = 0 ;
			} 
			break ;
		*/
		case ALM_ALARM_MASTER_CO:
                        /*  This is a CO Alarm only
			//
			// If smoke flag has become active, exit slave CO mode and go back
			// to Alarm None Mode.  The Alarm None logic will then decide what to do.
			// This causes smoke alarm to override CO Alarm.
			//
			if (FLAG_MASTER_SMOKE_ACTIVE || FLAG_SLAVE_SMOKE_ACTIVE)
			{
                            //
                            // Clear alarm flag to stop the temporal pattern and go back
                            // to Alarm none state.
                            //
                            FLAG_CO_ALARM_ACTIVE = 0 ;
                            ALM_State = ALM_STATE_ALARM_NONE ;
			}
                        */

			if (!FLAG_CO_ALARM_CONDITION)
			{
                #ifdef _CONFIGURE_DIAG_HIST
                    // Only Record if exiting alarm
					if(FLAG_CO_ALARM_ACTIVE)
					{
						Diag_Hist_Que_Push(HIST_ALARM_OFF_EVENT);
					}
                #endif

                FLAG_CO_ALARM_ACTIVE = 0 ;
                ALM_State = ALM_STATE_ALARM_NONE ;
			}
			else
			{
                #ifdef  _CONFIGURE_BAT_CONSERVE
                    UINT retval;
					if (retval = ALM_Alarm_Bat_Conserve_Logic() )
					{
						return  Make_Return_Value(retval) ;
					} 
                #endif
			}
		break ;

        /*  This is a CO Alarm only
		case ALM_ALARM_SLAVE_SMOKE:
		
		  #ifdef    _CONFIGURE_INTERCONNECT
			if (FLAG_SLAVE_SMOKE_ACTIVE) 
			{
				//
				// Re-init alarm timeout timer.
				//
				ALM_Init_Slave_Timer() ;
				FLAG_SMOKE_ALARM_ACTIVE = 0 ;
			} 
			else if (b1SecTimer - ALM_Start_Time > TIMER_SMK_SLAVE_ALARM_TIMEOUT)
			{
				// Timeout has occurred.  Clear the alarm enable flag and go to 
				// the Alarm None state.
				ALM_State = 0 ;
				FLAG_SMOKE_ALARM_ACTIVE = 0 ;
			}
		  #endif
			
		break ;
		*/

        /*  This is a CO Alarm only
		case ALM_ALARM_SLAVE_CO:
		
		  #ifdef	_CONFIGURE_INTERCONNECT			
			if (FLAG_MASTER_SMOKE_ACTIVE || FLAG_SLAVE_SMOKE_ACTIVE) 
			{
				// Exit Slave CO if either smoke condition is entered.
				FLAG_INTCON_CO_RECEIVED = 0 ;
				FLAG_SLAVE_CO_ACTIVE = 0 ;
				FLAG_CO_ALARM_ACTIVE = 0 ;		
				ALM_State = 0 ;		
			} 
			else if (FLAG_INTCON_CO_RECEIVED)
			{
				// Another Hardwire CO message was received.  Re-init the timeout
				// timer and check for battery conserve mode.
				ALM_Init_Slave_Timer() ;
				FLAG_INTCON_CO_RECEIVED = 0 ;
								
				if (UINT retval = ALM_Alarm_Bat_Conserve_Logic())
				{
                                    return	Make_Return_Value(retval) ;
				} 
			}
			else if (b1SecTimer - ALM_Start_Time > TIMER_CO_SLAVE_ALARM_TIMEOUT)
			{
				// If the alarm timeout timer has expired, exit CO slave mode.
				FLAG_INTCON_CO_RECEIVED = 0 ;
				FLAG_SLAVE_CO_ACTIVE = 0 ;
				FLAG_CO_ALARM_ACTIVE = 0 ;		
				ALM_State = 0 ;							
			}
			else
			{
				// Here we're in slave CO and we're waiting for the timeout to
				// occur.  Until then, just check for battery conserve.
				if (UINT retval = ALM_Alarm_Bat_Conserve_Logic())
				{
					return	Make_Return_Value(retval) ;			
				} 
			}
		  #endif
		 
		break ;
                */
	}

	return	Make_Return_Value(ALARM_PRIORITY_INTERVAL_MS) ;	
}



#ifdef	_CONFIGURE_INTERCONNECT			

//********************************************************************************
void ALM_Init_Slave_Timer(void)
{
	ALM_Start_Time = b1SecTimer ;
}
#endif


#ifdef	_CONFIGURE_BAT_CONSERVE
//********************************************************************************
void ALM_Init_Bat_Conserve_Timer(void)
{
	ALM_Timer_Bat_Conserve = bOneMinuteTic ;
}



//********************************************************************************
// Returns the number of milliseconds to be returned to the scheduling engine or
// zero to use the default time.
UINT ALM_Alarm_Bat_Conserve_Logic(void)
{
	// If we're in battery conserve mode already, see if it's time to sound
	// the sounder.
	if (FLAG_BAT_CONSRV_MODE_ACTIVE )
	{
		// We are currently in battery conserve mode.
		// If AC is back on, clear the battery conserve mode flag as
		// battery conserve is not active on AC.
		//
		if (FLAG_AC_DETECT)
		{
			#ifdef _CONFIGURE_DIGITAL_DISPLAY
				FLAG_DISPLAY_OFF = 0;
			#endif
			FLAG_BAT_CONSRV_MODE_ACTIVE = 0 ;
			FLAG_CO_ALARM_ACTIVE = 1 ;
			ALM_Init_Bat_Conserve_Timer() ;
			return ALM_4_SEC_RETURN_TIME ;
		}
		//
		// If the alarm is already on, turn it off.
		//
		else if (FLAG_CO_ALARM_ACTIVE)
		{
			FLAG_CO_ALARM_ACTIVE = 0 ;
			#ifdef _CONFIGURE_DIGITAL_DISPLAY
				FLAG_DISPLAY_OFF = 1;
			#endif
		}
		else
		{
			// If a minute has passed, re-init the timer and set the CO active
			// flag for the sound module.
			if (ALM_Timer_Bat_Conserve != bOneMinuteTic)
			{
				FLAG_CO_ALARM_ACTIVE = 1 ;
				#ifdef _CONFIGURE_DIGITAL_DISPLAY
					FLAG_DISPLAY_OFF = 0;
				#endif
				ALM_Init_Bat_Conserve_Timer() ;
				return ALM_4_SEC_RETURN_TIME ;
			}	
		}
	}
	else
	{
		//
		// If the unit is on AC, don't check for battery conserve.  Re-init the
		// battery conserve timer so that if the AC power goes off the unit will
		// start from the beginning of the battery conserve timeout.
		//
		if (FLAG_AC_DETECT)
		{
			ALM_Init_Bat_Conserve_Timer() ;
		}
		//
		// Check the battery conserve timer to see if we need to go into 
		// battery conserve mode.
		//
		else if (bOneMinuteTic - ALM_Timer_Bat_Conserve > TIMER_BAT_CONSERVE)
		{
			FLAG_CO_ALARM_ACTIVE = 0 ;
			FLAG_BAT_CONSRV_MODE_ACTIVE = 1 ;
			ALM_Init_Bat_Conserve_Timer() ;
			#ifdef _CONFIGURE_DIGITAL_DISPLAY
				FLAG_DISPLAY_OFF = 1;
			#endif

		}
	}

	return ALM_4_SEC_RETURN_TIME ;
	
}
#endif





