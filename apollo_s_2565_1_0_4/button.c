/****************************************************************
 * The contents of this file are Kidde proprietary and confidential.
 * *************************************************************/
// BUTTON.C
// Debounces buttons and sets button flags
#define	_BUTTON_C


#include	"common.h"
#include	"button.h"

#include	"sound.h"
#include	"comeasure.h"
#include	"cocompute.h"
#include	"battery.h"
#include	"life.h"

#ifdef _CONFIGURE_PEAK_BUTTON
    #include "peak_button.h"
#endif

#ifdef _CONFIGURE_VOICE
    #include    "voice.h"
#endif

#define		BTN_QUICK_CHECK_INTERVAL_MS		30		// Button Sample rate
#define		BTN_CHECK_INTERVAL_MS			100		// Button Sample rate
#define		BTN_FUNCT_TEST_INTERVAL_MS		1000	// 1 second 
#define		BTN_4_SEC_DWELL_BEFORE_TEST		4000	// 4 seconds
#define     BTN_3_SEC_DWELL_BEFORE_TEST		3000	// 3 seconds
#define     BTN_1_SEC_RETURN_TIME           1000    // 1 second

#define		BTN_5_SEC_COUNT					50	// unused 
#define		BTN_8_SEC_COUNT					80	//wifi iot clear function
#define		BTN_10_SEC_COUNT				100 	// CO Func Test
#define		BTN_12_SEC_COUNT				120	//stuck button
#define		BTN_4_SEC_COUNT				40	//stuck button sounder cycle

#ifdef _CONFIGURE_CO_FUNC_TEST
    #define	BTN_MAX_COUNT			150	// 15 secs
#else
    #define	BTN_MAX_COUNT			30	// 3 secs
#endif

#define		FUNC_CO_TIMEOUT_TIME			120	// Seconds
#define     WAIT_RELEASE_TIMEOUT_TIME       15      // 1.5 Seconds


static unsigned char BTN_State = 0 ;
static unsigned char BTN_Counter ;
static unsigned char BTN_Lang_Sel_Timout  = 0; //12 * 100ms = 1.2 sec.
static unsigned char button_press_counter = 0;
static unsigned char Stuck_Sound_counter = 0;

// Global / Static Initialization (not needed for PIC)
/*
void Button_Init(void)
{
    BTN_State = 0 ;
}
*/

//******************************************************************************
unsigned int BTN_Task(void)
{

    switch (BTN_State)
    {
        case	BTN_STATE_IDLE:
            FLAG_BUTTON_NOT_IDLE = 0 ;
			
            #ifdef _CONFIGURE_TAMPER_SWITCH
            // No Button action if in Active Tamper Alert
            if(FLAG_TAMPER_ACTIVE == 0)
            {
            #endif

			if( (FLAG_CALIBRATION_COMPLETE == TRUE) && (FLAG_CALIBRATION_UNTESTED == FALSE) )
			{
				if(PORT_TEST_BTN == 1)
				{
					FLAG_BUTTON_NOT_IDLE = 1 ;		// Leaving Idle State
					BTN_State = BTN_STATE_DEBOUNCE ;
				}
				FLAG_BUTTON_EDGE_DETECT = 0 ;
			}

            #ifdef _CONFIGURE_TAMPER_SWITCH
            }
            #endif
            break ;

		case    BTN_STATE_DEBOUNCE:
			// See if button is still depressed
			if (PORT_TEST_BTN == 1)
			{
				//SER_Send_Int('T', BTN_State, 10) ;

				// Audible Button Detected Feedback Beep
				SND_Do_Chirp();

				// Button is debounced.  Start timer and transition to wait
				// for button to be released.
				BTN_Counter = 0 ;
				BTN_State = BTN_STATE_TIMER ;

			}
			else
			{
				// Button is up, go back and wait for another button press.
				BTN_State = BTN_STATE_IDLE ;
				FLAG_BUTTON_EDGE_DETECT = 0 ;
			}
			break ;


		case BTN_STATE_TIMER:

			//SER_Send_Int('T', BTN_State, 10) ;

			if (PORT_TEST_BTN == 1)
			{

				#ifdef _CONFIGURE_PEAK_BUTTON
					// 1st Check Peak button, if pressed, clear Peak Memory and wait for
					// release of test button.
					if ( PEAK_BTN_STATE_IDLE != Peak_BTN_Get_State() )
					{
						// Clear peak ppm value
						wPeakPPM = 0;
						
						Payload.peak_co_ppm_msb = 0;
						Payload.peak_co_ppm_lsb = 0;
										
						FLAG_CO_PEAK_MEMORY = 0 ;
						//FLAG_ENABLE_CO_PEAK_MEMORY = 0 ;  // Note: currently unused

						#ifdef _CONFIGURE_DIGITAL_DISPLAY
							MAIN_Reset_Task(TASKID_DISP_UPDATE) ;
						#endif
						
						// Wait for Button Released
						BTN_Counter = WAIT_RELEASE_TIMEOUT_TIME ;
						BTN_State = BTN_STATE_WAIT_RELEASE ;
						return Make_Return_Value(BTN_CHECK_INTERVAL_MS) ;
					}
				#endif

				BTN_Counter++ ;

				#ifdef _CONFIGURE_CO_FUNC_TEST
					if (BTN_Counter >= BTN_MAX_COUNT)
					{
						// Limit Button Count
						BTN_Counter = BTN_10_SEC_COUNT ;
						// Maximum Button Press proceed to process
						BTN_State = BTN_STATE_IDLE ;
					}
					else if (BTN_Counter == BTN_5_SEC_COUNT)
					{
						SND_Set_Chirp_Cnt(2) ;
					}
					else if (BTN_Counter == BTN_10_SEC_COUNT)
					{
						SND_Set_Chirp_Cnt(3) ;
					}
				#else
					// No Timed Button Function configured
//					if (BTN_Counter >= BTN_MAX_COUNT)
//					{
//						// Force to < 5 Sec Function
//						BTN_State = BTN_STATE_FUNCTION ;
//					}
//					if (BTN_Counter >= BTN_8_SEC_COUNT)
//					{
//						Payload.button_press = 0x09;
//					}
//					else 
					if ((BTN_Counter >= BTN_8_SEC_COUNT)&&(BTN_Counter < BTN_12_SEC_COUNT))
					{
						Payload.button_press = 0x09;
					}
					if(BTN_Counter >= BTN_12_SEC_COUNT)
					{
						Stuck_Sound_counter ++;
						if(Stuck_Sound_counter >= BTN_4_SEC_COUNT)
						{
							Stuck_Sound_counter = 0;
							// Audible Button Detected Feedback Beep
							SND_Do_Chirp();
//							 GREEN_LED_ON
						}
					}
				#endif
			}
			else	// button release.
			{
				if(FLAG_LANGUAGE_SEL)
				{
					BTN_State = BTN_STATE_FUNCTION ;
				}
				else
				{
//					if (BTN_Counter >= BTN_MAX_COUNT)
//					{
//						// Force to < 5 Sec Function
//						BTN_State = BTN_STATE_FUNCTION ;
//					}
//					else
					if(BTN_Counter >= BTN_12_SEC_COUNT)
					{
						BTN_State = BTN_STATE_IDLE;
					}
					if ((BTN_Counter >= BTN_8_SEC_COUNT)&&(BTN_Counter < BTN_12_SEC_COUNT))
					{
						Payload.button_press = 0x09;
						BTN_State = BTN_STATE_IDLE;
					}
					else
					{
						BTN_Lang_Sel_Timout = 40; //40 30ms sample check, 1.2 sec in lang selection.
					
						button_press_counter += 1;
	//					Payload.debug = button_press_counter;
						if(button_press_counter >= 2)//2
						{
							Payload.button_press = 0x08;
							button_press_counter = 0;
							BTN_State = BTN_STATE_IDLE ;
	//						BTN_State = BTN_STATE_FUNCTION ;
						}
						else
						{
							BTN_State = BTN_STATE_LANG_SELECT;
						}
					}
				}
			}
		  break ;


		case    BTN_STATE_FUNCTION:

			//SER_Send_Int('T', BTN_State, 10) ;

			if(BTN_Counter < BTN_5_SEC_COUNT)
			{

				// Button Press < 5 seconds (Short Press)

				 // Once in Fatal EOL, only respond to Alarm or EOL
			   if((FLAG_CO_ALARM_CONDITION) || (FLAG_EOL_FATAL))
				{
					// Perform Soft Reset
					FLAG_PTT_RESET = 1;
					SOFT_RESET
				}

			   if(FLAG_EOL_HUSH_ACTIVE)
			   {
					// Execute PTT/reset (will also clear EOL Hush)
					BTN_State = BTN_STATE_ISSUE_A_TEST ;
					return Make_Return_Value(BTN_CHECK_INTERVAL_MS) ;
			   }

				#ifdef _CONFIGURE_EOL_HUSH
					if( FLAG_EOL_MODE )
					{
						// A CO Sensor Fault condition or Low battery trouble
						//  during EOL Mode prevents unit entering EOL Hush
						if(FLAG_CO_FAULT_PENDING)
						{
							// Perform Soft Reset
							FLAG_PTT_RESET = 1;
							SOFT_RESET
						}
						else
						{
							// If unit is in EOL, enter EOL hush if not already in
							// EOL hush.
							LFE_UL_EOL_Snooze() ;

							Payload.button_press = 0x04;

							#ifdef _CONFIGURE_DIGITAL_DISPLAY
								MAIN_Reset_Task(TASKID_DISP_UPDATE) ;
							#endif
							BTN_Counter = WAIT_RELEASE_TIMEOUT_TIME ;
							BTN_State = BTN_STATE_WAIT_RELEASE ;

							#ifdef _CONFIGURE_VOICE
								// Announce Hush Mode
								VCE_Play(VCE_HUSH_ACTIVATED) ;
							#endif

							return Make_Return_Value(BTN_CHECK_INTERVAL_MS) ;
						}
					 }

				#endif


				#ifdef _CONFIGURE_LB_HUSH
					if( FLAG_LB_HUSH_ACTIVE)
					{

						#ifdef _CONFIGURE_VOICE
							// Play Peak Co Detected Voice
							VCE_Play(VCE_HUSH_CANCELLED) ;

							// Allow Voice to complete before beginning Test
							BTN_State = BTN_STATE_ISSUE_A_TEST ;
							return Make_Return_Value(BTN_3_SEC_DWELL_BEFORE_TEST) ;

						#else
							// Execute PTT/reset (will also clear LB Hush)
							BTN_State = BTN_STATE_ISSUE_A_TEST ;
							return Make_Return_Value(BTN_CHECK_INTERVAL_MS) ;
						#endif

					}

					//if(FLAG_LOW_BATTERY && !FLAG_LB_HUSH_INHIBIT)
					if(FLAG_LOW_BATTERY )
					{
						#ifdef _CONFIGURE_VOICE
							// Announce Hush Mode
							VCE_Play(VCE_LOW_BATTERY) ;

							#ifndef _CONFIGURE_CANADA
							// Do not send 2 part message Canada Voice
								FLAG_HUSH_SEND = 1;
							#endif
						#endif

						// If in low battery, initiate low battery hush.
						BAT_Init_LB_Hush() ;

						//if ( FLAG_CO_SENSOR_SHORT == 0)
						{
							Payload.button_press = 0x03;
						}

						BTN_Counter = WAIT_RELEASE_TIMEOUT_TIME ;
						BTN_State = BTN_STATE_WAIT_RELEASE ;

						Main_Init_Power_Dly_Timer();

						return Make_Return_Value(BTN_CHECK_INTERVAL_MS) ;

					}
				#endif

				if( FLAG_CO_PEAK_MEMORY)
				{

					// Disable the activation of peak memory or else it could be
					// set again on the next sample if in a CO condition and before
					// CO alarm.
					//FLAG_ENABLE_CO_PEAK_MEMORY = 0 ;  // Note: currently unused

					#ifndef _CONFIGURE_PEAK_BUTTON
							// If not a Peak Button, clear Peak CO memory here
							//  together with the Peak Flag
							wPeakPPM = 0 ;

							// Reset peak memory
							// For Digital Model only clear when Peak Memory is cleared
							//FLAG_CO_PEAK_MEMORY = 0 ;
					#endif

					#ifdef _CONFIGURE_VOICE
							// Play Peak Co Detected Voice
							VCE_Play(VCE_CO_PREVIOUSLY) ;

							// Allow Voice to complete before beginning Test
							BTN_State = BTN_STATE_ISSUE_A_TEST ;
							return Make_Return_Value(BTN_4_SEC_DWELL_BEFORE_TEST) ;

					#endif

					// Else Continue

				}

				if( (FLAG_FAULT_FATAL) || (FLAG_CO_ALARM_ACTIVE) )
				{
					// If in Fault Mode - Reset

					// If CO Fault Save Condition for Reset
					if(FLAG_CO_FAULT_PENDING)
						FLAG_SENSOR_FAULT_RST = 1;

					// Reset the CPU
					FLAG_PTT_RESET = 1;
					//SOFT_RESET

				}
				else
				{
					//SER_Send_Int('T', BTN_State, 10) ;

					// Do a PTT.
					FLAG_PTT_ACTIVE = 1 ;
					Payload.button_press = 0x01;
					BTN_State = BTN_STATE_WAIT_PTT ;
				}

			}
//			else if(BTN_Counter < BTN_10_SEC_COUNT)
//			{
//				// Button Press between 5 - 9 seconds
//				BTN_State = BTN_STATE_IDLE ;
//			}
			else if(BTN_Counter >= 120)
			{
				BTN_State = BTN_STATE_IDLE;
			}
			if ((BTN_Counter >= BTN_8_SEC_COUNT)&&(BTN_Counter < 120))
			{
				Payload.button_press = 0x09;
				BTN_State = BTN_STATE_IDLE;
			}
			else
			{
			  #ifdef _CONFIGURE_CO_FUNC_TEST
				// Button Press 10 seconds
				// Functional Test Mode Selected
				FLAG_FUNC_CO_TEST = 1 ;
				MAIN_Reset_Task(TASKID_COALARM) ;

				// Init Timer for Functional Testing
				BTN_Counter = FUNC_CO_TIMEOUT_TIME ;

				// Reset CO Measure state
				MEAS_Reset() ;


				BTN_State = BTN_STATE_FUNC_TEST ;

				return Make_Return_Value(2000) ;

			  #else

				BTN_State = BTN_STATE_IDLE ;

			  #endif

			}
			break ;

		case    BTN_STATE_WAIT_PTT:
				// Wait in this state until the PTT module clears the
				// Active flag.
				if (!FLAG_PTT_ACTIVE)
				{
					BTN_State = BTN_STATE_IDLE ;
				}

				break ;

		case    BTN_STATE_WAIT_RELEASE:
				//TODO: Add timeout Timer in case button gets stuck here

				if(--BTN_Counter == 0)
				{
					// Return to Idle on Timeout
					BTN_State = BTN_STATE_IDLE ;
				}

				// Wait in this state until the PTT Button is inactive
				if (PORT_TEST_BTN == 1)
				{
					// Remain here until Button Released

				}
				else
				{
					BTN_State = BTN_STATE_IDLE ;
				}
				break;


		case BTN_STATE_ISSUE_A_TEST:

			// Issue a PTT.
			FLAG_PTT_ACTIVE = 1 ;

			Payload.button_press = 0x01;

			BTN_State = BTN_STATE_WAIT_PTT ;

			break;

	 #ifdef _CONFIGURE_CO_FUNC_TEST
		case    BTN_STATE_FUNC_TEST	:
			// Functional Test in Progress
			// Monitor Button or Timeout Timer for Test Complete,
			// then Reset
			if(--BTN_Counter == 0)
			{
				// Reset after Button is released
				BTN_State = BTN_STATE_WAIT_FT_RELEASE ;
			}
			else if(PORT_TEST_BTN == 1)
			{
				// Button Press Feedback Chirp
				SND_Set_Chirp_Cnt(1) ;

				// Reset after Button is released
				BTN_State = BTN_STATE_WAIT_FT_RELEASE ;
			}

			return Make_Return_Value(BTN_FUNCT_TEST_INTERVAL_MS) ;

		case    BTN_STATE_WAIT_FT_RELEASE:
			// Wait in this state until the PTT module clears the
			// Active flag.
			if (PORT_TEST_BTN == 1)
			{
				// Remain here until Button Released

			}
			else
			{
				FLAG_FUNC_CO_TEST = 0 ;
				// Reset the CPU
				FLAG_PTT_RESET = 1 ;
				SOFT_RESET
			}
			break;
  #endif
		
		case BTN_STATE_LANG_SELECT:
			if (--BTN_Lang_Sel_Timout == 0)	// reach timeout but no button press.
			{
				BTN_State = BTN_STATE_FUNCTION;
				button_press_counter = 0;
			}
			else
			{
				if(PORT_TEST_BTN == 1)
				{
					BTN_State = BTN_STATE_DEBOUNCE; // 2nd button press detected. check debounce.
				}				
			}

			break;

    }

	if(FLAG_LANGUAGE_SEL)
	{
		return Make_Return_Value(BTN_CHECK_INTERVAL_MS) ;
	}
	else
	{
		return Make_Return_Value(BTN_QUICK_CHECK_INTERVAL_MS) ;
	}

}

//*************************************************************************
unsigned char BTN_Get_State(void)
{
    return  BTN_State ;
}

