/****************************************************************
 * The contents of this file are Kidde proprietary  and confidential.
 * *************************************************************/
// sounder.c
// Contains functionality for sounder timing
#define _SOUNDER_C

#include    "common.h"
#include    "sound.h"

#include    "cocalibrate.h"

#ifdef _CONFIGURE_VOICE
    #include	"voice.h"
#endif

struct	SND_Alarm_Params
    {
        UINT    on_time_ms ;
        UINT    off_time_ms ;
        UINT    temprl_time_ms ;
        UINT    num_beeps ;
        UCHAR   voice ;
    } ;

#define     SND_DEFAULT_INTERVAL	300     // 300 ms

#define     SND_TASK_TIME_100MS         100	// 100 ms
#define     SND_TASK_TIME_500MS         500	// 500 ms
#define     SND_TASK_TIME_1_SEC 	1000	// 1 sec
#define     SND_TASK_TIME_2_SEC 	2000	// 2 sec
#define     SND_TASK_TIME_30_SEC	30000	// 30 sec
#define     SND_TASK_TIME_60_SEC	60000	// 60 sec


#define	SND_SMOKE_TMPORAL_MS		1500
#define	SND_SMOKE_ON_TIME_MS		500
#define	SND_SMOKE_OFF_TIME_MS		500
#define	SND_NUM_BEEPS_SMOKE		3

#define	SND_CO_TMPORAL_MS		5000
#define	SND_CO_ON_TIME_MS		100
#define	SND_CO_OFF_TIME_MS		100
#define	SND_NUM_BEEPS_CO		4

/* Smoke not used in this project
const struct SND_Alarm_Params SND_Smoke_Params = 
    {
         SND_SMOKE_ON_TIME_MS ,
         SND_SMOKE_OFF_TIME_MS ,
         SND_SMOKE_TMPORAL_MS ,
         SND_NUM_BEEPS_SMOKE ,
         1
    } ;
*/

// Set chirp time in ms
#define SND_CHIRP_TIME  30

const struct SND_Alarm_Params SND_CO_Params =
    {
         SND_CO_ON_TIME_MS ,
         SND_CO_OFF_TIME_MS ,
         SND_CO_TMPORAL_MS ,
         SND_NUM_BEEPS_CO ,
         2
    } ;


#define	SND_TROUBLE_IDLE		0
#define	SND_TROUBLE_FATAL_FAULT		1
#define SND_TROUBLE_EOL_MODE		2
#define	SND_TROUBLE_LB			3
#define	SND_WIFI_CLEAR			4


static unsigned char	BeepCounter ;
static unsigned char	TroubleChirpCounter = 0 ;
static unsigned int 	SounderState = SND_STATE_IDLE ;
static unsigned char	SndTroubleState = SND_TROUBLE_IDLE;
static unsigned char	LBConserveCount = 1;

unsigned char   FaultChirpDelayCntr ;
const struct    SND_Alarm_Params *ptrSoundParams ;


// Global / Static Initialization (not needed for PIC)
/*
void Sound_Init(void)
{
    SounderState = SND_STATE_IDLE ;
    TroubleChirpCounter = 0 ;
    SndTroubleState = SND_TROUBLE_IDLE ;
	
}
*/
//*************************************************************************
unsigned int SounderTask(void)
{
    switch    (SounderState)
    {
		case SND_STATE_IDLE:
			FLAG_SOUND_NOT_IDLE = 0 ;
			//
			/* This is CO only project
			if (FLAG_SMOKE_ALARM_ACTIVE)
			{
					HORN_ON
					RLED_On() ;

					ptrSoundParams = &SND_Smoke_Params ;
					BeepCounter = (*ptrSoundParams).num_beeps ;
					SounderState = SND_STATE_ACTIVE ;
					FLAG_SOUND_NOT_IDLE = 1 ;
					return	Make_Return_Value((*ptrSoundParams).on_time_ms) ;
			}
			*/
			if (FLAG_CO_ALARM_ACTIVE)
			{
				// If the calibration state machine is in verify state, turn
				// on the sounder continuously.
				if (CAL_Get_State() == CAL_STATE_VERIFY)
				{
					HORN_ON

					SounderState = SND_STATE_VERIFY ;
					FLAG_SOUND_NOT_IDLE = 1 ;
					break ;
				}

				HORN_ON

				ptrSoundParams = &SND_CO_Params ;
				BeepCounter = (*ptrSoundParams).num_beeps ;
				SounderState = SND_STATE_ACTIVE ;
				FLAG_SOUND_NOT_IDLE = 1 ;
				return	Make_Return_Value((*ptrSoundParams).on_time_ms) ;
			}

			// If excape light cycling FW then do not turn OFF Light here
			#ifndef _CONFIGURE_ESC_LIGHT_CYCLING
			  #ifdef	_CONFIGURE_ESCAPE_LIGHT
				  FLAG_ESC_LIGHT_ON = 0;
			  #endif
			#endif

			if (TroubleChirpCounter)
			{
				if (!FLAG_VOICE_SEND)
				{
					TroubleChirpCounter-- ;
					SND_Do_Chirp() ;
				}
			}
			else if(FLAG_ENABLE_CHIRP)
			{
				if (--FaultChirpDelayCntr == 0)
				{
					SND_Do_Chirp() ;
					FLAG_ENABLE_CHIRP = 0 ;
				}
			}
			// Clear Active Chirp indicator if no more chirps
			if (TroubleChirpCounter == 0 )
			{
			   FLAG_CHIRPS_ACTIVE = 0 ;
			}

			break ;

		case SND_STATE_ACTIVE:

			#ifdef	_CONFIGURE_ESCAPE_LIGHT
				FLAG_ESC_LIGHT_ON = 1;
			#endif

			HORN_OFF

			if (!--BeepCounter)
			{
			  #ifdef _CONFIGURE_VOICE
				if (FLAG_PLAY_VOICE)
				{
					FLAG_PLAY_VOICE = 0 ;
					#ifdef _CONFIGURE_VOICE
						if (FLAG_CO_ALARM_ACTIVE)
							VCE_Play(VCE_CO_ENGLISH) ;
					#endif
				}
				else
				{
					FLAG_PLAY_VOICE = 1 ;
				}
			  #endif
				// Clear any stored trouble Beeps that have accumulated
				//  so they don't happen at the end of alarm
				TroubleChirpCounter = 0;


				// This is the last beep of the series.  Wait for the temporal
				// interval to expire.
				SounderState = SND_STATE_IDLE ;

				return	Make_Return_Value((*ptrSoundParams).temprl_time_ms) ;
			}
			else
			{
				SounderState = SND_STATE_ON ;
				return	Make_Return_Value((*ptrSoundParams).off_time_ms) ;
			}

		case SND_STATE_ON:
			HORN_ON

			SounderState = SND_STATE_ACTIVE ;
			return Make_Return_Value((*ptrSoundParams).on_time_ms) ;

		case SND_STATE_CHIRP:
			SounderState = SND_STATE_IDLE;
			break ;

		case	SND_STATE_VERIFY:
			HORN_ON

			break ;

		default:
			break ;
    }

    return  Make_Return_Value(SND_DEFAULT_INTERVAL) ;
}


//*************************************************************************
unsigned char SND_Get_State(void)
{
    return  SounderState ;
}


//*************************************************************************
void SND_Do_Chirp(void)
{
	// Turn on the sounder, delay and turn off the sounder
	HORN_ON
	
	// Delay here
        delay_ms(SND_CHIRP_TIME);
	
	HORN_OFF
}

//*************************************************************************
void SND_Reset_Task(void)
{
	TroubleChirpCounter = 0 ;
	MAIN_Reset_Task(TASKID_SOUND_TASK) ;
	
}

//*************************************************************************
void SND_Set_Chirp_Cnt(UCHAR count)
{
	FLAG_CHIRPS_ACTIVE = 1 ;	// Chirp Counter Active with Chirp count
	TroubleChirpCounter = count ;
	
}

//*************************************************************************
UCHAR SND_Get_Chirp_Cnt(void)
{
	return TroubleChirpCounter ;
}


//*************************************************************************

#define	LBAT_CONSERVE_VOICE_TIME	15		//15 minutes
// Test Only
//#define 	LBAT_CONSERVE_VOICE_TIME	3		//3 minutes
//static  UCHAR  WIFI_CLEAR_FALG = 0;
UCHAR  WIFI_CLEAR_FALG = 0;

unsigned int SND_Trouble_Manager(void)
{
 
	switch	(SndTroubleState) 
	{
            case SND_TROUBLE_IDLE:
                if (FLAG_EOL_FATAL)
                {
                    SndTroubleState = SND_TROUBLE_FATAL_FAULT ;
                }
                if (FLAG_EOL_MODE)
                {
                    SndTroubleState = SND_TROUBLE_EOL_MODE ;
                }
                else if(FLAG_FAULT_FATAL)
                {
                    SndTroubleState = SND_TROUBLE_FATAL_FAULT ;
                }
                else if(FLAG_LOW_BATTERY)
                {
                    SndTroubleState = SND_TROUBLE_LB ;
                }
	      else  if(Payload.button_press == 0x09)
	       {
		  SndTroubleState = SND_WIFI_CLEAR;
	        }
				
                break;
		
            case SND_TROUBLE_FATAL_FAULT:
                if (FLAG_FAULT_FATAL)
                {
                    // Trouble chirp in Fatal Fault Mode
                    TroubleChirpCounter ++ ;

                    if(FLAG_EOL_FATAL)
                    {
                        // Double chirp if EOL Fatal Fault
                        TroubleChirpCounter ++ ;
                    }

                    return  Make_Return_Value(SND_TASK_TIME_30_SEC) ;
                }
                else
                {
                    // Return to IDLE if not in Fault
                    SndTroubleState = SND_TROUBLE_IDLE ;
                }
                break;

            case SND_TROUBLE_EOL_MODE:
            {
                // If this is during a Nighttime Hour, Hush EOL chirps
                //  during EOL Mode (after 30 days this is no longer allowed)
                //  as we enter Fatal Fault chirping State (SND_TROUBLE_FATAL_FAULT)
                if(FLAG_LB_CHIRP_INHIBIT == 0)
                {
                    if(!FLAG_EOL_HUSH_ACTIVE)
                    {
                        // Double chirp for EOL
                        TroubleChirpCounter ++ ;
                        TroubleChirpCounter ++ ;
                    }
                }

                // Return to IDLE after this state
                SndTroubleState = SND_TROUBLE_IDLE ;

                return	Make_Return_Value(SND_TASK_TIME_30_SEC) ;
            }

            case SND_TROUBLE_LB:
            {
                // No LB Voice/chirps if in Alarm
                if ((FLAG_CO_ALARM_ACTIVE) || (FLAG_FAULT_FATAL))
                {
                    // Return to IDLE if in Alarm
                    SndTroubleState = SND_TROUBLE_IDLE ;

                }
                else if ( FLAG_LB_HUSH_ACTIVE == 1 )
                {
                    // Every 60 Secs Switch to active mode for a few seconds
                    //  to display LB
                    Main_Init_Power_Dly_Timer();

                    return  Make_Return_Value(SND_TASK_TIME_60_SEC) ;

                }
                else if (FLAG_LOW_BATTERY)
                {

                    if(FLAG_MODE_ACTIVE)
                    {
                        // There is a low voltage limit to Hushing the LB Chirp
                        //  Test for that limit being reached. (FLAG_LB_HUSH_INHIBIT == 1)
                        //  Test for Night Time LB Hush active - FLAG_LB_CHIRP_INHIBIT == 1
                        if( (FLAG_LB_CHIRP_INHIBIT) && (!FLAG_LB_HUSH_INHIBIT) )
                        {
                            // No LB Chirp (Hushed!)
                        }
                        else
                        {
                            // LB chirp in Active Mode
                            TroubleChirpCounter ++ ;

                            #ifdef _CONFIGURE_VOICE
                                VCE_Play(VCE_LOW_BATTERY) ;
                            #endif
                        }

                        // Keep LBat Conserve Voice Time Initialized
                        LBConserveCount = 1;
                    }
                    else
                    {
                        // There is a low voltage limit to Inhibiting the LB Chirp
                        //  Test for that limit being reached. (LB Hush Inhibit)
                        if( (FLAG_LB_CHIRP_INHIBIT) && (!FLAG_LB_HUSH_INHIBIT) )
                        {
                            // No LB Chirp
                        }
                        else
                        {
                            // Low Power Mode LB chirp
                            SND_Do_Chirp() ;

                            if(--LBConserveCount == 0)
                            {
                                #ifdef _CONFIGURE_VOICE
                                    VCE_Play(VCE_LOW_BATTERY) ;
                                #endif
                                LBConserveCount = LBAT_CONSERVE_VOICE_TIME;
                            }
                        }
                     }

                    // Switch to active mode for a few seconds
                    //  to display LB
                    Main_Init_Power_Dly_Timer();

                    // Return to IDLE after this state
                    SndTroubleState = SND_TROUBLE_IDLE ;

                    return  Make_Return_Value(SND_TASK_TIME_60_SEC) ;
                }
                else
                {
                    // Return to Idle State if Low Battery is not set
                    SndTroubleState = SND_TROUBLE_IDLE ;
                }

            }
            break;
			
	   case SND_WIFI_CLEAR :
			// Turn on the sounder, delay and turn off the sounder
		   if(!WIFI_CLEAR_FALG)
		   {
			HORN_ON
			// Delay here
			delay_ms(15);
			HORN_OFF
			delay_ms(100);
			HORN_ON
			// Delay here
			delay_ms(15);
			HORN_OFF
			WIFI_CLEAR_FALG = 1;
		   }
	    break;
	}
	
	return	Make_Return_Value(SND_TASK_TIME_100MS) ;
}

