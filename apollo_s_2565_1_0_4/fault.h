/****************************************************************
 * The contents of this file are Kidde proprietary and confidential.
 * *************************************************************/
// FAULT.H
//
//  This file contains fault status definitions.
//

#ifndef	_FAULT_H_
#define _FAULT_H_

    //Prototypes
    UCHAR FLT_Fault_Manager(UCHAR faultcode) ;

    // Global variables
    extern UCHAR FatalFaultCode ;

    // Fault Codes
    #define FAULT_0                 0
    #define FAULT_1                 1
    #define FAULT_CONVERSION_SETUP  2   // CO sensor Test fault
    #define FAULT_3                 3
    #define FAULT_SENSOR_SHORT		4   // CO sensor fault
    #define FAULT_GAS_SENSOR		5   // Gas sensor fault
    #define FAULT_CALIBRATION		6   // CO calibration fault
    #define FAULT_PTT               7
    #define FAULT_MEMORY            8
    #define FAULT_EXPIRATION		9
    #define FAULT_PHOTOCHAMBER		10  // Reserved for Photo Smoke models
    #define FAULT_MEMORY_FLASH		11
    #define FAULT_TEMPERATURE		12

    #define SUCCESS_FAULT_0         0x80 | FAULT_0
    #define SUCCESS_FAULT_1         0x80 | FAULT_1
    #define SUCCESS_CONVERSION_SETUP	0x80 | FAULT_CONVERSION_SETUP
    #define SUCCESS_FAULT_3         0x80 | FAULT_3
    #define SUCCESS_SENSOR_SHORT	0x80 | FAULT_SENSOR_SHORT
    #define SUCCESS_GAS_SENSOR		0x80 | FAULT_GAS_SENSOR
    #define SUCCESS_CALIBRATION		0x80 | FAULT_CALIBRATION
    #define SUCCESS_PTT             0x80 | FAULT_PTT
    #define SUCCESS_MEMORY          0x80 | FAULT_MEMORY
    #define SUCCESS_EXPIRATION		0x80 | FAULT_EXPIRATION
    #define SUCCESS_PHOTOCHAMBER	0x80 | FAULT_PHOTOCHAMBER
    #define SUCCESS_MEMORY_DIAG		0x80 | FAULT_MEMORY_FLASH
    #define SUCCESS_TEMPERATURE		0x80 | FAULT_TEMPERATURE

    // Fault Return Codes
    #define FAULT_NO_ACTION_TAKEN	0	/* no action taken (fault/success code
                                                   not supported or already in Fatal Fault) */
    #define FAULT_SUCCESS_ACTION	1	// Success logged
    #define FAULT_PENDING_ACTION	2	// Fault logged but not in Fatal Fault yet
    #define FAULT_FATAL_ACTION          3	// Fault logged and Fatal Fault Set
    #define FAULT_RECOVERY_ACTION	4	// Fatal Fault was cleared

	
#endif

