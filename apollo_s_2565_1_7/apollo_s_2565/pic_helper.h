/* 
 * File:   pic_helper.h
 * Author: Bill.Chandler
 *
 * Created on July 18, 2013, 7:18 AM
 */

#ifndef PIC_HELPER_H
#define	PIC_HELPER_H

#ifdef	__cplusplus
extern "C" {
#endif

    #define BANK0   0
    #define BANK1   1
    #define BANK2   2
    #define BANK3   3
    #define BANK4   4
    #define BANK5   5
    #define BANK6   6



    #define CLEAR_WDT  asm("clrwdt");
    #define GOTOSLEEP  asm("sleep");
    #define SOFT_RESET asm("reset");

    #define _XTAL_FREQ  16000000
    // NOTE: To use the macros below, YOU must have previously defined _XTAL_FREQ
    // They convert time (us or ms) into number of instruction cycles used by _delay().
    #define delay_us(x) _delay((unsigned long)((x)*(_XTAL_FREQ/4000000.0)))
    #define delay_ms(x) _delay((unsigned long)((x)*(_XTAL_FREQ/4000.0)))


#ifdef	__cplusplus
}
#endif

#endif	/* PIC_HELPER_H */

