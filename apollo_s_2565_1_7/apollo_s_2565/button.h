/****************************************************************
 * The contents of this file are Kidde proprietary and confidential.
 * *************************************************************/
// BUTTON.H

#ifndef	_BUTTON_H
#define	_BUTTON_H

    // Prototypes
    unsigned int BTN_Task(void) ;
    unsigned char BTN_Get_State(void) ;


 
    #define	BTN_STATE_IDLE              0
    #define	BTN_STATE_DEBOUNCE          1
    #define	BTN_STATE_TIMER             2
    #define	BTN_STATE_FUNCTION          3
    #define	BTN_STATE_WAIT_PTT          4
    #define	BTN_STATE_FUNC_TEST         5
    #define	BTN_STATE_WAIT_RELEASE		6
    #define	BTN_STATE_ISSUE_A_TEST		7
    #define	BTN_STATE_WAIT_FT_RELEASE	8
    #define BTN_STATE_LANG_SELECT       9
	

#endif

