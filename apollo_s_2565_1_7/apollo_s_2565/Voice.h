/****************************************************************
 * The contents of this file are Kidde proprietary and confidential.
 * *************************************************************/
// VOICE.H
//
//  This file contains voice file definitions.
//

#ifndef	_VOICE_H_
#define _VOICE_H_


        // Prototypes
	UINT VCE_Process(void) ;
	UCHAR VCE_Play(UCHAR voice) ;

 
	#define	VCE_TEST_TONE			0
	#define	VCE_FIRE_ENGLISH		0x01
	#define	VCE_CO_ENGLISH			0x02
	#define	VCE_HUSH_ACTIVATED		0x03
	#define	VCE_HUSH_CANCELLED		0x04
	#define	VCE_PUSH_TEST_BUTTON            0x05
	#define	VCE_CO_PREVIOUSLY		0x06
	#define	VCE_LOW_BATTERY			0x07


#endif

