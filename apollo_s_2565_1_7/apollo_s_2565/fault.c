/****************************************************************
 * The contents of this file are Kidde proprietary  and confidential.
 * *************************************************************/
// fault.c
#define	_FAULT_C

#include    "common.h"
#include    "fault.h"
#include    "diaghist.h"

#define FAULT_CONVERSION_THRESHOLD	2	// 2 attempts (60 seconds apart)
#define FAULT_SENSOR_SHORT_THRESHOLD	30	// 30 attempts (1 sec apart)
#define FAULT_GAS_THRESHOLD		4	// 4 attempts 

// Local Protoypes
void	Fault_Set_Fatal_Mode(UCHAR fault);

// Local static variables
static UCHAR bErrorConversionCount = 0 ;
static UCHAR bErrorShortCount = 0 ;

#ifdef _CONFIGURE_GAS	
    static UCHAR bGasFaultCount = 0 ;
#endif


// This holds current Fault Mode Status code
UCHAR FatalFaultCode = 0;       // This works
//UCHAR FatalFaultCode = 0x80;    // This does not work as expected


UCHAR FLT_Fault_Manager(UCHAR faultcode)
{
    
    // Test only
    /*
    SER_Send_Int('f', faultcode, 16) ;
    SER_Send_Prompt() ;
    // Test only
    SER_Send_Int('F', FatalFaultCode, 16) ;
    SER_Send_Prompt() ;
    */

  // Success Code or Fault Code?
	if(faultcode & 0x80)
	{
		// Success Code was passed
		// Process Passed Success Code
		switch (faultcode)
		{
	  	
			case SUCCESS_CONVERSION_SETUP:
				bErrorConversionCount=0;
				// If current fault status is Sensor Test Fault
				if(FAULT_CONVERSION_SETUP == FatalFaultCode)
					FLAG_CO_FAULT_PENDING = 0;

			break;

			case SUCCESS_SENSOR_SHORT:
				bErrorShortCount=0;
				FLAG_CO_SENSOR_SHORT = 0;
				// If current fault status is Sensor Short Fault
				if(FAULT_SENSOR_SHORT == FatalFaultCode)
					FLAG_CO_FAULT_PENDING = 0;

			break;

			case SUCCESS_GAS_SENSOR:
				#ifdef _CONFIGURE_GAS	
								bGasFaultCount = 0 ;
				#endif
			break;
			
			default:
				return (FAULT_NO_ACTION_TAKEN) ;

		  }

	   /* In Above Success Code processing we can also clear pending
            * Fault Flags or let calling function do that since
            * fault manager returns FAULT_RECOVERY_ACTION when applicable.
            */
	  
		if(FLAG_FAULT_FATAL)
		{
		  // Clear Success Bit and compare against current Fatal Fault
		  if(FatalFaultCode == (faultcode & 0x7F))
		  {

			  // Clear Current Fatal Fault condition if a match
			  FatalFaultCode = faultcode;
			  FLAG_FAULT_FATAL = 0;
			  FLAG_PTT_FAULT = 0;
			  // Test only
			  //SER_Send_Int('X', FatalFaultCode, 16) ;
			  //SER_Send_Prompt() ;

			  return (FAULT_RECOVERY_ACTION) ;
		  }

				  // Test only
				  //SER_Send_Int('Z', FatalFaultCode, 16) ;
				  //SER_Send_Prompt() ;

		}
		// Clear Pending CO Faults if both error counts are cleared
		if((bErrorShortCount==0) && (bErrorConversionCount==0))
		  FLAG_CO_FAULT_PENDING = 0;

		return (FAULT_SUCCESS_ACTION) ;

	}
	else
	{
		// Fault Code was passed
		if(FLAG_FAULT_FATAL)
		{
		  // No Action if Already in Fatal Fault
		  return (FAULT_NO_ACTION_TAKEN) ;

		}

		// Process Fault Code
		switch (faultcode)
		{

		  case FAULT_CONVERSION_SETUP:

			  if( bErrorConversionCount == FAULT_CONVERSION_THRESHOLD)
			  {
				  #ifdef _CONFIGURE_DIAG_HIST
					  Diag_Hist_Que_Push(HIST_SENS_HEALTH_FAULT);
				  #endif

				  Fault_Set_Fatal_Mode(faultcode) ;

			  }
			  else
			  {
				  FLAG_CO_FAULT_PENDING = 1;

				  bErrorConversionCount++;
				  return (FAULT_PENDING_ACTION) ;
			  }

				break ;

			case FAULT_SENSOR_SHORT:

			  if( bErrorShortCount == FAULT_SENSOR_SHORT_THRESHOLD)
			  {
				  #ifdef _CONFIGURE_DIAG_HIST
					  Diag_Hist_Que_Push(HIST_SENS_SHORT_FAULT);
				  #endif

				  Fault_Set_Fatal_Mode(faultcode);
				  FLAG_CO_SENSOR_SHORT = 1;
			  }
			  else
			  {
				  FLAG_CO_FAULT_PENDING = 1;

				  bErrorShortCount++;
				  return (FAULT_PENDING_ACTION) ;

			  }
				break ;
		
			case FAULT_GAS_SENSOR:

			  #ifdef _CONFIGURE_GAS

				if( bGasFaultCount == FAULT_GAS_THRESHOLD)
				{
					#ifdef _CONFIGURE_DIAG_HIST
						Diag_Hist_Que_Push(HIST_SENS_GAS_FAULT);
					#endif

					Fault_Set_Fatal_Mode(faultcode);

				}
				else
				{
					bGasFaultCount++;
					return (FAULT_PENDING_ACTION) ;

				}

			  #endif
				break ;

			case FAULT_CALIBRATION:

				#ifdef _CONFIGURE_DIAG_HIST
					Diag_Hist_Que_Push(HIST_SENS_CALIB_FAULT);
				#endif

				Fault_Set_Fatal_Mode(faultcode);

				break ;
		
			case FAULT_PTT:

				#ifdef _CONFIGURE_DIAG_HIST
					Diag_Hist_Que_Push(HIST_PTT_FAULT);
				#endif

				Fault_Set_Fatal_Mode(faultcode);
				FLAG_PTT_FAULT = 1;

				break ;
		
			case FAULT_MEMORY:

				#ifdef _CONFIGURE_DIAG_HIST
					Diag_Hist_Que_Push(HIST_MEMORY_FAULT);
				#endif
				FLAG_MEMORY_FAULT = 1;	
				Fault_Set_Fatal_Mode(faultcode);


				break ;

			case FAULT_EXPIRATION:

				#ifdef _CONFIGURE_DIAG_HIST
					Diag_Hist_Que_Push(HIST_LIFE_EXP);
				#endif

				FLAG_EOL_FATAL = 1 ;
				Payload.button_press = 0x04;

				Fault_Set_Fatal_Mode(faultcode);

				break ;


			default:
				return (FAULT_NO_ACTION_TAKEN) ;

		}
		
		return (FAULT_FATAL_ACTION) ;
	}
   
}

void    Fault_Set_Fatal_Mode(UCHAR fault)
{

    
    FatalFaultCode = fault;
    FLAG_FAULT_FATAL=1;
    FLAG_CO_ALARM_ACTIVE = 0;

    // Test only
    
    SER_Send_Int('F', FatalFaultCode, 16) ;
    SER_Send_Prompt() ;

		
}



