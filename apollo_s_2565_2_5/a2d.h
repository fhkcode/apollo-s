/****************************************************************
 * The contents of this file are Kidde proprietary and confidential.
 * *************************************************************/
// a2d.h
// Routines for Analog to digital support

#ifndef _A2D_H
#define _A2D_H

    //Prototypes
    unsigned int A2D_Convert(UCHAR channel)  ;
#ifdef	_CONFIGURE_ACDC
    void A2D_Test_AC(void);
#endif

    

    // TBD - AC Detect Voltage (set to 250 counts for now)
    #define	AC_DETECT_THRESHOLD             250

    #define	A2D_BATTERY_VOLTAGE_CHAN        0
    #define	A2D_CO_VOLTAGE_CHAN             1
    #define	A2D_AC_DETECT_CHAN              3
    #define     A2D_LIGHT_CHAN              10

    #define     A2D_FVR_CHAN                31

#ifdef _CONFIGURE_TEMP
    #define	A2D_TEMP_CHAN                   29
#endif

    /*;
    ;   Define ADCON0 channel data necessary to select proper channel
    ;    by shifting into CHS0-CHS4 bit positions.
    ;
    */
    #define	A2D_BATTERY_VOLTAGE     (A2D_BATTERY_VOLTAGE_CHAN << 2)
    #define	A2D_CO_VOLTAGE          (A2D_CO_VOLTAGE_CHAN << 2)
    #define	A2D_AC_DETECT_VOLTAGE	(A2D_AC_DETECT_CHAN << 2)
    #define	A2D_LIGHT_VOLTAGE       (A2D_LIGHT_CHAN << 2)
    #define	A2D_FVR_VOLTAGE         (A2D_FVR_CHAN << 2)

#ifdef _CONFIGURE_TEMP
    #define	A2D_TEMP_VOLTAGE	(A2D_TEMP_CHAN << 2)
#endif

#endif
