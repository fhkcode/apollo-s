/****************************************************************
 * The contents of this file are Kidde proprietary and confidential.
 * *************************************************************/
// battery.h


#ifndef	_BATTERY_H
#define	_BATTERY_H

        // Prototypes
        unsigned int BatteryTest_Task(void) ;
        void BAT_Set_Test_Interval(UCHAR mode) ;

        #ifdef _CONFIGURE_LB_HUSH
            void BAT_Init_LB_Hush(void) ;
        #endif

        extern volatile unsigned int BAT_Last_Volt;
        extern volatile unsigned int  BAT_Scale ;

#define INTERVAL_TIMER_RESET   0
#define ALG_RESET              1

	
#endif

