/* 
 * File:   memory_1509.h
 * Author: Bill.Chandler
 *
 * Created on July 23, 2013, 7:51 AM
 */

#ifndef _MEMORY_1509_H
#define	_MEMORY_1509_H

#ifdef	__cplusplus
extern "C" {
#endif

    // Protoypes
    void Memory_Read_Block(UCHAR num_bytes, UINT source, UCHAR *dest);
    void Memory_Write_Block(UCHAR num_bytes, UCHAR *source, UINT dest);
    void Memory_Write_Backup_Block(UCHAR num_bytes, UCHAR *source, UINT dest);
    UCHAR Memory_Row_Erase(UINT addr);
    UCHAR Memory_Row_Read(UINT addr);
    UCHAR Memory_Row_Write(UINT addr);
    UCHAR Memory_Byte_Read(UINT addr);

    UCHAR Memory_Read_Verify(UINT dest);
    UCHAR Memory_Write_Verify(UINT dest);
    UCHAR Memory_Erase_Verify(UINT dest);
    

    // Location of Start of Primary Copy of Structure
    #define MEMORY_HIGH_ADDRESS     0x1F00

    // Locate Structure Backup out of the Primary Row
    #define MEMORY_BACKUP_ADDRESS   0x1EE0

    #define FLASH_SUCCESS   0
    #define FLASH_FAIL      1
    #define ROW_SIZE        32

    extern volatile UCHAR b1memorybuffer[ROW_SIZE];


    
    



#ifdef	__cplusplus
}
#endif

#endif	/* _MEMORY_1509_H */

