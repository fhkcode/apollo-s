/****************************************************************
 * The contents of this file are Kidde proprietary and confidential.
 * *************************************************************/
#ifndef _DEFMSG_H
#define _DEFMSG_H

#warning *******************************************
#ifndef _CONFIGURE_PRODUCTION_UNIT
    #warning *** NON-PRODUCTION FIRMWARE VERSION ***
 #else
    #warning *** PRODUCTION FIRMWARE VERSION ***
#endif

#warning ***       Firmware Version 0.1

#warning *******************************************

#ifdef	_CONFIGURE_2547
    #warning *** ACDC CO Digital, 3V-Lithium, p/n 2547-PG01
#endif

#ifdef	_CONFIGURE_2548
    #warning *** ACDC CO Digital, 3V-Lithium, Escape Light p/n 2548-PG01
#endif

#ifdef	_CONFIGURE_2549
    #warning *** ACDC CO Digital, 3V-Lithium, Voice p/n 2549-PG01
#endif

#ifdef	_CONFIGURE_2549_CANADA
    #warning *** ACDC CO Digital, 3V-Lithium, Voice p/n 2549-PG02
#endif

#ifdef	_CONFIGURE_2551
    #warning *** ACDC CO Digital, 3V-AA, p/n 2551-PG01
#endif

#ifdef	_CONFIGURE_2565
    #warning *** ACDC CO Digital, 3V-AA, p/n 2565-PG01
#endif

#ifdef _CONFIGURE_NO_REGULATOR
    #warning *** A/D VREG Scaling Enabled
#endif

#ifdef _CONFIGURE_ACCEL_LIGHT_TEST
    #warning *** Acclerated Light Firmware
#endif

#ifdef	_CONFIGURE_PHOTO_SMOKE
       #warning *** Photo Smoke Chamber
#endif
	
#ifndef __DEBUG
       #warning *** Release Build
#else
       #warning *** Debug Build
#endif

#ifdef _CONFIGURE_10_YEAR_LIFE
	#warning *** 10 Year Life
#endif

#ifdef _CONFIGURE_INTERCONNECT
	#warning *** Interconnect
#endif

#ifdef _CONFIGURE_TAMPER_SWITCH
	#warning *** Anti-theft
#endif

#ifdef _CONFIGURE_ALARM_UL_LINEAR
	#warning *** Linear UL
#endif

#ifdef _CONFIGURE_UL_ALARM_CURVE
	#warning *** Curve UL
#endif

#ifdef _CONFIGURE_ALARM_CENELEC
	#warning *** CENELEC
#endif

#ifdef  _CONFIGURE_DIAG_HIST
        #warning *** Diagnostic History Enable
#endif

#ifdef	_CONFIGURE_CO
	#warning *** CO Sensor
#endif

#ifdef  _CONFIGURE_GAS
	#warning *** Gas Detector
#endif

#ifdef	_CONFIGURE_LITHIUM
	#warning *** Lithium Battery
#endif

#ifdef	_CONFIGURE_RECHARGEABLE
	#warning *** Rechargeable Battery
#endif

#ifdef _CONFIGURE_MANUAL_CO_CAL
    #warning *** Manual CO Calibration        
#endif

#ifdef _CONFIGURE_MANUAL_SMOKE_CAL
	#warning *** Manual Smoke Calibration
#endif

#ifdef _CONFIGURE_DISABLE_FAULTS
	#warning *** Faults are Disabled
#endif

#ifdef _CONFIGURE_DISPLAY_ALL_PPM
	#warning *** Display All PPM Values
#endif

#ifdef _CONFIGURE_DISPLAY_NEGATIVE_PPM
	#warning *** Display Negative PPM Values
#endif

#ifdef _CONFIGURE_24_HOUR_TEST
	#warning *** 24 Hour Test Timing
#endif

#ifdef _CONFIGURE_EOL_TEST
	#warning *** End of Life Test Firmware
#endif

#ifdef _CONFIGURE_42_HOUR_TEST
	#warning *** 42 Hour Test Timing
#endif

#ifdef	_CONFIGURE_NO_INTRO_VOICE		
	#warning *** Startup Voice Disabled
#endif

#ifdef	_CONFIGURE_FORCE_COALARM		
	#warning *** Forced CO Alarm
#endif

#ifdef	_CONFIGURE_FORCE_SMOKEALARM		
	#warning *** Forced Smoke Alarm
#endif

#ifdef	_CONFIGURE_FORCE_LB		
	#warning *** Forced Low Battery Condition
#endif

#ifdef	_CONFIGURE_FAST_CO_ALM_TIME		
	#warning *** CO Accumulators Preloaded
#endif

#ifdef	_CONFIGURE_PPM_FILTER
	#warning *** PPM Filtering Code Enabled
#endif

#ifdef	_CONFIGURE_TEMP
	#warning *** Temperature Sensor
#endif

#ifdef _CONFIGURE_DIGITAL_DISPLAY
	#warning *** Digital Display
#endif

#ifdef _CONFIGURE_PEAK_BUTTON
       #warning *** Peak Button
#endif

#ifdef _CONFIGURE_ESCAPE_LIGHT
        #warning *** Escape Light
#endif

#ifdef _CONFIGURE_ESC_LIGHT_CYCLING
        #warning *** Escape Light Cycling Test FW
#endif


#ifdef _CONFIGURE_SIMULATE_SLEEP
	#warning *** Simulated Sleep Enabled
#endif

#ifdef	_CONFIGURE_SHORT_HUSH_TEST
	#warning *** Low Battery Hush Test Firmware
#endif

#ifdef	_CONFIGURE_VOICE
	#warning *** Voice Enabled
#endif

#ifdef	_CONFIGURE_LB_HUSH
	#warning *** Low Battery Hush
#endif

#ifdef	_CONFIGURE_EOL_HUSH
	#warning *** EOL Hush
#endif

#ifdef _CONFIGURE_DIAG_HIST_TEST
        #warning *** Diagnostic Test Firmware
#endif

#ifdef _CONFIGURE_LIGHT_SENSOR
    #warning *** Light Sensor Enabled
#endif

#ifdef _CONFIGURE_ID_MANAGEMENT
    #warning *** ID Management Enabled
#endif

#ifdef _CONFIGURE_SN_TEST_FIRMWARE
    #warning *** Signal Noise Test Firmware
#endif

#ifdef _CONFIGURE_WATCHDOG_TIMER_OFF
    #warning *** Watchdog Timer OFF
#else
    #warning *** Watchdog Timer ON
#endif

#warning *******************************************

#endif