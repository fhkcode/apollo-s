2022/03/17
1. FW rev update to 2.5
2. delete G cmd printf FW ver string, add C\r cmd output FW 2.5.


2022/03/3
1. FW rev update to 2.4
2. during AC and DC mode, pressed peak button then LCD will dispaly peak ppm if peak ppm less then 100PM
3. during DC mode, pressed peak button then LCD will dispaly peak ppm if peak ppm less then 100PM
4. during DC mode, disable wifi ssid clear function

2022/02/17
1. FW rev update to 2.3
2. during PTT , the button press return should not clear


2022/02/16
1. FW rev update to 2.2
2. PTT cmd return should 0x06,during EOL mode PTT, cmd return should 0xff.

2022/02/16
1. FW rev update to 2.1
2. stuck button counter need clear, if counte over stack is error.

2022/02/14
1. FW rev update to 2.0
2. When the CO sensor fault is sent, 0xff is sent, not 0xFF
3. There is also a problem with the LCD backlight control output. At present, 
the output is discontinuous and has been outputting a 0x08 or 0x07. Please update 
the clearing 0 function as shown in the following example and disable payload at 
the same time_ cmd_ Returns repeat the assignment.


2022/02/8
1. FW rev update to 1.9
2. EOL hush timeout cmd returns should set value at exit.
3. After recieve H cmd adn EOL fatal mode, the cmd returns should return 0xFF.

2022/01/28
1. FW rev update to 1.8
2、before low power clear flag and conter:
                wifi_power_off_timeout = 0;
                FLAG_BUTTON_PRESS_VALID = 0;
3. detlete no use code


2022/01/27
1. FW rev update to 1.7
2. add LCD light control
3. during stuck button and low battery....,disable LCD trun on 
4. detlete no use code

2022/01/19
1. FW rev update to 1.6
2. peak record is 10PPM,peak LCD display is 100PPM
3. during co alarm, pressed button more then 8s, will continues co alarm

2022/01/14
1. FW rev update to 1.5
2. clear wifi two short chirp add after power on 60s

2022/01/14
1. FW rev update to 1.4
2. disable lange set on softreset ,only power on reset can be set langer

2022/01/13
1. FW rev update to 1.3
2. During the PTT event, the status byte will be set to 0x81, and button value will be 0x01, continuously for 7 seconds.
But in ONLY co alarm, status byte will be 0x81 (DC ONLY) / 0x83 (ACDC), button value will be 0x00.
3. the wifi reset function no chrip,should two short chirp.


2022/01/12
1. FW rev update to 1.2
2. during low battery 60s blinks LCD,should close.
3.co test LCD should display.
4. stuck button clear wifi connetct info,should stuck button chirp.
5. only clear peak mode with power on or pressed peak button.
6. sys load data(SYS_Init) should run before Main_Initialize. set flag

2021/12/30
1. FW rev update to 1.1
2. during low battery,pressed LCD no display LOW BATTERY,if pressed button at low battery, LCD will dispaly LOW BATTERY
open co sensor ,pressed button LCD no dispaly ERROR;co senser open ,pressed button LCD display ERROR
co alarm,LCD no disaplay,
during EOL mode ,LCD display Incomplete 
3.add new Uart Protocol.at 6 bit add cmd return

2021/12/14
1. FW rev update to 1.0.5
2. during low battery,pressed LCD no display LOW BATTERY,if pressed button at low battery, LCD will dispaly LOW BATTERY
open co sensor ,pressed button LCD no dispaly ERROR;co senser open ,pressed button LCD display ERROR
co alarm,LCD no disaplay,
during EOL mode ,LCD display Incomplete 
3.add new Uart Protocol.at 6 bit add cmd return

2021/11/25
1. FW rev update to 1.0.4
2. only AC and alarm trun on LCD,other mode will pressed button.

2021/11/10
1. FW rev update to 1.0.3
2. when calibratioing ,Lcd show calibration


2021/11/10
1. FW rev update to 1.0.2
2. pressed test button,during eol fatal,should fast 4 chirp and 4 blink,but after 2 chirp and 2 blink is later,
so close power on delay 500ms,at main.c lines 339
3.EOL fatal mode serial not send data,so add status at main.c 1889 lines
4. DC mode show 5 data during EOL fatal.
5. add remove WIFI IOT data sounder chirp,after power on pressed test button 8s ,then 2 chirp
6. add lithium to discharge battery 

2021/10/22
1. FW rev update to 1.0.1
2.  LOW battery LCD show LOW BATTERY,
 PTT show LCD 10sec
 PTT LCD show CO Test,
 Peak LCD show,
 short co sensor LCD show ERROR.

2021/10/09
1. FW rev update to 1.0.0
2.  Add a button press for switching LCD on for 30 seconds to display the state of the unit during Low battery, Error(CO sensor short/open) and EOL conditions


2021/09/29
1. FW rev update to 0.3.9
2.	LCD off during DC only scenario; Add a button press for switching LCD on for 5 seconds to display the state of the unit during Low battery, Error(CO sensor short/open) and EOL conditions
3. DC power on display LCD ,after 5s colse LCD
4、After PTT display LCD
2021/09/16
1. FW rev update to 0.3.8
1.	Add wifi provisioning clear action: press and hold the test button for 8 seconds, alarm will issue 0x09 on the protocol to indicate a wifi provisioning clear action happens. After 12 seconds inidicates a stuck button ocurrs, sounder will chirp every 4 seconds.
2.	Disable LCD display and esp32 power during low battery/EOL/fatal error.Iin low battery and CO alarm occurs, I suggest disable the LCD display too.



2021/09/10
1. FW rev update to 0.3.7
2. main.c lines 661 add DIGITAL_DISPLAY
3. main.c lines 806 disable LCD POWER
4. serial.c disable       
	//TXSTAbits.TXEN = 1;
      	//TXSTAbits.BRGH = 1;

2021/09/03
1. FW rev update to 0.3.6
2. if calibration complete,set uart output byte7 0x03

2021/09/02
1. FW rev update to 0.3.5
2. if calibration complete,set uart output byte7 0xAA

2021/09/01
1. FW rev update to 0.3.4
2. disable debug task

2021/08/26
1. FW rev update to 0.3.3
2. Fix battery capacity is not calculating correctly when unit is initially powered a depleted battery 
that voltage lower than the detection threshold.


2021/08/09
1. FW rev update to 0.3.2
2. Add AC detect condition uart output, when AC loss, set the Tx to low for power save
3. Add a 500ms power on delay to wait power being stable
4. Move EE data loading operation after power on delay.
5. Remove comment on battery conserve mode.
6. Redefine payload structure.
7. Add language selection to button press.
8. Add LCD power control.
9. Add uart output control, when AC is off, or no active events, stop uart output.


2021/07/26 revision changes
1. FW rev update to 0.3.1
2. Add calibration error code to payload.
3. Add 'T' command to support remote push-to-test

2021/07/23 revision changes
1. FW rev update to 0.2
2. Add payload output function in main.c, output co ppm, peak ppm, button press,cal status, etc.
3. Update co ppm to payload in Calculate_PPM() of cocompute.c
4. Update peak co ppm to payload in Update_Peak_PPM() of cocompute.c
5. Update battery usage to payload in Battery Test_Task() of battery.c
6. Load battery scale in Main_Initialize() of main.c
7. Add 2 bytes battery scale in primary database, location 0x06-0x07
8. Change buad rate to 19200 
9. Parity check is not supported by PIC MCU hardware, noted from datasheet.
10. Add peak button press-hold (>=2 sec) and press-release (<2 sec) functions support.
11. If peak button press-hold > 8 sec, give a chirp.
12. Remove unused uart output.

2021/06/16 revision changes
1. Modify source code from digital CO
2. FW Rev reset to Rev 0.1
3. Disable the led_display, like the Timer 0, Display_Refresh_Task().
4. Add 2565 Configuration to config.h, and remove the 
5. Remove Display related functions, variants etc.




***********************Below is historical revision changes byBill Chandler*************************************************

 
 * Revision History:
 *
 *  Revision
 *  Number          Date            Author
 *  -------------------------------------------------------------------------
 *  Version 0.1		11/11/13		Bill Chandler
 *  Version 0.n versions are Rev X (experimental) versions
 *  Initial Code ACDC CO Digital (using pic16LF1936)
 *  Created from Low Cost non-digital project vesion 1.3
 *
X* 1. Add pic16lf1936.h to project (remove 1509 files/modules)
X* 2. Create memory modules memory_1936 (.c and .h)
X*         void Memory_Read_Block(UCHAR num_bytes, UINT source, UCHAR *dest);
X*         void Memory_Write_Block(UCHAR num_bytes, UCHAR *source, UINT dest);
X*    b. Update void SYS_Save(void) routine
X* 3. Update system data modules (.c and .h)
X* 4. Define I/O (io.h)
X*    Re-write I/O initialize in main.c
 *
X* 5. Update DiagHist Modules (.c and .h)
 *
X* 6. Create Digital Display modules
X*    Implement Display Refresh Interrupt
X*    Implement Display Intensity Feature
X*    Add Display Error Code routine
 * 
X* 10. Remove Red LED code as it will be implemented in hardware
 *     (tied to horn driver circuitry)
 * 
X* 11. Create Green LED light measurement test code
X*     a. Periodically set Green Led pin as an Analog Input
X*     b. Read Green LED Voltage and output via serial port
X*     c. restore Green LED pin as Digital Output
 *
X* 12. Create Voice module code
 *     a. Add Voice implementation calls
 *
X* 13  Add Peak Button Code
 *
X* 14. Create Escape Light Code
 *
 * Verify following updates have been added from LCACDC project
 ok * a. Tamper Stuck in Horn On issue
 ok * b. Green LED blinks while in Low Power
 ok * c. Unit_ID command to support Auto ID detect
 ok * d. Fix 2.5%/year math to work with XC8 compiler
 ok * e. CO sensor fault recovery update
 ok * f. CO sensor fault thresholds (mv not A/D)
 * 
 *
 ****************************************************************************
 ****************************************************************************
 * 0.1  03/05/2014  Bill Chandler
 *      1. Add Light Sampling Module
 *      2. Update Escape Light Module for CREE circuit
 *      3. Upgrade PIC to PIC16lf1938 (16K words memory)
 *      4. Disable Tamper option (not designed in these models)
 *
 *
 * 0.2  05/02/2014  Bill Chandler
 *      1. Update Light Sampling Module
 *      2. Add LED Display for Peak CO "PC0"
 *      3. Remove Green LED blinks for Peak CO  and Hush Modes
 *      4. Implement Night Light On/Off routines
 *      5. Implement LED display dim routines
 *      6. Add Light/dark Threshold Hysteresis
 *      7. Add Interupt on Change Button Detect Filtering
 *      8. Filter the Peak CO "PCO' feature for EMI testing
 *
 *      9. Changed Task engine code to always set interval, and reset task time
 *         with current task tic time.
 *
 * 0.3  06/10/2014  Bill Chandler
 *      1. Disable Peak CO and PPM filtering during CO Cal process
 *
 * 0.4  06/13/2014  Bill Chandler
 *      1. Center 150 Cal limits, they were set too High
 *
 * 0.5  06/18/2014  Bill Chandler
 *      1. Enable LB trouble chirp in Low Power Mode
 *      2. Expand CO Cal limits
 *         (we are seeing large variation from Sensor to Sensor)
 *
 * 0.6  06/30/2014  Bill Chandler
 *      1. Update to Escape Light control for LP Mode
 *          Need to keep ESC Light signal off in LP mode which requires
 *          change to ESC Light circuit.
 *
 * 0.7  07/15/2014
 *      1. For CO Cal memory fault, attempt to restore before issuing
 *          memory fault.
 *      2. For Life memory fault, attempt to restore before issuing
 *          memory fault.
 *      3. Update to Peak Button/NL select feature to allow NL ON/OFF
 *          and more user friendly feedback using the digital display.
 *      07/16/2014
 *      4. If serial port active, Keep the Escape light PWM drive Off
 *
 * 0.8  08/26/2014
 *      1. Add Light Measure Active Flag so Green LED will delay until after
 *          light measurement in Low Power Mode.
 *      2. Disable Interrupts during the EE memory write sequence
 *          to prevent EE memory write faults
 *
 * 0.9  08/28/2014
 *      1. Add EE Write Verify code to verify Byte Writes
 *          (Allows Write Retries)
 * 1.0  09/11/2014
 *      1. Increase Low Power Mode battery test to 50 ms
 *         AC Power battery test lowered to 1 ms
 *
 * 1.1  09/16/2014
 *      1. During Memory Writes, do not re-enable Ints if in Low
 *          Power Mode. Ints. must not occur in Low Power Sleep mode
 *
 * 1.2  09/17/2014
 *      1. Add 1 hour Battery Test interval when Light Algorithm
 *          is enabled.
 *      2. Add Testing Code for LB chirp inhibit in sound module
 *
 * 1.3  09/30/2014
 *      1. Remove Peak Button Chirp (marketing request)
 *      2. Disable Peak Button during PTT active
 *      3. Full intensity Escape Light during PTT (marketing request)
 *      4. Fine tuning to Battery Threshold for 2.6 LB Hush OFF and 2.65 LB TH
 *         Data being collected for Voice and non-Voice models
 *
 * 1.4  10/7/2014
 *      1. Event counter updates was corrupting cal memory leading to memory ERR
 *         Created default in event count switch statement so that no counter
 *          writes occur during non-counter events.
 *      2. Change Button action priority between Peak CO and Low Battery conditions.
 *          Low Battery condition is higher priority.
 *      3. Clear stored trouble chirps when coming out of Alarm.
 *      4. Update to Light Algorithm set Threshold Code  (set to 40% min/max delta)
 *         Lower default light TH to 425 mVolts.
 *      5. Update to NL Set Intensity from Peak Button code
 *
 * 1.5 10/13/2014
 *      1. Button Press to clear LB Hush was not working correctly
 *          While in LB Hush, Test Button should execute PTT which also
 *          clears LB Hush. Voice models should announce Hush Cancelled message.
 *
 * 1.6 10/21/2014
 *      1. Add EOL Night Time chirp delay when Light ALG is enabled
 *      2. Allow EOL Mode to take priority over Faults and LB in
 *          display, sound and button modules.
 * 
 * 1.7 10/28/2014
 *      1. Found a condition during EOL Hush, at Low Battery (above 50mv inhibit)
 *          When Button was pressed LB Hush mode was being activated.
 *          We want to disable all LB actions during EOL,
 *           and Low battery during EOL mode should disable EOL Hush
 *
 * 1.8 11/12/2014
 *      1. Lower the default Light/Dark Threshold to 300 mv
 *      2. Initialize Current Battery value in Diag. History at 1st Low Battery Test
 *           after Power On.
 *      3. Fixed bug where Push Test Button message was being announced
 *          in LP Mode when a Peak CO 'PCO' message was displayed every 60 seconds.
 *
 * 1.9 2/13/2015
 *      1. Only Do Current Battery History Initialization if not a production model.
 *         (item 2 in Version 1.8 history above)
 * 
 * 2.0 3/05/2015
 *      1. Increase LB Inhibit window (in Hours) based upon field test data.
 *         For conditions where dark measurements are more that 12 hours
 *         (often closer to 16 hours low light, and 8 ours of higher light
 *          measurements inside a house)L
 *
 *      2. Lower Light/Dark Threshold to 30% of Light signal (from 40%)
 *         Lower NightLight Default TH to 125 mv (used when ALG Off)
 *      3. Create separate Light/day and Night light Thresholds
 * 
 *      3/18/2015
 *      4. Added Green Led On during PTT/alarm (for FHK)
 *         Added Abnormally Low battery Event in Diag History
 *          (To help distinguish LB from Open Battery Activation switch)
 *
 * 2.1 5/04/2015 Bill Chandler
 *      1. Implement Data Structure Checksum validation every minute
 *          to supervise Data Structure corruption
 *      2. Disable Voice during 0/150 Calibration Process (for FHK)
 *
 *
 * 2.2 9/25/2015 Bill Chandler
 *      1. CO Peak Memory not recording Peak values less than 30 PPM
 *         Moved 30 PPM CO test after Peak Memory record routine.
 *