/****************************************************************
 * The contents of this file are Kidde proprietary and confidential.
 * *************************************************************/
// MAIN.H
#ifndef	_MAIN_H
#define	_MAIN_H


/*********************************************************/
#define	REVISION_MAJOR	0
#define	REVISION_MINOR	4
#define	REV_STRING	"FW Ver 0.4e"

#warn	("      - Firmware Version 0.4e")

/*********************************************************/


struct _tag_Analog_Regs
{
	UCHAR _ASSIREVID ;  
	UCHAR _ASSIIOTMR ;  
	UCHAR _ASSIIO1TMR ;  
	UCHAR _ASSIIO2TMR ;    
	UCHAR _ASSIIO3TMR ;        
	UCHAR _ASSIIOMODE1 ;          
	UCHAR _ASSIIOMODE2 ;          
	UCHAR _ASSIIOEN ;              
	UCHAR _ASSIIOU_DATA ;             
	UCHAR _ASSISLPTMR1 ;             
	UCHAR _ASSISLPTMR2 ;            
	UCHAR _ASSIION1 ;               
	UCHAR _ASSIION2_CO ;             
	UCHAR _ASSIPH_CTRL ;             
	UCHAR _ASSIPH_DAC ;              
	UCHAR _ASSIVBOOST ;              
	UCHAR _ASSICNTL1 ;               
	UCHAR _ASSICNTL2 ;               
	UCHAR _ASSICNTL3 ;               
	UCHAR _ASSIADC_CNTR ;          
	UCHAR _ASSIADC1 ;                
	UCHAR _ASSIADC2 ;             
	UCHAR _ASSIINT ;              
	UCHAR _ASSIMASK ; 
} ;


	#define	TASKID_SOUND_TASK			0
	#define	TASKID_BUTTON_TASK			1
	#define	TASKID_RLED_MANAGER			2
	#define	TASKID_PTT_TASK				3
	#define	TASKID_BATTERY_TEST			4
	#define	TASKID_DEBUG_TASK			5
	#define	TASKID_ALARM_PRIORITY		6
	#define	TASKID_COALARM				7
	#define	TASKID_CALIBRATE			8
	#define	TASKID_COMEASURE			9
	#define	TASKID_GLED_MANAGER			10
	#define	TASKID_GAS_TASK				11
	#define	TASKID_TROUBLE_MANAGER		12
	#define	TASKID_DISP_UPDATE			13
	#define	TASKID_VOICE_TASK			14
	#define	TASKID_ALED_MANAGER			15
	#define	TASKID_PEAK_BUTTON_TASK		16

	void	MAIN_Reset_Task(char taskID) ;
	void 	Main_Delay(UINT) ;
		
#ifdef	_CONFIGURE_ANALOG_REG_DEBUG
	void	Main_Read_Analog_Regs(void) ;
#endif
	
	#define	CAPTURE1_INTERVAL	8
	#define	NUM_RX_BUFFER_CHARS	7

/*	
	#ifdef	_MAIN_C
	  #ifdef	_CONFIGURE_ANALOG_REG_DEBUG
		struct _tag_Analog_Regs
		{
			UCHAR _ASSIREVID ;  
			UCHAR _ASSIIOTMR ;  
			UCHAR _ASSIIO1TMR ;  
			UCHAR _ASSIIO2TMR ;    
			UCHAR _ASSIIO3TMR ;        
			UCHAR _ASSIIOMODE1 ;          
			UCHAR _ASSIIOMODE2 ;          
			UCHAR _ASSIIOEN ;              
			UCHAR _ASSIIOU_DATA ;             
			UCHAR _ASSISLPTMR1 ;             
			UCHAR _ASSISLPTMR2 ;            
			UCHAR _ASSIION1 ;               
			UCHAR _ASSIION2_CO ;             
			UCHAR _ASSIPH_CTRL ;             
			UCHAR _ASSIPH_DAC ;              
			UCHAR _ASSIVBOOST ;              
			UCHAR _ASSICNTL1 ;               
			UCHAR _ASSICNTL2 ;               
			UCHAR _ASSICNTL3 ;               
			UCHAR _ASSIADC_CNTR ;          
			UCHAR _ASSIADC1 ;                
			UCHAR _ASSIADC2 ;             
			UCHAR _ASSIINT ;              
			UCHAR _ASSIMASK ; 
		} Analog_Regs ;
	  #endif
*/	

	#ifdef	_MAIN_C
	
		#ifdef	_CONFIGURE_ANALOG_REG_DEBUG
			struct _tag_Analog_Regs Analog_Regs ;
		#endif	

		UINT	Make_Return_Value(UINT milliseconds) ;
		void 	Main_Init_AC_Detect(void) ;	
		void 	Main_Test_AC(void) ;
		void	Main_Set_Low_Power(void) ;
		void	Main_Set_Active(void) ;
		void	Main_Check_Low_Power_Logic(void) ;
		void	Initialize_Engine(void) ;

	#else
		UINT 	Make_Return_Value(UINT milliseconds) ;
		extern 	char receive_buffer[NUM_RX_BUFFER_CHARS] ;
		extern 	UCHAR bOneMinuteTic ;
		extern 	UCHAR b1SecTimer ;
		
		extern 	struct _tag_Analog_Regs Analog_Regs ;
		
		void	Main_TP_On(void);
		void	Main_TP_Off(void);
	
		
	#endif

#endif
