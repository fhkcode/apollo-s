/****************************************************************
 * The contents of this file are Kidde proprietary and confidential.
 * *************************************************************/
// diaghist.c
// Routines for Analog to digital support
#define _DIAGHIST_C

#include    "common.h"
#include    "memory_1936.h"
#include    "diaghist.h"

#include    "life.h"
#include    "systemdata.h"
#include    "cocalibrate.h"
#include    "comeasure.h"

#include    <stddef.h>


#ifdef _CONFIGURE_DIAG_HIST
    UCHAR event_buf[2];
#endif

/*
 * DiagHist is declared and initialized within the EE_Data structure
 * so that it can be placed within EE data starting at EE Address 0x20
// Initialize Diag History in Flash Memory
struct HistData eeprom DiagHist =
{
    // 0x20 - 0x2F
    0,0,0,00,00,00,0,0,00,0,REVISION_MAJOR,REVISION_MINOR,
    // 0x30 - 0x3F
    0,0,0,0,00,00,0,0,0,0,0,QPTR_CKSUM_INIT,MAJOR_QPTR_INIT,MINOR_QPTR_INIT,

    // 0x40 - 0x9F
    0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
    0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
    0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
    0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
    0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
    0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,

    // 0xA0 - 0xFF
    0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
    0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
    0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
    0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
    0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
    0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
} ;
*/

#ifdef _CONFIGURE_DIAG_HIST

/* Push a recordable Event Code into the History memory
 *
 * Inputs - Event Code to record (see header file event definitions)
 * Outputs - none
 *
 */

#define DIAG_QUE_PTR_CSUM    0
#define DIAG_QUE_PTR_MAJOR   1
#define DIAG_QUE_PTR_MINOR   2

void Diag_Hist_Que_Push(UCHAR event_code)
{

    UINT day = 0000;
    UCHAR day_h, day_l = 00;
    UCHAR event;
    UCHAR event_ptr[3];     //Holds Event Queue Pointer values
    UCHAR count_address;

    // No Diagnostic Record, If not Calibrated except for Cal faults
    if ( CAL_STATE_CALIBRATED != CAL_Get_State() )
    {
            if (HIST_SENS_CALIB_FAULT != event_code)
                    return ;
    }

    // First read the 3 Event Queue Pointer values for use by QPush
    Memory_Read_Block(3, DIAG_QUE_CKSUM, &event_ptr);

    // Verify Event Pointers are valid before any writes to EEProm
    // Had to Cast the CKSUM PTR addition to UCHAR for the comparison to work
    if(event_ptr[DIAG_QUE_PTR_CSUM] != (UCHAR)(event_ptr[DIAG_QUE_PTR_MAJOR]
            + event_ptr[DIAG_QUE_PTR_MINOR]) )
    {
        // Try a second time
        Memory_Read_Block(3, DIAG_QUE_CKSUM, &event_ptr);

        // Verify Event Pointers are valid before any writes to EEProm
        if(event_ptr[DIAG_QUE_PTR_CSUM] != (UCHAR)(event_ptr[DIAG_QUE_PTR_MAJOR]
                + event_ptr[DIAG_QUE_PTR_MINOR]) )
        {
            //SER_Send_Int('X', event_ptr[DIAG_QUE_PTR_CSUM], 16) ;

            // Inhibit Writing to Daig History if Que pointers are corrupted
            FLAG_DIAG_HIST_DISABLE = 1;
            return;
        }
        else
        {
            FLAG_DIAG_HIST_DISABLE = 0;
        }
    }
    else
    {
        //SER_Send_Int('+', event_ptr[DIAG_QUE_PTR_CSUM], 16) ;

        FLAG_DIAG_HIST_DISABLE = 0;
    }

    // Save for Event Counter Lookup
    event = event_code ;

    // If MSB is set, this is a major event
    if (event_code & 0x80)
    {
            // Major Events
            event_code = event_code << 4;		// Place in MS nibble
            day = LIFE_Get_Current_Day();

            // Format Day to fit into Diag data Structure
            //day_h = day & 0x0F00;
            day_h = day >> 8;
            day_l = day & 0x00FF;
            event_code = event_code | day_h;

            event_buf[0] = event_code;
            event_buf[1] = day_l;

            // At this point need to write Hist events into EE memory
            /*
             * 1. Write Event Code/Day at Queue pointer address
             * 2. Update Queue pointer and Checksum
             * 3. Write back into EEProm
             */

            // Record Event in EEProm History
            Memory_Write_Block(2, &event_buf[0], event_ptr[DIAG_QUE_PTR_MAJOR]);

            // Update Queue Pointer
            event_ptr[DIAG_QUE_PTR_MAJOR]+= 2;

            if(event_ptr[DIAG_QUE_PTR_MAJOR] > DIAG_MAJOR_QUE_END )
            {
                event_ptr[DIAG_QUE_PTR_MAJOR] = DIAG_MAJOR_QUE_START;
            }

    }
    else
    {

            // Minor Events
            event_code = event_code << 4;		// Place in MS nibble
            day = LIFE_Get_Current_Day();

            // Format Day to fit into Diag data Structure
            //day_h = day & 0x0F00;
            day_h = day >> 8;
            day_l = day & 0x00FF;
            event_code = event_code | day_h;

            event_buf[0] = event_code;
            event_buf[1] = day_l;

            // At this point need to write Hist events into EE memory
            /*
             * 1. Write Event Code/Day at Queue pointer address
             * 2. Update Queue pointer and Checksum
             * 3. Write back into EEProm
             */

            // Record Event in EEProm History
            Memory_Write_Block(2, &event_buf[0], event_ptr[DIAG_QUE_PTR_MINOR]);

            // Update Queue Pointer
            event_ptr[DIAG_QUE_PTR_MINOR]+= 2;

            if(event_ptr[DIAG_QUE_PTR_MINOR] < DIAG_MINOR_QUE_START )
            {
                event_ptr[DIAG_QUE_PTR_MINOR] = DIAG_MINOR_QUE_START;
            }

    }
        // Update the Queue Checksum
        // Add the 2 Queue pointers and write into queue csum
        event_ptr[DIAG_QUE_PTR_CSUM] = (UCHAR)(event_ptr[DIAG_QUE_PTR_MAJOR]
                + event_ptr[DIAG_QUE_PTR_MINOR]);

        Memory_Write_Block(3, &event_ptr[0], DIAG_QUE_CKSUM);


	// Test Event for counter updates
	// Add Events if event counter is supported for that event
	switch(event)
	{
		case HIST_POWER_RESET:
                    count_address = DIAG_CNT_POWER_RESET;
		break ;

		case HIST_PTT_EVENT:
                    count_address = DIAG_CNT_PTT;
		break ;

		case HIST_PEAK_BTN_EVENT:
                    count_address = DIAG_CNT_PEAK_BTN;
		break ;

                case HIST_LOW_BATT_MODE:
                   count_address = DIAG_CNT_LOW_BATT;
 		break ;

                case HIST_ABN_LOW_BATT_EVENT:
                   count_address = DIAG_CNT_LOW_BATT;
 		break ;

            case HIST_ALARM_ON_EVENT:
                   count_address = DIAG_CNT_ALARM_ON;

                    // Also good place to record Alarm CO PPM
                   Memory_Byte_Write_Verify((UCHAR)(wPPM & 0xff),DIAG_CO_LAST_ALARM);
                   Memory_Byte_Write_Verify((UCHAR)(wPPM >> 8),DIAG_CO_LAST_ALARM + 1);

		break ;

		case HIST_SENS_SHORT_FAULT:
                   count_address = DIAG_CNT_SENS_SHORT;
		break ;

		case HIST_SENS_HEALTH_FAULT:
                   count_address = DIAG_CNT_SENS_HEALTH;
 		break ;

		case HIST_AC_POWER_ON:
                   count_address = DIAG_CNT_AC_POWER;
		break ;

                default:
                    // Not a counter event
                    count_address = EE_UNUSED;
                break;

        }

        if(count_address != EE_UNUSED)
        {
            // Check/Update the count here
            UCHAR count = Memory_Byte_Read(count_address);
            if(count < 255)
            {
                count++;
                Memory_Byte_Write_Verify(count,count_address);
            }
        }

}

/* Record Battery Voltages
 * Battery voltages are recorded at end of Day 1, Year 1 and
 * at the end of each 24 hour period.
 * 
 * Inputs - battime = (BAT_DAY_ONE, BAT_YEAR_ONE or BAT_CURRENT)
 *          batvolt =  value to be recorded
 *
 * Outputs - none
 */
void Diag_Hist_Bat_Record(UCHAR battime, UINT batvolt)
{
    UCHAR buffer[2];

    // Load write buffer
    buffer[0] = (UCHAR)(batvolt & 0xFF);
    buffer[1] = (UCHAR)(batvolt >> 8);

    switch(battime)
    {
        case BAT_DAY_ONE:
             Memory_Write_Block(2, &buffer[0], DIAG_BATTERY_DAY_1);
            break;

       case BAT_YEAR_ONE:
             Memory_Write_Block(2, &buffer[0], DIAG_BATTERY_YEAR_1);
            break;

        case  BAT_CURRENT:
             Memory_Write_Block(2, &buffer[0], DIAG_BATTERY_CURRENT);
             break;
    }
}

/* Test for Peak CO update to record unit lifetime Peak CO
 *
 *     Inputs  - ppm value to test against the current peak value
 *               in history memory
 *
 *     Outputs - none
 */
void Diag_Hist_Test_Peak(UINT ppm)
{
    UINT day;

    // Do not record Peak CO if in CO fault or if
    // unit is not calibrated.
    if ( CAL_STATE_CALIBRATED == CAL_Get_State() )
    {
        if(!FLAG_CO_FAULT_PENDING)
        {
            // If ppm > recorded peak CO, save new peak CO
            // If ppm > recorded peak CO, save new peak CO
            UINT peak_low = (UINT)Memory_Byte_Read(DIAG_PEAK_CO_VALUE);
            UINT peak_high = (UINT)Memory_Byte_Read(DIAG_PEAK_CO_VALUE + 1);
            UINT  peak = ((peak_high << 8 ) + peak_low);

            // TODO: Test only, comment out
            /*SER_Send_String("PK- ");
            SER_Send_Int('h', peak_high, 16) ;
            SER_Send_Prompt() ;
            SER_Send_Int('l', peak_low, 16) ;
            SER_Send_Prompt() ;
            */

            if(ppm > peak)
            {
                // Write New peak
                UCHAR low = (UCHAR)(ppm & 0xff);
                UCHAR high = (UCHAR)(ppm >> 8);
                Memory_Byte_Write_Verify(low,DIAG_PEAK_CO_VALUE);
                Memory_Byte_Write_Verify(high,DIAG_PEAK_CO_VALUE + 1);

                // Update Peak Day
				day = LIFE_Get_Current_Day();
                low = (UCHAR)(day & 0xff);
                high = (UCHAR)(day >> 8);
                Memory_Byte_Write_Verify(low,DIAG_PEAK_CO_DAY);
                Memory_Byte_Write_Verify(high,DIAG_PEAK_CO_DAY + 1);

            }
        }
    }
	
}


// Added to monitor # of EE Write errors encountered
void Diag_Hist_EE_Error(void)
{
    // Inc EE Error Count
    UCHAR count = Memory_Byte_Read(DIAG_CNT_EE_ERR);
    if(count < 255)
    {
        count++;
        Memory_Byte_Write(count,DIAG_CNT_EE_ERR);
    }
}
#endif