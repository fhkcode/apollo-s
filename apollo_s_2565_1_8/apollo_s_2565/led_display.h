#ifndef LED_DISPLAY_H_
#define LED_DISPLAY_H_



    void Display_Refresh_Task(void) ;
    UINT Display_Update_Task(void) ;
    void Display_Set_Intensity(UCHAR) ;
    UCHAR Display_Get_Intensity(void) ;
    UCHAR Display_Get_Intensity_Int(void) ;

    void Display_Off(void);

#define	LED_DISP_OFF	0
#define	LED_DISP_BRIGHT	1
#define	LED_DISP_MEDIUM	2
#define	LED_DISP_LOW	5



#endif /*LED_DISPLAY_H_*/
