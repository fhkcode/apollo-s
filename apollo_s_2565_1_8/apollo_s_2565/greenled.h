/****************************************************************
 * The contents of this file are Kidde proprietary and confidential.
 * *************************************************************/
// GREENLED.H
//
//  This file contains Green LED definitions.
//

#ifndef	_GREENLED_H_
#define _GREENLED_H_

	// Prototypes
	UINT GLED_Manager(void) ;
        UINT GLED_Blink(void);
	void GLED_On(void) ;
	void GLED_Off(void) ;

        //UINT GLED_Light_Sense(void);
#endif

