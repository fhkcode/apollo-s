/****************************************************************
 * The contents of this file are Kidde proprietary and confidential.
 * *************************************************************/
// PEAK_BUTTON.H

#ifndef	PEAK_BUTTON_H
#define	PEAK_BUTTON_H


        unsigned int Peak_BTN_Task(void) ;
        void Peak_Button_Init(void) ;
        unsigned char Peak_BTN_Get_State(void) ;

	
	#define PEAK_BTN_STATE_IDLE             0
	#define	PEAK_BTN_STATE_DEBOUNCE         1
	#define	PEAK_BTN_STATE_FUNCTION         2
    #define PEAK_BTN_STATE_NIGHTLIGHT       3
    #define PEAK_BTN_STATE_WAIT_RELEASE    4
	

#endif

