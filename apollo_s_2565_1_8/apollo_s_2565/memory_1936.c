/****************************************************************
 * The contents of this file are Kidde proprietary and confidential.
 * *************************************************************/
// memory_1936.c
// Routines for EEProm Memory support
#define _MEMORY_1936_C


#include    "common.h"
#include    "memory_1936.h"
#include    "fault.h"
#include    "diaghist.h"
#include    "systemdata.h"


/******************************************************************************
;	Read byte from EE memory
;	Inputs - addr holds address to read
;	Outputs - returns read data byte
;
*/
UCHAR Memory_Byte_Read(UCHAR addr)
{

    // Load EE Address to read
    EEADRL = addr;

    EECON1bits.CFGS = 0;
    EECON1bits.EEPGD = 0;
    EECON1bits.RD = 1;

    // Set EEADR to unused location when not reading/writing EE memory
    EEADRL = EE_UNUSED;

    return(EEDAT);

}

/******************************************************************************
;Function:	Memory_Byte_Write
; Description:	Writes 1 byte at EEProm address
;
; In:           byte:       Data to be written into EEProm
;               dest:       Address of first EE byte (0-255)
; Out: 		TRUE - Write verified
;               FALSE - Write error
*/
UCHAR Memory_Byte_Write(UCHAR byte, UCHAR dest)
{
    CLRWDT();

    // Set EE address of write (0-255)
    EEADRL = dest;

    // Load Data to be written
    EEDAT = byte;

    // Disable Interrupts
    INTCONbits.GIE = 0;

    // Not sure this is needed, but some have recommended clearing EEIF.
    PIR2bits.EEIF = 0;

    EECON1bits.CFGS = 0;
    EECON1bits.EEPGD = 0;
    EECON1bits.WREN = 1;

    EECON2 = 0x55;
    EECON2 = 0xAA;

    // Begin EE Write
    EECON1bits.WR = 1;

    EECON1bits.WREN = 0;

    if(FLAG_MODE_ACTIVE)
    {
        // Re-Enable Interrupts (but not if in Low Power Mode)
        INTCONbits.GIE = 1;
    }

    // Wait for the write complete
    // WR flag is cleared by hardware after completion
    while(EECON1bits.WR);

    // Short delay before verify read
    UCHAR delay = 10;
    while(--delay != 0);

    // Verify that the byte was written correctly
    UCHAR v_byte = Memory_Byte_Read(dest);

    // Set EEADR to unused location when not reading/writing EE memory
    EEADRL = EE_UNUSED;

    if(byte == v_byte)
    {
        // Verify confirmed
        return TRUE;
    }
    else
    {
        // Write Verify error
        return FALSE;
    }
}


/******************************************************************************
; Function:	Memory_Read_Block
; Description:	Reads n bytes starting at an address
; In:           num_bytes:  Number of sequential bytes to read
;               source:     Starting address in EE Memory (0 - 255)
;               dest:       Address of first byte in RAM
; Out: 		None
*/
void Memory_Read_Block(UCHAR num_bytes, UCHAR source, UCHAR *dest)
{
    for(UCHAR i=0; i<num_bytes; i++, source++, dest++)
    {
        *dest = Memory_Byte_Read(source);
    }
}

/******************************************************************************
;Function:	Memory_Write_Block
; Description:	Writes n bytes starting at an address
;        
; In:           num_bytes:  Number of sequential bytes to write
;               source:     Pointer to starting address in Ram 
;               dest:       Address of first EE byte (0-255)
; Out: 		TRUE - write verified
;               FALSE - write error
*/
UCHAR Memory_Write_Block(UCHAR num_bytes, UCHAR *source, UCHAR dest)
{
    // Init Error Flag
    FLAG_EE_WRITE_ERR = 0;

    for(UCHAR i = 0; i<num_bytes; i++)
    {
        if(FALSE == Memory_Byte_Write(*source, dest))
        {
            // Memory Write Error
            Diag_Hist_EE_Error();   // Count this error in DIAG_CNT_EE_ERR

            // Try again if Write Verify was not confirmed
            if(FALSE == Memory_Byte_Write(*source, dest))
            {
                // Memory Write Error
                Diag_Hist_EE_Error();   // Count this error in DIAG_CNT_EE_ERR
                // For now just set an error Flag
                FLAG_EE_WRITE_ERR = 1;
            }
        }
        source++;
        dest++;
    }
    
    // Check Error Flag and return result
    if(FLAG_EE_WRITE_ERR)
    {
        return FALSE;
    }
    else
    {
        return TRUE;
    }
}


/******************************************************************************
;Function:	Memory_Byte_Write with verify
; Description:	Writes 1 byte at EEProm address
;               Verifies the write and retries 1 time if needed
;
; In:           byte:       Data to be written into EEProm
;               dest:       Address of first EE byte (0-255)
; Out: 		TRUE - Write verified
;               FALSE - Write error
;
;
*/
UCHAR Memory_Byte_Write_Verify(UCHAR byte, UCHAR dest)
{
    if(FALSE == Memory_Byte_Write(byte, dest) )
    {
        // Memory Write Error
        Diag_Hist_EE_Error();   // Count this error in DIAG_CNT_EE_ERR
        if(FALSE == Memory_Byte_Write(byte, dest) )
        {
            // Memory Write Error
            Diag_Hist_EE_Error();   // Count this error in DIAG_CNT_EE_ERR
            return FALSE;
        }
    }
    return TRUE;
}

