/****************************************************************
 * The contents of this file are Kidde proprietary and confidential.
 * *************************************************************/
// battery.c
// Functions for battery test.
#define	_BATTERY_C

#include    "common.h"
#include    "battery.h"

#include    "a2d.h"
#include    "sound.h"
#include    "diaghist.h"
#include	"memory_1936.h"
#include    "led_display.h"

#define	BAT_STATE_INIT		0
#define	BAT_STATE_IDLE		1

// Test Only
//#define	BAT_SAMPLE_INTERVAL_MS		5000 	// 5 seconds
//#define BAT_TST_LONG			1	// 1 interval

#define	BAT_SAMPLE_INTERVAL_MS		60000 	// one minute
#define	BAT_DELAY_TIME_MS			4000 	// milliseconds
#define BAT_INIT_TIME               5000    // 5 seconds

// A/D Reference for PIC = VDD
// FVR to calcultae VDD = 1024 mv
#define FVR_MVOLTS			1024
// FVR_MVOLTS * 1024
#define FVR_FACTOR                      1048576


#ifdef _CONFIGURE_VOICE

// Voice Models
// Using divider of 2 equal values (measured average)
// Low Battery Threshold in mv 
#define	BAT_LB_THRESHOLD_MVOLTS         2650
// Detected Thresholds at A/D pin (resistor divider)
// Divider is 29.4 ohms over 21.5 ohms
// current = 2.65V/50.9 ohms = 51.1 mA
// V LowBat = 21.5 ohms X 52.06 mA = 1.119 Volts

// Avg LB at 2.65 Measured Value
#define BAT_LB_DETECT_MVOLTS            1077

// Abnormmaly low Threshold to detect open Battery Connection
#define BAT_LB_ABNORMAL_TH              250


// 50 mv of Low Battery Hush (2600 mv)
#define BAT_LB_HUSH_OFF_BAT_MVOLTS          (BAT_LB_THRESHOLD_MVOLTS - 50)
// Detected Thresholds at A/D pin (resistor divider)
// Divider is 29.4 ohms over 21.5 ohms
// Hush voltage = 21.5/50.9 X 50mV = 21 mvolts
// Bat Hush Off Threshold
#define BAT_LB_HUSH_OFF_MVOLTS          (BAT_LB_DETECT_MVOLTS - 21)

#else

// NON - Voice Models
// Using divider of 2 equal values (measured average)
// Low Battery Threshold in mv
#define	BAT_LB_THRESHOLD_MVOLTS         2650
// Detected Thresholds at A/D pin (resistor divider)
// Divider is 43.2 ohms over 32.4 ohms
// current = 2.65V/75.6 ohms = 35.05 mA
// V LowBat = 32.4 ohms X 35.05 mA = 1.114 Volts

// Avg LB at 2.65 Measured Value
#define BAT_LB_DETECT_MVOLTS            1114

// Below this level is abnormally Low, probably open battery connection
//  or battery activation switch
#define BAT_LB_ABNORMAL_TH              250

// 50 mv of Low Battery Hush (2600 mv)
#define BAT_LB_HUSH_OFF_BAT_MVOLTS          (BAT_LB_THRESHOLD_MVOLTS - 50)
// Detected Thresholds at A/D pin (resistor divider)
// Divider is 29.4 ohms over 21.5 ohms
// Hush voltage = 21.5/50.9 X 50mV = 21 mvolts
// Bat Hush Off Threshold
#define BAT_LB_HUSH_OFF_MVOLTS          (BAT_LB_DETECT_MVOLTS - 21)

#endif

// 10 mv of hysteresis
#define BAT_LB_HYSTERISIS_MVOLT         (BAT_LB_DETECT_MVOLTS + 10)

#define BAT_TST_SHORT					1		// 1 Minute Test interval
#define BAT_TST_LONG					720		// 12 Hour interval
#define BAT_TST_ALG                     60              // 1 Hour interval

#define BAT_TST_STARTUP_TIME            30		// 1/2 Hour

#define BAT_TEST_1MS_PULSE_AC           1               // ms
#define BAT_TEST_50MS_PULSE_DC          50              // ms



#ifdef	_CONFIGURE_SHORT_HUSH_TEST
	#define	TIME_LB_HUSH_MINUTES	3		// 3 minutes 
#else
	#define	TIME_LB_HUSH_MINUTES	600		// 10 Hour 
    //#define	TIME_LB_HUSH_MINUTES	1440		// 24 Hour 
#endif

static unsigned char BAT_Battery_State = BAT_STATE_INIT;
static unsigned int  BAT_Tst_Timer;
static unsigned char BAT_Startup_Timer;
static unsigned int  BAT_FVR_Measured;
volatile unsigned int BAT_Last_Volt;

volatile unsigned int BAT_Scale;

#ifdef _CONFIGURE_LB_HUSH
    static unsigned int	BAT_LB_Hush_Timer;
#endif

// Local Protoypes
UINT BAT_Do_Test(void);
    

// ***********************************************************************
UINT BAT_Do_Test(void)
{
	UINT retval ;
	
	// To make the battery test, raise the battery test pin
	// and do an A/D conversion.

	BAT_TEST_ON
                
        if(FLAG_AC_DETECT)
        {
            // Fast Battery Test time (no capacitor)
            delay_ms(BAT_TEST_1MS_PULSE_AC);
        }
        else
        {
            // Add delay to discharge Capacitance on VBat
            delay_ms(BAT_TEST_50MS_PULSE_DC);
        }

        // Read 1.024 FVR for VDD calculation
	BAT_FVR_Measured = A2D_Convert(A2D_FVR_VOLTAGE) ;

	retval = A2D_Convert(A2D_BATTERY_VOLTAGE) ;

	BAT_TEST_OFF

        // Fixed Vref Off after testing
        FVRCONbits.FVREN = 0;

        // Test only
        //SER_Send_Int('b', BAT_FVR_Measured, 10) ;
        //SER_Send_Prompt() ;

        return retval ;
}

// ***********************************************************************
unsigned int BatteryTest_Task(void)
{
    switch	(BAT_Battery_State)
    {
        case	BAT_STATE_INIT:
            // Init Battery Test I/O
            BAT_TEST_OFF

            //  start with 1 minute Battery Test
            BAT_Tst_Timer = BAT_TST_SHORT	 ;
            FLAG_BAT_TST_SHORT = 1;

            BAT_Startup_Timer = BAT_TST_STARTUP_TIME;

            BAT_Battery_State = BAT_STATE_IDLE ;
		
			
            return Make_Return_Value(BAT_INIT_TIME) ;


        case	BAT_STATE_IDLE:
            // TODO: Remove, test only to measure avg stby w/o battery pulse
            // return  Make_Return_Value(BAT_SAMPLE_INTERVAL_MS) ;

            // First check to see if any conditions exist that would delay
            // the battery test.
            // 1.  If in alarm, no reason to do a battery test since alarm
            // overrides low battery.
            // 2. Delay if LEDs or Voice is active

            if (FLAG_CO_ALARM_ACTIVE)
            {
                 return	Make_Return_Value(BAT_DELAY_TIME_MS) ;
            }

            #ifdef _CONFIGURE_VOICE
                else if( FLAG_VOICE_ACTIVE == 1 )
                {
                   return   Make_Return_Value(BAT_DELAY_TIME_MS) ;
                }
            #endif

            else
            {
                #ifdef _CONFIGURE_LB_HUSH
                  if(FLAG_LB_HUSH_ACTIVE)
                  {
                    if(--BAT_LB_Hush_Timer == 0)
                    {
                        FLAG_LB_HUSH_ACTIVE = 0 ;
                        Payload.cmd_returns = 0x03;
                    }
                  }
                #endif

                if(--BAT_Tst_Timer == 0)
                {
                    unsigned int batvolt = BAT_Do_Test() ;		// Test Battery under Load

                    // Calculate current VDD voltage
                    unsigned long BAT_Vdd = (FVR_FACTOR / BAT_FVR_Measured);

                    // From VDD calculation, calculate the Battery Test Voltage
                    unsigned long BAT_Measured = ((unsigned long)(BAT_Vdd * batvolt)/1024);

                    // Save for battery Recording
                    BAT_Last_Volt = (UINT)BAT_Measured;
					
					
                    #ifndef _CONFIGURE_PRODUCTION_UNIT
                        // Test only:
                        // For production firmware we only want to record
                        // current battery at the end of each day, to preserve
                        // the history value in case of customer returns.
                        if(bOneMinuteTic == 0)
                        {
                            // Init Battery Current History for 1st battery test
                            //   which occurs before 1st Minute after Power On
                            Diag_Hist_Bat_Record(BAT_CURRENT, BAT_Last_Volt);
                        }
                    #endif

                    #ifdef _CONFIGURE_LB_HUSH
                        // Update Low battery Hush Inhibit Flag
                        if (BAT_Measured < BAT_LB_HUSH_OFF_MVOLTS)
                        {
                            FLAG_LB_HUSH_INHIBIT = 1;
                        }
                        else
                        {
                            FLAG_LB_HUSH_INHIBIT = 0;
                        }
                    #endif

                    // The low battery flag is only set if the battery voltage is
                    // below the threshold and low battery pending is set.
                    if (BAT_Measured < BAT_LB_DETECT_MVOLTS)
                    {

                        if (FLAG_LB_PENDING == 1)
                        {
                            #ifdef _CONFIGURE_DIAG_HIST
                                // Only Record if entering LB
                                if(!FLAG_LOW_BATTERY)
                                {
                                    // Test for the LB event we want to record
                                    if(BAT_Measured < (UCHAR)BAT_LB_ABNORMAL_TH)
                                    {
                                        // Possibly an Open Battery Connection or Switch
                                        Diag_Hist_Que_Push(HIST_ABN_LOW_BATT_EVENT);
                                    }
                                    else
                                    {
                                        Diag_Hist_Que_Push(HIST_LOW_BATT_MODE);
                                    }
                                }
                            #endif

                            FLAG_LOW_BATTERY = 1 ;
                        }
                        else
                        {
                            FLAG_LB_PENDING = 1 ;
                        }


                    }
                    else if (BAT_Measured > (BAT_LB_HYSTERISIS_MVOLT))
                    {
                        FLAG_LB_PENDING = 0;
                        FLAG_LOW_BATTERY = 0 ;
                        // Insure LB Hush is Off also
                        FLAG_LB_HUSH_ACTIVE = 0;
                    }

//                    if (FLAG_SERIAL_PORT_ACTIVE && (SERIAL_ENABLE_BATTERY) )
//                    {
//                        //SER_Send_Int('R', BAT_FVR_Measured, 10) ;
//                        //SER_Send_Prompt() ;
//                        //SER_Send_Int('D', BAT_Vdd, 10) ;
//                        //SER_Send_Prompt() ;
//
//                        SER_Send_Int('B', BAT_Measured, 10) ;
//                        if (FLAG_LOW_BATTERY)
//                        {
//                            if(FLAG_LB_HUSH_ACTIVE)
//                                SER_Send_Char('"') ;
//                            else
//                                SER_Send_Char('!') ;
//
//                            if(FLAG_LB_HUSH_INHIBIT)
//                                SER_Send_Char('!') ;    // Add another ! if very low battery
//
//                        }
//                        SER_Send_Char(',') ;
//                        SER_Send_Char('L') ;
//                        SER_Send_Int('B', BAT_LB_DETECT_MVOLTS, 10) ;
//
//                        SER_Send_Prompt() ;
//                    }

                    // Reset the test interval
                    BAT_Set_Test_Interval(INTERVAL_TIMER_RESET);
                }
            }
 
    }
    return  Make_Return_Value(BAT_SAMPLE_INTERVAL_MS) ;
}


// ***********************************************************************
#ifdef _CONFIGURE_LB_HUSH

void BAT_Init_LB_Hush(void)
{
    FLAG_LB_HUSH_ACTIVE = 1 ;
    BAT_LB_Hush_Timer = TIME_LB_HUSH_MINUTES ;

    // LB Hush indication to start immediately
    MAIN_Reset_Task(TASKID_GLED_MANAGER) ;
		
}

#endif

// *****************************************************************************
// Reset battery test interval based upon light Algorithm status
//  and time of operation after reset.
void BAT_Set_Test_Interval(UCHAR mode)
{

    #ifdef _CONFIGURE_ACCEL_LIGHT_TEST
         // For testing only use 1 minute battery test times
         BAT_Tst_Timer = BAT_TST_SHORT ;
        return;
    #else

        // Reset The Battery Test Timer to proper interval
        if(FLAG_BAT_TST_SHORT)
        {
            if(--BAT_Startup_Timer == 0)
            {
                // This switches to long battery test mode
                FLAG_BAT_TST_SHORT = 0;
            }
            BAT_Tst_Timer = BAT_TST_SHORT ;

        }
        else
        {
            // If the Light Algorithm is enabled, we can test battery
            //  more often (1 Hour instead of 12 hours)
            if(FLAG_LIGHT_ALG_ON)
            {
                // The Light algorithm is enabled 1 Hour Test Interval
                BAT_Tst_Timer = BAT_TST_ALG ;
            }
            else
            {
                if(mode == ALG_RESET)
                {
                    // Do nothing, allow current interval to complete
                }
                else
                {
                    // Longer Battery test Interval (12 hours)
                    BAT_Tst_Timer = BAT_TST_LONG ;
                }
            }
        }
    #endif
}