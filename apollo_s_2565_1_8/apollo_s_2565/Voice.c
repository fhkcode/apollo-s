/****************************************************************
 * The contents of this file are Kidde proprietary  and confidential.
 * *************************************************************/
// voice.c
#define	_VOICE_C

#include	"common.h"
#include	"voice.h"
#include	"cocalibrate.h"
#include 	"systemdata.h"

#ifdef	_CONFIGURE_VOICE

//***********************************************************
// If voice and data lines need to be inverted, uncomment.
#define	_INVERT_VOICE_CLOCK_DATA
//***********************************************************

// Define Voice Clock and Data I/O actions
#ifdef	_INVERT_VOICE_CLOCK_DATA

	#define	VOICE_CLOCK_HIGH    PORT_VOICE_CLK=0;
	#define	VOICE_CLOCK_LOW     PORT_VOICE_CLK=1;

	#define	VOICE_DATA_HIGH     PORT_VOICE_DATA=0;
	#define	VOICE_DATA_LOW      PORT_VOICE_DATA=1;

#else

	#define	VOICE_CLOCK_HIGH    PORT_VOICE_CLK=1;
	#define	VOICE_CLOCK_LOW     PORT_VOICE_CLK=0;

	#define	VOICE_DATA_HIGH     PORT_VOICE_DATA=1;
	#define	VOICE_DATA_LOW      PORT_VOICE_DATA=0;

#endif

#define	POWERUP_VOICE_DELAY         200     // in MS
#define VOICE_DWELL_3SEC_TIME       3000    // 3 secs
#define VOICE_DWELL_1SEC_TIME       1000    // 1 sec

// Voice chip requires 10 ms tic times
#define VOICE_CLOCK_TIME             10     // in MS  (clock pulse time)

#define	VCE_STATE_IDLE			0
#define	VCE_STATE_CLOCK_ACTIVE		1
#define	VCE_STATE_CLOCK_INACTIVE	2
#define	VCE_STATE_END			3
#define VCE_STATE_DWELL                 4

static UCHAR	bVoiceState = 0;
static UCHAR 	bCurrentVoiceCode = 0;
static UCHAR 	bBitCounter = 0;



UINT VCE_Process(void)
{
	switch (bVoiceState)
	{
		case VCE_STATE_IDLE:

                        FLAG_VOICE_ACTIVE = 0;

			//
			// FLAG_VOICE_SEND will be set to begin voice chip
                        //   serial communication
			//
			if (!FLAG_VOICE_SEND)
			{

                          #ifndef _CONFIGURE_CANADA
                          // Do not send dual message for LB Hush w/Canada Voice
                            if(FLAG_HUSH_SEND)
                            {
                                // Announce Hush Mode
                                VCE_Play(VCE_HUSH_ACTIVATED) ;
                                FLAG_HUSH_SEND = 0;
                            }
                           #endif

                            break ;
			}

                        // Indicate Voice Circuit Active
                        FLAG_VOICE_ACTIVE = 1;

			//
			// Set clock High to start.
			//
			VOICE_CLOCK_HIGH

			//
			// Initialize the bit counter and go to next state.
			//
                        // Proceed to Clock states
			bVoiceState++ ;
                        // 8 bits
			bBitCounter = 8 ;

			return Make_Return_Value(POWERUP_VOICE_DELAY) ;

		case VCE_STATE_CLOCK_ACTIVE:
			// Clock Pulse active
			VOICE_CLOCK_HIGH 

			//
			// Clock out bit.  Voice module expects the data
			// LSB first.
			//
			if (bCurrentVoiceCode & (0x80>>--bBitCounter))
			{
				// Bit to send is a one.
				VOICE_DATA_HIGH
			}
			else
			{
				// Bit to send is a zero.
				VOICE_DATA_LOW
			}
                        // Proceed to Clock inactive state
			bVoiceState++ ;

			break ;

		case VCE_STATE_CLOCK_INACTIVE:
			if ( 0 == bBitCounter)
			{
                                // Transmission complete
				bVoiceState = VCE_STATE_END ;

			}
			else
			{
				// Proceed to Clock Active state
				bVoiceState-- ;
			}			
			
			// Clock Pulse inactive
			VOICE_CLOCK_LOW 
			break ;
			
		case VCE_STATE_END:
                        VOICE_CLOCK_HIGH
                        VOICE_DATA_HIGH
                        FLAG_VOICE_SEND = 0 ;
                        bVoiceState = VCE_STATE_DWELL ;
			break ;

                case VCE_STATE_DWELL:
                        bVoiceState = VCE_STATE_IDLE ;
                        // Add Dwell Time Here to Allow Voice to Complete
                        //  This keeps the VOICE_ACTIVE Flag on during
                        //  Voice annunciation time
                        if(FLAG_HUSH_SEND)
                            return Make_Return_Value(VOICE_DWELL_1SEC_TIME) ;
                        else
                            return Make_Return_Value(VOICE_DWELL_3SEC_TIME) ;
			
	}
	
	return Make_Return_Value(VOICE_CLOCK_TIME) ;
}

//**************************************************************************
UCHAR	VCE_Play(UCHAR voice)
{

    // Don't announce voice messages during CO 0/150 Calibration
    UCHAR calstate = CAL_Get_State() ;
    if ( !((CAL_STATE_VERIFY == calstate) ||
             (CAL_STATE_CALIBRATED == calstate)) )
    {
        return FALSE ;
    }


#ifdef	_CONFIGURE_PEAK_BUTTON

    // Since Peak button and voice clock are shared, test for Peak Btn Active
    if(FLAG_PEAK_BUTTON_NOT_IDLE == 0)
    {
	if (FLAG_VOICE_SEND)
	{
            return FALSE ;
	}
	
	FLAG_VOICE_SEND = 1 ;


        // Insure Voice I/O has been set
        // Disable Peak Button Input (voice clk function enabled)
        TRISBbits.TRISB6 = 0;

	bCurrentVoiceCode = voice ;

	return (TRUE) ;
    }

    return(FALSE);

#else

    if (FLAG_VOICE_SEND)
    {
        return FALSE ;
    }

    FLAG_VOICE_SEND = 1 ;

    bCurrentVoiceCode = voice ;

    return (TRUE) ;

#endif

}

#else

// No Voice option
UINT VCE_Process(void)
{
    return Make_Return_Value(61000) ;
}

UCHAR VCE_Play(UCHAR voice) 
{
    return (TRUE);
}


#endif


