/****************************************************************
 * The contents of this file are Kidde proprietary and confidential.
 * *************************************************************/
// COMEASURE.H

#ifndef _COMEASURE_H
#define _COMEASURE_H

    // Prototypes
    UINT COMeasureTask(void) ;
    void MEAS_Reset(void) ;
    void MEAS_CO_Charge(UINT) ;
		
    // Global variables
    extern UINT wPPM ;
    extern UINT wRawCOCounts ;
    extern UINT wRawAvgCOCounts ;
    extern UINT wVout_Baseline ;
    extern UINT CO_Vdd_Calc;
    #ifdef _CONFIGURE_MANUAL_PPM_COMMAND
        extern UINT ManualPPMValue;
    #endif


#endif
		
		
