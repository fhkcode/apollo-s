/****************************************************************
 * The contents of this file are Kidde proprietary and confidential.
 * *************************************************************/
// COALARM.H

#ifndef _COALARM_H
#define _COALARM_H

    //Prototypes
    UINT COAlarmTask(void) ;
    void COAlarm_Init(void) ;
 
    //
    //  Definition Statements
    //

    // For Functional Test use 40 PPM Alarm Threshold
    #define FUNC_CO_ALARM_THRESHOLD             40
    #define CO_PRESENT_THRESHOLD		100


#ifdef	_CONFIGURE_UL_ALARM_CURVE
	
    #define	AL_ALARM_THRESHOLD              8260

    //
    //  The threshold limit constant sets the accumulator limit.  The accumulator
    //  limit is not set to the alarm threshold because algorithm isn't allowed to
    //  to decay quickly enough.  The limit is set by subtracting the PPM value or
    //  a multiple thereof where the decay should begin, from the threshold.
    //
    #define	AL_ALARM_THRESHOLD_LIMIT        8108
    #define	AL_REALARM_THRESHOLD            7489

    #define AL_ACCUMULATOR_MINIMUM_THRESHOLD	45
    #define AL_ACCUMULATOR_COMP_THRESHOLD	38
    #define AL_ACCUMULATOR_DECAY_CONSTANT	13
    #define AL_ACCUMULATOR_DECAY_THRESHOLD	97

    #define AL_THRESHOLD_SLOPE_MULTIPLE		4
    #define AL_THRESHOLD_SLOPE_HIGH_MULTIPLE	8
    #define AL_THRESHOLD_SLOPE_THRESHOLD	228

#endif


//
//  BSI Alarm Definitions
//
#ifdef	_CONFIGURE_BSI_ALARM

    #define AL_RESET_SINGLE_THRESHOLD		82
    #define AL_RESET_INHIBIT_THRESHOLD		229

    #define	AL_ALARM_THRESHOLD		3590

    //
    //  The threshold limit constant sets the accumulator limit.  The accumulator
    //  limit is not set to the alarm threshold because algorithm isn't allowed to
    //  to decay quickly enough.  The limit is set by subtracting the PPM value or
    //  a multiple thereof where the decay should begin, from the threshold.
    //
    #define	AL_ALARM_THRESHOLD_LIMIT	3514

    //
    //  The realarm threshold must be high enough to ensure the test button logic
    //  activates the alarm immediately.
    //
    #define	AL_REALARM_THRESHOLD		3388

    #define AL_ACCUMULATOR_MINIMUM_THRESHOLD	45
    #define AL_ACCUMULATOR_COMP_THRESHOLD	38
    #define AL_ACCUMULATOR_DECAY_CONSTANT	18
    #define AL_ACCUMULATOR_DECAY_THRESHOLD	65

    #define AL_THRESHOLD_SLOPE_MULTIPLE		2
    #define AL_THRESHOLD_SLOPE_HIGH_MULTIPLE	8
    #define AL_THRESHOLD_SLOPE_THRESHOLD	206

#endif


//
//  CENELEC Alarm Definitions
//
#ifdef	_CONFIGURE_CENELEC_ALARM

    #define AL_RESET_SINGLE_THRESHOLD           82
    #define AL_RESET_INHIBIT_THRESHOLD          229

    // Set Forced Alarm Condition Threshold at 200 PPM
    #define	CENELEC_300_PPM_THRESHOLD	200

    #define	AL_ALARM_THRESHOLD		4200

    //
    //  The threshold limit constant sets the accumulator limit.  The accumulator
    //  limit is not set to the alarm threshold because algorithm isn't allowed to
    //  to decay quickly enough.  The limit is set by subtracting the PPM value or
    //  a multiple thereof where the decay should begin, from the threshold.
    //
    #define	AL_ALARM_THRESHOLD_LIMIT	4124

    //
    //  The realarm threshold must be high enough to ensure the test button logic
    //  activates the alarm immediately.
    //
    #define	AL_REALARM_THRESHOLD		4074

    #define AL_ACCUMULATOR_MINIMUM_THRESHOLD	45
    #define AL_ACCUMULATOR_COMP_THRESHOLD	32
    #define AL_ACCUMULATOR_DECAY_CONSTANT	18
    #define AL_ACCUMULATOR_DECAY_THRESHOLD	63

    #define AL_THRESHOLD_SLOPE_MULTIPLE		3
    #define AL_THRESHOLD_SLOPE_HIGH_MULTIPLE	14
    #define AL_THRESHOLD_SLOPE_THRESHOLD	192

    #define AL_CENELEC_PPM_AVG_MAX		90	//Performing Running Average if < 90 PPM

    #define AL_CENELEC_50_PPM_MAX		70
    #define AL_CENELEC_50_PPM			53
    #define AL_CENELEC_50_PPM_MIN		41


#endif



#endif
		
		
