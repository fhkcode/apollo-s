#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
# default configuration
CND_ARTIFACT_DIR_default=dist/default/production
CND_ARTIFACT_NAME_default=apollo_s_2565_1_0_1.production.hex
CND_ARTIFACT_PATH_default=dist/default/production/apollo_s_2565_1_0_1.production.hex
CND_PACKAGE_DIR_default=${CND_DISTDIR}/default/package
CND_PACKAGE_NAME_default=apollos2565101.tar
CND_PACKAGE_PATH_default=${CND_DISTDIR}/default/package/apollos2565101.tar
# 2565 configuration
CND_ARTIFACT_DIR_2565=dist/2565/production
CND_ARTIFACT_NAME_2565=apollo_s_2565_1_0_1.production.hex
CND_ARTIFACT_PATH_2565=dist/2565/production/apollo_s_2565_1_0_1.production.hex
CND_PACKAGE_DIR_2565=${CND_DISTDIR}/2565/package
CND_PACKAGE_NAME_2565=apollos2565101.tar
CND_PACKAGE_PATH_2565=${CND_DISTDIR}/2565/package/apollos2565101.tar
