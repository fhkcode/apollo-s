/****************************************************************
 * The contents of this file are Kidde proprietary and confidential.
 * *************************************************************/
// A2D.c
// Routines for Analog to digital support
#define _A2D_C

#include    "common.h"
#include    "a2d.h"
#include    "diaghist.h"

#ifdef	_CONFIGURE_ACDC
    static UCHAR ACDetectCount = 3;
#endif


/*;
;	A/D register bit assignments are different between PIC16F88x and PIC16F193x
;
;	ADCON0      16F88x		16LF193x and 16LF1509
;		    (Bank0)		(Bank1)
;
;	Bit 7		ADCS1		  -
;	Bit 6		ADCS2		CHS4		
;	Bit 5		CHS3		CHS3
;	Bit 4		CHS2		CHS2
;	Bit 3		CHS1		CHS1
;	Bit 2		CHS0		CHS0
;	Bit 1		GO/DONE		GO/DONE		
;	Bit 0		ADON		ADON
;
;	ADCON1      16F88x		16LF193x and 16LF1509
;                   (Bank1)		(Bank1)
;
;	Bit 7		ADFM		ADFM
;	Bit 6		  -  		ADCS2		
;	Bit 5		VCFG1		ADCS1
;	Bit 4		VCFG0		ADCS0
;	Bit 3		  - 		  - 
;	Bit 2		  - 		ADNREF
;	Bit 1		  - 		ADPREF1		
;	Bit 0		  - 		ADPREF0
;
*/


#define AD_CHANNEL_MASK  0b01111100
#define AD_CON1          0b11010000      // Right justified, FOSC/16 clk, VRef = VDD


#define	VREF_INIT_1024	0b10000001	// Set A/D reference at 1.024V, Enable FVR
#define	VREF_OFF		0x00            // Turn Off FVR (save power)




// *************************************************************
// channel:  constant ADC_CHAN_xxx found in a2d.h

/*;****************************************************
; Channel to convert is passed in channel (0 - 15).
; A 10 bit conversion is done with a right justified result.
; Result is returned in adc_value
*/
UINT A2D_Convert(UCHAR channel)
{
    CLRWDT();

    if(A2D_FVR_VOLTAGE == channel)
    {
        // Set FVR for 1.024 Voltage A/D Reference
        FVRCON = VREF_INIT_1024;
        // Wait for FVR stabilization
        do
        {
            //CLRWDT();

        }while(FVRCONbits.FVRRDY == 0);

        // Wait the required acquisition time, at least 20 microseconds.
        delay_us(60);     // Delay usecs
    }

    // Select the Channel
    ADCON0 = (channel & AD_CHANNEL_MASK);
    ADCON1 = AD_CON1;   // FVR is positive A/D reference

    // Enable A/D Power
    ADCON0bits.ADON = 1;

    // Wait the required AD ON time
    // Need about 50usecs for lower VDD voltages
    delay_us(50);     // Delay usecs

    /*	;
	; Start the conversion and wait for the Go/Done bit
	; to be cleared, which signals the end of the conversion.
    */
    ADCON0bits.ADGO = 1;
    do
    {
        //CLRWDT();

    }while(ADCON0bits.ADGO == 1);    // Wait for conversion complete

 
    // Turn Off A/D after measurement to save current
    ADCON0bits.ADON = 0;

    // Turn FVR Off
    FVRCON = VREF_OFF;
    
    return (ADRES) ;
}


#ifdef	_CONFIGURE_ACDC

// Monitor AC Detect voltage and update AC Power status
void A2D_Test_AC(void)
{
    UINT AD_AC_Volt;

    // Test Only
    //FLAG_AC_DETECT = 1 ;
    //return;

    
    AD_AC_Volt = A2D_Convert(A2D_AC_DETECT_VOLTAGE);

    if(AC_DETECT_THRESHOLD > AD_AC_Volt)
    {
        if(FLAG_AC_DETECT)
        {
            if(--ACDetectCount == 0)
            {
                FLAG_AC_DETECT = 0 ;
					
                ACDetectCount = 3;

                #ifdef _CONFIGURE_OUTPUT_AC_DETECT
                  if (FLAG_SERIAL_PORT_ACTIVE)
                  {
                        SER_Send_String("AC Off") ;
                        SER_Send_Prompt() ;
                  }
                #endif

                #ifdef _CONFIGURE_DIAG_HIST
                    Diag_Hist_Que_Push(HIST_AC_POWER_OFF);
                #endif

            }
        }
        else
        {
            ACDetectCount = 3;
        }

    }
    else
    {
        if(!FLAG_AC_DETECT)
        {
            if(--ACDetectCount == 0)
            {
                FLAG_AC_DETECT = 1 ;

                ACDetectCount = 3;

                #ifdef _CONFIGURE_OUTPUT_AC_DETECT
                  if (FLAG_SERIAL_PORT_ACTIVE)
                  {
                        SER_Send_String("AC On") ;
                        SER_Send_Prompt() ;
                  }
                #endif

                #ifdef _CONFIGURE_DIAG_HIST
                    Diag_Hist_Que_Push(HIST_AC_POWER_ON);
                #endif

            }
        }
        else
        {
            ACDetectCount = 3;
        }
    }

}

#endif




