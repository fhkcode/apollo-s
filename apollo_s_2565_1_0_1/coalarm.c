/****************************************************************
 * The contents of this file are Kidde proprietary and confidential.
 * *************************************************************/
// COALARM.C
// Contains functionality for CO alarm calculations.
#define _COALARM_C

#include	"common.h"
#include	"coalarm.h"
#include	"systemdata.h"
#include	"main.h"
#include	"comeasure.h"
#include	"cocalibrate.h"


// These constants are used with the Alarm Curve calculations.
// A value of 1.0 is equal to 0x10000, therefore these hex values are fractional
// constants.
#define AL_ALARM_SUM_CONSTANT		0xb80b		// == 0.7189
#define AL_PREALARM_SUM_CONSTANT	0x49fb		// == 0.289

// Located in Non-Reset Common memory
//volatile persistent unsigned int  AlarmPastSum  ;
//volatile persistent unsigned int  AlarmInterimSum ;
//volatile persistent unsigned int  AlarmSum ;

// Located in Non-Reset Common memory
 persistent static unsigned int  AlarmPastSum  ;
 persistent static unsigned int  AlarmInterimSum ;
 persistent static unsigned int  AlarmSum ;

#ifdef	_CONFIGURE_CENELEC_ALARM
// Preserved average, so declared here
	UINT w1SlopeAverage ;
#endif

// Global / Static Initialization
void COAlarm_Init(void)
{
    AlarmPastSum = 0 ;
    AlarmInterimSum = 0 ;
    AlarmSum = 0 ;
	
    #ifdef  _CONFIGURE_CENELEC_ALARM
	w1SlopeAverage	= 0 ;
    #endif

}

/*
;  This portion of the alarm algorithm uses the second order accumulation equation
;  given as follows:
;
;    Accum = INT( PreviousAccum * A1 + PrePreviousAccum * A2 + PPM - Constant )
;
;    Equation Definitions:
;
;      Accum            = new accumulation value
;      PreviousAccum    = previous accumulation value
;      PrePreviousAccum = Accumulation value before previous accumulation
;      A1               = Constant fraction
;      A2               = Constant fraction
;      PPM              = current CO reading in PPM with restrictions
;
;  The equation was derived to alarm at 7.5 percent COHb to allow for the
;  maximum amount of error on either side.  To alarm at 7.5 percent COHb
;  the unit must alarm at the following constant CO concentrations:
;
;  The UL standard states the unit must alarm according to the following
;  table:
;
;    CO        Minimum     Maximum     Geometric
;    Level     Alarm       Alarm       Center
;    (PPM)     Time        Time        Time
;              (minutes)   (minutes)   (minutes)
;  -----------------------------------------------
;      70        60.0       189.0       106.5
;     150        10.0        60.0        24.5
;     400         4.0        15.0         7.8
*/

//*************************************************************************
UINT COAlarmTask(void)
{
    UINT local ;
    unsigned long Alarm_Temp1;

	
#ifdef	_CONFIGURE_CENELEC_ALARM
    UINT wPPMBuffer	;
#endif
	
#ifdef _CONFIGURE_CO_FUNC_TEST
    if (FLAG_FUNC_CO_TEST)
    {
        // Functional CO Test Code here
        FLAG_CO_ALARM_CONDITION = 0;
        if (wPPM > FUNC_CO_ALARM_THRESHOLD)
        {
            FLAG_CO_ALARM_CONDITION = 1;
        }

        return Make_Return_Value(2000) ;

    }
#endif
    
  
    // Don't do calculation if we're in PTT mode.
    if (FLAG_PTT_ACTIVE)
    {
        return Make_Return_Value(30000) ;
    }
	
    // Don't calculate if we're not calibrated.
    UCHAR calstate = CAL_Get_State() ;
    if ( !((CAL_STATE_VERIFY == calstate) ||
             (CAL_STATE_CALIBRATED == calstate)) )
    {
            return Make_Return_Value(30000) ;   // milliseconds
    }

    #ifdef  _CONFIGURE_FORCE_COALARM
            // Use this to put unit in CO alarm for testing
            FLAG_CO_ALARM_CONDITION = 1 ;
            return Make_Return_Value(30000) ;
    #endif


    // Store the last accumulation value
    AlarmPastSum = AlarmInterimSum ;

    //
    //  Multiply previous accumulation value by fraction.
    //
    AlarmInterimSum = AlarmSum ;

    //local = (UINT)( ((unsigned long)AlarmInterimSum * AL_ALARM_SUM_CONSTANT) >> 16) ;
     Alarm_Temp1 = ((unsigned long)AlarmInterimSum * AL_ALARM_SUM_CONSTANT);
    local = Alarm_Temp1 >> 16;
	
#ifdef	_CONFIGURE_CENELEC_ALARM
    // Because Alarm Curve is so flat at 50 PPM, Set PPM value
    // to a common value when within a MIN/MAX Window for Alarm Time stablilty.
    // in Cenelec configuration only
    if (FLAG_CALIBRATION_COMPLETE)
    {
            if (wPPM < AL_CENELEC_PPM_AVG_MAX)
            {
                    w1SlopeAverage = ((3 * w1SlopeAverage) + wPPM) / 4 ;
                    wPPM = w1SlopeAverage ;
            }
    }

    wPPMBuffer = wPPM ;

    if ( (wPPM < AL_CENELEC_50_PPM_MAX) && (wPPM > AL_CENELEC_50_PPM_MIN) )
    {
            wPPMBuffer = AL_CENELEC_50_PPM ;
    }
#endif

    //
    //  If the PPM level is not greater than the accumulator decay constant,
    //  do not perform the curve calculation as the accumulator will not decay
    //  after it has reached a certain value.
    //
    if ( wPPM >= AL_ACCUMULATOR_MINIMUM_THRESHOLD)
    {
        /*
        AlarmSum = local +
         (UINT)( ((unsigned long)AlarmPastSum * AL_PREALARM_SUM_CONSTANT) >> 16) ;
        */

        Alarm_Temp1 = (unsigned long)AlarmPastSum * AL_PREALARM_SUM_CONSTANT;
        Alarm_Temp1 = Alarm_Temp1 >> 16;
        AlarmSum = local + Alarm_Temp1;

    }
	
#ifdef	_CONFIGURE_CENELEC_ALARM
    // Note: wPPMBuffer was getting destroyed here (modified)
    if (AL_ACCUMULATOR_DECAY_THRESHOLD < wPPMBuffer)
    {
            wAlarmSum = (wAlarmSum + wPPMBuffer + AL_ACCUMULATOR_DECAY_CONSTANT) ;
    }
    else
    {
            wAlarmSum += wPPMBuffer ;
    }
#else

    // Note: wPPM was getting destroyed here (modified)
    if (AL_ACCUMULATOR_DECAY_THRESHOLD < wPPM)
    {
        AlarmSum = (AlarmSum + wPPM + AL_ACCUMULATOR_DECAY_CONSTANT) ;
    }
    else
    {
        AlarmSum += wPPM ;
    }
#endif

    // Note: Subtract AL_ACCUMULATOR_COMP_THRESHOLD
    
    if (AlarmSum > AL_ACCUMULATOR_COMP_THRESHOLD)
    {
        AlarmSum -= AL_ACCUMULATOR_COMP_THRESHOLD ;

        // check for alarm condition
        if (AlarmSum > AL_ALARM_THRESHOLD)
        {
                AlarmSum = AL_ALARM_THRESHOLD_LIMIT ;
                FLAG_CO_ALARM_CONDITION = 1 ;
        }
        else
        {
            #ifdef	_CONFIGURE_CENELEC_ALARM
                // The Cenelec configuration is the same except that wPPMBuffer is used
                // the accumulator is below the alarm threshold
                if (wPPMBuffer > AL_THRESHOLD_SLOPE_THRESHOLD)
                {
                    if( (AL_ALARM_THRESHOLD - wAlarmSum) < (wPPMBuffer * AL_THRESHOLD_SLOPE_HIGH_MULTIPLE) )
                    {
                            wAlarmSum = AL_ALARM_THRESHOLD_LIMIT ;
                            FLAG_CO_ALARM_CONDITION = 1 ;
                    }
                    else
                    {
                            FLAG_CO_ALARM_CONDITION = 0 ;
                    }
                }
                else
                {
                    if( (AL_ALARM_THRESHOLD - wAlarmSum) < (wPPMBuffer * AL_THRESHOLD_SLOPE_MULTIPLE) )
                    {
                            wAlarmSum = AL_ALARM_THRESHOLD_LIMIT ;
                            FLAG_CO_ALARM_CONDITION = 1 ;
                    }
                    else
                    {
                            FLAG_CO_ALARM_CONDITION = 0 ;
                    }
                }
            #else
                // The UL configuration uses wPPM
                if (wPPM > AL_THRESHOLD_SLOPE_THRESHOLD)
                {
                  
                    if( (AL_ALARM_THRESHOLD - AlarmSum) < (wPPM * AL_THRESHOLD_SLOPE_HIGH_MULTIPLE) )
                    {
                            AlarmSum = AL_ALARM_THRESHOLD_LIMIT ;
                            FLAG_CO_ALARM_CONDITION = 1 ;
                     }
                    else
                    {
                            FLAG_CO_ALARM_CONDITION = 0 ;
                    }
 
                }
                else
                {
                    if( (AL_ALARM_THRESHOLD - AlarmSum) < (wPPM * AL_THRESHOLD_SLOPE_MULTIPLE) )
                    {
                            AlarmSum = AL_ALARM_THRESHOLD_LIMIT ;
                            FLAG_CO_ALARM_CONDITION = 1 ;

                    }
                    else
                    {
                            FLAG_CO_ALARM_CONDITION = 0 ;
                    }
                }
                
            #endif

        }
    }
    else
    {
        AlarmSum = 0 ;
        AlarmInterimSum = 0 ;
        FLAG_CO_ALARM_CONDITION = 0 ;
    }

    
    // Test only
    /*
    if (FLAG_SERIAL_PORT_ACTIVE)
    {
        // Send accumulator value.
        SER_Send_Int('A', AlarmSum, SERIAL_CONV_BASE_DECIMAL) ;
        SER_Send_Prompt() ;
    }
    */

    // This value cannot change as the calculations are based on this
    // 30 second interval.
    return Make_Return_Value(30000) ;   // milliseconds
}


