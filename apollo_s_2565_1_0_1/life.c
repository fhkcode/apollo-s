/****************************************************************
 * The contents of this file are Kidde proprietary  and confidential.
 * *************************************************************/
// LIFE.c
// Routines to manage life counter
#define _LIFE_C

#include    "common.h"
#include    "life.h"

#include    "systemdata.h"
#include    "fault.h"
#include    "battery.h"
#include    "diaghist.h"

#ifdef _CONFIGURE_24_HOUR_TEST
    #define	LFE_MINUTES_PER_DAY	1440
#endif

#ifdef _CONFIGURE_EOL_TEST
    #define	LFE_MINUTES_PER_DAY	1
#endif

#ifdef _CONFIGURE_DIAG_HIST_TEST
    #define	LFE_MINUTES_PER_DAY	1
#endif

#ifdef _CONFIGURE_DEPASSIVATION_TEST
    #define	LFE_MINUTES_PER_DAY	1
#endif

#ifdef _CONFIGURE_42_HOUR_TEST
    #define	LFE_MINUTES_PER_DAY	1
#endif

#ifndef	LFE_MINUTES_PER_DAY
    #define	LFE_MINUTES_PER_DAY	1440
#endif

static UINT wTimerLifeCount = 0 ;

#ifdef	_CONFIGURE_EOL_HUSH
    static UINT	wEOLHushTimer = 0 ;
#endif

#ifdef _CONFIGURE_LITHIUM
    void LFE_Check_Depassivation(void);
    void LFE_Depassivation(void);

    UCHAR Depass_Timer = DEPASSIVATION_INTERVAL;
    UCHAR Depass_Pulse_Timer ;
#endif

#define LFE_DAY_ONE     1
#define LFE_YEAR_ONE    366


// Verify Valid Life
void Life_Init(void)
{
    unsigned char sum;
    sum = (SYS_RamData.Life_LSB + SYS_RamData.Life_MSB);
    if(sum != SYS_RamData.Life_Checksum)
    {
        // Perform another Sys Init to attempt to restore 
        // Then repeat Init.
        if(FALSE == SYS_Init() )
        {
            sum = (SYS_RamData.Life_LSB + SYS_RamData.Life_MSB);
            if(sum != SYS_RamData.Life_Checksum)
            {
                // There is still an error in the checksum byte.
                // Force a memory fault.
                FLT_Fault_Manager(FAULT_MEMORY) ;
            }
        }
        else
        {
             FLT_Fault_Manager(FAULT_MEMORY) ;
        }

    }

    // Verify Life is not expired
    LFE_Check_EOL();

}

//*****************************************************************************
UINT LIFE_Get_Current_Day(void) 
{
    // Return the current day as an integer
    return *((UINT*)&(SYS_RamData.Life_LSB)) ;
}


//*****************************************************************************
// This function depends on being called once per minute to keep track
// of time.
void LFE_Check(void)
{
    // Increment and check the life timer
    if (++wTimerLifeCount >= LFE_MINUTES_PER_DAY)
    {
        // A day has expired.  Increment the day counter and check for
        // expiration.
        // Cast the LSB to an integer so that 16 bit math will be done.
        //*((UINT*)&(SYS_RamData.Life_LSB)) += 1 ;
        SYS_RamData.Life_LSB = (SYS_RamData.Life_LSB + 1);
        if(SYS_RamData.Life_LSB == 0)
        {
            SYS_RamData.Life_MSB = (SYS_RamData.Life_MSB + 1);
        }

        SYS_RamData.Life_Checksum = (SYS_RamData.Life_LSB + SYS_RamData.Life_MSB) ;
        SYS_Save();
        LFE_Check_EOL() ;

        // Reset minute counter for next day.
        wTimerLifeCount = 0 ;

        // *******************************************
        // Call anything that executes every day here.
        #ifdef _CONFIGURE_DIAG_HIST
            Diag_Hist_Bat_Record(BAT_CURRENT, BAT_Last_Volt);

            if(LIFE_Get_Current_Day() == LFE_DAY_ONE)
            {
                // Record battery Voltage after 1st 24 hours of operation
                Diag_Hist_Bat_Record(BAT_DAY_ONE, BAT_Last_Volt);
            }
            if(LIFE_Get_Current_Day() == LFE_YEAR_ONE)
            {
                // Record battery Voltage after 1st year of operation
                Diag_Hist_Bat_Record(BAT_YEAR_ONE, BAT_Last_Volt);
            }
        #endif


        #ifdef _CONFIGURE_LITHIUM
            // Call Depassivation Check once a day
            LFE_Check_Depassivation();
        #endif

    }
}


//*****************************************************************************
void LFE_Check_EOL(void)
{
#ifdef	_CONFIGURE_EOL_HUSH
    if (LIFE_Get_Current_Day() >= LFE_UL_EXTENSION)
    {
        // EOL fatal fault, no more EOL Hush Extension
        FLT_Fault_Manager(FAULT_EXPIRATION) ;
    }
    else if (LIFE_Get_Current_Day() >= LFE_EXPIRATION_DAY)
    {
        // This begins EOL chirping
        FLAG_EOL_MODE = 1 ;
    }
#else
    if (LIFE_Get_Current_Day() >= LFE_EXPIRATION_DAY)
    {
        // EOL fatal fault
        FLT_Fault_Manager(FAULT_EXPIRATION) ;
    }
#endif

#ifdef	_CONFIGURE_EOL_HUSH
    // Check here to see if EOL hush mode should be cancelled.  This is a good
    // spot to do it since it is called every day.  Subtract the stored timer
    // from the current day to see if enough time has expired.
    if ( (LIFE_Get_Current_Day() - wEOLHushTimer) >= 3)	// 3 days max
    {
        FLAG_EOL_HUSH_ACTIVE = 0 ;
    }
#endif

}

#ifdef	_CONFIGURE_EOL_HUSH
//*****************************************************************************
// Sets the EOL hush timer to the current day.
void LFE_UL_EOL_Snooze(void)
{
    wEOLHushTimer = LIFE_Get_Current_Day() ;
    FLAG_EOL_HUSH_ACTIVE = 1 ;
}
#endif


#ifdef _CONFIGURE_LITHIUM
	
#define NUM_DEPASS_PULSES	6
#define DEPASS_PULSE_TIME	5		// 500 ms in 100 ms tics

/* This is called once a day
 * Every 30 days begin a Depassivation Cycle 
 * (set FLAG_DEPASSIVATION_ACTIVE, Init cycle
 * 
 * This triggers Active Mode Depass Timer (10 ms tics)
 *  and calls the LFE_Depassivation routine to execute
 *  six 500ms on / 500 ms off battery test pulses.
 * 
*/
void LFE_Check_Depassivation(void)
{   // Every 30 Days Execute depassivation cycle
    if(--Depass_Timer == 0)
    {
        FLAG_DEPASSIVATION_ACTIVE = 1;		// Also triggers active mode
        Depass_Timer = NUM_DEPASS_PULSES;  	// Use for Pulse Count
        Depass_Pulse_Timer = DEPASS_PULSE_TIME;

        BAT_TEST_ON     			// Start 1st Pulse to
        FLAG_DEPASSIVATION_ON = 1;		// Monitor Battery Test State
    }
	
}

// called every Active tic time
void LFE_Depassivation(void)
{
    if(--Depass_Pulse_Timer == 0)
    {
        Depass_Pulse_Timer = DEPASS_PULSE_TIME;	// Reset Timer

        if(FLAG_DEPASSIVATION_ON == 1)
        {
            FLAG_DEPASSIVATION_ON = 0;
            BAT_TEST_OFF

            if(--Depass_Timer == 0)
            {
                // Depassivation Completed!
                FLAG_DEPASSIVATION_ACTIVE = 0;
                // Re-Init 30 Day Interval
                Depass_Timer = DEPASSIVATION_INTERVAL;
            }

        }
        else
        {
            FLAG_DEPASSIVATION_ON = 1;
            BAT_TEST_ON
        }

    }

}

#endif



