# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

1. Firstly, make a folder for local repository:
    mkdir xx_repo
2. Initialize the repository:
    git init
3. Add a remote repository to local:
    git remote add origin <remote repo url....>
4. Pull the repository to local:
    git pull origin master

5. Use git add command to add the changes you made for the repository
    git add <files>
6. Commit the changes:
    git commit -m "xxx"
7. Then push the changes to remote repository:
    git push <-f> origin master.

How to remove an unused or empty folder?
1.	First in this folder new an empty file, like "unused.txt".
    echo unused.txt
2.	Add this change to git.
    git add <folder>/unused.txt
3.	Commit this change
    git commit -m "add unused.txt"
4.	Remove this empty file 
    git rm <folder>/unused.txt
5.	Commit the change again.
    git commit -m "remove unused.txt"


* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* daniel.gee@carrier.com
* steven.tang@carrier.com
* Other community or team contact