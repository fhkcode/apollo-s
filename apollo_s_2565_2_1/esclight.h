/* 
 * File:   esclight.h
 * Author: bill.chandler
 *
 * Created on December 12, 2013, 9:11 AM
 */

#ifndef ESCLIGHT_H
#define	ESCLIGHT_H

#ifdef	__cplusplus
extern "C" {
#endif

    #define NEXT_NL_STATE   0
    #define INIT_NL_STATE   1

    unsigned int Esc_Light_Process(void);
    void Esc_Init(void);
    void Esc_Set_PWM_Night(UCHAR);
    void Esc_Next_NL_Intensity(unsigned char);

    extern unsigned int NL_Intensity_Disp;


#ifdef	__cplusplus
}
#endif

#endif	/* ESCLIGHT_H */

