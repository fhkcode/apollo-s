/****************************************************************
 * The contents of this file are Kidde proprietary and confidential.
 * *************************************************************/
// serial.h



#ifndef	_SERIAL_H
#define	_SERIAL_H

	#define SERIAL_CONV_BASE_DECIMAL	10
	#define	SERIAL_CONV_BASE_HEX		16

        //Prototypes
        void	SER_Send_Int(char tag, UINT val, char base) ;
        void 	SER_Send_Byte(UCHAR byte) ;
        void	SER_Send_String(char const *stringtosend) ;
        void 	SER_Send_Prompt(void) ;
        void 	SER_Send_Error(void) ;
        void	SER_Send_Char(char) ;
        void 	SER_Enable_Init(void) ;
        void	SER_Enable_Output(UINT bitmask) ;
        void	SER_Disable_Output(UINT bitmask) ;
        UINT 	SER_Strx_To_Int(char const *str) ;
        void 	SER_Init(void) ;
        void    SER_Process_Rx(void);

        char* itoa( unsigned int value, char* result, char base) ;

        extern volatile Flags_t SER_OutputFlags;

        #define SERIAL_ENABLE_SMOKE     	SER_OutputFlags._0
        #define SERIAL_ENABLE_CO                SER_OutputFlags._1
        #define SERIAL_ENABLE_BATTERY   	SER_OutputFlags._2
        #define SERIAL_ENABLE_HEALTH		SER_OutputFlags._3
        #define SERIAL_ENABLE_UNUSED_4          SER_OutputFlags._4
        #define SERIAL_ENABLE_TEMPERATURE	SER_OutputFlags._5
        #define SERIAL_ENABLE_UNUSED_6          SER_OutputFlags._6
        #define SERIAL_ENABLE_GAS		SER_OutputFlags._7


#endif

