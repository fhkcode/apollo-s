/****************************************************************
 * The contents of this file are Kidde proprietary and confidential.
 * *************************************************************/
// systemdata.h
// Contains the system data structure

#ifndef	_SYSTEMDATA_H
#define _SYSTEMDATA_H

#include "diaghist.h"

    // Prototypes
    UCHAR   SYS_Init(void);
    void    SYS_Save(void);

    UCHAR SYS_Validate_Data_Structure(void);



    extern struct SystemData  volatile SYS_RamData ;
    extern eeprom struct EE_Memory EE_Data;

    // Offsets within Non-Volatile Data
    #define PRIMARY_START  0x00
    #define BACKUP_START   0x10

#ifdef	_CONFIGURE_MANUAL_CO_CAL
	#define CO_STATUS_INIT_VAL		0x03
	#define CO_CHECKSUM_INIT_VAL		0xfC
	#define CO_OFFSET_LSB_INIT_VAL		0x9D
	#define CO_OFFSET_MSB_INIT_VAL		0
	#define CO_SCALE_LSB_INIT_VAL		0x69
	#define CO_SCALE_MSB_INIT_VAL		0
#else
	#define CO_STATUS_INIT_VAL          0
	#define CO_CHECKSUM_INIT_VAL		0xff
	#define CO_OFFSET_LSB_INIT_VAL		0
	#define CO_OFFSET_MSB_INIT_VAL		0
	#define CO_SCALE_LSB_INIT_VAL		0
	#define CO_SCALE_MSB_INIT_VAL		1
#endif

#ifdef	_CONFIGURE_EOL_TEST
	#define	LIFE_LSB_INIT_VAL		((LFE_EXPIRATION_DAY & 0xff) - 1)
	#define	LIFE_MSB_INIT_VAL		(LFE_EXPIRATION_DAY >> 8)
	#define	LIFE_CHECKSUM_INIT_VAL		(LIFE_LSB_INIT_VAL + LIFE_MSB_INIT_VAL)
#else
	#define	LIFE_LSB_INIT_VAL		0
	#define	LIFE_MSB_INIT_VAL		0
	#define	LIFE_CHECKSUM_INIT_VAL  0
#endif

#define BAT_SCALE_LSB               0
#define BAT_SCALE_MSB               0    
    
#define PAD_INIT_VAL				0
#define UNUSED_INIT_VAL             0
#define UNIT_ID_INIT                0


#define	CHECKSUM_CO     CO_STATUS_INIT_VAL + CO_CHECKSUM_INIT_VAL + CO_OFFSET_LSB_INIT_VAL + CO_OFFSET_MSB_INIT_VAL + CO_SCALE_LSB_INIT_VAL + CO_SCALE_MSB_INIT_VAL
#define	CHECKSUM_LIFE	LIFE_LSB_INIT_VAL + LIFE_MSB_INIT_VAL + LIFE_CHECKSUM_INIT_VAL
#define	CHECKSUM        (UCHAR) (CHECKSUM_CO + CHECKSUM_LIFE)

	
	struct	CO_Calibration
	{
            UCHAR	status ;
            UCHAR	checksum ;
            UCHAR	offset_LSB ;
            UCHAR	offset_MSB ;
            UCHAR	scale_LSB ;
            UCHAR	scale_MSB ;
	} ;
	
	
	// If the number of bytes in this structure is odd then sizeof() will not 
	// work properly.  In that case the Pad will have to be added.  If even number
	// of bytes, remove Pad and adjust initialization accordingly.
	struct SystemData
	{
            struct  CO_Calibration CO_Cal ;
		
            UCHAR Bat_Scal_LSB;
            UCHAR Bat_Scal_MSB;

            UCHAR Life_LSB ;
            UCHAR Life_MSB ;
            UCHAR Life_Checksum ;

            UCHAR unused3;
            UCHAR unused4;
            UCHAR Unit_ID_Low;
            UCHAR Unit_ID_High;
		
            UCHAR Checksum ;    // Sum of previous 15 structure bytes
	} ;

    // Set Up an EE Prom Data Structure
    //  to organize EEProm data address locations
    struct EE_Memory
    {
        struct SystemData Primary;
        struct SystemData Backup;
        struct HistData DiagHist;
    };






#endif
