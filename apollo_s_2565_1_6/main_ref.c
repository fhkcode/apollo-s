/****************************************************************
 * The contents of this file are Kidde proprietary  and confidential.
 * *************************************************************/
//MAIN.C
#define _MAIN_C

/*  
 * Revision History:
 *
 *	Revision
 *	Number	Date		Author
 *	-------------------------------------------------------------------------------------
 *	0.1		7/16/13		Bill Chandler
 *          Initial Code ACDC CO Low Cost

*/

/******* Remember to change Revision Info **************/
/*******       in Main header file        **************/


#include <stdio.h>
#include <stdlib.h>
#include <xc.h>


#include "common.h"



// Prototypes
void Initialize_PowerUp(void) ;


UINT    DebugTask(void) ;

void	Main_TP_On(void);
void	Main_TP_Off(void);
void	Main_TP_Toggle(void);


#define	MAX_NUM_TASKS		18
#define	NUM_MODES		2

#define	NUM_TASKS_ACTIVE	17
#define	NUM_TASKS_LOWPOWER	6


#define	MODE_ACTIVE		0
#define	MODE_LOW_POWER          1
#define	MS_PER_TIC_ACTIVE	10
#define	MS_PER_TIC_LOWPOWER	1000

// Number of SMCLK's per ACLK when ACLK=32KHz, SMCLK=4MHz
#define	SMCLK_DESIRED           4000000
#define	ACLK			32768
#define DELTA 			(SMCLK_DESIRED/ACLK)

//*******************************  Clock Stuff  *************************************
// Timer A0 Clock Rate in Hz
#define TIMERA0_CLOCK_FREQ		62500
	
// Timer A0 Clock Period in uSec	
#define TIMERA0_CLOCK_TIME		16		// (1000000 / TIMERA0_CLOCK_FREQ)

// Task Time Tic Intervals in ms
#define	MAIN_TIC_INTERVAL_ACTIVE	10
#define	MAIN_TIC_INTERVAL_LOWPOWER	1000

#define	TIMERAO_TIME_ACTIVE		625		//(MAIN_TIC_INTERVAL_ACTIVE * 1000)/TIMERA0_CLOCK_TIME	
#define	TIMERAO_TIME_LOWPOWER	62500	//(MAIN_TIC_INTERVAL_LOWPOWER * 1000)/TIMERA0_CLOCK_TIME	


#define	TIMER_1SEC_ACTIVE_COUNT		(1000/MAIN_TIC_INTERVAL_ACTIVE)			// in Active Tics
#define	TIMER_ONE_MINUTE_COUNT		60										// in 1000ms Tics


//***********************************************************************************


static UINT bTicTimer ;
static UINT CurrentMsPerTic ;
static UCHAR CurrentMode ;
static UCHAR b1SecTimer ;
static UCHAR b1SecCount ;
static UCHAR ACDetectCount;

static UINT	PowerupAwakeTimer ;
volatile UCHAR intsource = 0 ;


// Global Variables (extern in header file)
UCHAR bOneMinuteTic ;
UCHAR OneMinuteCount ;

char receive_buffer[NUM_RX_BUFFER_CHARS] ;


//////////////////////////////////////////////////////////////////
// To add another task:
//	1. Declare a TaskInfo variable and initialize.
//	2. Add a pointer to the TaskInfo structure to TaskTable
//	3. Add a task ID in main.h.  Make sure that the ID corresponds to the 
//		position of the TaskInfo pointer in TaskTable
//	4. Include the header file of the task module above.  Task prototype is
//		UINT Task(void)
//  5. Add the task ID to the desired mode in Main_Mode and increment the
//		NUM_TASKS_xxx for the mode.

/////////////////////////////////////////////////////////////////
struct TaskInfo
{
  UINT TaskTime ;
  UINT TaskInterval;
  UINT (*funcptr)(void) ;
} ;

struct TaskInfo SoundTaskInfo ;
struct TaskInfo ButtonTaskInfo ;
struct TaskInfo RedLedInfo ;
struct TaskInfo PTT_TaskInfo ;
struct TaskInfo BatteryTestInfo ;
struct TaskInfo DebugTaskInfo ;
struct TaskInfo AlarmPrioInfo ;
struct TaskInfo COAlarmInfo ;
struct TaskInfo CalManagerInfo ;
struct TaskInfo COMeasureInfo ;
struct TaskInfo GrnLedInfo ;
struct TaskInfo GasTaskInfo ;
struct TaskInfo TroubleManagerInfo ;
struct TaskInfo DispUpdateInfo ;
struct TaskInfo VoiceInfo ;
struct TaskInfo AmberLedInfo ;
struct TaskInfo PeakButtonInfo ;



//  The number of tasks in each mode.
const unsigned int TaskNumbers[NUM_MODES] = {NUM_TASKS_ACTIVE, NUM_TASKS_LOWPOWER} ;

struct TaskInfo  * const(TaskTable)[] = { &SoundTaskInfo,
					&ButtonTaskInfo, 
					&RedLedInfo, 
					&PTT_TaskInfo, 
					&BatteryTestInfo,
					&DebugTaskInfo, 
					&AlarmPrioInfo, 
					&COAlarmInfo,
					&CalManagerInfo, 
					&COMeasureInfo, 
					&GrnLedInfo,
					&GasTaskInfo,
					&TroubleManagerInfo,
					&DispUpdateInfo,
					&VoiceInfo,
					&AmberLedInfo,
					&PeakButtonInfo };
										

// Define the tasks that are in each mode.
const unsigned char Main_Mode[NUM_MODES][MAX_NUM_TASKS] = {
						{	TASKID_SOUND_TASK,	
							TASKID_BUTTON_TASK,
							TASKID_RLED_MANAGER,
							TASKID_PTT_TASK,
							TASKID_BATTERY_TEST,
							TASKID_ALARM_PRIORITY,
							TASKID_DEBUG_TASK,
							TASKID_COALARM,
							TASKID_CALIBRATE,
							TASKID_COMEASURE,
							TASKID_GLED_MANAGER,
							TASKID_GAS_TASK,
							TASKID_TROUBLE_MANAGER,
							TASKID_DISP_UPDATE,
							TASKID_VOICE_TASK,
							TASKID_ALED_MANAGER,
							TASKID_PEAK_BUTTON_TASK
						},
							
						{	
							TASKID_COMEASURE,
							TASKID_COALARM,	
							TASKID_BATTERY_TEST,
							TASKID_TROUBLE_MANAGER,
							TASKID_DISP_UPDATE,
							TASKID_GLED_MANAGER
						} } ;


		

// Global / Static Initialization
void Main_Init(void)
{
bTicTimer = 0 ;
PowerupAwakeTimer = 0 ;
ACDetectCount = 3;

// Global Variables (extern in header file)
bOneMinuteTic = 0;
OneMinuteCount = TIMER_ONE_MINUTE_COUNT ;
b1SecTimer = 0;
b1SecCount = TIMER_1SEC_ACTIVE_COUNT;
	
}


//*************************************************************************
int main( void ) 
{
	unsigned int i = 0 ;
	unsigned int interval ;
	
	
	/*
         * We have multiple sources of Reset to detect
	   1. A Push to Test Reset
	      
	   2. Power On Reset (none of the above detected)
         *
	 */
	
	// Read Source of Reset
	 UINT rst =  0;
	
	// Test Reset Vector and PTT Flag to determine source of reset
	if( (FLAG_PTT_RESET) && (rst == 0x18) )
	{
		
		// Preserve NR Flags and CO Accumulators
		// *( (UINT*)&Flags_NR) = 0;
		// COAlarm_Init() ;			// Init Accumulators at Power Reset

		// Just like a power on reset except non-reset flags, accums, 
		// and peak memory
		Initialize_PowerUp() ;
		
		#ifdef _CONFIGURE_VOICE
			FLAG_INHIBIT_INTRO_VOICE = 1 ;
		#endif

	}
	#ifdef	_CONFIGURE_SIMULATE_SLEEP
	    // This is WD Timer Password Violation (i.e simulated Sleep Timer Wake)
		else if ( (rst == SYSRSTIV_WDTKEY) )
	#else	
	  	// Sleep Timer, Wake Pin (Button), or INT_IN can
	  	// wake up the Controller (INT_IN used for AC Detect)
	  	// so process Wake Up code (instead of a Pwr Reset)
		else if (intsource & (ASSSLEEP | ASSWAKE | ASSINT_IN)) 
	#endif
	{
		// Since UCA0CLK pin was initialized as an input, change to output to support
		// debug functions.
		#ifdef	_CONFIGURE_DEBUG_PIN_ENABLED
			// Init UCA0CLK as Output for use in Debug 
//			P3DIR |= PORT_UCA0CLK_PIN ;
		#else
			// Init. UCA0CLK as Input or Output when Debug is Disabled
			// and Pin is used as an alternate function
			//P3DIR &= ~PORT_UCA0CLK_PIN ;		// Set to Input for this project
			P3DIR |= PORT_UCA0CLK_PIN ;			// Set to Output for this project
			
		#endif

		// This occurred as a result of the analog die waking the 
		// MCU up.  Check the interrupt souces to see what the wakeup
		// source was.


		

	
		// Test/Reset Button Pressed?
		if (intsource & ASSWAKE)
		{  
			FLAG_BUTTON_EDGE_DETECT = 1 ;
			
			#ifdef	_CONFIGURE_DEBUG_PIN_ENABLED
				Main_TP_On();
			#endif

			
		}

		
		
	}
	#ifdef	_CONFIGURE_SIMULATE_SLEEP
		// else - This is a Power reset
		else
	#else
		else if ( (intsource & ASSBROWN) || (intsource == 0) )
	#endif
	{
		

		// This is a powerup on application of power rather than a PTT.	
		#ifdef	_CONFIGURE_SERIAL_WAKESLEEP_DEBUG
		  if (FLAG_SERIAL_PORT_ACTIVE)
			SER_Send_Char('.') ;
		#endif

		
			// This is a Power UP reset
			*( (UINT*)&Flags_NR) = 0;	// Init non-reset Flags at Power Reset

			// Do power-up init
			Initialize_PowerUp() ;


//			COAlarm_Init() ;			// Init Accumulators at Power Reset
//			wPeakPPM = 0 ;				// Init CO Peak Memory at Power On
		
			#ifdef _CONFIGURE_DIAG_HIST
//				Diag_Hist_Que_Push(HIST_POWER_RESET);
			#endif
		
	}
	
	#ifdef _CONFIGURE_FORCE_MEM_FAULT
		FLT_Fault_Manager(FAULT_MEMORY) ;		
	#endif

//	LFE_Check_EOL() ;

#ifdef	_CONFIGURE_RST_TEST
  if (FLAG_SERIAL_PORT_ACTIVE)
  {
	// *** Test Only ***
	SER_Send_Int('r', rst, 16) ;
	SER_Send_Char(',');
	SER_Send_Int('i', intsource, 16) ;
	SER_Send_Prompt() ;

  }
#endif

// This resets WD Timer every Wake / Reset Cycle
#ifdef _CONFIGURE_WATCHDOG_TIMER_OFF
	// No Watchdog Timer
    BLAZE_SET_WDT_OFF
#else
    // Reset Watchdog Timer (to 64s )
	//BLAZE_SET_WDT_64MS
	BLAZE_SET_WDT_64S
#endif
	
	//	Main Task Process/Sleep Loop
	 while(1)
        {
        	
			// *** TEST POINT ***
			#ifdef	_CONFIGURE_DEBUG_PIN_ENABLED
				//Main_TP_On();
			#endif
			
          if ( (bTicTimer - TaskTable[ Main_Mode[CurrentMode][i] ]->TaskTime) >=
			  TaskTable[ Main_Mode[CurrentMode][i] ]->TaskInterval)
          {
				// task timer has expired 
				#ifdef	_CONFIGURE_DEBUG_PIN_ENABLED
					//Main_TP_On();
				#endif

				interval = TaskTable[ Main_Mode[CurrentMode][i] ]->funcptr() ;

				#ifdef	_CONFIGURE_DEBUG_PIN_ENABLED
					//Main_TP_Off();
				#endif
				
				if (interval != TaskTable[Main_Mode[CurrentMode][i]]->TaskInterval)
				{
				  TaskTable[Main_Mode[CurrentMode][i]]->TaskInterval = interval ;
				  TaskTable[Main_Mode[CurrentMode][i]]->TaskTime = bTicTimer ;
				}
				else
				{
				  TaskTable[Main_Mode[CurrentMode][i]]->TaskTime += interval ;
				}
			  
          }

          i++ ;
          
		  if (i > TaskNumbers[CurrentMode] - 1)
		  {
			// *** TEST POINT ***
			#ifdef	_CONFIGURE_DEBUG_PIN_ENABLED
				//Main_TP_Off();
			#endif
			
			i = 0 ;
			bTicTimer++ ;
			
			// ****************************************************************
			// Put anything here that has to happen at the end of all the tasks
			
			
			// BC - Good place to update any Global system timers
			// i.e - b1SecTimer, bOneMinuteTimer... etc

			if(FLAG_MODE_ACTIVE)
			{
				#ifdef _CONFIGURE_LITHIUM
					if(FLAG_DEPASSIVATION_ACTIVE == 1)
					{
						LFE_Depassivation();
					}
				#endif
				
				// based on 10 ms Tics
				if(--b1SecCount == 0)
				{
					b1SecTimer ++;
					b1SecCount = TIMER_1SEC_ACTIVE_COUNT;
					
					#ifdef	_CONFIGURE_ACTIVE_TEST
				  	if (FLAG_SERIAL_PORT_ACTIVE)
				  	{
						// *** Test Only (Output Active Flags ***
						SER_Send_Int(' ', *( (UINT*)&Flags_Active), 16) ;
						SER_Send_Char(',');
						SER_Send_Int(' ', *( (UINT*)&Flags_Active2), 16) ;
						SER_Send_Prompt() ;
				  	}
					#endif

					// Test and Update Power Status
		    		Main_Test_AC() ;

				}
			}
			else
			{
				// We're in Low Power Mode
				// based on 1 Second Tics
				b1SecTimer ++;
					
				// Test and Update Power Status
		    	Main_Test_AC() ;
			
			}	
			
			if(OneMinuteCount == b1SecTimer) 
			{
				bOneMinuteTic ++;
				OneMinuteCount = b1SecTimer + TIMER_ONE_MINUTE_COUNT;
					
				// Update and Test life counter every minute
				LFE_Check() ;

				#ifdef _CONFIGURE_DIGITAL_DISPLAY				
					if(FLAG_MODE_ACTIVE == 0)
					{
						// At PPM Detect Threshold start enabling display if in Low Power Mode
						if(wPPM > 38)
						{
							// Once a minute in Low Power Mode (switch to Active Mode)
							// to enable display for few seconds (use FLAG_POWERUP_DELAY_ACTIVE for now)
							FLAG_POWERUP_DELAY_ACTIVE = 1 ; 
							FLAG_LOW_POWER_ACTIVE_CYCLE = 1 ;	// Indicate that this is not Reset cycle
							PowerupAwakeTimer = 0 ;				// reset the Delay Timer too
						}
					}
				#endif
			}
			
			// If a command has been received, send to command processor
			if (FLAG_SERIAL_PORT_ACTIVE && FLAG_RX_COMMAND_RCVD )
			{
				FLAG_RX_COMMAND_RCVD = 0 ;
				Command_Processor(receive_buffer) ;
			}
 			
			Main_Check_Low_Power_Logic() ;

			#ifdef	_CONFIGURE_ANALOG_REG_DEBUG
				Main_Read_Analog_Regs() ;
			#endif
			
			//Don't go to sleep until the transmit buffer is empty.
			//while (!(UCA0IFG & UCTXIFG));
			while ((UCA0STAT & UCBUSY)) ;

// ***************************************************************************
// ***************************************************************************
// 			All tasks have finished.  Set WDT wakeup and then sleep
// ***************************************************************************
// ***************************************************************************
				#ifndef	_CONFIGURE_NO_SLEEP
				{
					#ifdef	_CONFIGURE_SIMULATE_SLEEP
					{
 						#ifdef	_CONFIGURE_DEBUG_PIN_ENABLED
							//Main_TP_Off();
						#endif
 
 						if (FLAG_POWERUP_DELAY_ACTIVE) 
						{
							PowerupAwakeTimer++ ;

							if ((PowerupAwakeTimer == 8) && (!FLAG_LOW_POWER_ACTIVE_CYCLE) )
							{
								BLAZE_HORN_OFF
								RLED_Off();
							}

							#ifdef	_CONFIGURE_VOICE
								if (PowerupAwakeTimer == 100)
								{
									// Only play Intro voice on powerup
									if (!FLAG_INHIBIT_INTRO_VOICE)
										VCE_Play(VCE_PUSH_TEST_BUTTON) ;								
								}
							#endif
							if (PowerupAwakeTimer >= 300)
							{
								FLAG_POWERUP_DELAY_ACTIVE = 0 ;
								
								if (!FLAG_INHIBIT_REV_MSG)
									Main_Check_Revision();	

							}
						}
						
						if(!FLAG_MODE_ACTIVE)
						{
							// *************************************************************
							// *************** Simulated Low Power Mode ********************
							// *************************************************************
							// *** We're in Simulated Low Power Sleep Mode


							// Since the debugger can't debug through sleep, for testing
							// we try to simulate a sleep instruction to the analog die.
							
							#ifdef	_CONFIGURE_DEBUG_PIN_ENABLED
								//Main_TP_Toggle();
							#endif
							
							TA0CCTL0 &= ~CCIFG ;
							TA0CCR0 = TA0R + TIMERAO_TIME_LOWPOWER ;	// 1000 ms tic	
							do
							{
							  #ifdef _CONFIGURE_INTERCONNECT
								if(P3IN & PORT_INT_IN_PT_PIN)
								{
									break ;
								} 
							  #endif

								if (FLAG_BUTTON_EDGE_DETECT) 
								{
									break ;								// Process Button immediately
								}
							}
							while (!(TA0CCTL0 & CCIFG)) ;
							
							// This will cause a reset. SYSRSTIV_WDTTO will be used after reset
							// in place of the Analog die SLEEP bit to sense this condition.
							WDTCTL = WDTHOLD ;
							
							// *************************************************************
							// *************** END Simulated Low Power Mode ****************
						}
						else
						{
							// *************************************************************
							// **************** Simulated Active Mode **********************
							// *************************************************************
							// *** We're in Simulated Active Mode
							
							while (!(TA0CCTL0 & CCIFG)) ;			// Wait for CCR0 Int Flag												
							TA0CCTL0 &= ~CCIFG ;					// Reset CCR0 Int Flag
							TA0CCR0 = TA0R + TIMERAO_TIME_ACTIVE ;	// 10 ms tic time	
							
							// This resets WD Timer every Simulated Active Wake
							#ifdef _CONFIGURE_WATCHDOG_TIMER_OFF
								// No Watchdog Timer
    							BLAZE_SET_WDT_OFF
							#else
    							// Reset Watchdog Timer (to 64s )
								//BLAZE_SET_WDT_64MS
								BLAZE_SET_WDT_64S
							#endif

							// *************************************************************
							// **************** END Simulated Active Mode ******************
						}
					}
	    			#else	//<_CONFIGURE_SIMULATE_SLEEP>
	    			{
				    //************************************************************************
	    			//************************************************************************
	    			//		This is Low Power Sleep Mode 
	    			//************************************************************************
	    			//************************************************************************
				
						if (FLAG_POWERUP_DELAY_ACTIVE) 
						{
							#ifdef	_CONFIGURE_DEBUG_PIN_ENABLED
								//Main_TP_Off();
							#endif
			
							PowerupAwakeTimer++ ;
							if ((PowerupAwakeTimer == 8) && (!FLAG_LOW_POWER_ACTIVE_CYCLE) )
							{
								BLAZE_HORN_OFF
								RLED_Off();
							}

							#ifdef	_CONFIGURE_VOICE
								if (PowerupAwakeTimer == 100)
								{
									// Only play Intro voice on powerup
									if (!FLAG_INHIBIT_INTRO_VOICE)
										VCE_Play(VCE_PUSH_TEST_BUTTON) ;								
								}
							#endif
							
							if (PowerupAwakeTimer >= 300)
							{
								FLAG_POWERUP_DELAY_ACTIVE = 0 ;

								if (!FLAG_INHIBIT_REV_MSG)
									Main_Check_Revision();	

							}
							
							while (!(TA0CCTL0 & CCIFG)) ;			// Wait for CCR0 Int Flag												
							TA0CCTL0 &= ~CCIFG ;					// Reset CCR0 Int Flag
							TA0CCR0 = TA0R + TIMERAO_TIME_ACTIVE ;	// 10 ms tic time	
						}
						else
						{
							if(FLAG_MODE_ACTIVE)
							{
								// *************************************************************
								// ****************** Normal Active Mode ***********************
								// *************************************************************
								// *** We're in Normal Operation Active Mode

								#ifdef	_CONFIGURE_DEBUG_ACTIVE_PULSE
								do 
								{
									#ifdef	_CONFIGURE_DEBUG_PIN_ENABLED
										//Main_TP_On();
										//Main_TP_Off();
									#endif
								}
								#endif
								while (!(TA0CCTL0 & CCIFG)) ;			// Wait for CCR0 Int Flag												
								TA0CCTL0 &= ~CCIFG ;					// Reset CCR0 Int Flag
								TA0CCR0 = TA0R + TIMERAO_TIME_ACTIVE ;	// 10 ms tic time	
								
								// This reset WD Timer every Active cycle
								#ifdef _CONFIGURE_WATCHDOG_TIMER_OFF
									// No Watchdog Timer
    								BLAZE_SET_WDT_OFF
								#else
    								// Reset Watchdog Timer (to 64s )
									//BLAZE_SET_WDT_64MS
									BLAZE_SET_WDT_64S
								#endif

								// *************************************************************
								// **************** END Normal Active Mode *********************
							}
							else
							{
								// *************************************************************
								// ******************** Low Power Mode *************************
								// *************************************************************
								// *** We're in Normal Operation Low Power Sleep Mode

								#ifdef	_CONFIGURE_DEBUG_PIN_ENABLED
									Main_TP_Off();
								#endif

								// Configure the sleep time.  If the time does not change, this
								// can be done once in sleep initialization which will save time
								// here.
								//SPI_Write(ASSISLPTMR1, CurrentMsPerTic/256) ;
								//SPI_Write(ASSISLPTMR2, CurrentMsPerTic & 0xFF ) ;

								// *** Turn off stuff here before sleep ***
								//BLAZE_CO_DISABLE
					
								// Turn off INT_IN while asleep (Should not need to do this, test only)
								// Turned off because INT_IN was causing a Power Reset instead
								// of a Wake (under investigation)
								//BLAZE_INTCON_IN_DISABLE
		
								// Test only
								//intsource = SPI_Read(ASSIINT);	// This should Clear Ints
								//intsource = 0;
								
								// Enable all Ints
								FLAG_DISABLE_SHADOW = 1;
								SPI_Update_Reg(ASSIMASK, 0x1E, 0xFF) ;
								
								
								//BLAZE_INTCON_OUT_ENABLE		
								BLAZE_INTCON_OUT_DISABLE	
								BLAZE_INTCON_IN_ENABLE		
								//BLAZE_INTCON_IN_DISABLE		
								

								
								// Set the sleep bit.  Since the analog die clears the sleep bit
								// automatically, don't use the shadow registers.
								UCHAR regval = SPI_Read(ASSICNTL2) ;
								// Mask off bits
								regval |= ASSSLEEP_EN ;
								
								// -------------------------------------------------------------------------	
								// Disable P1 interrupt because PORT1 interrupt handler reads SPI port.
								//P1IE &= ~PORT_INTERRUPT_PIN ;
							
								// Enable Analog Die SPI
							    P3OUT |= PORT_SRESET_PIN; 				// CS high
							    
								// Send the address
							  	UCB0TXBUF = (ASSICNTL2 << 1) | 1 ;      // Transmit address     
								while (!(UCB0IFG & UCTXIFG));           // USCI_A0 TX buffer ready?
								
							  	UCB0TXBUF = regval ;                    // Transmit Data     
								while (!(UCB0IFG & UCTXIFG));			// USCI_A0 TX buffer ready?
								
							    P3OUT &= ~PORT_SRESET_PIN ;				// CS low
								
								// -------------------------------------------------------------------------	
													
								//BLAZE_SLEEP
								// Take note of the following from the Blaze Data Sheet
								// The Boost Convertor, A/D module, Photo Amp, Ion Amp and ION regulator, 
								//  if being used, must be re-enabled after wake up.
								
								/* From Data Sheet...
								 
								When entering SLEEP mode the ION gain amplifier is disabled to save quiescent current. 
								IGAIN_EN bit will be cleared automatically.
								
								When entering SLEEP mode the ION regulator is disabled to save quiescent current. 
								IREG_EN bit will be cleared automatically.
								
								When entering SLEEP mode the photo input amplifer is disabled to save quiescent current. 
								PAMP_EN bit will be cleared automatically.
								
								When entering SLEEP mode the photo gain amplifer is disabled to save quiescent current. 
								PGAIN_EN bit will be cleared automatically.
								
								When entering SLEEP mode internal state machine disables the boost converter and resets the BST_EN bit.
								
								When entering SLEEP mode the ADC is disabled to save quiescent current. 
								ADC_EN bit will be cleared automatically.
								
								*/
								// *************************************************************
								// ******************** END Low Power Mode *********************
									
							}	
						}	
	    			}
	    			#endif	// <_CONFIGURE_SIMULATE_SLEEP>
				}
				#else	// (_CONFIGURE_NO_SLEEP - These are Configured Non-Sleep Modes)
				{
					// *************************************************************
					// ******************* NON-Sleep Modes *************************
					// *************************************************************

					if (FLAG_POWERUP_DELAY_ACTIVE) 
					{
						PowerupAwakeTimer++ ;

						if ((PowerupAwakeTimer == 8) && (!FLAG_LOW_POWER_ACTIVE_CYCLE) )
						{
							BLAZE_HORN_OFF
							RLED_Off();
						}

						#ifdef	_CONFIGURE_VOICE
							if (PowerupAwakeTimer == 100)
							{
								// Only play Intro voice on powerup
								if (!FLAG_INHIBIT_INTRO_VOICE)
									VCE_Play(VCE_PUSH_TEST_BUTTON) ;								
							}
						#endif
						
						if (PowerupAwakeTimer >= 300)
						{
							FLAG_POWERUP_DELAY_ACTIVE = 0 ;
							PowerupAwakeTimer = 0 ;
							
							if (!FLAG_INHIBIT_REV_MSG)
								Main_Check_Revision();	
													
						}
					}

					#ifdef	_CONFIGURE_DEBUG_PIN_ENABLED
						//Main_TP_Toggle();
					#endif

					// This reset WD Timer every non-sleep cycle
					#ifdef _CONFIGURE_WATCHDOG_TIMER_OFF
						// No Watchdog Timer
    					BLAZE_SET_WDT_OFF
					#else
    					// Reset Watchdog Timer (to 64s )
						//BLAZE_SET_WDT_64MS
						BLAZE_SET_WDT_64S
					#endif
					
					if(!FLAG_MODE_ACTIVE)
					{
						// *************************************************************
						// **************** Non-Sleep Standby Mode *********************
						// *************************************************************
						// *** We're in Standby non-sleep

						do
						{
							if (FLAG_BUTTON_EDGE_DETECT) 
							{
								break ;								// Process Button immediately
							}
						}
						while (!(TA0CCTL0 & CCIFG)) ;
						
						TA0CCTL0 &= ~CCIFG ;						// Reset CCR0 Int Flag
						TA0CCR0 = TA0R + TIMERAO_TIME_LOWPOWER ;	// Reset 1000 ms tic time
	
						// *************************************************************
						// ************** END Non-Sleep Standby Mode *******************
					}
					else
					{
						// *************************************************************
						// **************** Non-Sleep Active Mode *********************
						// *************************************************************
						// *** We're in Active non-sleep

						TA0CCTL0 &= ~CCIFG ;					// Reset CCR0 Int Flag
						TA0CCR0 = TA0R + TIMERAO_TIME_ACTIVE ;	// 10 ms tic time	
						while (!(TA0CCTL0 & CCIFG)) ;			// Wait for CCR0 Int Flag
												
						// **************** END Non-Sleep Active Mode ******************
						// *************************************************************
					}
				}
				#endif	// <_CONFIGURE_NO_SLEEP>
				
			// Test for need to Change to Active or Standby	
 			Main_Check_Low_Power_Logic() ;
			
			#ifdef	_CONFIGURE_DIGITAL_WATCHDOG_TIMER_OFF
				WDTCTL = WDTPW + WDTHOLD; 		// WDT Off
			#else			
				// Reset WDT 
				WDTCTL = WDTPW + WDTHOLD; 		// WDT Off
			
				// This clears the WDT counter and sets the WDT interval
				// WDT clocked by SMCLK (4MHZ) (Interval little over 2 sec)
				WDTCTL = WDTPW + WDTCNTCL + WDTIS1;
			
			#endif	
			
		  }

   }	//end of Engine Task Process/Sleep Loop

	
}	// end of Main


//*************************************************************************
void	MAIN_Reset_Task(char taskID)
{
	// To reset the task, set the time to the current time and
	// set the interval to 0.  This will cause the task to execute
	// on the next tic.
	TaskTable[taskID]->TaskTime = bTicTimer ;
	TaskTable[taskID]->TaskInterval = 0 ;
}


//*************************************************************************
// Test Task Only
// 
unsigned int DebugTask(void)
{
	
	
	
	
    return	Make_Return_Value(100) ;
}

//****************************************************************
// Output Revision if Configured
void Main_Check_Revision(void)
{
	// Only let this occur at Power On
	FLAG_INHIBIT_REV_MSG = 1;


	#ifdef _CONFIGURE_OUTPUT_REVISION
		// To track interim FW version changes during development
		// append letter of primary version number
	  if (FLAG_SERIAL_PORT_ACTIVE)
	  {
	  	SER_Send_Prompt() ;
	  	
	  	#ifdef	_CONFIGURE_2546
	  		SER_Send_String("Model 2546 ") ;
	  	#endif
	  	#ifdef	_CONFIGURE_2547
	  		SER_Send_String("Model 2547 ") ;
	  	#endif
	  	#ifdef	_CONFIGURE_2548
	  		SER_Send_String("Model 2548 ") ;
	  	#endif
	  	#ifdef	_CONFIGURE_2549
	  		SER_Send_String("Model 2549 ") ;
	  	#endif
	  	#ifdef	_CONFIGURE_2538
	  		SER_Send_String("Model 2538 ") ;
	  	#endif
	  	
	  	SER_Send_Prompt() ;
		SER_Send_String(REV_STRING) ;
		SER_Send_Prompt() ;
		
	  }
		
	#endif
	
	if (FLAG_SERIAL_PORT_ACTIVE)
		SER_Send_Prompt() ;

	
}

//*************************************************************************
void Initialize_PowerUp(void)
{
	// Initialize flags
	*( (UINT*)&Flags1) = 0 ;
	*( (UINT*)&Flags2) = 0 ;
	*( (UINT*)&Flags3) = 0 ;
	*( (UINT*)&Flags4) = 0 ;

	*( (UINT*)&Flags_Active) = 0 ;
	*( (UINT*)&Flags_Active2) = 0 ;

	// Start the Reset Beep
//  	BLAZE_HORN_ON
//	RLED_On();

//	CAL_Init() ;		// Init CO Cal Status Flags
	
	if (!FLAG_CALIBRATION_COMPLETE)
	{
		FLAG_INHIBIT_INTRO_VOICE = 1 ;	
	}

	FLAG_POWERUP_DELAY_ACTIVE = 1 ;
//	bTicTimer = 0 ;             //done in call to Main_Init
//	PowerupAwakeTimer = 0 ;     //done in call to Main_Init
	
	CurrentMode = MODE_ACTIVE ;
	Initialize_Engine() ;

#ifdef	_CONFIGURE_ACDC
	Main_Init_AC_Detect();		// Init AC Detect I/O
#endif

	Init_GS_Variables () ;		// Init Globals and Static Variables

	// See if Serial port is connected at Power On
//	if (P3IN & PORT_UCARX_PIN)
//	{
//		FLAG_SERIAL_PORT_ACTIVE = 1 ;	// Enable Serial Transmission
//		SER_Enable_Init() ;
//
//	}

	// This forces Initialization into Active Mode.
	FLAG_MODE_ACTIVE = 0 ;
	Main_Set_Active() ;
	Main_Check_Low_Power_Logic() ;

}

// Global / Static Initialization
void Init_GS_Variables (void)
{
	Main_Init() ;
//	Sound_Init() ;
//	Battery_Init() ;
//	Alarmprio_Init() ;
//	Button_Init() ;
//	COCalibrate_Init() ;
//	COCompute_Init() ;
//	COMeasure_Init() ;
//	Command_Proc_Init() ;
//	Fault_Init() ;
//	GLED_Init() ;
//	RLED_Init() ;
//	Led_Display_Init() ;
//	Life_Init() ;
//	Ptt_Init() ;
//	SER_Init() ;
	
	#ifdef	_CONFIGURE_AMBER_LED
		ALED_Init() ;
	#endif
	
	#ifdef	_CONFIGURE_VOICE
		VCE_Init() ;
	#endif
	
	#ifdef	_CONFIGURE_GAS
		Gas_Init() ;
	#endif

//	#ifdef	_CONFIGURE_PEAK_BUTTON
//		Peak_Button_Init();
//	#endif
}




//*****************************************************************************
unsigned int Make_Return_Value(unsigned int milliseconds)
{
	return milliseconds/CurrentMsPerTic ;
}

//*****************************************************************************
// This function calculates the number of tics in the current mode before the
// task is scheduled to execute.
UINT Main_Calc_Current_Tics(UCHAR Task_ID)
{
	UINT tics ;	
	//
	// Time in tics = Interval - (tics - Last time)
	// ==>Time in tics = Interval - tics + Last time
	//
	tics = TaskTable[Task_ID]->TaskInterval ;
	tics -= bTicTimer ;
	tics += TaskTable[Task_ID]->TaskTime ;
	return tics ;	
}

// *****************************************************************************
//
// 
void Main_Recalc_Int_Active(UCHAR Task_ID)
{
	if (TaskTable[Task_ID]->TaskInterval != 0)
	{
		UINT tics = Main_Calc_Current_Tics(Task_ID) ;
		
		// Multiply the number of tics by 100 to get the number of tics
		// in active mode and update interval.
		TaskTable[Task_ID]->TaskInterval = tics * 100 ;
		
		// Change last execution time of task to current time.
		TaskTable[Task_ID]->TaskTime = bTicTimer ;	
	}
}


// *****************************************************************************
//
// 
void Main_Recalc_Int_Standby(UCHAR Task_ID)
{
	// In certain cases a task may have reset another task.  If this happens,
	// the TaskInterval will be zero.  The task recalculations will break if
	// the TaskInterval is zero.
	if (TaskTable[Task_ID]->TaskInterval != 0)
	{
		UINT tics = Main_Calc_Current_Tics(Task_ID) ;
		
		// Divide the number of tics by 100 to get the number of tics
		// in standby mode and update interval.
		TaskTable[Task_ID]->TaskInterval = 
						tics / (MAIN_TIC_INTERVAL_LOWPOWER/MAIN_TIC_INTERVAL_ACTIVE) ;
		
		// Add one to make up for truncation in division
		TaskTable[Task_ID]->TaskInterval++ ;
		
		// Change last execution time of task to current time.
		TaskTable[Task_ID]->TaskTime = bTicTimer ;
	}	
}





// ****************************************************************
// Call this function to transition from low power to active mode.
//
void Main_Set_Active(void)
{
	// If we're already in active mode just return.
	if (FLAG_MODE_ACTIVE)
		return ;
	//
	// This flag notifies all interested tasks that they are operating
	// in active mode.
	//
	FLAG_MODE_ACTIVE = 1 ;

	//
	// Set the mode.
	//
	CurrentMode = MODE_ACTIVE ;
	CurrentMsPerTic = MS_PER_TIC_ACTIVE ;
	
	MAIN_Reset_Task(TASKID_PTT_TASK) ;
	MAIN_Reset_Task(TASKID_SOUND_TASK) ;
	Main_Recalc_Int_Active(TASKID_COALARM) ;
	
	
	return ;
}

// ****************************************************************
// Call this function to transition from active to low power mode.
//
void Main_Set_Low_Power(void)
{
	// If we're already in low power mode just return.
	if (!FLAG_MODE_ACTIVE)
		return ;
	//
	// This flag notifies all interested tasks that they are not operating
	// in active mode.
	//
	FLAG_MODE_ACTIVE = 0 ;
	//
	// Set the mode.
	//
	CurrentMode = MODE_LOW_POWER ;
	CurrentMsPerTic = MAIN_TIC_INTERVAL_LOWPOWER ;
	
	//
	// Tasks need to be reset here so that they start fresh
	// with the new wakeup time.
	//
	MAIN_Reset_Task(TASKID_DEBUG_TASK) ;
	MAIN_Reset_Task(TASKID_BATTERY_TEST) ;
	MAIN_Reset_Task(TASKID_COMEASURE) ;
	Main_Recalc_Int_Standby(TASKID_COALARM) ;
	MAIN_Reset_Task(TASKID_GLED_MANAGER) ;
	MAIN_Reset_Task(TASKID_ALED_MANAGER) ;
	
	MAIN_Reset_Task(TASKID_DISP_UPDATE) ;
	
//	SND_Reset_Task() ;
//	RLED_Reset_Task() ;
	
	//
	// Turn off red LED in case it is turned on.
	//	
//	BLAZE_RED_LED_OFF

	return ;
}

// ****************************************************************
// This function performs the logic to determine if the firmware needs
// to be in low power or active.
//
void Main_Check_Low_Power_Logic(void)
{
 #ifdef	_CONFIGURE_SERIAL_WAKESLEEP_DEBUG
	char char2send ;
 #endif
 
 	// Set Active Mode if any Active Flags are set
 	// Includes:
 	// FLAG_PTT_ACTIVE, FLAG_BUTTON_EDGE_DETECT, FLAG_POWERUP_DELAY_ACTIVE, 
 	// FLAG_VOICE_SEND, FLAG_CO_ALARM_CONDITION, FLAG_SERIAL_PORT_ACTIVE,
 	// FLAG_CALIBRATION_NONE, FLAG_CALIBRATION_PARTIAL, FLAG_CALIBRATION_UNTESTED,
 	// FLAG_SOUND_NOT_IDLE, FLAG_BUTTON_NOT_IDLE, FLAG_CHIRPS_ACTIVE
 	// FLAG_FAULT_FATAL
 	if (*( (UINT*)&Flags_Active1) != 0)
 	{
 		#ifdef	_CONFIGURE_SERIAL_WAKESLEEP_DEBUG
			char2send = '@' ;
		#endif
 		
 		Main_Set_Active() ;
 	}
 	else if (*( (UINT*)&Flags_Active2) != 0)
 	{
  		#ifdef	_CONFIGURE_SERIAL_WAKESLEEP_DEBUG
			char2send = '#' ;
		#endif
 		
 		Main_Set_Active() ;
  	}
	else
	{
		#ifdef	_CONFIGURE_SERIAL_WAKESLEEP_DEBUG
			char2send = '!' ;
		#endif
		Main_Set_Low_Power() ;
	}
	
	#ifdef	_CONFIGURE_SERIAL_WAKESLEEP_DEBUG
	  if (FLAG_SERIAL_PORT_ACTIVE)
		SER_Send_Char(char2send) ;
	#endif
}


//****************************************************************
void	Initialize_Engine(void)
{
	int i ;
	
	// Since TaskTable is made of integer pointers, divide by two
	// to get actual number of structs to init.
	for (i=0 ; i<sizeof(TaskTable)/2; i++)
	{
		TaskTable[i]->TaskInterval = 0 ;
		TaskTable[i]->TaskTime = 0 ;
	}
	
	SoundTaskInfo.funcptr = SounderTask ;
	ButtonTaskInfo.funcptr = BTN_Task ;
	RedLedInfo.funcptr = RLED_Manager ;
	PTT_TaskInfo.funcptr = PTT_Task ;
	BatteryTestInfo.funcptr = BatteryTest_Task ;
	DebugTaskInfo.funcptr = DebugTask ;
	AlarmPrioInfo.funcptr = AlarmPriorityTask ;
	COAlarmInfo.funcptr = COAlarmTask ;
	CalManagerInfo.funcptr = CAL_Manager ;
	COMeasureInfo.funcptr = COMeasureTask ;
	GrnLedInfo.funcptr = GLED_Manager ;
	TroubleManagerInfo.funcptr = SND_Trouble_Manager ;
//	DispUpdateInfo.funcptr = Display_Update_Task ;
//	AmberLedInfo.funcptr = ALED_Manager ;
//	GasTaskInfo.funcptr = GasMeasureTask ;
	VoiceInfo.funcptr = VCE_Process ;
//	PeakButtonInfo.funcptr = Peak_BTN_Task ;
	
}

//**********************************************************************************
// Uses a timer to delay a number of microseconds 
void Main_Delay(UINT microseconds)
{
	UINT timer ;
	
	// Init timer with current value of timer register plus desired timeout
//	timer = TA1R ;
	
	// Loop until
//	while ( (TA1R - timer) < (microseconds) ) ;

}

#ifdef	_CONFIGURE_ANALOG_REG_DEBUG
//**********************************************************************************
// A call to this function can be inserted at critical points in the code
// and the Analog registers can be read out and verified.
void	Main_Read_Analog_Regs(void)
{
	int i ;
	
	// clear structure first
	// Not sure if we need to clear regs first
	for (i=0 ; i<0x18; i++)
	{
		*( ((UCHAR*)&Analog_Regs) + i) = 0 ;
	}
	
	for (i=0 ; i<0x18; i++)
	{
		*( ((UCHAR*)&Analog_Regs) + i) = SPI_Read(i) ;
	}
}

#endif



#ifdef	_CONFIGURE_ACDC
   
 // AC Detect voltage is read thru INT_IN Pin
void Main_Init_AC_Detect(void)
{
	// Enable Interconnect Input (in Analog Die)
//	BLAZE_INTCON_INPUT

}

 

void Main_Test_AC(void)
{
//	FLAG_INTCON_EDGE_DETECT = 0;
	
	if(P3IN & PORT_INT_IN_PT_PIN)
	{
		
		if(!FLAG_AC_DETECT)
		{
			// Consecutive Samples of AC Detect ON required
			if(--ACDetectCount == 0)
			{
				FLAG_AC_DETECT = 1 ;
				
				MAIN_Reset_Task(TASKID_GLED_MANAGER) ;

				if (FLAG_SERIAL_PORT_ACTIVE)
				{
					SER_Send_String("AC On") ;
					SER_Send_Prompt() ;
				}
			
				#ifdef _CONFIGURE_DIAG_HIST
					Diag_Hist_Que_Push(HIST_AC_POWER_ON);
				#endif
				
				ACDetectCount = 3;
			}
		}
		else
		{
			ACDetectCount = 3;
		}

	}
	else
	{
		if(FLAG_AC_DETECT)
		{
			// Consecutive Samples of AC Detect OFF required
			if(--ACDetectCount == 0)
			{
				FLAG_AC_DETECT = 0 ;
				MAIN_Reset_Task(TASKID_GLED_MANAGER) ;

				if (FLAG_SERIAL_PORT_ACTIVE)
				{
					SER_Send_String("AC Off") ;
					SER_Send_Prompt() ;
				}
				// Disable AC Power off logging and only
				//  use AC Power On to indicate Power Cycle
				/*
				#ifdef _CONFIGURE_DIAG_HIST
					Diag_Hist_Que_Push(HIST_AC_POWER_OFF);
				#endif
				*/
				
				ACDetectCount = 3;
			}
			
		}
		else
		{
			ACDetectCount = 3;
		}
	}

}

#endif



// Test point Code only
void	Main_TP_On(void)
{
  #ifdef	_CONFIGURE_DEBUG_PIN_ENABLED
//	P3OUT |= PORT_UCA0CLK_PIN ;		// Used as Test Point Signal
  #endif
}

void	Main_TP_Off(void)
{
  #ifdef	_CONFIGURE_DEBUG_PIN_ENABLED
//	P3OUT &= ~PORT_UCA0CLK_PIN ;	// Used as Test Point Signal
  #endif
}

void	Main_TP_Toggle(void)
{
  #ifdef	_CONFIGURE_DEBUG_PIN_ENABLED
//	P3OUT ^= PORT_UCA0CLK_PIN ;		// Used as Test Point Signal
  #endif
}






//*************************************************************************
// Dummy Tasks Only
//
unsigned int SounderTask(void)
{
    return	Make_Return_Value(100) ;
}
unsigned int BTN_Task(void)
{
    return	Make_Return_Value(100) ;
}
unsigned int RLED_Manager(void)
{
    return	Make_Return_Value(100) ;
}
unsigned int PTT_Task(void)
{
    return	Make_Return_Value(100) ;
}
unsigned int BatteryTest_Task(void)
{
    return	Make_Return_Value(100) ;
}
unsigned int AlarmPriorityTask(void)
{
    return	Make_Return_Value(100) ;
}
unsigned int COAlarmTask(void)
{
    return	Make_Return_Value(100) ;
}
unsigned int CAL_Manager(void)
{
    return	Make_Return_Value(100) ;
}
unsigned int COMeasureTask(void)
{
    return	Make_Return_Value(100) ;
}
unsigned int GLED_Manager(void)
{
    return	Make_Return_Value(100) ;
}
unsigned int SND_Trouble_Manager(void)
{
    return	Make_Return_Value(100) ;
}
unsigned int VCE_Process(void)
{
    return	Make_Return_Value(100) ;
}

//*****************************************************************************
//	INTERRUPT VECTORS
//*****************************************************************************

