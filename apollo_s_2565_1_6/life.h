/****************************************************************
 * The contents of this file are Kidde proprietary and confidential.
 * *************************************************************/
// LIFE.H

#ifndef	_LIFE_H
#define	_LIFE_H

	#define	LFE_EXPIRATION_DAY  (3650 + 100)
	#define	LFE_UL_EXTENSION (LFE_EXPIRATION_DAY + 30)

        #ifdef _CONFIGURE_LITHIUM
            #define DEPASSIVATION_INTERVAL  30	// in days
        #endif

	void LFE_Check_EOL(void) ;
        UINT LIFE_Get_Current_Day(void) ;
        void LFE_Check(void) ;
        void Life_Init(void);

        #ifdef _CONFIGURE_LITHIUM
            void LFE_Depassivation(void) ;
            void LFE_Check_Depassivation(void) ;

            extern UCHAR Depass_Timer ;
        #endif

        #ifdef	_CONFIGURE_EOL_HUSH
            void LFE_UL_EOL_Snooze(void) ;
        #endif


#endif

