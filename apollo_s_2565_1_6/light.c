/****************************************************************
 * The contents of this file are Kidde proprietary and confidential.
 * *************************************************************/
// light.c
// Functions for light measurement
#define	_LIGHT_C

#include    "common.h"
#include    "light.h"
#include    "main.h"
#include    "greenled.h"
#include    "a2d.h"
#include    "led_display.h"
#include    "command_proc.h"
#include    "battery.h"

#ifdef _CONFIGURE_LIGHT_SENSOR


#define     LIGHT_INTERVAL_100_MS   100
#define     LIGHT_INTERVAL_500_MS   500
#define     LIGHT_INTERVAL_1000_MS  1000
#define     LIGHT_INTERVAL_2000_MS  2000
#define     LIGHT_INTERVAL_3000_MS  3000
#define     LIGHT_INTERVAL_30_SEC   30000


#define     LIGHT_TIMER_5_MINUTES   300
#define     LIGHT_TIMER_2_ACTIVE_SECONDS   2
#define     LIGHT_TIMER_1_ACTIVE_SECONDS   1
#define     LIGHT_TIMER_20_STBY_SECONDS    10


#define     LOW_BAT_INHIBIT_MAX_HR  16
#define     LOW_BAT_INHIBIT_MAX     (LOW_BAT_INHIBIT_MAX_HR * 3600)

#define     NIGHT_DAY_TH_HYST           20


// Light/Day Threshold = % of Min/Max Range
#define LGT_TH_PERCENT  30
#define NIGHT_LGT_TH_PERCENT  10

// A/D Reference for PIC = VDD
// FVR to calcultae VDD = 1024 mv
#define FVR_MVOLTS			1024
// FVR_MVOLTS * 1024
#define FVR_FACTOR                      1048576

#define STATE_LGT_INIT      0
#define STATE_LGT_IDLE      1
#define STATE_LGT_TEST      2
#define STATE_LGT_MEAS      3


// define minimum signal limit (delta in mv)
#define LGT_DETECT_MIN  110
#define MIN_DARK_PERIOD 6

//Local Prototypes
void Light_Analysis(void);

#ifdef _CONFIGURE_LIGHT_TEST_TABLE
    // Test Only
    void LGT_Struct_Init(void);
#endif

//LED Light Sense Definitions
static UCHAR LGT_State = STATE_LGT_IDLE;
static UINT  LGT_Timer = LIGHT_TIMER_2_ACTIVE_SECONDS;

static UINT  LB_Inhibit_Timer = 0;

static unsigned long Light_Meas_Avg = NIGHT_DAY_TH_DEFAULT + 100;
static UINT Light_Sample_Last = NIGHT_DAY_TH_DEFAULT + 100;
persistent UCHAR Current_Hour;
UINT Light_Min = 0;
UINT Light_Max = 0;
persistent UINT Night_Day_TH;
persistent UINT Night_Light_TH;


//UINT Night_DAY_TH_Hyst = 0;

//UCHAR Light_Tot_Drk_Count;
//UCHAR Light_Tot_Lgt_Count;

UCHAR Light_Longest_Drk_Count;
UCHAR Dark_Count;
UCHAR Light_Count;
UCHAR Dark_Hour;


// 24 Hour Light Data Structure {Hours 0 - 23)
persistent struct light_data Hour[24];


// Clear Light Data Structure
void Light_Data_Init(void)
{
    for(UCHAR i=0; i<24; i++)
    {
        Hour[i].Light_Current=0;
        Hour[i].Light_Previous=0;
        Hour[i].Attrib=0;

        FLAG_LIGHT_ALG_ON = 0;
    }
    Current_Hour = 0;
    Night_Day_TH = 0;
    Night_Light_TH = 0;

#ifdef _CONFIGURE_LIGHT_TEST_TABLE
    // Test Code Only
    LGT_Struct_Init();
    Light_Analysis();
#endif
}


/*
 * Light Manager handles timing for Light Measurement Sampling
 * In AC Power Mode, we want to sample faster for nightlight response (few seconds)
 * In DC Power mode, there is no nightlight so we can sample at a slower rate
 *
 *
 * Notes:  For Escape Light Models, in Alarm Mode the Escape Light ON
 *         could affect light Sampling measurements, so we need to evaluate data
 *         and either use previous hour data for that hour, previous 24 hour data
 *         for that sample or a FFFF Value to indicate alarm was present
 *         and handle that during 24 Hour analysis of the data
 */
unsigned int Light_Manager(void)
{

    // Test Only
//    SER_Send_Int('l',LGT_State, 10) ;

    switch (LGT_State)
    {

        case STATE_LGT_IDLE:
        {
            // If CO calibration/final test is in progress
            //  do not perform any Light sampling
            if((FLAG_CALIBRATION_COMPLETE == 0) ||
               (FLAG_CALIBRATION_UNTESTED == 1))
            {
                return  Make_Return_Value(LIGHT_INTERVAL_30_SEC) ;
            }

            if(--LGT_Timer == 0 )
            {
                if(FLAG_MODE_ACTIVE)
                {
                    // In Active Mode this is 1 second
                    LGT_Timer = LIGHT_TIMER_1_ACTIVE_SECONDS;
                }
                else
                {
                    // In Low Power Mode this is 20 seconds
                    LGT_Timer = LIGHT_TIMER_20_STBY_SECONDS;
                }
                
                // Delay this Light measurement if Green LED is ON
                // Normally the Green LED is OFF since it only blinks once
                // every 30 seconds. Resetting the Light Timer should guarantee
                // Green LED off for a period before a measurement.
                if((PORT_GREEN_LED == 0) && (FLAG_ESC_LIGHT_ON == 0))
                {
                    FLAG_LIGHT_MEAS_ACTIVE = 1;     // Delay Green LED blink
                    // Take a light Sample
                    LGT_State++;

                }
            }

//            Night_DAY_TH_Hyst = (Night_Day_TH + NIGHT_DAY_TH_HYST);

            // Test for LED Display bright/dim control
            if( (FLAG_LIGHT_ALG_ON) && (FLAG_CO_ALARM_CONDITION == 0) )
            {
                if(Light_Sample_Last < Night_Light_TH)
                {
                    Display_Set_Intensity(LED_DISP_LOW) ;
                    
                }
                else
                {
                    // Add Hysteresis if returning to Bright Intensity
                    if(LED_DISP_LOW == Display_Get_Intensity() )
                    {
                        if(Light_Sample_Last > (Night_Light_TH + NIGHT_DAY_TH_HYST) )
                        {
                            Display_Set_Intensity(LED_DISP_BRIGHT) ;
                        }
                    }
                    else
                    {
                        Display_Set_Intensity(LED_DISP_BRIGHT) ;
                    }
                }
            }
            else
            {
                if( FLAG_CO_ALARM_CONDITION == 0 )
                {
                    if(Light_Sample_Last < NIGHT_DAY_TH_DEFAULT)
                    {
                        Display_Set_Intensity(LED_DISP_LOW) ;
                        //Payload.cmd_returns = 0x07;
                    }
                    else
                    {
                        // Add Hysteresis if returning to Bright Intensity
                        if(LED_DISP_LOW == Display_Get_Intensity() )
                        {
                            if(Light_Sample_Last > (NIGHT_DAY_TH_DEFAULT + NIGHT_DAY_TH_HYST ))
                            {
                                Display_Set_Intensity(LED_DISP_BRIGHT) ;
                            }
                        }
                        else
                        {
                            Display_Set_Intensity(LED_DISP_BRIGHT) ;
                            //Payload.cmd_returns = 0x08;
                        }
                    }
                }

            }

            // ******* Low Battery Inhibit, Night Flag Control *******
            // If Night Data is valid and LOW Battery Inhibit is enabled and
            // If the current hour has night attribute set, Delay LB chirp
            if( (FLAG_LIGHT_ALG_ON) && (!FLAG_LOW_BAT_INHIBIT_DISABLE) )
            {
                // Does current hour match a dark hour?
                if(Hour[Current_Hour].Attrib & ATTRIB_NIGHT_BIT_SET)
                {
                    if(FLAG_LB_CHIRP_INHIBIT == 0)
                    {
                        // Delay the LB Chirp
                        FLAG_LB_CHIRP_INHIBIT = 1;

                        // Init the maximun allowable time for LB Inhibit
                        LB_Inhibit_Timer = LOW_BAT_INHIBIT_MAX;
//                        SER_Send_String("FLAG_LB_CHIRP_INHIBIT SET") ;
//                        SER_Send_Prompt() ;
                    }
                    else
                    {
                        if(--LB_Inhibit_Timer == 0)
                        {
                            // Allow LB Chirps
                            FLAG_LB_CHIRP_INHIBIT = 0;

                            // Disable LB Inhibit feature after Timer expires
                            // Flag only cleared by Soft or PWR Reset
                            FLAG_LOW_BAT_INHIBIT_DISABLE = 1;
//                            SER_Send_String("FLAG_LB_CHIRP_INHIBIT CLEAR") ;
//                            SER_Send_Prompt() ;
                        }
                    }
                }
                else
                {
                    FLAG_LB_CHIRP_INHIBIT = 0;
                }
            }
            else
            {
                FLAG_LB_CHIRP_INHIBIT = 0;
            }

           #ifdef _CONFIGURE_ESCAPE_LIGHT

            // ******* Test for Night Light ON/OFF *******
            if (PORT_PEAK_BTN == 0)
            {
                if( (FLAG_LIGHT_ALG_ON) && (FLAG_AC_DETECT) )
                {
                    // Is current light below dark/light threshold?
                    if(Light_Sample_Last < Night_Light_TH)
                    {
                        FLAG_NIGHT_LIGHT_ON = 1;
                    }
                    else
                    {
                        // Add Hysteresis if Night Light is On
                        if(FLAG_NIGHT_LIGHT_ON == 1)
                        {
                            if(Light_Sample_Last > (Night_Light_TH + NIGHT_DAY_TH_HYST))
                            {
                                FLAG_NIGHT_LIGHT_ON = 0;
                            }
                        }
                        else
                        {
                            FLAG_NIGHT_LIGHT_ON = 0;
                        }
                    }
                }
                else
                {
                    // We could test light against a default light value
                    // here and set the Light On since the Dark/Light Algorithm
                    // was not enabled.
                    if(FLAG_AC_DETECT)
                    {
                        if(Light_Sample_Last < NIGHT_DAY_TH_DEFAULT)
                        {
                            FLAG_NIGHT_LIGHT_ON = 1;
                        }
                        else
                        {
                            // Add Hysteresis if Night Light is On
                            if(FLAG_NIGHT_LIGHT_ON == 1)
                            {
                                if(Light_Sample_Last > (NIGHT_DAY_TH_DEFAULT + NIGHT_DAY_TH_HYST ))
                                {
                                    FLAG_NIGHT_LIGHT_ON = 0;
                                }
                            }
                            else
                            {
                                FLAG_NIGHT_LIGHT_ON = 0;
                            }
                        }

                    }
                    else
                    {
                        FLAG_NIGHT_LIGHT_ON = 0;
                    }
                }
            }
         #endif



        }
        break;

        case STATE_LGT_TEST:
        {
            // Read LED as Analog Input
            LIGHT_LED_INPUT
            LIGHT_LED_ANALOG_IN

            LGT_State++;

            // Add Some Light Sample Stabilization time for
            //  Light Detector Voltage
            return  Make_Return_Value(LIGHT_INTERVAL_2000_MS) ;

        }

        case STATE_LGT_MEAS:
        {
            // Read 1.024 FVR for VDD calculation
            UINT GLED_FVR_Measured = A2D_Convert(A2D_FVR_VOLTAGE) ;

            // Take a Light Sample
            UINT result = A2D_Convert(A2D_LIGHT_VOLTAGE);

            for (UCHAR i=0;i<4;i++)
            {
                delay_ms(1);
                // Take a Light Sample
                result = ( result + A2D_Convert(A2D_LIGHT_VOLTAGE) ) /2;
            }

            // Return Green Led Pin to digital output
            LIGHT_LED_OUTPUT
            LIGHT_LED_DIGITAL_OUT


            // Calculate current VDD voltage
            unsigned long GLED_Vdd = (FVR_FACTOR / GLED_FVR_Measured);

            // From VDD calculation, calculate the Green Led measured Voltage
            unsigned long GLED_Measured = ((unsigned long)(GLED_Vdd * result)/1024);
            // Save Light Sample
            Light_Sample_Last = GLED_Measured;

            if(Light_Meas_Avg == 0)
            {
                Light_Meas_Avg = (UINT)GLED_Measured;
            }
            Light_Meas_Avg = (((Light_Meas_Avg * 3) + GLED_Measured)/4);

//           if (FLAG_SERIAL_PORT_ACTIVE)
//           {
//                #ifdef _CONFIGURE_OUTPUT_RAW_LIGHT
//                    SER_Send_String("Lra");
//                    SER_Send_Int('w', (UINT)Light_Sample_Last, 10) ;
//                    SER_Send_String(", Lav");
//                    SER_Send_Int('g', (UINT)Light_Meas_Avg, 10) ;
//                    SER_Send_Prompt() ;
//                #else
//                    SER_Send_Int('L', (UINT)Light_Meas_Avg, 10) ;
//                    SER_Send_Prompt() ;
//                #endif
//
//                /*
//                // Test Only  Math Test
//                #define TH_PERCENT  35
//                Light_Min = 100;
//                Light_Max = 950;
//
//                unsigned long Matha = ((( unsigned long)Light_Max - Light_Min) << 16);
//                Matha = ( ((Matha * TH_PERCENT)/100)  >> 16) ;
//                Matha += Light_Min;
//
//                SER_Send_String("Test TH");
//                SER_Send_Int('-', (UINT)Matha, 10) ;
//                SER_Send_Prompt() ;
//                */
//
//           }

            FLAG_LIGHT_MEAS_ACTIVE = 0;         // Light Sample Completed
            LGT_State = STATE_LGT_IDLE;
        }

        default:
            LGT_State = STATE_LGT_IDLE;
            break;

    }

    if(FLAG_MODE_ACTIVE)
        return  Make_Return_Value(LIGHT_INTERVAL_1000_MS) ;
    else
        return  Make_Return_Value(LIGHT_INTERVAL_2000_MS) ;


}


/*
 * Analysis of Light Data Structure to determine Dark/Light Threshold
 * and determine a night day cycle for Low Battery Chirp control.
 *
 */
void Light_Analysis(void)
{

    // Collect signal levels from most recent 24 hours
    Light_Max = Hour[0].Light_Current;
    Light_Min = Hour[0].Light_Current;

    // Record min and max light samples for previous 24 hours
    for(UINT h=0;h<24;h++)
    {
        if(Hour[h].Light_Current > Light_Max)
        {
            Light_Max = Hour[h].Light_Current;
        }
        if(Hour[h].Light_Current < Light_Min)
        {
            Light_Min = Hour[h].Light_Current;
        }
    }

//    SER_Send_Int( '1',Light_Min, 10 ) ;
//    SER_Send_Prompt() ;
//    SER_Send_Int( '2',Light_Max, 10 ) ;
//    SER_Send_Prompt() ;


    // If light signal detected < LGT_DETECT_MIN then disable
    // Light Algorithm Flag and start next 24 Hours
    // (either all light or all dark measured)
    if( (Light_Max - Light_Min) < LGT_DETECT_MIN)
    {
        FLAG_LIGHT_SIGNAL_GOOD = 0;

        // Disable Light ALG
        // This will use default settings for any Night Light Control
        // and Low Battery Chirp control.
        FLAG_LIGHT_ALG_ON = 0;

        for(UCHAR i=0; i<24; i++)
        {
            // Save to previous 24 hours data and prepare
            // for next 24 hour measurement period
            Hour[i].Light_Previous = Hour[i].Light_Current;
            Hour[i].Light_Current=0;
            // Clear Attributes
            Hour[i].Attrib = 0;
        }


    }
    else
    {
        FLAG_LIGHT_SIGNAL_GOOD = 1;

        // Set Dark/Light Threshold for this analysis
        unsigned long Matha = ((( unsigned long)Light_Max - Light_Min) << 16);
        Matha = ( ((Matha * LGT_TH_PERCENT)/100)  >> 16) ;
        Matha += Light_Min;
        Night_Day_TH = (UINT)Matha;

        unsigned long Matha = ((( unsigned long)Light_Max - Light_Min) << 16);
        Matha = ( ((Matha * NIGHT_LGT_TH_PERCENT)/100)  >> 16) ;
        Matha += Light_Min;
        Night_Light_TH = (UINT)Matha;


        //Locate 1st Light Transition and set starting hour
        // for data light analysis.
        if(Hour[0].Light_Current > Night_Day_TH)
        {
            // Look for Light to Dark transition
            for(UINT h=1;h<24;h++)
            {
                if(Hour[h].Light_Current < Night_Day_TH)
                {
                    FLAG_LIGHT=0;       //Init the Light Flag
                    Current_Hour = h;
                    // Set to 24 to immediately terminate loop
                    h=24;
                }
            }
        }
        else
        {
            // Look for Dark to Light transition
            for(UINT h=1;h<24;h++)
            {
                if(Hour[h].Light_Current > Night_Day_TH)
                {
                    FLAG_LIGHT=1;       //Init the Light Flag
                    Current_Hour = h;
                    // Set to 24 to immediately terminate loop
                    h=24;
                }
            }
        }


        // Init counters
        // Light_Tot_Drk_Count=0;
        // Light_Tot_Lgt_Count=0;
        Light_Longest_Drk_Count=0;
        Dark_Count=0;
        Light_Count=0;

        // Sweep 24 hour data and determine dark hour counts.
        // Current hour is pointing to 1st detected light transition hour
        for(UINT h=0;h<24;h++)
        {
            if(Current_Hour > 23)
                Current_Hour=0;

//            SER_Send_Int( 'c',Current_Hour, 10 ) ;
//            SER_Send_Prompt() ;


            if(Hour[Current_Hour].Light_Current > Night_Day_TH)
            {

                // Set Light Detected attribute for this hour
                Hour[Current_Hour].Attrib = 0x80;

                // Light Hour
                if(FLAG_LIGHT==0)
                {
                    //Dark to Light Transition detected, Init Light counter
                    Light_Count =1;
                }
                else
                {
                    // Next Light Hour
                    Light_Count++;
                }

               //Light_Tot_Lgt_Count++;
               FLAG_LIGHT=1;        // Indicate we are in Light Counting sequence
            }
            else
            {
                // Clr Light Detected attribute for this hour
                Hour[Current_Hour].Attrib = 0x00;

                // Dark Hour
                if(FLAG_LIGHT==1)
                {
                    //Light to Dark transition
                    if (Light_Count < 4)
                    {
                        // Filter out (disregard) Light Hours if under 4 hours and
                        // Count towards dark hours
                        Dark_Count += Light_Count;
                    }
                    else
                    {
                        //Light to dark transition, init dark counter
                        Dark_Count = 1;
                    }

                }
                else
                {
                    //Next dark hour
                    Dark_Count++;
                }

                // Record the longest dark Period detected
                if(Dark_Count > Light_Longest_Drk_Count)
                {
                    // Save longest detected Dark Hour and Count
                    Light_Longest_Drk_Count = Dark_Count;
                    Dark_Hour = Current_Hour;

                }

                //Light_Tot_Drk_Count++;
                FLAG_LIGHT=0;   // Indicate we are in Dark Counting sequence

            }

            Current_Hour++;
        }

        // OK
        // Dark Hours have been tested and hour counts set.
        // Attempt to define night period from dark hours
        //

        // 1st move data to previous storage area of structure
        for(UCHAR h=0; h<24; h++)
        {
            // Save to previous 24 hours data and prepare
            // for next 24 hour measurement period
            Hour[h].Light_Previous = Hour[h].Light_Current;
            Hour[h].Light_Current=0;
        }

        // Dark count must be a large enough period (MIN_DARK_PERIOD)
        //  to be accepted as a recognized night period.
        if(Light_Longest_Drk_Count > (MIN_DARK_PERIOD-1) )
        {
            // Find the center hour of longest dark period
            //  then set night attribute +/- 6 hours from center
            Current_Hour = Dark_Hour;
            for (UCHAR i=(Dark_Count>>1);i>0;i--)
            {
                //Backup into center of dark period
                if(--Current_Hour == 0xFF)
                    Current_Hour = 23;       // wrap to last hour

            }
            // Back up another 6 hours
            for (UCHAR i= 6;i>0;i--)
            {
                if(--Current_Hour == 0xFF)
                    Current_Hour = 23;      // wrap to last hour

            }

            // Now we are pointing to the start of night period
            // For a 12 Hour period, set the Night Detect Attribute
            for(UCHAR i=0;i<12;i++)
            {
                Hour[Current_Hour].Attrib = Hour[Current_Hour].Attrib | ATTRIB_NIGHT_BIT_SET;

                if(++Current_Hour > 23)
                    Current_Hour=0;

            }

            // Also fill in any Zero Attribute hours with ATTRIB_NIGHT_BIT_SET
            //  to expand Night Hours greater than 12 hours.
            // These should be hours that were below the Light/Day threshold
            //  but just outside the 12 hour night period.
            for(UCHAR h=0; h<24; h++)
            {
                if(Hour[h].Attrib == 0)
                {
                    Hour[h].Attrib = Hour[h].Attrib | ATTRIB_NIGHT_BIT_SET;
                }

            }

//            SER_Send_String("Night Detect") ;
//             SER_Send_Prompt() ;


            // Indicate that we have valid Light Algorithm data
            FLAG_LIGHT_ALG_ON = 1;

            // Reset FLAG that disables LB Chirp Inhibit
            FLAG_LOW_BAT_INHIBIT_DISABLE = 0;
        }
        else
        {
            // Unable to determine Night
            // Set Light Alg off
            FLAG_LIGHT_ALG_ON = 0;

        }

    }

    // Init for the next 24 Hours of data collection
    // We dynamically adjust Night detected for multiple reasons
    // 1. Our 24 hour timer is based upon a 1% clock so timing drifts
    //     requiring contunuous light sampling.
    // 2. Alarm may be moved to a different location.
    // 3. Light levels may change at the current location over time
    //     so we may need to redefine light/dark thresholds
    Current_Hour = 0;

    // After each 24 hour analysis, check for battery Test interval change
    BAT_Set_Test_Interval(ALG_RESET);
}


/*
 * Light record is called every Hour to fill the Light Data Structure
 * with Hourly Avg. Light measurements and call Light Analysis once
 * a full 24 hour cycle has been recorded.
 */
void Light_Record(void)
{
    Hour[Current_Hour].Light_Current = Light_Meas_Avg;
    // 24 hour Hour cycle
    if(++Current_Hour == 24)
    {
        // Analyze the Data for Light Algorithm and set attributes
        Light_Analysis();

#ifdef _CONFIGURE_OUTPUT_LIGHT_STRUCT
        if(FLAG_SERIAL_PORT_ACTIVE)
        {
            // For Test purposes and Data Collection transmit Data Structure
            //  thru the serial port
//            Command_Processor("L");
        }
#endif

    }

}

#else

// Placeholders for global routines
unsigned int Light_Manager(void)
{
    return  Make_Return_Value(59000) ;
}
void Light_Record(void)
{

}

#endif




#ifdef _CONFIGURE_LIGHT_TEST_TABLE

//*****************************************************************************
// This is used for testing only for the 1st 24 (accelerated minutes)

UINT LGT_TEST_TABLE[24]=
{
    1100,       //0
    1101,
    1102,
    1103,
    1104,
    105,
    106,
    107,
    108,
    109,
    110,
    111,
    112,
    113,
    114,
    1115,
    1116,
    1117,
    1118,
    1119,
    1120,
    1121,
    1122,
    1123        //23

};



/*
//*****************************************************************************
// Test Only,  with real light data
UINT LGT_TEST_TABLE[24]=
{
    34,       //0
    26,
    24,
    21,
    21,
    21,
    21,
    21,
    21,
    21,
    21,
    21,
    21,
    45,
    321,
    972,
    1186,
    1199,
    1196,
    1193,
    531,
    334,
    229,
    175        //23

};
*/

void LGT_Struct_Init(void)
{
    UINT x;

    for(UCHAR i=0;i<24;i++)
    {
        x = LGT_TEST_TABLE[i];
        Hour[i].Light_Current = x;
    }

}

#endif




