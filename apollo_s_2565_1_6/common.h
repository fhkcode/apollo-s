/****************************************************************
 * The contents of this file are Kidde proprietary and confidential.
 * *************************************************************/
// COMMON.H
// Contains info for all modules.

#ifndef	_COMMON_H
#define	_COMMON_H

    #define	TRUE	1
    #define	FALSE	0
    #define ON      1
    #define OFF     0

    #define LSB_TO_UINT(lsb) (*((UINT*)&(lsb)))

    typedef unsigned int UINT ;
    typedef unsigned char UCHAR ;

    #include    <xc.h>
    #include    <pic.h>

    #include    "config.h"
    #include    "main.h"

    #include    "pic_helper.h"
    #include    "pic16lf1938.h"

    #include    "io.h"
    #include    "flags.h"

    #include    "serial.h"

#endif
