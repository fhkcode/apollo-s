/****************************************************************
 * The contents of this file are Kidde proprietary and confidential.
 * *************************************************************/
// escape.c
// Functions for escape light
#define	_ESCLIGHT_C

#include    "common.h"
#include    "esclight.h"
#include    "main.h"


#ifdef	_CONFIGURE_ESCAPE_LIGHT

// prototypes
void Esc_Init_PWM(void);
void Esc_Set_PWM_Escape(void);
void Esc_Light_On(void);
void Esc_Light_Off(void);



// Calculations for PWM Period (16Mhz)
#define TOSC 62500           //in pico seconds (.0625 usecs)
#define TIMER_PRESCALER 1   // Prescaler value used for PWM Timer (TxCON)


/*
// PWM set for - 100KHZ w/8.0 usec on time
#define	ESC_LGT_PERIOD	10000      //in nano seconds (10 usecs) = 100KHZ
#define ESC_LGT_PR4	(ESC_LGT_PERIOD /((4 * TOSC * TIMER_PRESCALER) /1000))
#define	ESC_LGT_PW  	8000000    // in pSecs (8 usecs) = 80% duty cycle
#define ESC_LGT_CCPRxL	( ((ESC_LGT_PW / TOSC)/4) +1 )	// Sets MS 8 bits
*/

/*
// PWM set for - 59.880 KHZ w/13.35 usec on time
#define	ESC_LGT_PERIOD	16700      // in nano seconds (16.7 usecs) = 59.880 KHZ
#define ESC_LGT_PR4	(ESC_LGT_PERIOD /((4 * TOSC * TIMER_PRESCALER) /1000))
#define	ESC_LGT_PW	13350000   // in pSecs (13.35 usecs) PW ON time
#define ESC_LGT_CCPRxL	( ((ESC_LGT_PW / TOSC)/4) +1 )	// Sets MS 8 bits
*/


// PWM set for - 20 KHZ w/50% Duty cycle
#define	ESC_LGT_PERIOD	50000      // in nano seconds (50 usecs) = 20.0 KHZ
#define ESC_LGT_PR4	(ESC_LGT_PERIOD /((4 * TOSC * TIMER_PRESCALER) /1000))
#define	ESC_LGT_PW	25000000   // in pSecs (25 usecs) PW ON time
#define ESC_LGT_CCPRxL	( ((ESC_LGT_PW / TOSC)/4) +1 )	// Sets MS 8 bits

#define ESC_TEST_PW     47500000
#define ESC_LGT_TEST	( ((ESC_TEST_PW / TOSC)/4) +1 )	// Sets MS 8 bits

// Set default Night Light Pulse Width ~5uSec PW
#define NIGHT_LGT_PULSE  176



#define ESC_LIGHT_INTERVAL_100_MS       100
#define ESC_LIGHT_INTERVAL_1000_MS      1000

#define	ESC_STATE_INIT      0
#define	ESC_STATE_IDLE      1
#define	ESC_STATE_ON        2
#define ESC_STATE_NIGHT_ON  3
#define ESC_STATE_TEST      4

#define  NL_RAMP_UP         0
#define  NL_RAMP_DOWN       1
#define  NL_OFF             2

static unsigned char Esc_Light_State = ESC_STATE_INIT;
static unsigned char NL_Intensity_State = NL_RAMP_UP;

persistent static unsigned char Night_Light_PW;

unsigned int NL_Intensity_Disp;


/*
 * The escape Light option uses high intensity CREE LED
 * When Alarm is acive the Escape Light is enabled by
 * 1. Enabling the Escape Light Lithium Battery Power Source
 * 2. Activate the Escape Light PWM drive signal.
 *
 * The Night Light option also uses the CREE LED, however,
 * it is run at a lower light level by controlling the duty cycle
 * of the PWM drive signal, and operates on AC Power source only.
 * 1. If AC Power is available:
 *    Disable the Escape Light battery Power source.
 * 2. Activate the Night Light PWM drive signal.
 * 
 */

void Esc_Init(void)
{
    Night_Light_PW = NIGHT_LGT_PULSE;
}


unsigned int Esc_Light_Process(void)
{

    switch	(Esc_Light_State)
    {
        case	ESC_STATE_INIT:
            if(!FLAG_SERIAL_PORT_ACTIVE)
            {
                Esc_Init_PWM();
                Esc_Light_State = ESC_STATE_IDLE;
            }
            else
            {
                // Escape / Night Light Disabled
                Esc_Init_PWM();
                return  Make_Return_Value(45000) ;
            }

        break;

        case	ESC_STATE_IDLE:

            if(!FLAG_SERIAL_PORT_ACTIVE)
            {

                if( FLAG_ESC_LIGHT_ON )
                {
                    //Set the Escape Light Duty Cycle
                    Esc_Set_PWM_Escape();
                    Esc_Light_State = ESC_STATE_ON;
                }
                else if( (FLAG_NIGHT_LIGHT_ON) && (FLAG_AC_DETECT) && (FLAG_NIGHT_LIGHT_DISABLED == 0) )
                {

                    //Set the NightLight Duty Cycle
                    Esc_Set_PWM_Night(Night_Light_PW);
                    Esc_Light_State = ESC_STATE_NIGHT_ON;
                }

                return  Make_Return_Value(ESC_LIGHT_INTERVAL_100_MS) ;
            }
        break;

        case	ESC_STATE_ON:
            if(FLAG_ESC_LIGHT_ON)
            {
                Esc_Light_On();
            }
            else
            {
                Esc_Light_Off();
                Esc_Light_State = ESC_STATE_IDLE;

            }
            return  Make_Return_Value(ESC_LIGHT_INTERVAL_100_MS) ;

        case	ESC_STATE_NIGHT_ON:
 
                // ESC Light takes precedence over Night Light
                // AC Power is required to run the Night Light
                if( (FLAG_NIGHT_LIGHT_ON) && 
                    (FLAG_AC_DETECT) &&
                    (!FLAG_ESC_LIGHT_ON) )
                {
                    // Enables PWM to Active
                    // CCP4 control, Single Output(7:6)=00,PWM Mode(3:0)=1100,
                    //  and LS Pulse Width Output(bits 5:4)
                    CCP4CON = 0x0C;
                }
                else
                {
                    Esc_Light_Off();
                    Esc_Light_State = ESC_STATE_IDLE;
                }

                return  Make_Return_Value(ESC_LIGHT_INTERVAL_100_MS) ;

    }

    return  Make_Return_Value(ESC_LIGHT_INTERVAL_1000_MS) ;

}

// Set the PWM duty cycle for Escape Light
void Esc_Set_PWM_Escape(void)
{

 /* Marketing wanted the Full Intensity during PTT, comment out this code
    if (FLAG_PTT_ACTIVE)
    {
        // Lower Intensity for Test
        CCPR4L = ESC_LGT_TEST;
    }
    else
    {
 */
        /*
         CCPR4L(7:0) and CCP4CON(5:4) = 00 1000 0000 = 0x80 = 128
         CCP4 Pulse Output (ESC_LGT_PW usecs) (each count = 62.5 ns)
         with 16Mhz clock and prescaler of 1
        */
        CCPR4L = ESC_LGT_CCPRxL;
 // }

}

// Set the PWM duty cycle for Night Light
// Night_Light_PW holds current Light Intensity PW
void Esc_Set_PWM_Night(UCHAR pw)
{
    /*
     CCPR4L(7:0) and CCP4CON(5:4) = 00 1000 0000 = 0x80 = 128
     CCP4 Pulse Output (ESC_LGT_PW usecs) (each count = 62.5 ns)
     with 16Mhz clock and prescaler of 1
    */
    CCPR4L = pw;

}

// Here we define the PWM values for Max and Min Range
// Note: Max PWM translates to the Lowest Light Intensity
#define NL_PW_MAX 200
#define NL_PW_MIN 140

// Sets the Digital Display NL Intensity units
#define NL_FACTOR ( (NL_PW_MAX - NL_PW_MIN ) / 10)


// Cycle Night Light Intensity to next intensity
void Esc_Next_NL_Intensity(unsigned char i)
{
    if(INIT_NL_STATE == i)
    {
        NL_Intensity_State = NL_RAMP_UP;
        return;

    }
    switch (NL_Intensity_State)
    {

        case NL_RAMP_UP:

            // Insure PW is in range in case NL started from OFF condition
            if(Night_Light_PW >NL_PW_MAX)
                Night_Light_PW = NL_PW_MAX;

            NL_Intensity_Disp = (NL_PW_MAX - Night_Light_PW) / NL_FACTOR;

            // Keep in Bounds for display
            if(NL_Intensity_Disp > 10)
                NL_Intensity_Disp = 10;

            //Set the NightLight Duty Cycle
            Esc_Set_PWM_Night(Night_Light_PW);
            FLAG_NIGHT_LIGHT_DISABLED = 0;

            Night_Light_PW = Night_Light_PW - 2;

            if (Night_Light_PW < NL_PW_MIN)
            {
                NL_Intensity_State = NL_RAMP_DOWN;
            }

            break;


        case NL_RAMP_DOWN:

            NL_Intensity_Disp = (NL_PW_MAX - Night_Light_PW) / NL_FACTOR;
            // Keep in Bounds for display
            if(NL_Intensity_Disp > 10)
                NL_Intensity_Disp = 10;

            //Set the NightLight Duty Cycle
            Esc_Set_PWM_Night(Night_Light_PW);

            Night_Light_PW = Night_Light_PW + 2;

            if (Night_Light_PW > NL_PW_MAX)
            {
                //  Increase to insure NL stays OFF
                Night_Light_PW = NL_PW_MAX + 8;
                Esc_Set_PWM_Night(Night_Light_PW);
                NL_Intensity_State = NL_OFF;
                FLAG_NIGHT_LIGHT_DISABLED = 1;
            }
            break;


        case NL_OFF:
            // TODO:  I think it should be (Night_Light_PW - 8) to start
            //        ramping up where we left Off
            Night_Light_PW = Night_Light_PW - 8;
            //Night_Light_PW = NL_PW_MAX - 8;
            NL_Intensity_State = NL_RAMP_UP;
            break;

    }

}

/*
;   Init PWM channel for Escape/Night Light output, Timer 4 used.
;   PR4 contains frequency Period time, Pulse ON time is in CCPR4L and CCP4CON
;
;   Set Period to about 20KHz, (50 usec)
;   Set Pulse to 25 usec (50/50 duty cycle)=(00 0011 0010) 32 Hex
*/
void Esc_Init_PWM(void)
{
    Esc_Light_Off();
    /*
     Set Test Signal Period to 100usec
     w/Timer 4 Control register PS = 1, Fosc=16MHZ
     (139+1)* 4 * .0625usec * 1 = 10 usec
    */

    // Set fixed 20KHz Period
    PR4 = ESC_LGT_PR4;

    /*
     CCPR4L(7:0) and CCP4CON(5:4) = 0000 0000 00
     CCP4 Pulse Output (ESC_LGT_PW usecs) (each count = 62.5 ns)
     with 16Mhz clock and prescaler of 1
     *
     * each CCPR4L bit represents (62.5 x 4) ns = .25uSec
     * max PW = .25 x 255 = 63.75 uSec
     * 
    */
    CCPR4L = ESC_LGT_CCPRxL;
    
    // Use Timer 4 module for PWM CCP4
    CCPTMRS0 = 0x40;    // Route Timer 4 to CCP4 Module

    T4CON = 0x04;       // Timer 2 Contol, Postscaler = 1, Prescaler =  1, Timer OFF
                        // NOTE: Set T4CON bit 2 to start PWM timer

    // Disable PWM Ints
    PIE3 = 0x00;
    PIR3 = 0x00;

}

// Lower Level Driver routines
void Esc_Light_On(void)
{
    // Set the High Current Battery Power Source
    ESCAPE_BATTERY_OUTPUT            // Battery I/O Enabled
    ESCAPE_BATTERY_ON                // Battery Active

    ESCAPE_LIGHT_OUTPUT

    // Enables PWM to Active
    // CCP4 control, Single Output(7:6)=00,PWM Mode(3:0)=1100,
    //  and LS Pulse Width Output(bits 5:4)
    CCP4CON = 0x0C;

}

void Esc_Light_Off(void)
{
    // CCPr control - disable PWM drive
    CCP4CON = 0x00;
    PORT_ESCAPE_LIGHT = 1;
    ESCAPE_LIGHT_OUTPUT

    
    // High Current Battery Power Source is OFF
    ESCAPE_BATTERY_OUTPUT
    ESCAPE_BATTERY_OFF

}


// No Escape Light option
#else

unsigned int Esc_Light_Process(void)
{

    return  Make_Return_Value(61000) ;
}


void Esc_Next_NL_Intensity(unsigned char i)
{

}
#endif