/* 
 * File:   main.h
 * Author: bill.chandler
 *
 * Created on May 3, 2013, 9:48 AM
 */

#ifndef MAIN_H
#define	MAIN_H

#ifdef	__cplusplus
extern "C" {
#endif




#ifdef	__cplusplus
}
#endif

typedef struct
{
   unsigned char status;
   unsigned char status_csum;
   unsigned int offset;
   unsigned int slope;
   unsigned char unused1;
   unsigned char unused2;
   unsigned int age;
   unsigned char age_csum;

} DataStructure;







#endif	/* MAIN_H */

