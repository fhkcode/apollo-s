/****************************************************************
 * The contents of this file are Kidde proprietary  and confidential.
 * *************************************************************/
// systemdata.c
#define	_SYSTEMDATA_C


#include	"common.h"
#include 	"systemdata.h"
#include    "memory_1936.h"
#include    "diaghist.h"
#include    "fault.h"
#include    "life.h"

// Prototypes
UCHAR   SYS_Calc_Checksum(UCHAR addr);
void    SYS_Load(UCHAR addr);



// Define and locate Working Data Structure
struct SystemData volatile SYS_RamData @ 0x00A0;

/*
 * System data structures are declared and initialized in EE_Data Structure
 * so that they can be located at EEProm address 0x00 and 0x10
// Non Volatile memory resides in EEProm 0x00
eeprom struct SystemData SYS_FlashData_Primary = {
    CO_STATUS_INIT_VAL,
    CO_CHECKSUM_INIT_VAL,
    CO_OFFSET_LSB_INIT_VAL,
    CO_OFFSET_MSB_INIT_VAL,
    CO_SCALE_LSB_INIT_VAL,
    CO_SCALE_MSB_INIT_VAL,
    UNUSED_INIT_VAL,
    UNUSED_INIT_VAL,
    LIFE_LSB_INIT_VAL,
    LIFE_MSB_INIT_VAL,
    LIFE_CHECKSUM_INIT_VAL,
    UNUSED_INIT_VAL,
    UNUSED_INIT_VAL,
    UNIT_ID_INIT,
    UNIT_ID_INIT,
    CHECKSUM

 };

// Locate in EEprom 0x10
eeprom struct SystemData SYS_FlashData_Backup ={
    CO_STATUS_INIT_VAL,
    CO_CHECKSUM_INIT_VAL,
    CO_OFFSET_LSB_INIT_VAL,
    CO_OFFSET_MSB_INIT_VAL,
    CO_SCALE_LSB_INIT_VAL,
    CO_SCALE_MSB_INIT_VAL,
    UNUSED_INIT_VAL,
    UNUSED_INIT_VAL,
    LIFE_LSB_INIT_VAL,
    LIFE_MSB_INIT_VAL,
    LIFE_CHECKSUM_INIT_VAL,
    UNUSED_INIT_VAL,
    UNUSED_INIT_VAL,
    UNIT_ID_INIT,
    UNIT_ID_INIT,
    CHECKSUM

 };
*/

// EEProm Data contains Primary, Backup data structures
// and Diagnostic History Information.
eeprom struct EE_Memory EE_Data =
{
     // Primary Copy of data Structure (0x00 - 0x0F)
     {
        CO_STATUS_INIT_VAL,
        CO_CHECKSUM_INIT_VAL,
        CO_OFFSET_LSB_INIT_VAL,
        CO_OFFSET_MSB_INIT_VAL,
        CO_SCALE_LSB_INIT_VAL,
        CO_SCALE_MSB_INIT_VAL,
        BAT_SCALE_LSB,
        BAT_SCALE_MSB,
        LIFE_LSB_INIT_VAL,
        LIFE_MSB_INIT_VAL,
        LIFE_CHECKSUM_INIT_VAL,
        UNUSED_INIT_VAL,
        UNUSED_INIT_VAL,
        UNIT_ID_INIT,
        UNIT_ID_INIT,
        CHECKSUM

     },

     // backup Copy of data Structure (0x10 - 0x1F)
     {
        CO_STATUS_INIT_VAL,
        CO_CHECKSUM_INIT_VAL,
        CO_OFFSET_LSB_INIT_VAL,
        CO_OFFSET_MSB_INIT_VAL,
        CO_SCALE_LSB_INIT_VAL,
        CO_SCALE_MSB_INIT_VAL,
        UNUSED_INIT_VAL,
        UNUSED_INIT_VAL,
        LIFE_LSB_INIT_VAL,
        LIFE_MSB_INIT_VAL,
        LIFE_CHECKSUM_INIT_VAL,
        UNUSED_INIT_VAL,
        UNUSED_INIT_VAL,
        UNIT_ID_INIT,
        UNIT_ID_INIT,
        CHECKSUM

     },

     // Diagnostic History Data Structure
     {
        // 0x20 - 0x2F
        0,0,0,00,00,00,0,0,00,0,REVISION_MAJOR,REVISION_MINOR,
        // 0x30 - 0x3F
        0,0,0,0,00,00,0,0,0,0,0,QPTR_CKSUM_INIT,MAJOR_QPTR_INIT,MINOR_QPTR_INIT,

        // 0x40 - 0x9F
        0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
        0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
        0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
        0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
        0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
        0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,

        // 0xA0 - 0xFF
        0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
        0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
        0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
        0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
        0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
        0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,

     }
};

//*************************************************************************
// Copies the flash version of the system data structure into RAM.
UCHAR	SYS_Init(void)
{
	// Make sure that the data saved in flash is valid by verifying the checksum.
	// The primary copy is checked first.

	if ( SYS_Calc_Checksum(PRIMARY_START) != Memory_Byte_Read((UCHAR)&EE_Data.Primary.Checksum) )
	{
		// Checksum of primary block is corrupted.  See if backup block is correct.
		if ( SYS_Calc_Checksum(BACKUP_START) != Memory_Byte_Read((UCHAR)&EE_Data.Backup.Checksum) )
		{
			// Both copies are corrupt.  That's a fatal fault.
			FLT_Fault_Manager(FAULT_MEMORY) ;
			return (TRUE) ;	
		}
		else
		{
			// Copy Backup Data into System Ram Data Structure
			// and restore Primary Data Structure
			SYS_Load(BACKUP_START);
			SYS_Save();
			FLAG_MEMORY_FAULT = 0;
		}
	}
	else
    {
		// Copy Primary Data into System Ram Data Structure
		SYS_Load(PRIMARY_START);
		FLAG_MEMORY_FAULT = 0;
    }

	return (FALSE) ;
}


//*************************************************************************
// Calculates checksum of system data structure
// Data Structure is 16 bytes long, CSUM is sum of 1st 15 bytes
// addr - points to start of Data Structure to calculate
UCHAR SYS_Calc_Checksum(UCHAR addr)
{
	UCHAR sum = 0 ;
    UINT i ;

	for (i=0 ; i<( sizeof(SYS_RamData)-1 ); i++)
	{
            UCHAR byte = Memory_Byte_Read(addr+i);
            sum += byte ;
	}

	return sum ;
}

//*************************************************************************
// Calculates checksum of system data structure
// Data Structure is 16 bytes long, CSUM is sum of 1st 15 bytes
UCHAR SYS_Calc_Ram_Checksum(UCHAR *addr)
{
	UCHAR sum = 0 ;
        UINT i ;

	for (i=0 ; i<( sizeof(SYS_RamData)-1 ); i++)
	{
            UCHAR byte = *addr++;
            sum += byte ;
	}

	return sum ;
}

//*************************************************************************
// Load SYS Ram Data structure from EEProm copy
void SYS_Load( UCHAR addr)
{
	UCHAR * ptr;
	ptr = (UCHAR*) &SYS_RamData;

	for (UCHAR i=0 ; i!=(sizeof(SYS_RamData)); i++)
	{
		*ptr = Memory_Byte_Read(addr+i);
		ptr++;
	}

}


//**************************************************************************
// Saves contents of SYS_RamData to NV memory.
// Row 0 of Upper Flash will contain Primary and Backup structure data.
// Rows 1-7 will contain Diagnostic History information.

void SYS_Save(void)
{
    // 1st Calculate the new RamData checksum
    SYS_RamData.Checksum = SYS_Calc_Ram_Checksum((UCHAR*)&SYS_RamData);

    // Save Ram Data Structure to Non-Volatile Flash memory structures
    Memory_Write_Block((UCHAR) sizeof(SYS_RamData),(UCHAR*)&SYS_RamData,PRIMARY_START);

    // Update backup Copy 
    Memory_Write_Block((UCHAR) sizeof(SYS_RamData),(UCHAR*)&SYS_RamData,BACKUP_START);

}

// Verify Data Structure Integrity
// Return FALSE = if Checksum Error detected
UCHAR SYS_Validate_Data_Structure(void)
{
    // Validata Data Structure Integrity every minute
    if( SYS_RamData.Checksum != SYS_Calc_Ram_Checksum((UCHAR*)&SYS_RamData) )
    {
        // Data Structure has checksum error!
        return FALSE;
    }

    // Checksum OK
    return TRUE;

}