/****************************************************************
 * The contents of this file are Kidde proprietary and confidential.
 * *************************************************************/
// COMEASURE.C
// Contains functionality for sounder timing
#define _COMEASURE_C

#include    "common.h" 
#include    "comeasure.h"

#include    "a2d.h"
#include    "systemdata.h"

#include    "sound.h"

#include    "fault.h"
#include    "cocompute.h"







#define	CO_SAMPLE_INTERVAL_MS	1000	// 1 second

#define	STABILIZE_INITIAL_MS	1000	// 1 Second
#define	MAX_STABILIZE_TIME	45	/* 45 seconds (+ last 10 seconds)
                                                        = 55 seconds total*/
#define STABILIZE_ADDITIONAL_MS	10000	// 10 Seconds

//
// Note:  These were originally defined in A/D counts
//        They need to be scaled to mvolts since we convert
//        A/D counts to millivolts in the measurement
//
//        At 3.0 volts, 1 A/D count = 3.0/1024 = 2.93mv
//        At 2.5 volts, 1 A/D count = 2.5/1024 = 2.44mv
//          3.0 volts (DC model)
//        250 A/D counts = 250*2.93 = 733 mv
//        300 A/D counts = 300*2.93 = 879 mv
//        400 A/D counts = 400*2.93 = 1172 mv
//
//          2.5 volts (AC model)
//        250 A/D counts = 250*2.44 = 610 mv
//        300 A/D counts = 300*2.44 = 732 mv
//        400 A/D counts = 400*2.44 = 976 mv
//
//
//        For now pick a value between the 2 VDD voltages

#ifdef _CONFIGURE_NO_REGULATOR
    #define SENSOR_SHORT_LIMIT			700     // mv
    #define VMEAS_AD_CHARGE_THRESHOLD	500     // mv
    #define VMEAS_CIRCUIT_STABILIZE		600	// (0.6 volt Output is Off the Rail)
#else
    #define SENSOR_SHORT_LIMIT		250
    #define VMEAS_AD_CHARGE_THRESHOLD	300
    #define VMEAS_CIRCUIT_STABILIZE 	400	// 400 counts (Output is Off the Rail)
#endif

// These values should be OK for mvolt measurements
#ifdef _CONFIGURE_NO_REGULATOR
    #define	VHIGH_TEST_DIFFERENTIAL		100
    #define	VHIGH_TEST_MAX				2000
    #define	VLOW_TEST_DIFFERENTIAL		70
#else
    #define	VHIGH_TEST_DIFFERENTIAL		50
    #define	VHIGH_TEST_MAX			1024
    #define	VLOW_TEST_DIFFERENTIAL		30
#endif


// Normal Test Interval
#define	TEST_INTERVAL_COUNTER		60	// 60 x 1000ms = 60 secs
// Low Power Test Interval
#define	TEST_INTERVAL_COUNTER_LP	120	// 120 x 1000ms = 120 secs
// Fault Pending Test Interval
#define	TEST_INTERVAL_COUNTER_FP	20	// 20 x 1000ms = 20 secs

#define	MEAS_PPM_TEST_THRESHOLD		30      // in PPM


#define	TEST_LENGTH_COUNTER		20
#define	VHIGH_TEST_SAMPLE		(TEST_LENGTH_COUNTER - 1)
#define	VLOW_TEST_SAMPLE		2



// This is the number of samples that will be averaged during CO measurement.
#define	NUM_SAMPLES_CO_AVG      4

// Constants for state machine
#define MEAS_INIT			0
#define MEAS_STABILIZE		1
#define MEAS_SAMPLE			2
#define MEAS_TEST_INIT		3
#define MEAS_TEST_MONITOR	4

// Local Prototypes
// Functions for this module.
UINT MEAS_Make_Measurement(void) ;
void MEAS_Send_Charge_Pulse(void) ;


// Local Static variables
static UCHAR CO_State = MEAS_INIT ;
static UCHAR bTestCounter ;
static UINT wPrev_RawCOCount ;
static UINT wTest_Baseline ;

#ifdef _CONFIGURE_NO_REGULATOR
    static unsigned int  CO_FVR_Measured;

    // A/D Reference for PIC = VDD
    // FVR to calcultae VDD = 1024 mv
    #define FVR_MVOLTS      1024
    // FVR_MVOLTS * 1024
    #define FVR_FACTOR      1048576

#endif

// Glocal variables
UINT	wPPM = 0 ;
UINT	wRawCOCounts ;
UINT	wRawAvgCOCounts ;
UINT	wVout_Baseline ;
UINT    CO_Vdd_Calc;

#ifdef _CONFIGURE_MANUAL_PPM_COMMAND
    UINT ManualPPMValue = 0;
#endif


// Global / Static Initialization (not needed for PIC)
/*
void COMeasure_Init(void)
{
    CO_State = MEAS_INIT ;
    wPPM = 0 ;
	
#ifdef _CONFIGURE_MANUAL_PPM_COMMAND
    ManualPPMValue = 0;
#endif
}
*/
//*************************************************************************
UINT COMeasureTask(void)
{
    //To measure CO task time
    //Main_TP_On();

    //SER_Send_Int('c', CO_State, 10) ;
    //SER_Send_Int('c', bTestCounter, 10) ;

    switch (CO_State)
    {
        case MEAS_INIT:

            bTestCounter = MAX_STABILIZE_TIME ;
            wRawAvgCOCounts = 0 ;
            CO_State++ ;

            return	Make_Return_Value(STABILIZE_INITIAL_MS) ;


        case MEAS_STABILIZE:

            if(FLAG_PTT_ACTIVE)
            {
                // If PTT, Proceed to Sample State now
                CO_State++ ;
                bTestCounter = TEST_INTERVAL_COUNTER ;

                return	Make_Return_Value(1000) ;
            }

            if (bTestCounter == 0)
            {
                // Since the stabilize counter has expired, there must be something
                // wrong.  Test for Fault or High CO
                if((!FLAG_POWERUP_COMPLETE) || (!FLAG_SENSOR_FAULT_RST))
                {
                    // Proceed to Sample State (Assume this is high CO)
                    CO_State++ ;
                    bTestCounter = TEST_INTERVAL_COUNTER ;

                    return	Make_Return_Value(CO_SAMPLE_INTERVAL_MS) ;
                }
                else
                {
                    if(!FLAG_FAULT_FATAL)
                    {
                        #ifndef _CONFIGURE_NO_FAULT
                            // If not already in Fatal Fault Log a fault and remain
                            // in Stabilization
                            FLT_Fault_Manager(FAULT_SENSOR_SHORT) ;
                        #endif
                    }
                    else
                    {
                        // If Fatal Fault, Proceed to Sample State
                        CO_State++ ;
                        bTestCounter = TEST_INTERVAL_COUNTER ;
                    }
                }
            }
            else
            {
                bTestCounter--;
                wRawCOCounts = MEAS_Make_Measurement() ;

                if(FLAG_PTT_ACTIVE)
                {
                    // If PTT, Proceed to Sample State now
                    CO_State++ ;
                    bTestCounter = TEST_INTERVAL_COUNTER ;

                    return	Make_Return_Value(1000) ;
                }

                // See if the sensor output is less than the stabilize threshold.  If
                // so go to next state.  If not, stay in this state to measure again.
                if ( VMEAS_CIRCUIT_STABILIZE > wRawCOCounts )
                {
                    CO_State++ ;
                    bTestCounter = TEST_INTERVAL_COUNTER ;

                    return  Make_Return_Value(STABILIZE_ADDITIONAL_MS) ;
                }
            }
            break ;

        case MEAS_SAMPLE:

                FLAG_SENSOR_CIRCUIT_STABLE = 1 ;
                FLAG_POWERUP_COMPLETE =1 ;

                // Save last raw reading for later.
                wPrev_RawCOCount = wRawCOCounts ;

                wRawCOCounts = MEAS_Make_Measurement() ;
                
                // Break up the following instruction into multiple C instructions
                
                wRawAvgCOCounts = ( (((unsigned long)wRawAvgCOCounts*(NUM_SAMPLES_CO_AVG-1))
                                         + wRawCOCounts)/NUM_SAMPLES_CO_AVG );
                

                if (FLAG_PTT_ACTIVE)
                {
                    // If PTT calculate PPM and return
                    Calculate_PPM(wRawCOCounts) ;

                    return	Make_Return_Value(CO_SAMPLE_INTERVAL_MS) ;
                }

                wVout_Baseline = wRawCOCounts ;

                //If CO Sensor Fault Status skip Short Test
                // and check Sensor Test count expiration
                if(!(FLAG_CO_FAULT_PENDING && FLAG_FAULT_FATAL) )
                {
                  // No CO Short testing during CO Calibration
                  if( (FLAG_CALIBRATION_COMPLETE == TRUE) || (FLAG_CALIBRATION_UNTESTED == TRUE) )
                  {
                    // If the rate of rise is greater than the limit, start short
                    // processing.
                    if (wRawCOCounts > wPrev_RawCOCount)
                    {
                      #ifndef _CONFIGURE_NO_FAULT
                        if (wRawCOCounts - wPrev_RawCOCount > SENSOR_SHORT_LIMIT)
                        {
                            // Short has been detected.
                            FLT_Fault_Manager(FAULT_SENSOR_SHORT) ;

                            // Reset back to previous measurement so Short Fault
                            // can be tested again on next measurement.
                            // (else there will be no Delta)
                            wRawCOCounts = wPrev_RawCOCount ;

                            return  Make_Return_Value(CO_SAMPLE_INTERVAL_MS) ;
                         }
                      #endif
                    }
                    else
                    {
                        // No short.  Log a success.
                        FLT_Fault_Manager(SUCCESS_SENSOR_SHORT) ;
                    }
                  }
                    Calculate_PPM(wRawAvgCOCounts) ;
                }

            #ifndef _CONFIGURE_SN_TEST_FIRMWARE
                // See if it is time to do a sensor test.
                if (--bTestCounter != 0)
                {
                    // Test interval has not expired.  No Action
                    //break ;
                }
                else
                {
                    #ifdef _CONFIGURE_MANUAL_PPM_COMMAND
                        if(ManualPPMValue != 0)
                        {
                            // Manually set CO being Used, no Sensor Test
                            bTestCounter = TEST_INTERVAL_COUNTER ;
                            //break ;
                            return  Make_Return_Value(CO_SAMPLE_INTERVAL_MS) ;
                        }
                    #endif

                    // Test interval has expired.
                    if ((SYS_RamData.CO_Cal.status == 1) || (SYS_RamData.CO_Cal.status == 0))
                    {
                        // CO Cal not complete, no Sensor Test
                        bTestCounter = TEST_INTERVAL_COUNTER ;
                        //break ;
                    }
                    else if ( (FLAG_CO_FAULT_PENDING == 0) && ( wPPM > MEAS_PPM_TEST_THRESHOLD) || (FLAG_CO_ALARM_ACTIVE))
                    {
                        // Don't do the test if the sensor output is above the
                        // "no-test" threshold or in Alarm
                        bTestCounter = TEST_INTERVAL_COUNTER ;
                        //break ;
                    }
                    else
                    {
                        // Transition to Sensor Test Init State
                        CO_State++ ;
                    }
                }
            #endif
                break ;

        case MEAS_TEST_INIT:

          #ifdef _CONFIGURE_CO_FUNC_TEST
                if(FLAG_FUNC_CO_TEST)
                {
                    CO_State = MEAS_SAMPLE ;
                    bTestCounter = TEST_INTERVAL_COUNTER ;

                    return	Make_Return_Value(CO_SAMPLE_INTERVAL_MS) ;
                }
          #endif

                // Calculate the Test Baseline for charge control
                wTest_Baseline = wVout_Baseline + VMEAS_AD_CHARGE_THRESHOLD ;

                MEAS_CO_Charge(wTest_Baseline) ;

                FLAG_SENSOR_TEST = 1 ;
                bTestCounter = TEST_LENGTH_COUNTER ;

//                if (FLAG_SERIAL_PORT_ACTIVE && (SERIAL_ENABLE_HEALTH))
//                {
//                    SER_Send_Char('B') ;
//                    SER_Send_Int('L', wVout_Baseline, 10) ;
//                    SER_Send_Prompt() ;
//                }

                CO_State++ ;

                break ;

        case MEAS_TEST_MONITOR:

                // Decrement the test counter
                if ( !(--bTestCounter))
                {
                    // Test timer has expired.  Return to the measurement state.
                    CO_State = MEAS_SAMPLE ;
                    // Re-init the interval timer for measurement state.
                    if(FLAG_MODE_ACTIVE)
                        bTestCounter = TEST_INTERVAL_COUNTER ;
                    else
                        bTestCounter = TEST_INTERVAL_COUNTER_LP;

                    FLAG_SENSOR_TEST = 0 ;
                    //break ;
                }
                else
                {
                    if(FLAG_FUNC_CO_TEST)
                    {
                        // No Action, just return
                    }
                    else
                    {
                        // Make a CO Measurement to use in this state
                        wRawCOCounts = MEAS_Make_Measurement() ;

                        if (bTestCounter == VHIGH_TEST_SAMPLE)
                        {
                            // Send out counts if enabled.
//                            if (FLAG_SERIAL_PORT_ACTIVE && (SERIAL_ENABLE_HEALTH))
//                            {
//                                SER_Send_Char('H') ;
//                                SER_Send_Int('1', wRawCOCounts, 10) ;
//                                SER_Send_Prompt() ;
//                            }
                            //
                            // At this test sample, the sensor output should be above
                            // the test threshold.
                            //(Vmeasured - Vbaseline) = Vdifferential > VHIGH_TEST_DIFFERENTIAL  ?
                            // If measurement is less than baseline (occurs sometimes with sensor Open) delta will be negative (> VHIGH_TEST_MAX)
                            //
                          #ifndef _CONFIGURE_NO_FAULT
                                if ( (wRawCOCounts - wVout_Baseline < VHIGH_TEST_DIFFERENTIAL) || (wRawCOCounts - wVout_Baseline > VHIGH_TEST_MAX) )
                                {
                                    // The amount of output from the sensor was not enough.  Log
                                    // a conversion error and abort the test cycle.
                                    FLT_Fault_Manager(FAULT_CONVERSION_SETUP) ;
                                    // Go back to measurement state.
                                    CO_State = MEAS_SAMPLE ;
                                    // Re-init the interval timer for measurement state.
                                    // Since a fault was detected, repeat test at shorter interval
                                    bTestCounter = TEST_INTERVAL_COUNTER_FP ;
                                    FLAG_SENSOR_TEST = 0 ;
                                }
                          #endif
                                else
                                {
                                    // Log Success and complete Test Cycle
                                    FLT_Fault_Manager(SUCCESS_CONVERSION_SETUP) ;
                                }

                        }


                        // If the test sample is the low test sample, compare to the low
                        // test threshold.
                        if (bTestCounter == VLOW_TEST_SAMPLE)
                        {
                            // Send out counts if enabled.
//                            if (FLAG_SERIAL_PORT_ACTIVE  && (SERIAL_ENABLE_HEALTH))
//                            {
//                                SER_Send_Char('H') ;
//                                SER_Send_Int('2', wRawCOCounts, 10) ;
//                                SER_Send_Prompt() ;
//                            }

                            // This Sample Test was removed as it is not needed to detect Opens
                            // and CO introduced in middle of sensor test can cause this
                            // Sample Test to fail, as Sensor Output may remain above Baseline.

                            if (wRawCOCounts - wVout_Baseline < VLOW_TEST_DIFFERENTIAL)
                            {
                                // If Sensor Output has returned to near Baseline, Clear any Sensor Short faults
                                FLT_Fault_Manager(SUCCESS_SENSOR_SHORT) ;
                                FLT_Fault_Manager(SUCCESS_CONVERSION_SETUP) ;
                            }

                            /*
                            else if(wRawCOCounts - wVout_Baseline > VHIGH_TEST_MAX)
                            {
                                FLT_Fault_Manager(FAULT_SENSOR_SHORT) ;

                                // Reset back to previous measurement so Short Fault
                                // can be tested again on next measurement.
                                // (else there will be no measured Delta)
                                wRawCOCounts = wVout_Baseline ;

                                // Abort Test Return to the measurement state.
                                CO_State = MEAS_SAMPLE ;
                                // Re-init the interval timer for measurement state.
                                if(FLAG_MODE_ACTIVE)
                                    bTestCounter = TEST_INTERVAL_COUNTER ;
                                else
                                    bTestCounter = TEST_INTERVAL_COUNTER_LP;

                                FLAG_SENSOR_TEST = 0 ;
                            }
                            */
                        }

                    }
                }
                break ;
    }

    return  Make_Return_Value(CO_SAMPLE_INTERVAL_MS) ;
}

//Max battery Voltage
#define VBAT_MAX 3200

//*************************************************************************
UINT MEAS_Make_Measurement(void)
{
    UINT adval ;

    // If VDD is powered directly from battery, we need to scale measurement
    //  since VRef uses VDD in this controller.
    #ifdef _CONFIGURE_NO_REGULATOR
        // Read 1.024 FVR for VDD calculation
	CO_FVR_Measured = A2D_Convert(A2D_FVR_VOLTAGE) ;

        // Take CO measurement
        adval = A2D_Convert(A2D_CO_VOLTAGE) ;

        // Calculate current VDD voltage
        unsigned long CO_Vdd = (unsigned long)(FVR_FACTOR / (unsigned long)CO_FVR_Measured);
        CO_Vdd_Calc = (UINT)CO_Vdd;

        // From VDD calculation, calculate the Battery Test Voltage
        unsigned long CO_Measured = ((unsigned long)(CO_Vdd * adval)/1024);
        adval = (UINT)CO_Measured;

        // There is some CO sensor circuit effects from VBAT changes
        //  about 1 mv per 20 mv of VDD change from 3.1 volts VBAT
        if(CO_Vdd_Calc < VBAT_MAX )
        {
            UINT CO_Vdd_Delta = (VBAT_MAX - CO_Vdd_Calc);
            // Don't adjust if < 20 mv of VBat drop from VBat Max
            if(CO_Vdd_Delta > 16)
            {
                adval = adval + (CO_Vdd_Delta / 17);
            }
        }

    #else
        adval = A2D_Convert(A2D_CO_VOLTAGE) ;
    #endif

  #ifdef _CONFIGURE_MANUAL_PPM_COMMAND	
    // SER_Send_Int('P', ManualPPMValue, 16) ;
    // Override measurement with Manually Set Co Sensor value if enabled
    if(ManualPPMValue != 0)
    {
        adval = ManualPPMValue ;
    }
  #endif

    if (FLAG_SENSOR_TEST)
    {
        if (!(FLAG_SERIAL_PORT_ACTIVE && (SERIAL_ENABLE_HEALTH)) )
        {
            return adval ;
        }
    }

//    if (FLAG_SERIAL_PORT_ACTIVE && (SERIAL_ENABLE_CO))
//    {
//        //SER_Send_Int('D', CO_Vdd, 10) ;
//        //SER_Send_Prompt() ;
//
//        SER_Send_Int('V', adval, 10) ;
//
//        // If CO Sensor Fault Pending, append 1 pipe char
//        // If Fatal, append 2 pipe chars
//        if (FLAG_CO_FAULT_PENDING)
//        {
//            SER_Send_Char('|') ;
//
//            if (FLAG_FAULT_FATAL)
//                SER_Send_Char('|') ;
//
//        }
//        if (FLAG_SENSOR_CIRCUIT_STABLE == 0)
//        {
//            SER_Send_Char('?') ;
//
//        }
//
//        SER_Send_Prompt() ;
//    }
    return adval ;
}


//*************************************************************************
//  Place Negative Charge on CO Sensor for PTT operation
void MEAS_CO_Charge(UINT threshold)
{
    UINT i ;
    UINT adval ;

    for (i=0; i<100 ; i++)
    {
        // If VDD is powered directly from battery, we need to scale measurement
        //  since VRef uses VDD in this controller.
        #ifdef _CONFIGURE_NO_REGULATOR
            // Read 1.024 FVR for VDD calculation
            CO_FVR_Measured = A2D_Convert(A2D_FVR_VOLTAGE) ;

            // Take CO measurement
            adval = A2D_Convert(A2D_CO_VOLTAGE) ;

            // Calculate current VDD voltage
            unsigned long CO_Vdd = (FVR_FACTOR / CO_FVR_Measured);
            // From VDD calculation, calculate the Battery Test Voltage
            unsigned long CO_Measured = ((unsigned long)(CO_Vdd * adval)/1024);
            adval = (UINT)CO_Measured;

        #else
            adval = A2D_Convert(A2D_CO_VOLTAGE) ;
        #endif

        if (adval <= (threshold +10))
        {
            MEAS_Send_Charge_Pulse() ;
        }
        else
        {
            // If above Baseline we're done
            i = 100 ;
        }
    }

}

//*************************************************************************
void MEAS_Send_Charge_Pulse(void)
{
    PORT_CO_TEST = 1;
    delay_us(60);
    PORT_CO_TEST = 0 ;
    delay_us(400);
}


//*************************************************************************
// Reset CO Meas State in preparation for PTT
void MEAS_Reset(void) 
{
    FLAG_SENSOR_TEST = 0 ;
    bTestCounter = TEST_INTERVAL_COUNTER ;
    CO_State = MEAS_SAMPLE ;

}

