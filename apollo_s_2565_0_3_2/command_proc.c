/****************************************************************
 * The contents of this file are Kidde proprietary and confidential.
 * *************************************************************/
// COMMAND_PROC.C 
// Processes commands from serial port
#define	_COMMAND_PROC_C

#include "common.h"
#include "command_proc.h"

#include "sound.h"
#include "systemdata.h"
#include "comeasure.h"
#include "life.h"
#include "memory_1936.h"
#include "battery.h"
#include "light.h"

#ifdef _CONFIGURE_VOICE
    #include	"voice.h"
#endif


// Local Static variables
static	UINT current_address = 0 ;

char receive_buffer[NUM_RX_BUFFER_CHARS] ;

// Global / Static Initialization (not needed for PIC)
/*
void Command_Proc_Init(void)
{
    current_address = 0 ;
}
*/

unsigned char Command_Processor( char const *string)
{

    switch (*string)
    {
        // A return only was sent.  Reply with a prompt.
        case '\0':
            SER_Send_Prompt() ;
            break ;

        case '@':
            // Returns the byte and word addressed by the HEX value sent
            // with the command.
            {
                SER_Send_Char('\r') ;
                SER_Send_String("0x") ;

                volatile int address = SER_Strx_To_Int(string + 1) ;
                SER_Send_Int( ' ', *(UCHAR *)(address), 16 ) ;
                SER_Send_Char(',') ;
                SER_Send_String("0x") ;
                SER_Send_Int( ' ', *(UINT *)(address), 16 ) ;
                SER_Send_Prompt() ;
            }
            break ;

        // Sets the address to be referenced by the "D" command
        case 'A':
            current_address = SER_Strx_To_Int(string + 1) ;
            SER_Send_Prompt() ;
            break ;

        // Controls serial output.
        case 'B':
            SER_OutputFlags.ALL = SER_Strx_To_Int(string + 1) ;
            SER_Send_Prompt() ;
            break ;

        // Saves the current contents of the system data structure to
        // INFOA.
        case 'C':
            SYS_Save() ;
            SER_Send_Prompt() ;
            break ;

        // Writes the data sent with the command to the address set with the
        // "A" command.  Only one byte is written.
        case 'D':
            {
                unsigned char data ;
                data = (unsigned char)SER_Strx_To_Int(string + 1) ;
                *(unsigned char *)current_address = 	data ;
                SER_Send_Prompt() ;
            }
            break ;

        case 'E':
             break ;

        case 'F':
            {
                // Use the F command for general purpose development/test sequences

                #ifdef _CONFIGURE_LITHIUM
                    // Initiates a De-Passivation Cycle (Test Only)
                    Depass_Timer = 1;
                    LFE_Check_Depassivation();
                    SER_Send_String("De-Pass Initiated") ;
                #endif

            }
            SER_Send_Prompt() ;
            break ;

        case 'G':
            {
                UCHAR r, c, value ;
                UCHAR csum = 0 ;
                UCHAR addr = 0 ;
                //UINT hist_addr = 0;

                // Point to start of Non Volatile Data structures
                UINT hist_addr = PRIMARY_START;

                // Test Only
                //SER_Send_Int('h', hist_addr, 16) ;

                SER_Send_String("H:UU") ;

                for (r=0 ; r<16 ; r++)
                {
                    // New row
                    SER_Send_Char('\r') ;
                    // TX row starting Address
                    SER_Send_Byte(addr) ;

                    SER_Send_String(":  ") ;

                    for (c=0 ; c<16 ; c++)
                    {
                     // Read Diag History Structure (256 bytes)
                        value = Memory_Byte_Read(hist_addr++);
                        addr++ ;

                        // Update Checksum every byte
                        csum += value;

                        // Transmit hex asacii byte and space
                        SER_Send_Byte(value) ;
                        SER_Send_Char('\x20') ;
                    }
                }
                // Hist Dump complete
                SER_Send_Char('\r') ;

                // Tx Checksum
                SER_Send_String("+:") ;
                SER_Send_Byte(csum) ;

            }
            SER_Send_Prompt() ;
            break ;

        // Writes the data sent with the command to the address set with the
        // "A" command.  Two bytes are written (integer version of "D" command).
        // Data must be in hex format.
        /*
        case 'H':
            {
                UINT udata ;
                udata = SER_Strx_To_Int(string + 1) ;
                *(UINT*)current_address = udata ;
                SER_Send_Prompt() ;
            }
            break ;
        */

    #ifdef _CONFIGURE_ID_MANAGEMENT
        // Writes an ID number to the unit
        // "I" command.  2 bytes written
        case 'I':
            {
                UINT IDdata = SER_Strx_To_Int(string + 1) ;
                SYS_RamData.Unit_ID_Low = (UCHAR)IDdata;
                SYS_RamData.Unit_ID_High = (UCHAR)(IDdata >> 8);

                SYS_Save() ;

                #ifdef _CONFIGURE_ID_MANAGEMENT
                    Output_Unit_ID();
                #endif
            }
            break ;
    #endif


    #ifdef	_CONFIGURE_MANUAL_PPM_COMMAND
        // Used for testing Algorithm and Jump states on Bench
        case 'P':
            {
                // PPM Set ('P',ppm value (0000 - FFFF) )
                // Data must be in hex format.
                UINT udata ;
                udata = SER_Strx_To_Int(string + 1) ;
                ManualPPMValue = 	udata ;

                SER_Send_Prompt() ;
                SER_Send_Int(' ', ManualPPMValue, 16) ;
                SER_Send_Char(',') ;
                SER_Send_Int(' ', ManualPPMValue, 10) ;
                SER_Send_Prompt() ;
            }
            break ;
    #endif

    #ifdef	_CONFIGURE_MANUAL_SMK_COMMAND
        // Used for testing Algorithm and Hush Smoke states on Bench
        case 'S':
            {
                // Smoke Set ('S',smoke value (0000 - FFFF) )
                // Data must be in hex format.
                UINT udata ;
                udata = SER_Strx_To_Int(string + 1) ;
                ManualSmokeValue = 	udata ;

                SER_Send_Prompt() ;
                SER_Send_Int(' ', ManualSmokeValue, 16, 0) ;
                SER_Send_Char(',') ;
                SER_Send_Int(' ', ManualSmokeValue, 10, 1) ;
                //SER_Send_Prompt() ;

            }
            break ;
    #endif


    #ifdef	_CONFIGURE_VOICE
        case 'V':
            {
                //Voice Test ('V',voice# (0-7) )

                UCHAR voice = *(string+1) ;
                //SER_Send_Byte(voice) ;
                voice = (voice & 0x07) ;
                SER_Send_Byte(voice) ;

                VCE_Play(voice) ;

            }
            SER_Send_Prompt() ;
            break ;
    #endif

        // For testing only, output Ram data structure
        case 'R':
            {
                UCHAR r, c, value ;
                UCHAR addr = 0 ;
                // Point to start of Non Volatile Data structures
                UCHAR* ram_addr = (UCHAR*)&SYS_RamData;

                SER_Send_String("Ram Data Structure (0xA0)") ;

                // Test Only
                //SER_Send_Int('h', ram_addr, 16) ;

                for (r=0 ; r<1 ; r++)
                {
                    // New row
                    SER_Send_Char('\r') ;
                    // TX row starting Address
                    SER_Send_Byte(addr) ;

                    SER_Send_String(":  ") ;

                    for (c=0 ; c<16 ; c++)
                    {
                     // Read Diag History Structure (256 bytes)
                        value = *ram_addr++;
                        addr++ ;

                        // Transmit hex asacii byte and space
                        SER_Send_Byte(value) ;
                        SER_Send_Char('\x20') ;
                    }
                }
                // Hist Dump complete
                SER_Send_Char('\r') ;


            }
            SER_Send_Prompt() ;
            SER_Send_String("Current VDD Voltage (mv), ") ;
            SER_Send_Int( ' ', CO_Vdd_Calc, 10 ) ;
            SER_Send_Prompt() ;
            break ;


        case 'K':
            {
                UCHAR r, c, value ;
                UCHAR addr = 0 ;
                //UINT hist_addr = 0;
                
                // Point to start of Non Volatile Data structures
                UINT hist_addr = BACKUP_START;

                // Test Only
                //SER_Send_Int('h', hist_addr, 16) ;

                SER_Send_String("Backup Copy of Structure (0x10)") ;

                for (r=0 ; r<1 ; r++)
                {
                    // New row
                    SER_Send_Char('\r') ;
                    // TX row starting Address
                    SER_Send_Byte(addr) ;

                    SER_Send_String(":  ") ;

                    for (c=0 ; c<16 ; c++)
                    {
                     // Read Diag History Structure (256 bytes)
                        value = Memory_Byte_Read(hist_addr++);
                        addr++ ;

                        // Transmit hex asacii byte and space
                        SER_Send_Byte(value) ;
                        SER_Send_Char('\x20') ;
                    }
                }
                // Hist Dump complete
                SER_Send_Char('\r') ;

            }
            SER_Send_Prompt() ;
            break ;

#ifdef _CONFIGURE_LIGHT_SENSOR
        case 'L':
            {
                UCHAR r;

                SER_Send_String("Light Structure ") ;

                for (r=0 ; r<24 ; r++)
                {
                    // New row
                    SER_Send_Char('\r') ;
                    // TX row starting Address
                    if(r == Current_Hour)
                    {
                        SER_Send_Char('*') ;
                    }
                    SER_Send_Int( ' ',r, 10 ) ;
                    SER_Send_String(":  ") ;
                    SER_Send_Int( ' ',Hour[r].Light_Current, 10 ) ;
                    SER_Send_Char('\x20') ;
                    SER_Send_Int( ' ',Hour[r].Light_Previous, 10 ) ;
                    SER_Send_Char('\x20') ;
                    SER_Send_Int( ' ',Hour[r].Attrib, 16 ) ;

                }
                SER_Send_Prompt() ;

                if(FLAG_LIGHT_ALG_ON == 1)
                {
                    SER_Send_String("TH = ") ;
                    SER_Send_Int( ' ',Night_Day_TH, 10 ) ;
                    SER_Send_Prompt() ;
                    SER_Send_String("NL TH = ") ;
                    SER_Send_Int( ' ',Night_Light_TH, 10 ) ;
                    SER_Send_Prompt() ;
                    SER_Send_String("Alg ON") ;
                }
                else
                {
                    SER_Send_String("TH = ") ;
                    SER_Send_Int( ' ',NIGHT_DAY_TH_DEFAULT, 10 ) ;
                    SER_Send_Prompt() ;
                    SER_Send_String("NL TH = ") ;
                    SER_Send_Int( ' ',NIGHT_DAY_TH_DEFAULT, 10 ) ;
                    SER_Send_Prompt() ;
                    SER_Send_String("Alg OFF") ;
                }
                SER_Send_Prompt() ;

                if(FLAG_LIGHT_SIGNAL_GOOD == 1)
                {
                    SER_Send_String("Sig Good") ;
                }
                else
                {
                    SER_Send_String("Sig Low") ;
                }

            }
            SER_Send_Prompt() ;
            break ;

#endif

		case 'H':
			
			break;
		case 'T':
				if( (FLAG_FAULT_FATAL) || (FLAG_CO_ALARM_ACTIVE) )
				{
					// If in Fault Mode - Reset
					// If CO Fault Save Condition for Reset
					if(FLAG_CO_FAULT_PENDING)
						FLAG_SENSOR_FAULT_RST = 1;

					// Reset the CPU
					FLAG_PTT_RESET = 1;
					SOFT_RESET

				}
				else
				{
					//SER_Send_Int('T', BTN_State, 10) ;
					// Do a PTT.
					FLAG_PTT_ACTIVE = 1 ;
					Payload.button_press = 0x01;
				}
			break;
			
        default:
            SER_Send_Char('\r') ;
            SER_Send_Char('!') ;
            SER_Send_Prompt() ;
            break ;
    }
    return 0 ;
}
