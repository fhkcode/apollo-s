#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-2565.mk)" "nbproject/Makefile-local-2565.mk"
include nbproject/Makefile-local-2565.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=2565
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/apollo_s_2565.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/apollo_s_2565.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=-mafrlcsj
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS

else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=Voice.c a2d.c alarmprio.c battery.c button.c coalarm.c cocalibrate.c cocompute.c comeasure.c command_proc.c diaghist.c esclight.c fault.c greenled.c led_display.c life.c light.c main.c memory_1936.c peak_button.c ptt.c serial.c sound.c systemdata.c tamper.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/Voice.p1 ${OBJECTDIR}/a2d.p1 ${OBJECTDIR}/alarmprio.p1 ${OBJECTDIR}/battery.p1 ${OBJECTDIR}/button.p1 ${OBJECTDIR}/coalarm.p1 ${OBJECTDIR}/cocalibrate.p1 ${OBJECTDIR}/cocompute.p1 ${OBJECTDIR}/comeasure.p1 ${OBJECTDIR}/command_proc.p1 ${OBJECTDIR}/diaghist.p1 ${OBJECTDIR}/esclight.p1 ${OBJECTDIR}/fault.p1 ${OBJECTDIR}/greenled.p1 ${OBJECTDIR}/led_display.p1 ${OBJECTDIR}/life.p1 ${OBJECTDIR}/light.p1 ${OBJECTDIR}/main.p1 ${OBJECTDIR}/memory_1936.p1 ${OBJECTDIR}/peak_button.p1 ${OBJECTDIR}/ptt.p1 ${OBJECTDIR}/serial.p1 ${OBJECTDIR}/sound.p1 ${OBJECTDIR}/systemdata.p1 ${OBJECTDIR}/tamper.p1
POSSIBLE_DEPFILES=${OBJECTDIR}/Voice.p1.d ${OBJECTDIR}/a2d.p1.d ${OBJECTDIR}/alarmprio.p1.d ${OBJECTDIR}/battery.p1.d ${OBJECTDIR}/button.p1.d ${OBJECTDIR}/coalarm.p1.d ${OBJECTDIR}/cocalibrate.p1.d ${OBJECTDIR}/cocompute.p1.d ${OBJECTDIR}/comeasure.p1.d ${OBJECTDIR}/command_proc.p1.d ${OBJECTDIR}/diaghist.p1.d ${OBJECTDIR}/esclight.p1.d ${OBJECTDIR}/fault.p1.d ${OBJECTDIR}/greenled.p1.d ${OBJECTDIR}/led_display.p1.d ${OBJECTDIR}/life.p1.d ${OBJECTDIR}/light.p1.d ${OBJECTDIR}/main.p1.d ${OBJECTDIR}/memory_1936.p1.d ${OBJECTDIR}/peak_button.p1.d ${OBJECTDIR}/ptt.p1.d ${OBJECTDIR}/serial.p1.d ${OBJECTDIR}/sound.p1.d ${OBJECTDIR}/systemdata.p1.d ${OBJECTDIR}/tamper.p1.d

# Object Files
OBJECTFILES=${OBJECTDIR}/Voice.p1 ${OBJECTDIR}/a2d.p1 ${OBJECTDIR}/alarmprio.p1 ${OBJECTDIR}/battery.p1 ${OBJECTDIR}/button.p1 ${OBJECTDIR}/coalarm.p1 ${OBJECTDIR}/cocalibrate.p1 ${OBJECTDIR}/cocompute.p1 ${OBJECTDIR}/comeasure.p1 ${OBJECTDIR}/command_proc.p1 ${OBJECTDIR}/diaghist.p1 ${OBJECTDIR}/esclight.p1 ${OBJECTDIR}/fault.p1 ${OBJECTDIR}/greenled.p1 ${OBJECTDIR}/led_display.p1 ${OBJECTDIR}/life.p1 ${OBJECTDIR}/light.p1 ${OBJECTDIR}/main.p1 ${OBJECTDIR}/memory_1936.p1 ${OBJECTDIR}/peak_button.p1 ${OBJECTDIR}/ptt.p1 ${OBJECTDIR}/serial.p1 ${OBJECTDIR}/sound.p1 ${OBJECTDIR}/systemdata.p1 ${OBJECTDIR}/tamper.p1

# Source Files
SOURCEFILES=Voice.c a2d.c alarmprio.c battery.c button.c coalarm.c cocalibrate.c cocompute.c comeasure.c command_proc.c diaghist.c esclight.c fault.c greenled.c led_display.c life.c light.c main.c memory_1936.c peak_button.c ptt.c serial.c sound.c systemdata.c tamper.c



CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

# The following macros may be used in the pre and post step lines
Device=PIC16LF1938
ProjectDir="D:\Files of Daniel\Projects\Apollo-S\Source code\Main bord firmware\apollo_s_2565"
ProjectName=apollo_s_2565
ConfName=2565
ImagePath="dist\2565\${IMAGE_TYPE}\apollo_s_2565.${IMAGE_TYPE}.${OUTPUT_SUFFIX}"
ImageDir="dist\2565\${IMAGE_TYPE}"
ImageName="apollo_s_2565.${IMAGE_TYPE}.${OUTPUT_SUFFIX}"
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IsDebug="true"
else
IsDebug="false"
endif

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-2565.mk dist/${CND_CONF}/${IMAGE_TYPE}/apollo_s_2565.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
	@echo "--------------------------------------"
	@echo "User defined post-build step: [if ${IsDebug}=="false" ${MKDIR} HEX && cp ${ImageDir}\${PROJECTNAME}.${IMAGE_TYPE}.${OUTPUT_SUFFIX} HEX\${ConfName}-PG01_${IMAGE_TYPE}_x_x_x.${OUTPUT_SUFFIX}]"
	@if ${IsDebug}=="false" ${MKDIR} HEX && cp ${ImageDir}\${PROJECTNAME}.${IMAGE_TYPE}.${OUTPUT_SUFFIX} HEX\${ConfName}-PG01_${IMAGE_TYPE}_x_x_x.${OUTPUT_SUFFIX}
	@echo "--------------------------------------"

MP_PROCESSOR_OPTION=16LF1938
# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/Voice.p1: Voice.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/Voice.p1.d 
	@${RM} ${OBJECTDIR}/Voice.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1   -mdfp="${DFP_DIR}/xc8"  -fshort-double -fshort-float -Os -fasmfile -Og -maddrqual=require -xassembler-with-cpp -mwarn=0 -Wa,-a -DXPRJ_2565=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Voice.p1 Voice.c 
	@-${MV} ${OBJECTDIR}/Voice.d ${OBJECTDIR}/Voice.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Voice.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/a2d.p1: a2d.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/a2d.p1.d 
	@${RM} ${OBJECTDIR}/a2d.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1   -mdfp="${DFP_DIR}/xc8"  -fshort-double -fshort-float -Os -fasmfile -Og -maddrqual=require -xassembler-with-cpp -mwarn=0 -Wa,-a -DXPRJ_2565=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/a2d.p1 a2d.c 
	@-${MV} ${OBJECTDIR}/a2d.d ${OBJECTDIR}/a2d.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/a2d.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/alarmprio.p1: alarmprio.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/alarmprio.p1.d 
	@${RM} ${OBJECTDIR}/alarmprio.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1   -mdfp="${DFP_DIR}/xc8"  -fshort-double -fshort-float -Os -fasmfile -Og -maddrqual=require -xassembler-with-cpp -mwarn=0 -Wa,-a -DXPRJ_2565=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/alarmprio.p1 alarmprio.c 
	@-${MV} ${OBJECTDIR}/alarmprio.d ${OBJECTDIR}/alarmprio.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/alarmprio.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/battery.p1: battery.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/battery.p1.d 
	@${RM} ${OBJECTDIR}/battery.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1   -mdfp="${DFP_DIR}/xc8"  -fshort-double -fshort-float -Os -fasmfile -Og -maddrqual=require -xassembler-with-cpp -mwarn=0 -Wa,-a -DXPRJ_2565=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/battery.p1 battery.c 
	@-${MV} ${OBJECTDIR}/battery.d ${OBJECTDIR}/battery.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/battery.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/button.p1: button.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/button.p1.d 
	@${RM} ${OBJECTDIR}/button.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1   -mdfp="${DFP_DIR}/xc8"  -fshort-double -fshort-float -Os -fasmfile -Og -maddrqual=require -xassembler-with-cpp -mwarn=0 -Wa,-a -DXPRJ_2565=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/button.p1 button.c 
	@-${MV} ${OBJECTDIR}/button.d ${OBJECTDIR}/button.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/button.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/coalarm.p1: coalarm.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/coalarm.p1.d 
	@${RM} ${OBJECTDIR}/coalarm.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1   -mdfp="${DFP_DIR}/xc8"  -fshort-double -fshort-float -Os -fasmfile -Og -maddrqual=require -xassembler-with-cpp -mwarn=0 -Wa,-a -DXPRJ_2565=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/coalarm.p1 coalarm.c 
	@-${MV} ${OBJECTDIR}/coalarm.d ${OBJECTDIR}/coalarm.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/coalarm.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/cocalibrate.p1: cocalibrate.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/cocalibrate.p1.d 
	@${RM} ${OBJECTDIR}/cocalibrate.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1   -mdfp="${DFP_DIR}/xc8"  -fshort-double -fshort-float -Os -fasmfile -Og -maddrqual=require -xassembler-with-cpp -mwarn=0 -Wa,-a -DXPRJ_2565=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/cocalibrate.p1 cocalibrate.c 
	@-${MV} ${OBJECTDIR}/cocalibrate.d ${OBJECTDIR}/cocalibrate.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/cocalibrate.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/cocompute.p1: cocompute.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/cocompute.p1.d 
	@${RM} ${OBJECTDIR}/cocompute.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1   -mdfp="${DFP_DIR}/xc8"  -fshort-double -fshort-float -Os -fasmfile -Og -maddrqual=require -xassembler-with-cpp -mwarn=0 -Wa,-a -DXPRJ_2565=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/cocompute.p1 cocompute.c 
	@-${MV} ${OBJECTDIR}/cocompute.d ${OBJECTDIR}/cocompute.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/cocompute.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/comeasure.p1: comeasure.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/comeasure.p1.d 
	@${RM} ${OBJECTDIR}/comeasure.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1   -mdfp="${DFP_DIR}/xc8"  -fshort-double -fshort-float -Os -fasmfile -Og -maddrqual=require -xassembler-with-cpp -mwarn=0 -Wa,-a -DXPRJ_2565=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/comeasure.p1 comeasure.c 
	@-${MV} ${OBJECTDIR}/comeasure.d ${OBJECTDIR}/comeasure.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/comeasure.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/command_proc.p1: command_proc.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/command_proc.p1.d 
	@${RM} ${OBJECTDIR}/command_proc.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1   -mdfp="${DFP_DIR}/xc8"  -fshort-double -fshort-float -Os -fasmfile -Og -maddrqual=require -xassembler-with-cpp -mwarn=0 -Wa,-a -DXPRJ_2565=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/command_proc.p1 command_proc.c 
	@-${MV} ${OBJECTDIR}/command_proc.d ${OBJECTDIR}/command_proc.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/command_proc.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/diaghist.p1: diaghist.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/diaghist.p1.d 
	@${RM} ${OBJECTDIR}/diaghist.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1   -mdfp="${DFP_DIR}/xc8"  -fshort-double -fshort-float -Os -fasmfile -Og -maddrqual=require -xassembler-with-cpp -mwarn=0 -Wa,-a -DXPRJ_2565=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/diaghist.p1 diaghist.c 
	@-${MV} ${OBJECTDIR}/diaghist.d ${OBJECTDIR}/diaghist.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/diaghist.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/esclight.p1: esclight.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/esclight.p1.d 
	@${RM} ${OBJECTDIR}/esclight.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1   -mdfp="${DFP_DIR}/xc8"  -fshort-double -fshort-float -Os -fasmfile -Og -maddrqual=require -xassembler-with-cpp -mwarn=0 -Wa,-a -DXPRJ_2565=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/esclight.p1 esclight.c 
	@-${MV} ${OBJECTDIR}/esclight.d ${OBJECTDIR}/esclight.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/esclight.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/fault.p1: fault.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/fault.p1.d 
	@${RM} ${OBJECTDIR}/fault.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1   -mdfp="${DFP_DIR}/xc8"  -fshort-double -fshort-float -Os -fasmfile -Og -maddrqual=require -xassembler-with-cpp -mwarn=0 -Wa,-a -DXPRJ_2565=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/fault.p1 fault.c 
	@-${MV} ${OBJECTDIR}/fault.d ${OBJECTDIR}/fault.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/fault.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/greenled.p1: greenled.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/greenled.p1.d 
	@${RM} ${OBJECTDIR}/greenled.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1   -mdfp="${DFP_DIR}/xc8"  -fshort-double -fshort-float -Os -fasmfile -Og -maddrqual=require -xassembler-with-cpp -mwarn=0 -Wa,-a -DXPRJ_2565=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/greenled.p1 greenled.c 
	@-${MV} ${OBJECTDIR}/greenled.d ${OBJECTDIR}/greenled.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/greenled.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/led_display.p1: led_display.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/led_display.p1.d 
	@${RM} ${OBJECTDIR}/led_display.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1   -mdfp="${DFP_DIR}/xc8"  -fshort-double -fshort-float -Os -fasmfile -Og -maddrqual=require -xassembler-with-cpp -mwarn=0 -Wa,-a -DXPRJ_2565=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/led_display.p1 led_display.c 
	@-${MV} ${OBJECTDIR}/led_display.d ${OBJECTDIR}/led_display.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/led_display.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/life.p1: life.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/life.p1.d 
	@${RM} ${OBJECTDIR}/life.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1   -mdfp="${DFP_DIR}/xc8"  -fshort-double -fshort-float -Os -fasmfile -Og -maddrqual=require -xassembler-with-cpp -mwarn=0 -Wa,-a -DXPRJ_2565=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/life.p1 life.c 
	@-${MV} ${OBJECTDIR}/life.d ${OBJECTDIR}/life.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/life.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/light.p1: light.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/light.p1.d 
	@${RM} ${OBJECTDIR}/light.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1   -mdfp="${DFP_DIR}/xc8"  -fshort-double -fshort-float -Os -fasmfile -Og -maddrqual=require -xassembler-with-cpp -mwarn=0 -Wa,-a -DXPRJ_2565=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/light.p1 light.c 
	@-${MV} ${OBJECTDIR}/light.d ${OBJECTDIR}/light.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/light.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/main.p1: main.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/main.p1.d 
	@${RM} ${OBJECTDIR}/main.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1   -mdfp="${DFP_DIR}/xc8"  -fshort-double -fshort-float -Os -fasmfile -Og -maddrqual=require -xassembler-with-cpp -mwarn=0 -Wa,-a -DXPRJ_2565=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/main.p1 main.c 
	@-${MV} ${OBJECTDIR}/main.d ${OBJECTDIR}/main.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/main.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/memory_1936.p1: memory_1936.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/memory_1936.p1.d 
	@${RM} ${OBJECTDIR}/memory_1936.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1   -mdfp="${DFP_DIR}/xc8"  -fshort-double -fshort-float -Os -fasmfile -Og -maddrqual=require -xassembler-with-cpp -mwarn=0 -Wa,-a -DXPRJ_2565=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/memory_1936.p1 memory_1936.c 
	@-${MV} ${OBJECTDIR}/memory_1936.d ${OBJECTDIR}/memory_1936.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/memory_1936.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/peak_button.p1: peak_button.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/peak_button.p1.d 
	@${RM} ${OBJECTDIR}/peak_button.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1   -mdfp="${DFP_DIR}/xc8"  -fshort-double -fshort-float -Os -fasmfile -Og -maddrqual=require -xassembler-with-cpp -mwarn=0 -Wa,-a -DXPRJ_2565=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/peak_button.p1 peak_button.c 
	@-${MV} ${OBJECTDIR}/peak_button.d ${OBJECTDIR}/peak_button.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/peak_button.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/ptt.p1: ptt.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/ptt.p1.d 
	@${RM} ${OBJECTDIR}/ptt.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1   -mdfp="${DFP_DIR}/xc8"  -fshort-double -fshort-float -Os -fasmfile -Og -maddrqual=require -xassembler-with-cpp -mwarn=0 -Wa,-a -DXPRJ_2565=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/ptt.p1 ptt.c 
	@-${MV} ${OBJECTDIR}/ptt.d ${OBJECTDIR}/ptt.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/ptt.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/serial.p1: serial.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/serial.p1.d 
	@${RM} ${OBJECTDIR}/serial.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1   -mdfp="${DFP_DIR}/xc8"  -fshort-double -fshort-float -Os -fasmfile -Og -maddrqual=require -xassembler-with-cpp -mwarn=0 -Wa,-a -DXPRJ_2565=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/serial.p1 serial.c 
	@-${MV} ${OBJECTDIR}/serial.d ${OBJECTDIR}/serial.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/serial.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sound.p1: sound.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/sound.p1.d 
	@${RM} ${OBJECTDIR}/sound.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1   -mdfp="${DFP_DIR}/xc8"  -fshort-double -fshort-float -Os -fasmfile -Og -maddrqual=require -xassembler-with-cpp -mwarn=0 -Wa,-a -DXPRJ_2565=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/sound.p1 sound.c 
	@-${MV} ${OBJECTDIR}/sound.d ${OBJECTDIR}/sound.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sound.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/systemdata.p1: systemdata.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/systemdata.p1.d 
	@${RM} ${OBJECTDIR}/systemdata.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1   -mdfp="${DFP_DIR}/xc8"  -fshort-double -fshort-float -Os -fasmfile -Og -maddrqual=require -xassembler-with-cpp -mwarn=0 -Wa,-a -DXPRJ_2565=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/systemdata.p1 systemdata.c 
	@-${MV} ${OBJECTDIR}/systemdata.d ${OBJECTDIR}/systemdata.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/systemdata.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/tamper.p1: tamper.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/tamper.p1.d 
	@${RM} ${OBJECTDIR}/tamper.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1   -mdfp="${DFP_DIR}/xc8"  -fshort-double -fshort-float -Os -fasmfile -Og -maddrqual=require -xassembler-with-cpp -mwarn=0 -Wa,-a -DXPRJ_2565=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/tamper.p1 tamper.c 
	@-${MV} ${OBJECTDIR}/tamper.d ${OBJECTDIR}/tamper.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/tamper.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
else
${OBJECTDIR}/Voice.p1: Voice.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/Voice.p1.d 
	@${RM} ${OBJECTDIR}/Voice.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fshort-double -fshort-float -Os -fasmfile -Og -maddrqual=require -xassembler-with-cpp -mwarn=0 -Wa,-a -DXPRJ_2565=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/Voice.p1 Voice.c 
	@-${MV} ${OBJECTDIR}/Voice.d ${OBJECTDIR}/Voice.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Voice.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/a2d.p1: a2d.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/a2d.p1.d 
	@${RM} ${OBJECTDIR}/a2d.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fshort-double -fshort-float -Os -fasmfile -Og -maddrqual=require -xassembler-with-cpp -mwarn=0 -Wa,-a -DXPRJ_2565=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/a2d.p1 a2d.c 
	@-${MV} ${OBJECTDIR}/a2d.d ${OBJECTDIR}/a2d.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/a2d.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/alarmprio.p1: alarmprio.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/alarmprio.p1.d 
	@${RM} ${OBJECTDIR}/alarmprio.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fshort-double -fshort-float -Os -fasmfile -Og -maddrqual=require -xassembler-with-cpp -mwarn=0 -Wa,-a -DXPRJ_2565=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/alarmprio.p1 alarmprio.c 
	@-${MV} ${OBJECTDIR}/alarmprio.d ${OBJECTDIR}/alarmprio.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/alarmprio.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/battery.p1: battery.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/battery.p1.d 
	@${RM} ${OBJECTDIR}/battery.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fshort-double -fshort-float -Os -fasmfile -Og -maddrqual=require -xassembler-with-cpp -mwarn=0 -Wa,-a -DXPRJ_2565=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/battery.p1 battery.c 
	@-${MV} ${OBJECTDIR}/battery.d ${OBJECTDIR}/battery.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/battery.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/button.p1: button.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/button.p1.d 
	@${RM} ${OBJECTDIR}/button.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fshort-double -fshort-float -Os -fasmfile -Og -maddrqual=require -xassembler-with-cpp -mwarn=0 -Wa,-a -DXPRJ_2565=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/button.p1 button.c 
	@-${MV} ${OBJECTDIR}/button.d ${OBJECTDIR}/button.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/button.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/coalarm.p1: coalarm.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/coalarm.p1.d 
	@${RM} ${OBJECTDIR}/coalarm.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fshort-double -fshort-float -Os -fasmfile -Og -maddrqual=require -xassembler-with-cpp -mwarn=0 -Wa,-a -DXPRJ_2565=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/coalarm.p1 coalarm.c 
	@-${MV} ${OBJECTDIR}/coalarm.d ${OBJECTDIR}/coalarm.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/coalarm.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/cocalibrate.p1: cocalibrate.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/cocalibrate.p1.d 
	@${RM} ${OBJECTDIR}/cocalibrate.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fshort-double -fshort-float -Os -fasmfile -Og -maddrqual=require -xassembler-with-cpp -mwarn=0 -Wa,-a -DXPRJ_2565=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/cocalibrate.p1 cocalibrate.c 
	@-${MV} ${OBJECTDIR}/cocalibrate.d ${OBJECTDIR}/cocalibrate.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/cocalibrate.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/cocompute.p1: cocompute.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/cocompute.p1.d 
	@${RM} ${OBJECTDIR}/cocompute.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fshort-double -fshort-float -Os -fasmfile -Og -maddrqual=require -xassembler-with-cpp -mwarn=0 -Wa,-a -DXPRJ_2565=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/cocompute.p1 cocompute.c 
	@-${MV} ${OBJECTDIR}/cocompute.d ${OBJECTDIR}/cocompute.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/cocompute.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/comeasure.p1: comeasure.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/comeasure.p1.d 
	@${RM} ${OBJECTDIR}/comeasure.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fshort-double -fshort-float -Os -fasmfile -Og -maddrqual=require -xassembler-with-cpp -mwarn=0 -Wa,-a -DXPRJ_2565=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/comeasure.p1 comeasure.c 
	@-${MV} ${OBJECTDIR}/comeasure.d ${OBJECTDIR}/comeasure.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/comeasure.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/command_proc.p1: command_proc.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/command_proc.p1.d 
	@${RM} ${OBJECTDIR}/command_proc.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fshort-double -fshort-float -Os -fasmfile -Og -maddrqual=require -xassembler-with-cpp -mwarn=0 -Wa,-a -DXPRJ_2565=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/command_proc.p1 command_proc.c 
	@-${MV} ${OBJECTDIR}/command_proc.d ${OBJECTDIR}/command_proc.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/command_proc.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/diaghist.p1: diaghist.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/diaghist.p1.d 
	@${RM} ${OBJECTDIR}/diaghist.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fshort-double -fshort-float -Os -fasmfile -Og -maddrqual=require -xassembler-with-cpp -mwarn=0 -Wa,-a -DXPRJ_2565=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/diaghist.p1 diaghist.c 
	@-${MV} ${OBJECTDIR}/diaghist.d ${OBJECTDIR}/diaghist.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/diaghist.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/esclight.p1: esclight.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/esclight.p1.d 
	@${RM} ${OBJECTDIR}/esclight.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fshort-double -fshort-float -Os -fasmfile -Og -maddrqual=require -xassembler-with-cpp -mwarn=0 -Wa,-a -DXPRJ_2565=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/esclight.p1 esclight.c 
	@-${MV} ${OBJECTDIR}/esclight.d ${OBJECTDIR}/esclight.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/esclight.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/fault.p1: fault.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/fault.p1.d 
	@${RM} ${OBJECTDIR}/fault.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fshort-double -fshort-float -Os -fasmfile -Og -maddrqual=require -xassembler-with-cpp -mwarn=0 -Wa,-a -DXPRJ_2565=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/fault.p1 fault.c 
	@-${MV} ${OBJECTDIR}/fault.d ${OBJECTDIR}/fault.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/fault.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/greenled.p1: greenled.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/greenled.p1.d 
	@${RM} ${OBJECTDIR}/greenled.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fshort-double -fshort-float -Os -fasmfile -Og -maddrqual=require -xassembler-with-cpp -mwarn=0 -Wa,-a -DXPRJ_2565=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/greenled.p1 greenled.c 
	@-${MV} ${OBJECTDIR}/greenled.d ${OBJECTDIR}/greenled.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/greenled.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/led_display.p1: led_display.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/led_display.p1.d 
	@${RM} ${OBJECTDIR}/led_display.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fshort-double -fshort-float -Os -fasmfile -Og -maddrqual=require -xassembler-with-cpp -mwarn=0 -Wa,-a -DXPRJ_2565=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/led_display.p1 led_display.c 
	@-${MV} ${OBJECTDIR}/led_display.d ${OBJECTDIR}/led_display.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/led_display.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/life.p1: life.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/life.p1.d 
	@${RM} ${OBJECTDIR}/life.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fshort-double -fshort-float -Os -fasmfile -Og -maddrqual=require -xassembler-with-cpp -mwarn=0 -Wa,-a -DXPRJ_2565=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/life.p1 life.c 
	@-${MV} ${OBJECTDIR}/life.d ${OBJECTDIR}/life.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/life.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/light.p1: light.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/light.p1.d 
	@${RM} ${OBJECTDIR}/light.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fshort-double -fshort-float -Os -fasmfile -Og -maddrqual=require -xassembler-with-cpp -mwarn=0 -Wa,-a -DXPRJ_2565=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/light.p1 light.c 
	@-${MV} ${OBJECTDIR}/light.d ${OBJECTDIR}/light.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/light.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/main.p1: main.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/main.p1.d 
	@${RM} ${OBJECTDIR}/main.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fshort-double -fshort-float -Os -fasmfile -Og -maddrqual=require -xassembler-with-cpp -mwarn=0 -Wa,-a -DXPRJ_2565=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/main.p1 main.c 
	@-${MV} ${OBJECTDIR}/main.d ${OBJECTDIR}/main.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/main.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/memory_1936.p1: memory_1936.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/memory_1936.p1.d 
	@${RM} ${OBJECTDIR}/memory_1936.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fshort-double -fshort-float -Os -fasmfile -Og -maddrqual=require -xassembler-with-cpp -mwarn=0 -Wa,-a -DXPRJ_2565=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/memory_1936.p1 memory_1936.c 
	@-${MV} ${OBJECTDIR}/memory_1936.d ${OBJECTDIR}/memory_1936.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/memory_1936.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/peak_button.p1: peak_button.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/peak_button.p1.d 
	@${RM} ${OBJECTDIR}/peak_button.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fshort-double -fshort-float -Os -fasmfile -Og -maddrqual=require -xassembler-with-cpp -mwarn=0 -Wa,-a -DXPRJ_2565=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/peak_button.p1 peak_button.c 
	@-${MV} ${OBJECTDIR}/peak_button.d ${OBJECTDIR}/peak_button.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/peak_button.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/ptt.p1: ptt.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/ptt.p1.d 
	@${RM} ${OBJECTDIR}/ptt.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fshort-double -fshort-float -Os -fasmfile -Og -maddrqual=require -xassembler-with-cpp -mwarn=0 -Wa,-a -DXPRJ_2565=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/ptt.p1 ptt.c 
	@-${MV} ${OBJECTDIR}/ptt.d ${OBJECTDIR}/ptt.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/ptt.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/serial.p1: serial.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/serial.p1.d 
	@${RM} ${OBJECTDIR}/serial.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fshort-double -fshort-float -Os -fasmfile -Og -maddrqual=require -xassembler-with-cpp -mwarn=0 -Wa,-a -DXPRJ_2565=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/serial.p1 serial.c 
	@-${MV} ${OBJECTDIR}/serial.d ${OBJECTDIR}/serial.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/serial.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sound.p1: sound.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/sound.p1.d 
	@${RM} ${OBJECTDIR}/sound.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fshort-double -fshort-float -Os -fasmfile -Og -maddrqual=require -xassembler-with-cpp -mwarn=0 -Wa,-a -DXPRJ_2565=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/sound.p1 sound.c 
	@-${MV} ${OBJECTDIR}/sound.d ${OBJECTDIR}/sound.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sound.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/systemdata.p1: systemdata.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/systemdata.p1.d 
	@${RM} ${OBJECTDIR}/systemdata.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fshort-double -fshort-float -Os -fasmfile -Og -maddrqual=require -xassembler-with-cpp -mwarn=0 -Wa,-a -DXPRJ_2565=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/systemdata.p1 systemdata.c 
	@-${MV} ${OBJECTDIR}/systemdata.d ${OBJECTDIR}/systemdata.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/systemdata.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/tamper.p1: tamper.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/tamper.p1.d 
	@${RM} ${OBJECTDIR}/tamper.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fshort-double -fshort-float -Os -fasmfile -Og -maddrqual=require -xassembler-with-cpp -mwarn=0 -Wa,-a -DXPRJ_2565=$(CND_CONF)  -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib $(COMPARISON_BUILD)  -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     -o ${OBJECTDIR}/tamper.p1 tamper.c 
	@-${MV} ${OBJECTDIR}/tamper.d ${OBJECTDIR}/tamper.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/tamper.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assembleWithPreprocess
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/apollo_s_2565.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -Wl,-Map=dist/${CND_CONF}/${IMAGE_TYPE}/apollo_s_2565.${IMAGE_TYPE}.map  -D__DEBUG=1  -DXPRJ_2565=$(CND_CONF)  -Wl,--defsym=__MPLAB_BUILD=1   -mdfp="${DFP_DIR}/xc8"  -fshort-double -fshort-float -Os -fasmfile -Og -maddrqual=require -xassembler-with-cpp -mwarn=0 -Wa,-a -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib -std=c90 -gdwarf-3 -mstack=compiled:auto:auto        $(COMPARISON_BUILD) -Wl,--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml -o dist/${CND_CONF}/${IMAGE_TYPE}/apollo_s_2565.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}     
	@${RM} dist/${CND_CONF}/${IMAGE_TYPE}/apollo_s_2565.${IMAGE_TYPE}.hex 
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/apollo_s_2565.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -Wl,-Map=dist/${CND_CONF}/${IMAGE_TYPE}/apollo_s_2565.${IMAGE_TYPE}.map  -DXPRJ_2565=$(CND_CONF)  -Wl,--defsym=__MPLAB_BUILD=1   -mdfp="${DFP_DIR}/xc8"  -fshort-double -fshort-float -Os -fasmfile -Og -maddrqual=require -xassembler-with-cpp -mwarn=0 -Wa,-a -msummary=+psect,-class,+mem,-hex,-file  -ginhx032 -Wl,--data-init -mno-keep-startup -mosccal -mno-resetbits -mno-save-resetbits -mno-download -mstackcall -mc90lib -std=c90 -gdwarf-3 -mstack=compiled:auto:auto     $(COMPARISON_BUILD) -Wl,--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml -o dist/${CND_CONF}/${IMAGE_TYPE}/apollo_s_2565.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}     
	
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/2565
	${RM} -r dist/2565

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
