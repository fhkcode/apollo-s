/****************************************************************
 * The contents of this file are Kidde proprietary and confidential.
 * *************************************************************/
//SOUNDER.H

#ifndef	_SOUNDER_H
#define _SOUNDER_H

        //Prototypes
        // Global Access
        unsigned int SounderTask(void) ;
        unsigned char SND_Get_State(void) ;
        void SND_Do_Chirp(void) ;
        void SND_Reset_Task(void) ;
        void SND_Set_Chirp_Cnt(UCHAR count) ;
        unsigned char SND_Get_Chirp_Cnt(void) ;
        unsigned int SND_Trouble_Manager(void) ;


	#define SND_STATE_IDLE      0
	#define	SND_STATE_ACTIVE    1
	#define SND_STATE_ON        2
	#define SND_STATE_CHIRP     3
	#define SND_STATE_VERIFY    4


#endif

