/****************************************************************
 * The contents of this file are Kidde proprietary and confidential.
 * *************************************************************/
// CALIBRATE.C
// Contains functionality for CO alarm calculations.
#define _CALIBRATE_C

#include	"common.h"
#include	"cocalibrate.h"
#include	"systemdata.h"
#include	"comeasure.h"
#include	"fault.h"
#include	"sound.h"




#define	CAL_INITIAL_DELAY_MS	1000
#define	CAL_UPDATE_RATE			30000	// 30 Seconds
#define CAL_ACQ_RATE			10000	// 10 Seconds
#define	CAL_BEEP_TIME			10000	// 10 seconds
#define	CAL_BEEP_TIME_ERROR		500		// .5 seconds
#define	CAL_WAIT_TESTED_TIME	100


#ifdef	_CONFIGURE_FAST_CO_CAL
    #define CAL_ZERO_STABIL_PERIOD	4   // 2 Minute
    #define CAL_150_STABIL_PERIOD	4   // 2 Minute
    #define CAL_ZERO_CALIBRATION_PERIOD	4   // 2 Minute
    #define CAL_150_CALIBRATION_PERIOD	4   // 2 Minute

    #define	CAL_ERR_DELAY_PERIOD    4   // 2 minutes (in 30 second units)

#else
    #define CAL_ZERO_STABIL_PERIOD		12	// 6 minutes (30S Rate)
    #define CAL_150_STABIL_PERIOD		12	// 6 minutes (30S Rate)
    #define CAL_ZERO_CALIBRATION_PERIOD	21	// 3.5 minutes (10S rate)
    #define CAL_150_CALIBRATION_PERIOD	21	// 3.5 minutes (10S rate)

    #define	CAL_ERR_DELAY_PERIOD		40	// 20 minutes (in 30 second units)

#endif

#define CAL_STATE_DELAY				0
#define CAL_STATE_BEGIN				1
#define CAL_STATE_STABILIZATION		2
#define CAL_STATE_ACQUISITION		3
#define CAL_STATE_WAIT				4
#define CAL_STATE_WAIT_TESTED		5
#define	CAL_STATE_DELAY_TIMER		6

// Local static variables
static UCHAR CAL_MgrState = CAL_STATE_DELAY;
static UCHAR CAL_Timer ;
static UINT wCalibrationAverage  = 0;


// Global / Static Initialization (not needed for PIC)
/*
void COCalibrate_Init(void)
{
    CAL_MgrState = CAL_STATE_DELAY ;
    wCalibrationAverage = 0 ;
}
*/

//*****************************************************************
void CAL_Init(void)
{
    // Check cal status for validity
    if(SYS_RamData.CO_Cal.status + SYS_RamData.CO_Cal.checksum != 0xff)
    {

        // Perform another Sys Init to attempt to restore Calibration information
        // Then repeat CO Cal Init.
        if(FALSE == SYS_Init() )
        {
            if(SYS_RamData.CO_Cal.status + SYS_RamData.CO_Cal.checksum != 0xff)
            {
                // There is still an error in the calibration status.
                // Force a memory fault.
                FLT_Fault_Manager(FAULT_MEMORY) ;
            }

        }
        else
        {
             FLT_Fault_Manager(FAULT_MEMORY) ;
        }
    }
	
    switch (SYS_RamData.CO_Cal.status)
    {
        case 3:
            FLAG_CALIBRATION_COMPLETE = 1 ;
            break ;

        case 1:
            FLAG_CALIBRATION_PARTIAL = 1 ;
            break ;

        case 7:
            FLAG_CALIBRATION_UNTESTED = 1 ;
            break ;

        default:
            // Zero Cal not Complete
            
            FLAG_CALIBRATION_NONE = 1 ;
            break;
    }

}

//*****************************************************************
UINT CAL_Manager(void)
{
    // Test Only
    /*
    SER_Send_Int('L', CAL_MgrState, 10) ;
    SER_Send_Prompt() ;
    */

    switch(CAL_MgrState)
    {
        case CAL_STATE_DELAY:
            CAL_MgrState++ ;
            return  Make_Return_Value(CAL_INITIAL_DELAY_MS) ;

        case CAL_STATE_BEGIN:
            // Check to see if 400 PPM test is in progress.  
            if (STATUS_CAL_FINAL_TEST == SYS_RamData.CO_Cal.status)
            {
                // Proceed to Cal Wait Tested
                // Green Led manager will handle Green LED control
                CAL_MgrState = CAL_STATE_WAIT_TESTED ;
				Payload.calibration = STATUS_CAL_FINAL_TEST;
				
                return	Make_Return_Value(CAL_WAIT_TESTED_TIME) ;
            }
            else
            {
                if (STATUS_CAL_150 == SYS_RamData.CO_Cal.status)
                {
                    CAL_Timer = CAL_150_STABIL_PERIOD ;
					Payload.calibration = STATUS_CAL_150;
					
                }
                else if (STATUS_CAL_ZERO == SYS_RamData.CO_Cal.status)
                {
                    CAL_Timer = CAL_ZERO_STABIL_PERIOD ;
					Payload.calibration = STATUS_CAL_ZERO;
                }
                else
                {
                    // This alarm is already calibrated.  Just stay in this
                    // state.
					Payload.calibration = 0x03;
                    break ;
                }

                CAL_MgrState++ ;
                return	Make_Return_Value(1000) ;
            }

        case CAL_STATE_STABILIZATION:
            if (--CAL_Timer == 0)
            {
                if (1 == SYS_RamData.CO_Cal.status)
                {
                    CAL_Timer = CAL_150_CALIBRATION_PERIOD ;
                }
                else
                {
                    CAL_Timer = CAL_ZERO_CALIBRATION_PERIOD ;
                }

                CAL_MgrState++ ;	// Next state.
            }

            return	Make_Return_Value(CAL_UPDATE_RATE) ;

        case CAL_STATE_ACQUISITION:
            // Get the measured CO Average
            wCalibrationAverage =  wRawAvgCOCounts ;
			
//            SER_Send_Int('W', wCalibrationAverage, 10) ;
//            SER_Send_Prompt() ;
            // Test Only
            //SER_Send_Int('a', wCalibrationAverage, 10) ;
            //SER_Send_Prompt() ;

            if (--CAL_Timer == 0)
            {
                // Timer has timed out.  If this is a zero cal the current value of
                // wCalibrationAverage is the offset.  If this is the 150 cal, calculate
                // the calibration scale factor.

                // Check for errors in calibration based on the current calibration mode.
                if (0 == SYS_RamData.CO_Cal.status)
                {
                    // Check offset against limits.
                    if ( (wCalibrationAverage < CALIBRATION_OFFSET_MAXIMUM) &&
                        (wCalibrationAverage > CALIBRATION_OFFSET_MINIMUM) )
                    {
                        // calibration status byte and checksum.
                        SYS_RamData.CO_Cal.offset_LSB = wCalibrationAverage & 0x00ff ;
                        SYS_RamData.CO_Cal.offset_MSB = wCalibrationAverage / 256 ;
                        SYS_RamData.CO_Cal.status = 1 ;
                        SYS_RamData.CO_Cal.checksum = 0xfe ;

                        //Test Only
                        /*
                        SER_Send_Int('A', wCalibrationAverage, 10) ;
                        SER_Send_Prompt() ;
                        */

                        // Save Data Structure in Flash
                        SYS_Save() ;


                        //CAL_MgrState = CAL_STATE_WAIT ;
                        CAL_Timer = CAL_ERR_DELAY_PERIOD ;
                        CAL_MgrState = CAL_STATE_DELAY_TIMER ;
						
						Payload.calibration = 0x10;	// 0ppm cal pass
                    }
                    else
                    {
                        CAL_Timer = CAL_ERR_DELAY_PERIOD ;
                        CAL_MgrState = CAL_STATE_DELAY_TIMER ;
                        //FLAG_RLED_ON = 1;
                        FLAG_CAL_ERROR = 1 ;
						
						Payload.calibration = 0xFF;	// 0ppm cal failure
						
                        //FLT_Fault_Manager(FAULT_CALIBRATION) ;
                        //CAL_MgrState = CAL_STATE_WAIT ;
						
                        return	Make_Return_Value(CAL_UPDATE_RATE) ;
                    }
                }
                else
                {
                    // Check scal against limits.
                    UINT calval = LSB_TO_UINT(SYS_RamData.CO_Cal.offset_LSB) ;

                    UINT scale_cts = wCalibrationAverage - calval ;

                    // Test Only
//                    SER_Send_Int('S', scale_cts, 10) ;
//                    SER_Send_Prompt() ;

                    if ( (scale_cts < CALIBRATION_SLOPE_MAXIMUM) && 
                         (scale_cts > CALIBRATION_SLOPE_MINIMUM) )
                    {
                        // Valid 150 cal. Calculate the scale factor (PPM/count)
                        // scale =  150/(COcounts - offset)
                        UINT scale = (((unsigned long)150 << 16) / scale_cts) >> 8 ;
                        // If any rounding errors, error on High Side)
                        scale++;

                        LSB_TO_UINT(SYS_RamData.CO_Cal.scale_LSB) = scale ;
                        SYS_RamData.CO_Cal.status = 7 ;
                        SYS_RamData.CO_Cal.checksum = 0xf8 ;
                        SYS_Save() ;

                        CAL_Timer = CAL_ERR_DELAY_PERIOD ;
                        CAL_MgrState = CAL_STATE_DELAY_TIMER ;

						Payload.calibration = 0x11;	// 150ppm cal pass
                    }
                    else
                    {
                        CAL_Timer = CAL_ERR_DELAY_PERIOD ;
                        CAL_MgrState = CAL_STATE_DELAY_TIMER ;
                        //FLAG_RLED_ON = 1;
                        FLAG_CAL_ERROR = 1 ;	// 150ppm cal failure
						
						Payload.calibration = 0xFF;	// 150ppm cal fail

                        return	Make_Return_Value(CAL_UPDATE_RATE) ;
                    }
                }

            }
            return  Make_Return_Value(CAL_ACQ_RATE) ;

        case CAL_STATE_WAIT:

            break ;

        case CAL_STATE_WAIT_TESTED:
            // If the alarm flag has been set, the calibration is over.
            if (FLAG_CO_ALARM_CONDITION)
            {
                    // Update system data to reflect calibration status.
                    SYS_RamData.CO_Cal.status = 3 ;
                    SYS_RamData.CO_Cal.checksum = 0xfc ;
                    SYS_Save() ;
                    CAL_MgrState = CAL_STATE_WAIT ;
					
					Payload.calibration = 0x17;	// 400ppm cal pass
            }
            break ;

        case CAL_STATE_DELAY_TIMER:
            if (--CAL_Timer == 0)
            {
                if(FLAG_CAL_ERROR == TRUE)
                {
                    // Set Fatal Fault and proceed to Wait State
                    //FLAG_RLED_ON = FALSE ;
                    FLT_Fault_Manager(FAULT_CALIBRATION) ;
                    CAL_MgrState = CAL_STATE_WAIT ;
                }
                else
                {
                    // Chirp and remain in this state
                    SND_Do_Chirp() ;
                    CAL_Timer++ ;
                }
            }
            break;
    }
	
    return  Make_Return_Value(CAL_UPDATE_RATE) ;
}


UCHAR CAL_Get_State(void)
{
    if (CAL_MgrState == CAL_STATE_WAIT)
    {
        return	CAL_STATE_WAITING ;
    }
    else if (CAL_MgrState == CAL_STATE_DELAY_TIMER)
    {
        return	CAL_STATE_WAIT_DELAY ;
    }
    else if (SYS_RamData.CO_Cal.status == STATUS_CAL_ZERO)
    {
        return	CAL_STATE_ZERO ;
    }
    else if (SYS_RamData.CO_Cal.status == STATUS_CAL_150)
    {
        return	CAL_STATE_150 ;
    }
    else if (SYS_RamData.CO_Cal.status == STATUS_CAL_FINAL_TEST)
    {
        return	CAL_STATE_VERIFY ;
    }

    return CAL_STATE_CALIBRATED ;
}


