/****************************************************************
 * The contents of this file are Kidde proprietary and confidential.
 * *************************************************************/
// memory_1509.c
// Routines for flash memory row addressing support
#define _MEMORY_1509_C


#include    "common.h"
#include    "memory_1509.h"
#include    "fault.h"


/*
;   Non-Volatile memory is located at address 0x1F00 - 0x1FFF
;   the last 256 address locations in Program memory
;   The MS byte of each word is unused (only using LS Byte) since
;   MS byte is only 6 bits.
;
*/

// Non-Volatile Data will be located at 0x1F00 - 0x1FFF
// #define	MEMORY_HIGH_ADDRESS	0x1F00
// Flash Data can only be written 1 Row at a time in pic16lf1509
#define ROW_SIZE    32

volatile UCHAR b1memorybuffer[ROW_SIZE];




/******************************************************************************
;	Flash Memory Routines (for PIC16lf1509)
;
;	Non-Volatile memory is located in top portion of Program Flash
;	(256 Bytes residing at 0x1F00 - 0x1FFF)
;	To write to a location the following is required:
;	1. Read the entire 32 byte row containing location/s to be updated into Row Buffer
;	2. Update the location/s in the Row Buffer (use Row Offset definitions)
;	3. Erase the 32 byte Row in Flash
;	4. Write 32 byte row buffer back into Flash
;
*/

/******************************************************************************
;	Erase a Row of Flash (32 locations)
;	Inputs - addr holds an address within the Row to Erase
;	Outputs - none (Flash Row is erased)
;
*/
UCHAR Memory_Row_Erase(UINT addr)
{
    //Disable ints so required sequences will execute properly
    INTCONbits.GIE = 0;     

    // Program Memory Address can be set to any address in the row to be erased
    PMADRH = (addr>>8);     //MSByte 
    PMADRL = (UCHAR)addr;   //LSByte

    //Test only
    /*
    SER_Send_String("ER- ");
    SER_Send_Int('h', PMADRH & 0x7F, 16) ;
    SER_Send_String(", ");
    SER_Send_Int('l', PMADRL, 16) ;
    SER_Send_Prompt() ;
    */

    PMCON1bits.CFGS = 0;    // Not configuration space
    PMCON1bits.FREE = 1;    // Specify an erase operation
    PMCON1bits.WREN = 1;    // Enable writes

    PMCON2 = 0x55;          // Start required sequence to initiate erase
    PMCON2 = 0xAA;
    PMCON1bits.WR = 1;      // Set WR bit to begin erase

    //NOP instructions are forced as processor starts row erase
    _nop();
    _nop();

    /*  ; The processor stalls until the erase process is complete
	; after erase processor continues with 3rd instruction */

    PMCON1bits.WREN = 0;    // Disable writes

    //   Currently we do not use interrupts so GIE must be kept cleared
    //INTCONbits.GIE = 1;     // Enable interrupts


    // Point to start of row containing addr
    UINT address = (addr & 0xFFE0);

    // DONE: Add Write verify code here, and return WRITE_FAIL
    //        if verify fails
    for(UCHAR i=0; i<ROW_SIZE; i++)
    {
        if( Memory_Byte_Read(address+i) != 0xFF )
        {
            return (FLASH_FAIL);
        }
    }

    return (FLASH_SUCCESS);


}




/******************************************************************************
;	Read byte from Flash memory
;	Inputs - addr holds address to read
;	Outputs - returns read data byte
;
*/
UCHAR Memory_Byte_Read(UINT addr)
{
    // Program Memory Address can be set to any address in the row to be erased
    PMADRH = (addr>>8);     //MSByte
    PMADRL = (UCHAR)addr;   //LSByte

    PMCON1bits.CFGS = 0;    // Not configuration space
    PMCON1bits.RD = 1;      // Specify read operation
    _nop();
    _nop();
    
    return PMDATL;
}


/******************************************************************************
;	Read Row of Flash memory
;
;	Inputs - addr holds address within the row to read
;	Outputs - b1memorybuffer holds Flash Row data
;*/
UCHAR Memory_Row_Read(UINT addr)
{
    // Point to start of row containing addr
    UINT address = (addr & 0xFFE0);

    for (UCHAR i =0; i<32; i++,address++)
    {
         b1memorybuffer[i]= Memory_Byte_Read(address);
    }

    // DONE: Add Read verify code here, and return FLASH_FAIL
    //        if verify fails
    for(UCHAR i=0; i<ROW_SIZE; i++)
    {
        if( Memory_Byte_Read(address+i) != b1memorybuffer[i] )
        {
            return (FLASH_FAIL);
        }
    }
    return (FLASH_SUCCESS);
    
}

/******************************************************************************
;	Write to Flash memory (32 bytes)
;
;	Inputs - addr holds location within row address to write
;                b1MemoryBuffer holds data to write
;	Outputs - none
;*/
UCHAR Memory_Row_Write(UINT addr)
{
    // Point to start of row containing addr
    UINT address = (addr & 0xFFE0);

    INTCONbits.GIE = 0;     // Disable interrupts

    // Program Memory Address can be set to any address in the row to be erased
    PMADRH = (address>>8);  // Set the MS Byte of address
    PMDATH = 0;             // unused High Byte of data will always be 0

    PMCON1bits.CFGS = 0;    // Not configuration space
    PMCON1bits.WREN = 1;    // Enable writes
    PMCON1bits.LWLO = 1;    // Only Load Write Latches
    
    // 1st Load all the row data latches
    for (UCHAR i=0; i<32; i++)
    {
        PMADRL = ((UCHAR)address + i);
        PMDATL = b1memorybuffer[i];

        PMCON2 = 0x55;          // Start required write sequence
        PMCON2 = 0xAA;
        PMCON1bits.WR = 1;      // Set WR bit to begin writes to latch

        //NOP instructions are forced during writes to program memory write latches
       _nop();
       _nop();
    }

    // No more loading latches - Now start Flash program memory write
    PMCON1bits.LWLO = 0;

    PMCON2 = 0x55;          // Start required sequence to initiate write
    PMCON2 = 0xAA;
    PMCON1bits.WR = 1;      // Set WR bit to begin write row

    //NOP instructions are forced as processor starts row erase
    _nop();
    _nop();

    /*  ; The processor stalls until the write process is complete
	; after write, processor continues with 3rd instruction */

    PMCON1bits.WREN = 0;    // Disable writes

 //   Currently we do not use interrupts (GIE must be kept cleared)
 //   INTCONbits.GIE = 1;     // Enable interrupts


    if(PMCON1bits.WRERR == 1)
    {
        //SER_Send_String("FAIL") ;
        return (FLASH_FAIL);

    }
    else
    {
        // DONE: Add Write verify code here, and return WRITE_FAIL
        //        if verify fails
        for(UCHAR i=0; i<ROW_SIZE; i++)
        {
            if( Memory_Byte_Read(address+i) != b1memorybuffer[i] )
            {
                return (FLASH_FAIL);
            }
        }

        return (FLASH_SUCCESS);
    }

}

/******************************************************************************
; Function:	Memory_Read_Block
; Description:	Reads n bytes starting at an address
; In:           num_bytes:  Number of sequential bytes to read
;               source:     Relative Starting address in FLASH (0 - 255)
;               dest:       Address of first byte in RAM
; Out: 		None
*/
void Memory_Read_Block(UCHAR num_bytes, UINT source, UCHAR *dest)
{
    // Point relative address into Non-Volatile Flash Data
    source = (source + MEMORY_HIGH_ADDRESS);

    for(UCHAR i=0; i<num_bytes; i++, source++, dest++)
    {
        *dest = Memory_Byte_Read(source);
    }
}

/******************************************************************************
;Function:	Memory_Write_Block
; Description:	Writes n bytes starting at an address
;               Max 32 bytes block length
; In:           num_bytes:  Number of sequential bytes to write
;               source:     Pointer to starting address in Ram (0x00 - 0xFF)
;               dest:       Relative Address of first byte in Flash (0-255)
; Out: 		None
*/
void Memory_Write_Block(UCHAR num_bytes, UCHAR *source, UINT dest)
{
    // Test only
    //SER_Send_String("BW- ");
    //SER_Send_Int('d', dest, 16) ;
    //SER_Send_String(", ");
    //SER_Send_Int('n', num_bytes, 16) ;
    //SER_Send_Prompt() ;

    // Set the offset from start of Row
    //    used to edit the row buffer
    UCHAR row_offset = (UCHAR)(dest & 0x001F);

    // Point destination address into Non-Volatile Flash Data
    dest = (dest + MEMORY_HIGH_ADDRESS);

    /*;
    ;	1st we need to read the entire 32 byte flash row before writing
    ;	any changes to Flash Data
    */
    Memory_Read_Verify(dest);

    // Transfer source data into Row Buffer
    for(UCHAR i = 0; i<num_bytes; i++)
    {
        b1memorybuffer[row_offset++] = *source++;
    }

    // Erase Flash Row we are working on in preparation for write
    Memory_Erase_Verify(dest);

    // Finally write the edited Row Data back into Flash
    Memory_Write_Verify(dest);

}


/******************************************************************************
;Function:	Memory_Write_Backup_Block
; Description:	Writes n bytes starting at an address
;               Max 32 bytes block length
; In:           num_bytes:  Number of sequential bytes to write
;               source:     Pointer to starting address in Ram (0x00 - 0xFF)
;               dest:       Relative Address of first byte in Flash (0-255)
; Out: 		None
*/

void Memory_Write_Backup_Block(UCHAR num_bytes, UCHAR *source, UINT dest)
{

    // Set the offset from start of Row
    //    used to edit the row buffer
    UCHAR row_offset = (UCHAR)(dest & 0x001F);

    // Point destination address into Backup Copy Non-Volatile Flash Data
    dest = (dest + MEMORY_BACKUP_ADDRESS);

    /*;
    ;	1st we need to read the entire 32 byte flash row before writing
    ;	any changes to Flash Data
    */
    Memory_Read_Verify(dest);

    // Transfer source data into Row Buffer
    for(UCHAR i = 0; i<num_bytes; i++)
    {
        b1memorybuffer[row_offset++] = *source++;
    }

    // Erase Flash Row we are working on in preparation for write
    Memory_Erase_Verify(dest);
    
    // Finally write the edited Row Data back into Flash
    Memory_Write_Verify(dest);

}

// Row write with verify and repeat if needed
UCHAR Memory_Write_Verify(UINT dest)
{
    // Finally write the edited Row Data back into Flash
    if(Memory_Row_Write(dest) == FLASH_FAIL)
    {
        if(Memory_Row_Write(dest) == FLASH_FAIL)
        {
            return(FLASH_FAIL);
        }
    }
    return(FLASH_SUCCESS);
}

// Row read with verify and repeat if needed
UCHAR Memory_Read_Verify(UINT dest)
{
    // Finally write the edited Row Data back into Flash
    if(Memory_Row_Read(dest) == FLASH_FAIL)
    {
        if(Memory_Row_Read(dest) == FLASH_FAIL)
        {
            return(FLASH_FAIL);
        }
    }
    return(FLASH_SUCCESS);
}

// Row erase with verify and repeat if needed
UCHAR Memory_Erase_Verify(UINT dest)
{
    // Finally write the edited Row Data back into Flash
    if(Memory_Row_Erase(dest) == FLASH_FAIL)
    {
        if(Memory_Row_Erase(dest) == FLASH_FAIL)
        {
            return(FLASH_FAIL);
        }
    }
    return(FLASH_SUCCESS);
}
