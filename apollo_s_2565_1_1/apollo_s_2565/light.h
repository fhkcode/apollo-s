/* 
 * File:   light.h
 * Author: bill.chandler
 *
 * Created on March 05, 2014
 */

#ifndef LIGHT_H
#define	LIGHT_H

#ifdef	__cplusplus
extern "C" {
#endif

        struct light_data
        {
            UINT Light_Current;
            UINT Light_Previous;
            UCHAR Attrib;

        };


        // Light Structure Attribute Bit Masks
        #define ATTRIB_LIGHT_BIT_SET   0x80
        #define ATTRIB_NIGHT_BIT_SET   0x04

        // In Mvolts
        //#define NIGHT_DAY_TH_DEFAULT   425
        //#define NIGHT_DAY_TH_DEFAULT   300
        #define NIGHT_DAY_TH_DEFAULT   125



	// Prototypes
        // Light Sensing stuff
        UINT Light_Manager(void);
        void Light_Data_Init(void);
        void Light_Record(void);
        
        #ifdef _CONFIGURE_LIGHT_SENSOR
            extern persistent UCHAR Current_Hour;
            extern persistent struct light_data Hour[24];
            extern persistent UINT Night_Day_TH;
            extern persistent UINT Night_Light_TH;
        #endif





#ifdef	__cplusplus
}
#endif

#endif	/* LIGHT_H */

