/* 
 * File:   flags.h
 * Author: bill.chandler
 *
 * Created on May 3, 2013, 6:42 AM
 */

#ifndef FLAGS_H
#define	FLAGS_H

#ifdef	__cplusplus
extern "C" {
#endif


typedef union {
    struct {
        unsigned _0   :1;
        unsigned _1   :1;
        unsigned _2   :1;
        unsigned _3   :1;
        unsigned _4   :1;
        unsigned _5   :1;
        unsigned _6   :1;
        unsigned _7   :1;
    };

    unsigned char ALL;

} Flags_t;

// Create Flag bit structures
volatile Flags_t Flags1 ;
volatile Flags_t Flags2 ;
volatile Flags_t Flags3 ;
volatile Flags_t Flags4 ;
volatile Flags_t Flags5 ;
volatile Flags_t Flags6 ;
volatile Flags_t Flags_Active1 ;
volatile Flags_t Flags_Active2 ;
volatile Flags_t Flags_Active3 ;
volatile Flags_t persistent Flags_NR;
volatile Flags_t Status_Flag ;


// Declare as Global
extern volatile Flags_t Flags1; 
extern volatile Flags_t Flags2; 
extern volatile Flags_t Flags3; 
extern volatile Flags_t Flags4; 
extern volatile Flags_t Flags5;
extern volatile Flags_t Flags6;

extern volatile Flags_t Flags_Active1; 
extern volatile Flags_t Flags_Active2; 
extern volatile Flags_t Flags_Active3; 

extern volatile Flags_t persistent Flags_NR;

extern volatile Flags_t Status_Flag ;


#define FLAG_SENSOR_FAULT_RST		Flags_NR._0
#define FLAG_POWERUP_COMPLETE		Flags_NR._1
#define Flags_NR_2                  Flags_NR._2
#define Flags_NR_3                  Flags_NR._3
#define FLAG_CO_PEAK_MEMORY     	Flags_NR._4
#define FLAG_SENSOR_OPEN                  Flags_NR._5
#define FLAG_LIGHT_SIGNAL_GOOD		Flags_NR._6
#define FLAG_LIGHT_ALG_ON           Flags_NR._7


// Active Mode Only Trigger Flags
#define FLAG_PTT_ACTIVE             Flags_Active1._0
#define FLAG_BUTTON_EDGE_DETECT		Flags_Active1._1
#define FLAG_POWERUP_DELAY_ACTIVE	Flags_Active1._2
#define FLAG_BUTTON_NOT_IDLE		Flags_Active1._3
#define FLAG_FAULT_FATAL            Flags_Active1._4
#define FLAG_CHIRPS_ACTIVE          Flags_Active1._5
#define FLAG_SOUND_NOT_IDLE         Flags_Active1._6
#define FLAG_VOICE_SEND             Flags_Active1._7

#define FLAG_CO_ALARM_CONDITION         Flags_Active2._0 //bit7
#define FLAG_CALIBRATION_NONE           Flags_Active2._1
#define FLAG_CALIBRATION_PARTIAL        Flags_Active2._2
#define FLAG_CALIBRATION_UNTESTED       Flags_Active2._3
#define FLAG_CO_ALARM_ACTIVE            Flags_Active2._4    
#define FLAG_TAMPER_ACTIVE              Flags_Active2._5
#define FLAG_AC_DETECT                  Flags_Active2._6    //bit 1
#define FLAG_SERIAL_PORT_ACTIVE         Flags_Active2._7

#define FLAG_EOL_MODE                   Flags_Active3._0    //bit5
//#define Flags_A3_1                      Flags_Active3._1
#define Flags_PEAK_LCD_ACTIVE           Flags_Active3._1
#define	FLAG_PEAK_BUTTON_EDGE_DETECT	Flags_Active3._2   
#define FLAG_PEAK_BUTTON_NOT_IDLE       Flags_Active3._3
#define FLAG_VOICE_ACTIVE               Flags_Active3._4
#define FLAG_NIGHT_LIGHT_ON             Flags_Active3._5
#define FLAG_PEAK_VOICE_ON              Flags_Active3._6
#define FLAG_DEPASSIVATION_ACTIVE       Flags_Active3._7


#define FLAG_MODE_ACTIVE                Flags1._0
#define FLAG_RESET                      Flags1._1
#define FLAG_INHIBIT_INTRO_VOICE        Flags1._2
#define FLAG_BAT_TST_SHORT              Flags1._3
#define FLAG_LB_HUSH_ACTIVE             Flags1._4
#define FLAG_PTT_RESET                  Flags1._5
#define FLAG_ENABLE_CHIRP               Flags1._6
#define FLAG_RX_COMMAND_RCVD            Flags1._7


#define FLAG_LOW_BATTERY                Flags2._0   //bit3
#define FLAG_LB_CHIRP_INHIBIT           Flags2._1
#define FLAG_LB_PENDING                 Flags2._2
#define FLAG_LOW_BAT_INHIBIT_DISABLE	Flags2._3
#define FLAG_EOL_HUSH_ACTIVE            Flags2._4
#define FLAG_CAL_ERROR                  Flags2._5
#define FLAG_HUSH_SEND                  Flags2._6
#define FLAG_EOL_FATAL                  Flags2._7


#define FLAG_SENSOR_CIRCUIT_STABLE      Flags3._0
#define FLAG_SENSOR_TEST                Flags3._1
#define FLAG_CALIBRATION_COMPLETE       Flags3._2 //bit0
#define FLAG_FILTER_PPM_ENABLE          Flags3._3
#define FLAG_ENABLE_CO_PEAK_MEMORY      Flags3._4
#define FLAG_FUNC_CO_TEST               Flags3._5
//#define FLAG_3_6                        Flags3._6
//#define FLAG_LCD_POWER_DELAY            Flags3._6
#define FLAG_WIFI_RESET                         Flags3._6
#define FLAG_CO_FAULT_PENDING           Flags3._7


#define FLAG_PLAY_VOICE                	Flags4._0
#define FLAG_BAT_CONSRV_MODE_ACTIVE     Flags4._1
#define FLAG_VOICE_INHIBIT              Flags4._2
#define FLAG_LB_HUSH_INHIBIT        	Flags4._3
#define FLAG_DEPASSIVATION_ON           Flags4._4
#define FLAG_GLED_BLINK                 Flags4._5
#define FLAG_LOW_POWER_ACTIVE_CYCLE     Flags4._6
#define FLAG_INHIBIT_REV_MSG            Flags4._7

#define FLAG_DIAG_HIST_DISABLE         	Flags5._0
#define FLAG_EE_WRITE_ERR              	Flags5._1
#define FLAG_TAMPER_DISABLED           	Flags5._2
#define FLAG_ESC_LIGHT_ON               Flags5._3
#define FLAG_LANGUAGE_SEL               Flags5._4
#define FLAG_LED_DISP_DP                Flags5._5
#define FLAG_DISP_UPDATE_ENABLED        Flags5._6
#define FLAG_DISPLAY_OFF                Flags5._7

#define FLAG_LCD_POWER_ON               Flags6._0
#define FLAG_LIGHT                      Flags6._1
#define FLAG_CO_SENSOR_SHORT            Flags6._2 //bit4
//#define FLAG_LIGHT_LED_WAS_ON           Flags6._3
#define FLAG_BUTTON_PRESS_VALID         Flags6._3
#define FLAG_NIGHT_LIGHT_DISABLED       Flags6._4
#define FLAG_LIGHT_MEAS_ACTIVE          Flags6._5
#define FLAG_PTT_FAULT                  Flags6._6 //bit 2
#define FLAG_MEMORY_FAULT               Flags6._7   //bit6


#ifdef	__cplusplus
}
#endif

#endif	/* FLAGS_H */

