/****************************************************************
 * The contents of this file are Kidde proprietary and confidential.
 * *************************************************************/
// CONFIG.H
// Contains cofiguration flags

#ifndef	_CONFIG_H
#define	_CONFIG_H

// ****************************************************
// This section defines Firmware Version to Configure
//
/*
5.1	Rev X
All FW revisions created before an approved ATOR has been received fall outside
the scope of this procedure. Examples may include, FW created for demonstration
or demo boards, or early engineering trial and experimentation.
To avoid confusion with revision levels within the scope of this procedure,
these PCB designs will be identified with revision levels preceded by the
letter X (Experimental). Therefore, revisions X1 thru X3 would be examples of
three experimental firmwares necessary before an approved product specification
was completed.

5.2 Rev 0.1 (using Rev 1.0 and higher)
A Rev 0.1 level firmware design is the initial revision level falling within
the scope of this procedure. After an ATOR has been received, design and
development activities aiming towards Rev 0.1 level firmware will begin.
A Rev 0.1 level firmware is expected to deliver basic functionality.
A Rev 0.1 firmware design should have all non-unique features operational.

5.3	Rev 0.2 (using Rev 2.0 and higher)
A Rev 0.2 level firmware is the second revision level falling within the scope
of this procedure.  A Rev 0.2 level design is expected to add functionality for
hardware testing by EEs but is not expected to meet all requirements specified
in the ATOR or the applicable standards.

5.4	Rev 0.3 and Higher Levels (using Rev 3.0 and higher)
Rev 0.3 level and higher designs should meet most requirements specified in the
ATOR and applicable standards. An approved Rev 0.3 level design should be ready
for pre-agency validation testing and should enable serial data acquisition.
*/

/******************************************************************/
#define	REVISION_MAJOR	1
#define	REVISION_MINOR	1
#define	REV_STRING	"FW Ver 1.1"
// Note: Remember to also update compiler message in defmsgs.c file
/******************************************************************/


// ****************************************************
// This section maps project names to firmware P/N's
//
//****************************************************
// Select Model to Configure 
//****************************************************

/*
//Lithium Sealed Battery Models, Secondary power
 P/N - 2547-PG01
#define _CONFIGURE_2547     // CO ACDC Digital, 3V-Lithium sealed
 P/N - 2548-PG01
#define _CONFIGURE_2548     // CO ACDC Digital, 3V-Lithium sealed, escape Light
 P/N - 2549-PG01
#define _CONFIGURE_2549     // CO ACDC Digital, 3V-Lithium sealed, voice
 P/N - 2549-PG02
#define _CONFIGURE_2549_CANADA     // CO ACDC Digital, 3V-Lithium sealed, voice
 Added Models for AA Battery, Secondary power
 P/N - 2551-PG01
#define _CONFIGURE_2551       // CO ACDC Digital, 3V, 2-AA (can use 2547 firmware)
*/

// P/N - 2565-PG01
#define _CONFIGURE_2565

//****************************************************
// Model Specific Configurations
//****************************************************
#ifdef _CONFIGURE_2547
    #define _CONFIGURE_ACDC
    #define _CONFIGURE_DIGITAL_DISPLAY
    #define _CONFIGURE_UL_ALARM_CURVE
    #define _CONFIGURE_1000MS_TIC
    #define _CONFIGURE_10_YEAR_LIFE
    #define _CONFIGURE_LB_HUSH
    #define _CONFIGURE_EOL_HUSH
    #define _CONFIGURE_DIAG_HIST
    #define _CONFIGURE_CALIBRATION_MUTE
    #define _CONFIGURE_SERIAL_PORT_ENABLED
    #define _CONFIGURE_PEAK_BUTTON

    #define _CONFIGURE_LIGHT_SENSOR

#endif

#ifdef _CONFIGURE_2548
    #define _CONFIGURE_ACDC
    #define _CONFIGURE_DIGITAL_DISPLAY
    #define _CONFIGURE_UL_ALARM_CURVE
    #define _CONFIGURE_1000MS_TIC
    #define _CONFIGURE_10_YEAR_LIFE
    #define _CONFIGURE_LB_HUSH
    #define _CONFIGURE_EOL_HUSH
    #define _CONFIGURE_DIAG_HIST
    #define _CONFIGURE_CALIBRATION_MUTE
    #define _CONFIGURE_SERIAL_PORT_ENABLED
    #define _CONFIGURE_PEAK_BUTTON
    #define _CONFIGURE_ESCAPE_LIGHT

    #define _CONFIGURE_LIGHT_SENSOR

#endif

#ifdef _CONFIGURE_2549
    #define _CONFIGURE_ACDC
    #define _CONFIGURE_DIGITAL_DISPLAY
    #define _CONFIGURE_UL_ALARM_CURVE
    #define _CONFIGURE_1000MS_TIC
    #define _CONFIGURE_10_YEAR_LIFE
    #define _CONFIGURE_LB_HUSH
    #define _CONFIGURE_EOL_HUSH
    #define _CONFIGURE_DIAG_HIST
    #define _CONFIGURE_CALIBRATION_MUTE
    #define _CONFIGURE_SERIAL_PORT_ENABLED
    #define _CONFIGURE_PEAK_BUTTON
    #define _CONFIGURE_VOICE

    #define _CONFIGURE_LIGHT_SENSOR


#endif

#ifdef _CONFIGURE_2549_CANADA
    #define _CONFIGURE_ACDC
    #define _CONFIGURE_DIGITAL_DISPLAY
    #define _CONFIGURE_UL_ALARM_CURVE
    #define _CONFIGURE_1000MS_TIC
    #define _CONFIGURE_10_YEAR_LIFE
    #define _CONFIGURE_LB_HUSH
    #define _CONFIGURE_EOL_HUSH
    #define _CONFIGURE_DIAG_HIST
    #define _CONFIGURE_CALIBRATION_MUTE
    #define _CONFIGURE_SERIAL_PORT_ENABLED
    #define _CONFIGURE_PEAK_BUTTON
    #define _CONFIGURE_VOICE

    #define _CONFIGURE_LIGHT_SENSOR

    // Canadian Voice may require different voice timing and voice selection
    #define _CONFIGURE_CANADA

#endif

#ifdef _CONFIGURE_2551
    #define _CONFIGURE_ACDC
    #define _CONFIGURE_DIGITAL_DISPLAY
    #define _CONFIGURE_UL_ALARM_CURVE
    #define _CONFIGURE_1000MS_TIC
    #define _CONFIGURE_10_YEAR_LIFE
    #define _CONFIGURE_LB_HUSH
    #define _CONFIGURE_EOL_HUSH
    #define _CONFIGURE_DIAG_HIST
    #define _CONFIGURE_CALIBRATION_MUTE
    #define _CONFIGURE_SERIAL_PORT_ENABLED
    #define _CONFIGURE_PEAK_BUTTON

    #define _CONFIGURE_LIGHT_SENSOR

#endif

#ifdef _CONFIGURE_2565
    #define _CONFIGURE_ACDC
//    #define _CONFIGURE_DIGITAL_DISPLAY
    #define _CONFIGURE_UL_ALARM_CURVE
    #define _CONFIGURE_1000MS_TIC
    #define _CONFIGURE_10_YEAR_LIFE
    #define _CONFIGURE_LB_HUSH
    #define _CONFIGURE_EOL_HUSH
    #define _CONFIGURE_DIAG_HIST
    #define _CONFIGURE_CALIBRATION_MUTE
    #define _CONFIGURE_SERIAL_PORT_ENABLED
    #define _CONFIGURE_PEAK_BUTTON

    #define _CONFIGURE_LIGHT_SENSOR

#endif
//#define _CONFIGURE_SIMULATOR_DEBUG



//****************************************************
// Configurations Applying to All Models
//****************************************************

//  Define PIC Configuration Memory Here

// CONFIG1
#pragma config FOSC = INTOSC    // Oscillator Selection (INTOSC oscillator: I/O function on CLKIN pin)
#pragma config WDTE = SWDTEN    // Watchdog Timer Enable (WDT controlled by the SWDTEN bit in the WDTCON register)
//#pragma config WDTE = OFF     // Watchdog Timer Off
#pragma config PWRTE = ON       // Power-up Timer Enable (PWRT enabled)
#pragma config MCLRE = OFF      // MCLR Pin Function Select (MCLR/VPP pin function is digital input)
//#pragma config CP = OFF         // Flash Program Memory Code Protection (Program memory code protection is enabled)
//#pragma config CPD = OFF        // Data Memory Code Protection (Data memory code protection is enabled)
#pragma config CP = ON         // Flash Program Memory Code Protection (Program memory code protection is disabled)
#pragma config CPD = ON        // Data Memory Code Protection (Data memory code protection is disabled)
#pragma config BOREN = SBODEN   // Brown-out Reset Enable (Brown-out Reset controlled by the SBOREN bit in the BORCON register)
//#pragma config CLKOUTEN = OFF   // Clock Out Enable (CLKOUT function is enabled.)
#pragma config CLKOUTEN = ON   // Clock Out Enable (CLKOUT function is disabled. I/O or oscillator function on the CLKOUT pin)
#pragma config IESO = ON        // Internal/External Switchover (Internal/External Switchover mode is enabled)
#pragma config FCMEN = OFF      // Fail-Safe Clock Monitor Enable (Fail-Safe Clock Monitor is disabled)

// CONFIG2
#pragma config WRT = ALL        // Flash Memory Self-Write Protection (000h to 3FFFh write protected, no addresses may be modified by EECON control)
#pragma config PLLEN = OFF      // PLL Enable (4x PLL disabled)
#pragma config STVREN = ON      // Stack Overflow/Underflow Reset Enable (Stack Overflow or Underflow will cause a Reset)
#pragma config BORV = LO        // Brown-out Reset Voltage Selection (Brown-out Reset Voltage (Vbor), low trip point selected.)
#pragma config LVP = ON         // Low-Voltage Programming Enable (Low-voltage programming enabled)



#define   _CONFIGURE_LITHIUM
#define _CONFIGURE_OUTPUT_REVISION	
//#define _CONFIGURE_ID_MANAGEMENT
	
//	******** Button Feature Configurations ***********
//	#define 	_CONFIGURE_SMK_HUSH_ACTIVATE
//	#define 	_CONFIGURE_SMK_HUSH_CANCEL
//	#define		_CONFIGURE_LB_HUSH
//	#define 	_CONFIGURE_EOL_HUSH


//	********** CO Model Configurations ***************
//	#define 	_CONFIGURE_CO_FUNC_TEST
//      #define         _CONFIGURE_CO_H2_FAULTS

//	********** Interconnect Configurations ***********
//	#define		_CONFIGURE_INTERCONNECT


//	********** Power Configurations ******************
        // Alarm Battery Conserve
	#define 	_CONFIGURE_BAT_CONSERVE
    #define         _CONFIGURE_NO_REGULATOR

//      ********* Display Configurations *****************
//      #define         _CONFIGURE_DIGITAL_DISPLAY
    #define         _CONFIGURE_PPM_FILTER

//      ********* Feature Configurations *****************
//      #define         _CONFIGURE_VOICE
//      #define         _CONFIGURE_TEMP             // Temp Sensing
//      #define         _CONFIGURE_TAMPER_SWITCH    // Anti tamper switch


//      ********* Light Configurations *****************

    #define         _CONFIGURE_OUTPUT_LIGHT_STRUCT
    #define         _CONFIGURE_OUTPUT_RAW_LIGHT



//****************************************************
//****************************************************
//****************************************************
// Production Configuration? <select or deselect>
//****************************************************
#define	_CONFIGURE_PRODUCTION_UNIT


//#define     _CONFIGURE_USEC_TIMERS


#ifndef	_CONFIGURE_PRODUCTION_UNIT
//****************************************************
// Available Non-Production Configurations
//****************************************************

	//#define   _CONFIGURE_NO_SLEEP
	//#define   _CONFIGURE_SIMULATE_SLEEP

//	#define	_CONFIGURE_RST_TEST
	#define	_CONFIGURE_MANUAL_PPM_COMMAND


	#define	_CONFIGURE_MANUAL_CO_CAL
//        #define _CONFIGURE_FAST_CO_CAL

        // For debugging turn off WDT
//	#define _CONFIGURE_WATCHDOG_TIMER_OFF

	// *** Configure to Output Active Flags status ***
	// (to see what flag may be preventing Low Power mode)
//	#define _CONFIGURE_ACTIVE_TEST


	//#define 	_CONFIGURE_MANUAL_SMOKE_CAL
	//#define	_CONFIGURE_FAST_SMOKE_CAL
	
	//#define	_CONFIGURE_FORCE_SMOKEALARM
	//#define	_CONFIGURE_FORCE_COALARM
	
	
	//#define	_CONFIGURE_FORCE_LB 
	
	// *** Config for Short Low Battery Hush Test (3 minutes) ***
	//#define	_CONFIGURE_SHORT_HUSH_TEST

	// *** Config for Short EOL Test (1 minute) ***
	// 1 minute = 1 day, Hush = 3 minutes
	//#define	_CONFIGURE_EOL_TEST
	
        // *** Accelerates Life - 24 Hours = 1 Minute ***
        //#define _CONFIGURE_DIAG_HIST_TEST

        // *** Light Test Firmware ***
        //#define         _CONFIGURE_ACCEL_LIGHT_TEST
        //#define         _CONFIGURE_LIGHT_TEST_TABLE


	//#define	_CONFIGURE_DISABLE_FAULTS
	//#define	_CONFIGURE_42_HOUR_TEST
	//#define	_CONFIGURE_24_HOUR_TEST
	
	//#define	_CONFIGURE_VOICE
	//#define	_CONFIGURE_NO_INTRO_VOICE

        // Signal to Noise test firmware disables the CO sensor health test
        //#define _CONFIGURE_SN_TEST_FIRMWARE

// Add Accelerated Light testing to EOL Test firmware to allow testing
//  of EOL Night Time Hush
#ifdef _CONFIGURE_EOL_TEST
 #ifndef _CONFIGURE_ACCEL_LIGHT_TEST
    #define  _CONFIGURE_ACCEL_LIGHT_TEST

    #ifndef _CONFIGURE_LIGHT_TEST_TABLE
        #define _CONFIGURE_LIGHT_TEST_TABLE
    #endif

 #endif
#endif
	
	
//****************************************************
// 	Misc Test/debug Configurations
//****************************************************

	// Scope Test Point for Debugging
//	#define         _CONFIGURE_DEBUG_PIN_ENABLED
//      #define         _CONFIGURE_DEBUG_TASK_TIMING
//      #define         _CONFIGURE_DEBUG_INTS

	//#define	_CONFIGURE_SERIAL_WAKESLEEP_DEBUG
	//#define	_CONFIGURE_ANALOG_REG_DEBUG

        // Tests Escape Light w/Peak Button
        //#define _CONFIGURE_ESC_LIGHT_TEST

	#define         _CONFIGURE_OUTPUT_AC_DETECT

    //#define         _CONFIGURE_ESC_LIGHT_CYCLING


#endif	//#ifndef	_CONFIGURE_PRODUCTION_UNIT



#endif	//#ifndef	_CONFIG_H
