/* 
 * File:   main.c
 * Author: bill.chandler
 *
 * Created on May 3, 2013, 9:48 AM
 */

#include <stdio.h>
#include <stdlib.h>
#include <xc.h>


#include "flags.h"
#include "io.h"
#include "main.h"


#define MY_COMMON_FLAGS ((unsigned char * ) 0x0F0)

 DataStructure MyDataS @ 0x00A0;

 const DataStructure Primary @ 0x1F00 ={
    0x03,
    0xFC,
    0x0024,
    0x0100,
    0x00,
    0x00,
    0x0001,
    0x01
 };


unsigned char test1 (unsigned char i);
unsigned char test2 (unsigned char i);

/*
 * 
 */
int main(int argc, char** argv) @ 0x300
{

    	// load data structure
	unsigned char *ptrPrimary = (unsigned char*)&Primary ;
	unsigned char *ptrDataS = (unsigned char*)&MyDataS ;

	for (char i=0 ; i<sizeof(DataStructure) ; i++)
	{
		*(ptrDataS + i) = *(ptrPrimary + i) ;
        }







    Flags1.ALL = 0;

    unsigned char x = 0x00;
//    unsigned char *y = (unsigned char*)0xF0;
    unsigned char * y = MY_COMMON_FLAGS;

    for(unsigned char i = 1; i<10; i++)
    {
        (*y)+= 5;

        x = *y;
        x++;
       *y = x;
    }


    if(*y > 50)
        y =(unsigned char*)0xF;
    else
        y = (unsigned char*)0xF0;

    x = test1(3);

    x = test2(3);


    


    // Test only
    RESET();          // executes asm("reset");
    



    return (EXIT_SUCCESS);
}




unsigned char test1 (unsigned char i) @ 0x100
{
    
    i--;
    return i++ ;
}



unsigned char test2 (unsigned char i) @ 0x200
{
    i--;
    return i ;
}

    