/****************************************************************
 * The contents of this file are Kidde proprietary and confidential.
 * *************************************************************/
// command_proc.h


#ifndef	_COMMAND_PROC_H
#define	_COMMAND_PROC_H

    //Prototypes
    unsigned char Command_Processor( char const *string) ;

    extern char receive_buffer[NUM_RX_BUFFER_CHARS] ;


#endif
