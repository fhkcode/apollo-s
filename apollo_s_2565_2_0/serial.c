/****************************************************************
 * The contents of this file are Kidde proprietary  and confidential.
 * *************************************************************/
// Serial.c
// Basic routines for serial output.
#define	_SERIAL_C

#include    <string.h>
#include    "common.h"
#include    "serial.h"

#include    "a2d.h"
#include    "systemdata.h"

// Baud rate constants
/*Baud rate = FOSC/[16 (n+1)]*/
#define	BAUD_RATE	9600

#define	SER_9600BAUD_4MHZ	25
#define	SER_9600BAUD_8MHZ	51
#define	SER_9600BAUD_16MHZ	103
#define	SER_9600BAUD_20MHZ	129

#define SER_19200BAUD_16MHZ 51


// Global Serial Flag register
volatile Flags_t SER_OutputFlags;


// Global / Static Initialization
void SER_Init(void)
{
    SER_OutputFlags.ALL = 0 ;
    SER_Enable_Init();

}

//******************************************************************************
// Call before using the serial port.
void SER_Enable_Init(void)
{   
    //Test for Serial Port Connection
//    if (PORT_RX == 1)
//        FLAG_SERIAL_PORT_ACTIVE = 1;
//    else
//        FLAG_SERIAL_PORT_ACTIVE = 0;

//    if(FLAG_SERIAL_PORT_ACTIVE)
//    {
      // Initialize Serial Port for Diagnostics
      // 
      //TXSTAbits.TXEN = 1;
      //TXSTAbits.BRGH = 1;

//      SPBRGL = SER_9600BAUD_16MHZ;
      SPBRGL = SER_19200BAUD_16MHZ;
      
      RCSTAbits.SPEN = 1;
      RCSTAbits.CREN = 1;

#ifdef	_CONFIGURE_ESCAPE_LIGHT
      //
      // If The Serial Port is connected
      // we want to turn off the PWM Transistor,as High Current
      // will be passed From BT2 to GND
      // CREE LED will come on, however.
      //
      PORT_ESCAPE_LIGHT = 0;

#endif
      // Any serial output to be turned on at powerup can be put here.
      SERIAL_ENABLE_CO = 1;
      SERIAL_ENABLE_BATTERY = 1;
      SERIAL_ENABLE_HEALTH = 1;

//    }
//    else
//    {
//        // No Serial Port connection, make RX Output Low and
//        //  Tx Input, Set High (Escape Light Battery Off)
//        // Port C , bit 6 = TX
//
//#ifdef	_CONFIGURE_ESCAPE_LIGHT
//        ESCAPE_BATTERY_OUTPUT
//        ESCAPE_BATTERY_OFF
//#endif
//        // Port C , bit 7 = RX
//        TRISCbits.TRISC7 = 0;
//        PORTCbits.RC7 = 0;
//        
//    }
}


//******************************************************************************
// Check Receive flag for a character
void SER_Process_Rx(void)
{
    static unsigned char bufferptr = 0 ;

    if(RCSTAbits.OERR == 1)
    {
        RCSTAbits.CREN=0;
        _nop();
        RCSTAbits.CREN=1;
    }

    else if(RCSTAbits.FERR == 1)
    {
        // Read to clear framing error
        bufferptr = RCREG;
        bufferptr = 0;
    }
    
    else if(PIR1bits.RCIF == 1 )
    {
        PIR1bits.RCIF = 0;
        if(RCREG == '\r')
        {
            receive_buffer[bufferptr]=0;
            bufferptr=0;
            FLAG_RX_COMMAND_RCVD = 1;

        }
        else
        {
            receive_buffer[bufferptr++]=RCREG;
            if(bufferptr > NUM_RX_BUFFER_CHARS -1)
            {
              bufferptr = 0;
            }
        }

    }

}

//******************************************************************************
// value:  Value to be converted
// result: string representation of integer
// representation
//*************************************************************************
char* itoa( unsigned int value, char* result, char base) 
{ 
	char* out = result; 
	unsigned int quotient = value; 
	unsigned int absQModB; 
	char *start ;
	unsigned char temp; 
		
	do { 
		 absQModB=quotient % base; 
		 *out = "0123456789ABCDEF"[ absQModB ]; 
		 ++out; 
		 quotient /= base; 
	} while ( quotient ); 
	
	*out = 0; 
	
	start = result; 

	out--; 
	while (start < out) 
	{ 
             temp=*start;
             *start=*out;
             *out=temp;
             start++;
             out--;
	} 
	
	return result; 
} 



//*************************************************************************
// tag:  A character to send before sending the value
// val	value to be sent
// base: 10 for output in decimal, 16 for output in hex
//
// Call with ' ' in tag if no tag is desired
//*************************************************************************
void SER_Send_Int(char tag, unsigned int val, char base)
{
	char stringtosend[9];	// 2 char for tag, 6 for the int and a NULL
	
	if ( ' ' == tag)
	{
		// If there is no tag, just send the string.
		itoa(val, stringtosend, base) ;
	}
	else
	{
		// Send the tag and a colon, followed by the number
		*stringtosend = tag ;
		*(stringtosend + 1) = ':' ;
		itoa(val, stringtosend + 2, base) ;
	}
	
	SER_Send_String(stringtosend) ;
}

// Transmit byte in hex ascii format
// Tx order - upper nibble, lower nibble
void SER_Send_Byte(UCHAR byte)
{
	UCHAR nibble;
	
	nibble = ((byte & 0xF0) >> 4);
	if( nibble < 0x0A)
		nibble += 0x30 ;
	else
		nibble += 0x37 ;

	SER_Send_Char((char) nibble) ;
	
	nibble = byte & 0x0F;
	if( nibble < 0x0A)
		nibble += 0x30 ;
	else
		nibble += 0x37 ;

	SER_Send_Char((char) nibble) ;
	

}

//*************************************************************************
// Sends a NULL terminated string out the serial port.
void SER_Send_String(char const *stringtosend)
{
    if (!FLAG_VOICE_SEND)
    {
        char  *charpointer = (char*) stringtosend ;

        // Wait for TX Register ready for Char
        while (TXSTAbits.TRMT == 0);

        do
        {
            // Send the next character
            TXREG = *charpointer++ ;

            // First, make sure that the xmit buffer is empty.
            while (TXSTAbits.TRMT == 0);

        } while (*charpointer != 0) ;
    }
    
}

//*************************************************************************
void SER_Send_Prompt(void)
{
	SER_Send_String("\r>") ;
}

//*************************************************************************
void SER_Send_Error(void)
{
	SER_Send_Char('\r') ;
	SER_Send_Char('!') ;
	SER_Send_Prompt() ;	
}


//*************************************************************************
void SER_Send_Char(char c)
{
	char string[2] ;
	*string = c ;
	*(string + 1) = 0 ;
	SER_Send_String(string) ;
}

//*************************************************************************
unsigned int SER_Strx_To_Int(char const *str)
{
	UINT value = 0 ;
	UCHAR multiplier = 0 ;
	char i = strlen(str) - 1 ;
	
	do 
	{
		if (*(str+i) >= 'A')
		{
			value += (char)(*(str + i) + 0xC9) << multiplier ;
		}
		else
		{
			value += (char)(*(str + i) + 0xD0) << multiplier ;
		}
		
		multiplier += 4 ;	
		
	} while ( i-- != 0) ;  // this compares to zero before decrementing
	
	return value ;
}


// ************************************************************************
void	SER_Enable_Output(UINT bitmask)
{
	SER_OutputFlags.ALL |= bitmask ;
}


// ************************************************************************
void	SER_Disable_Output(UINT bitmask)
{
	SER_OutputFlags.ALL &= ~bitmask ;
}















