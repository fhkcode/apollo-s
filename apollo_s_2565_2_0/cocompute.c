/****************************************************************
 * The contents of this file are Kidde proprietary and confidential.
 * *************************************************************/
// COMPUTE.C
// Contains functionality for CO ppm calculations and filtering.
#define _COMPUTE_C

#include	"common.h"
#include	"cocompute.h"

#include	"systemdata.h"
#include	"coalarm.h"
#include	"comeasure.h"
#include	"cocalibrate.h"
#include	"life.h"
#include        "diaghist.h"

#ifdef _CONFIGURE_VOICE
    #include	"voice.h"
#endif

// Annual age correction percent
#define ANNUAL_PERCENT_INCREASE	2.5
#define PERCENT_PER_QTR 	((ANNUAL_PERCENT_INCREASE*1000) / 4)

#define	PEAK_THRESHOLD_PPM_DISP         10
#define	DISPLAY_PPM_THRESHOLD           30

// Peak Record Threshold
#define	PEAK_PPM_THRESHOLD              10
// Peak Indicator Flag Threshold
#define	PEAK_FLAG_THRESHOLD_PPM		100

#define PEAK_CO_VOICE_TIME_LOW		60		// in seconds
#define PEAK_CO_VOICE_TIME_HIGH		30		// in seconds

#define	HI_FILTER_COUNT 		30
#define	LO_FILTER_COUNT 		30
#define	HI_FILTER_PPM			400	

// Local Prototypes
void Update_Peak_PPM(void) ;
void Calculate_Filter_PPM(void) ;
UINT Age_Correct_PPM(UINT) ;

// Global Variables
UINT persistent wPeakPPM;

#ifdef _CONFIGURE_VOICE
    UINT Peak_CO_Voice_Timer;
#endif

#ifdef	_CONFIGURE_PPM_FILTER
    static UCHAR bPPMFilterCount = 0;
#endif

// Global / Static Initialization (not needed for PIC)
/*
void COCompute_Init(void)
{
    #ifdef  _CONFIGURE_PPM_FILTER
        bPPMFilterCount = 0 ;
    #endif
}
*/
    
//**********************************************************************
void Calculate_PPM(UINT raw_counts)
{
    UINT offset = SYS_RamData.CO_Cal.offset_LSB + (256 * SYS_RamData.CO_Cal.offset_MSB) ;

    // check for underflow first
    if ( raw_counts < offset)
    {
        wPPM = 0 ;
    }
    else
    {
        // subtract the offset and multipy by the scale.
        wPPM = (raw_counts - offset) ;

        // Calculate PPM using scale (slope)
        wPPM = (UINT)(((unsigned long)wPPM * (SYS_RamData.CO_Cal.scale_LSB + (256 * SYS_RamData.CO_Cal.scale_MSB))) >> 8) ;

        // Correct the PPM value by the age correction.
        wPPM = Age_Correct_PPM(wPPM) ;
        
        Payload.co_ppm_msb = (wPPM >> 8) & 0xFF;
        Payload.co_ppm_lsb = wPPM & 0xFF;

    }

    // Test Only
    //wPPM = 750 ;

//    if (FLAG_SERIAL_PORT_ACTIVE && (SERIAL_ENABLE_CO) )
//    {
//        SER_Send_Int('C', wPPM, 10) ;
//        if(FLAG_CO_ALARM_CONDITION)
//        {
//            SER_Send_String("*") ;
//        }
//        SER_Send_Prompt() ;
//    }

    if(CAL_STATE_CALIBRATED == CAL_Get_State())
    {
        if (!FLAG_PTT_ACTIVE)
        {
            #ifdef _CONFIGURE_PPM_FILTER
                // If PPM filtering is configured, filter the PPM value.
                Calculate_Filter_PPM() ;
            #endif

            Update_Peak_PPM() ;
        }
    }
    else
    {
        FLAG_DISP_UPDATE_ENABLED = 1;
    }

    //TODO:   This needs to move to after Peak Display Memory test
    #ifdef _CONFIGURE_DIGITAL_DISPLAY
        // Don't display or consider PPM levels < DISPLAY_PPM_THRESHOLD
        if(wPPM < DISPLAY_PPM_THRESHOLD)
        {
            wPPM = 0;
        }
    #endif

	
}

UINT Age_Correct_PPM(UINT ppm) 
{
    unsigned long num_qtrs ;
    UINT days = LIFE_Get_Current_Day() ;

    #ifdef _CONFIGURE_60_HOUR_TEST
            SER_Send_Int('-', ppm, 10) ;
            SER_Send_Prompt() ;
    #endif

    // # Qtrs of Life
    num_qtrs = ( ((unsigned long)days*(unsigned long)100) / (((unsigned long)365 * 100)/4) ) ;

    UINT ppm_corrected = ( ppm + ( ((unsigned long)ppm * (num_qtrs * (unsigned long)PERCENT_PER_QTR) ) / 100000 ) ) ;

    #ifdef _CONFIGURE_60_HOUR_TEST
            SER_Send_Int('+', ppm_corrected, 10) ;
            SER_Send_Prompt() ;
    #endif

    return(ppm_corrected);
}

#ifdef	_CONFIGURE_PPM_FILTER
//**********************************************************************
// PPM filtering is intended for Display Models only
// Display Filtering cycle is enabled:
//  1. At Power On or Button Reset
//  2. If CO is below Peak PPM Threshold level
void Calculate_Filter_PPM(void)
{

    // No Display filtering in Cal Modes, PTT, or Alarm, or Functional CO Test
    if ( CAL_STATE_CALIBRATED == CAL_Get_State() )
    {
      if(!FLAG_FUNC_CO_TEST)
      {
        if(!FLAG_PTT_ACTIVE)
        {
            if(!FLAG_CO_ALARM_ACTIVE)
            {
                if(PEAK_THRESHOLD_PPM_DISP < wPPM)
                {
                    if(FLAG_FILTER_PPM_ENABLE)
                    {
                        // PPM filtering in progress
                        // Halt Display Updates
                        FLAG_DISP_UPDATE_ENABLED = 0 ;
                        bPPMFilterCount++ ;

                        if(wPPM > HI_FILTER_PPM)
                        {
                            if(bPPMFilterCount > HI_FILTER_COUNT)
                            {
                                FLAG_FILTER_PPM_ENABLE = 0 ;
                                FLAG_DISP_UPDATE_ENABLED = 1;
                            }

                        }
                        else
                        {
                            if(bPPMFilterCount > LO_FILTER_COUNT)
                            {
                                FLAG_FILTER_PPM_ENABLE = 0 ;
                                FLAG_DISP_UPDATE_ENABLED = 1;
                            }

                        }
                        return ;
                    }
                    else
                    {
                        // PPM filtering complete
                        // Clear count but leave FLAG_FILTER_PPM_ENABLE cleared
                        bPPMFilterCount = 0;
                        // Re-Enable Display Updates
                        FLAG_DISP_UPDATE_ENABLED = 1 ;
                    }
                    return ;

                }
            }
        }
      }
    }

    // No filtering, timer reset, Enable Displayed PPM
    FLAG_FILTER_PPM_ENABLE = 1 ;		// Enable next Filtering cycle
    FLAG_DISP_UPDATE_ENABLED = 1 ;		// Display Updates Allowed
    bPPMFilterCount = 0;
}
#endif

//**********************************************************************
// Maintain Record of highest measured CO concentration (in PPM)
void Update_Peak_PPM(void)
{
  #ifdef _CONFIGURE_PPM_FILTER
     if(!FLAG_DISP_UPDATE_ENABLED)
            return ;
  #endif
  

  #ifdef _CONFIGURE_VOICE
  
     // Only record Peak memory values > Peak Threshold
	if( (PEAK_PPM_THRESHOLD < wPPM) && (FLAG_SENSOR_CIRCUIT_STABLE) )
	{
            // Record Highest PPM detected
            if(wPPM > wPeakPPM)
                wPeakPPM = wPPM ;
	}
  
	// Only announce Peak CO > Peak Threshold
	if( (PEAK_FLAG_THRESHOLD_PPM < wPPM) && (FLAG_SENSOR_CIRCUIT_STABLE) )
	{
           // Filter The Peak CO Flag as it enables PCO to be displayed
           //  on the LED Digital Display (for UL transient testing)
           if(FLAG_DISP_UPDATE_ENABLED)
           {
                if(FLAG_CO_PEAK_MEMORY == 0)
                {
                    // Set Flag for Peak CO used by button and LED tasks.
                    FLAG_CO_PEAK_MEMORY = 1 ;

                }

                // Announce Voice as long as CO is detected
                FLAG_PEAK_VOICE_ON = 1;

                #ifdef _CONFIGURE_DIAG_HIST
                    Diag_Hist_Test_Peak(wPPM) ;
                #endif
                
           }
	
	}
  	else
	{
            // Discontinue Co Voice, but retain Peak Memory
            FLAG_PEAK_VOICE_ON = 0;
            // Init with 1, so Voice begins immediately on Peak Co detected
            Peak_CO_Voice_Timer = 1;
	}
	
	// Annouce Carbon monoxide warning every few minutes
	if(FLAG_PEAK_VOICE_ON)
	{
	  if(!FLAG_CO_ALARM_CONDITION)
	  {
            if(--Peak_CO_Voice_Timer == 0)
            {
                if(wPPM >= 150)
                {
                    // Increase Voice Announce Rate if Higher CO
                    VCE_Play(VCE_CO_ENGLISH) ;
                    Peak_CO_Voice_Timer = PEAK_CO_VOICE_TIME_HIGH;
                }
                else
                {
                    // Slower Voice Announce Rate if Lower CO Level
                    VCE_Play(VCE_CO_ENGLISH) ;
                    Peak_CO_Voice_Timer = PEAK_CO_VOICE_TIME_LOW;
                }
            }
	  }
	}
  #else
	{
		// Only record Peak values > Peak Record Threshold
		if(	(PEAK_PPM_THRESHOLD < wPPM) &&
				(FLAG_SENSOR_CIRCUIT_STABLE) )
		{
			// Record Highest PPM detected
			if(wPPM > wPeakPPM)
			{
				wPeakPPM = wPPM ;
				
//				Payload.peak_co_ppm_msb = (wPeakPPM >> 8) & 0xFF;
//				Payload.peak_co_ppm_lsb = wPeakPPM  & 0xFF;
			}

			#ifdef _CONFIGURE_DIAG_HIST
				Diag_Hist_Test_Peak(wPPM) ;
			#endif

		}

		// Only Set Peak Flag for values > Peak Threshold
		if(	(PEAK_FLAG_THRESHOLD_PPM < wPPM) && (FLAG_SENSOR_CIRCUIT_STABLE) )
		{

		   // Filter The Peak Co Flag as it enables PCO to be displayed
		   //  on the LED Digital Display (for UL transient testing)
		   if(FLAG_DISP_UPDATE_ENABLED)
		   {
				if(FLAG_CO_PEAK_MEMORY == 0)
				{
					// Set Flag for Peak CO used by button and LED tasks.
					FLAG_CO_PEAK_MEMORY = 1 ;

				}
		   }
		}
        if(	(PEAK_FLAG_THRESHOLD_PPM < wPeakPPM) && (FLAG_SENSOR_CIRCUIT_STABLE) )
        {
            Payload.peak_co_ppm_msb = (wPeakPPM >> 8) & 0xFF;
            Payload.peak_co_ppm_lsb = wPeakPPM  & 0xFF;
        }
        
	}
  #endif	
			
}

